plugins {
    kotlin("plugin.spring")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-webflux")

    implementation(project(":ids-configuration"))

    // JSON Web Token support
    api("io.jsonwebtoken:jjwt-api:${LibraryVersions.Jwt.jjwt}")
    implementation("io.jsonwebtoken:jjwt-impl:${LibraryVersions.Jwt.jjwt}")
    runtimeOnly("io.jsonwebtoken:jjwt-jackson:${LibraryVersions.Jwt.jjwt}")

    // JWKS verification
    implementation("org.bitbucket.b_c:jose4j:${LibraryVersions.Jwt.jose4j}")

    // Key & Certificate manipulation
    implementation("org.bouncycastle:bcprov-jdk18on:${LibraryVersions.Jwt.bouncyCastle}")
    implementation("org.bouncycastle:bcprov-ext-jdk18on:${LibraryVersions.Jwt.bouncyCastle}")
    implementation("org.bouncycastle:bcpkix-jdk18on:${LibraryVersions.Jwt.bouncyCastle}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    testImplementation("com.github.tomakehurst:wiremock-jre8:${LibraryVersions.Test.wiremock}")
    testImplementation("com.google.guava:guava:${LibraryVersions.Test.guava}")
    testImplementation("org.bitbucket.b_c:jose4j:${LibraryVersions.Jwt.jose4j}")
    testImplementation(project(":ids-test-extensions"))
}