# IDS Token Manager

The IDS Token Manager module is responsible for the interaction with Dynamic Attribute Provisioning Service (DAPS) instances.

The primary exposed functionality is:
- Requesting a Dynamic Attribute Token (DAT) from the DAPS for this connector
- Verifying a Dynamic Attribute Token (DAT) from an incoming message. _Note: only the signature of the token will be validated. The validity of the claims is verified in the [`camel-daps-processor` module](../camel-daps-processor)_

Based on the configuration, the relevant `ClientAssertionGenerator` and `TokenManagerService` implementations will be loaded.
