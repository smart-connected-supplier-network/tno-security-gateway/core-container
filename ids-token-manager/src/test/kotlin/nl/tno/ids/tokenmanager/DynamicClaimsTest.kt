package nl.tno.ids.tokenmanager

/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import nl.tno.ids.testextensions.DapsTestExtension
import nl.tno.ids.tokenmanager.tokenmanagerserviceimpl.IDSATokenManagerService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.TestPropertySource
import java.util.*

/**
 * DAPS interaction test with mocked DAPS
 */
@SpringBootTest(
    classes = [DynamicClaimsTest.Application::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
    properties =
    [
        "spring.config.location = classpath:application.dynamicclaims.yaml",
        "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration",
        "artifacts.location=./resources"])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class DynamicClaimsTest {
    /** Port Spring listens on */
    @LocalServerPort private var port: Int = 0

    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Token manager service */
    @Autowired private lateinit var tokenManagerService: TokenManagerService
    @Autowired private lateinit var idsaTokenManagerService: IDSATokenManagerService

    /** Test requesting and validating DAT */
    @Test
    @Order(0)
    fun testDapsV2() {
        val tokenString = tokenManagerService.acquireToken("idsc:IDS_CONNECTORS_ALL")
        val claims = tokenManagerService.verifyToken(tokenString)
        Assertions.assertEquals("idsc:TRUST_SECURITY_PROFILE", claims["securityProfile"])
    }
}
