/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.tokenmanager

import nl.tno.ids.tokenmanager.clientassertion.IDSClientAssertionGenerator
import org.jose4j.jwt.consumer.JwtConsumerBuilder
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.TestPropertySource

/**
 * Client assertion generator test
 */
@SpringBootTest(classes = [ClientAssertionGeneratorTest.Application::class])
@TestPropertySource(properties = ["spring.config.location = classpath:application.yaml"])
class ClientAssertionGeneratorTest {
    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Client assertion generator */
    @Autowired private lateinit var idsClientAssertionGenerator: IDSClientAssertionGenerator

    /** Test generation of an IDS client assertion */
    @Test
    fun testClientAssertionGenerator() {
        val clientAssertion = idsClientAssertionGenerator.generate("idsc:IDS_CONNECTORS_ALL")

        val jwtConsumer =
            JwtConsumerBuilder()
                .setExpectedIssuer(
                    "88:A6:3F:79:FF:3F:57:A2:54:8F:99:8A:F8:A4:32:BC:7F:C9:B2:92:keyid:EC:23:DC:03:EC:6C:75:BB:0D:14:37:B4:1E:02:97:BB:E9:A4:D9:C1")
                .setExpectedSubject(
                    "88:A6:3F:79:FF:3F:57:A2:54:8F:99:8A:F8:A4:32:BC:7F:C9:B2:92:keyid:EC:23:DC:03:EC:6C:75:BB:0D:14:37:B4:1E:02:97:BB:E9:A4:D9:C1")
                .setExpectedAudience("idsc:IDS_CONNECTORS_ALL")
                .setSkipSignatureVerification()
                .build()

        val claims = jwtConsumer.processToClaims(clientAssertion)

        Assertions.assertEquals("ids:DatRequestToken", claims.getClaimValue("@type"))
        Assertions.assertEquals("idsc:IDS_CONNECTORS_ALL", claims.audience.first())
    }
}
