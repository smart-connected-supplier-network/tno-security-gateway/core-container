/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.tokenmanager.clientassertion

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import nl.tno.ids.configuration.keystore.KeystoreGenerator
import nl.tno.ids.configuration.model.KeystoreConfig
import nl.tno.ids.tokenmanager.IDSCertificateIdentifier
import org.springframework.stereotype.Component
import java.io.IOException
import java.security.GeneralSecurityException
import java.security.Key
import java.security.cert.X509Certificate
import java.time.Instant
import java.util.*

/** IDS DAPS Client Assertion Generator */
@Component
class IDSClientAssertionGenerator(
    /** Keystore configuration for signing client assertions */
    keystore: KeystoreConfig,
    /** PEM Reader parsing PEMs to java Keystores */
    private val keystoreGenerator: KeystoreGenerator,
    /** IDS Certificate Identifier utility to extract the IDS ID from a certificate */
    private val idsCertificateIdentifier: IDSCertificateIdentifier
) : ClientAssertionGenerator {
    /** Parsed Private Key */
    private lateinit var key: Key

    /** Parsed X509 certificate */
    private lateinit var cert: X509Certificate

    companion object {
        const val commonAudience = "idsc:IDS_CONNECTORS_ALL"
    }

    /** Generate Client Assertion, using global audience instead of the audience parameter */
    override fun generate(audience: String): String {
        val jwtsBuilder =
            Jwts.builder()
                .claim("@context", "https://w3id.org/idsa/contexts/context.jsonld")
                .claim("@type", "ids:DatRequestToken")
                .setIssuedAt(Date.from(Instant.now()))
                .setNotBefore(Date.from(Instant.now()))
                .signWith(key, SignatureAlgorithm.RS256)

        val certificateId = idsCertificateIdentifier.retrieve(cert)
        jwtsBuilder
            .setIssuer(certificateId)
            .setSubject(certificateId)
            .setExpiration(Date.from(Instant.now().plusSeconds(86400)))
            .setAudience(commonAudience)

        return jwtsBuilder.compact()
    }

    /** Initialization of the key and certificate */
    init {
        when (keystore.type) {
            KeystoreConfig.Type.PEM -> {
                keystore.pem?.let { pem ->
                    try {
                        keystoreGenerator.createKeystore(listOf(pem.cert), pem.key).let {
                            key = it.getKey("key", "".toCharArray())
                            cert = it.getCertificate("key") as X509Certificate
                        }
                    } catch (e: Exception) {
                        throw GeneralSecurityException("Can't load keystore", e)
                    }
                }
                    ?: throw GeneralSecurityException("No PEM configuration found at keystore.pem")
            }
            else -> {
                throw IOException("Can't load keystore of type " + keystore.type)
            }
        }
    }
}
