/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.tokenmanager

import io.jsonwebtoken.Claims
import nl.tno.ids.configuration.http.HttpClientGenerator
import nl.tno.ids.configuration.model.DapsConfig
import nl.tno.ids.configuration.model.KeystoreConfig
import nl.tno.ids.configuration.model.TruststoreConfig
import nl.tno.ids.tokenmanager.clientassertion.IDSClientAssertionGenerator
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpPost
import org.apache.http.message.BasicNameValuePair
import org.json.JSONArray
import org.json.JSONObject
import org.slf4j.LoggerFactory
import java.io.File
import java.io.IOException
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.ConcurrentHashMap

class TokenManagerException(message: String, cause: Throwable? = null) : Exception(message, cause)

/**
 * Token Manager Base Class, provides the basic functionality to acquire new tokens and
 * implementations will provide the means of verifying tokens
 */
abstract class TokenManagerService(
    /** DAPS configuration */
    val dapsConfig: DapsConfig,
    /** Keystore configuration */
    val keystore: KeystoreConfig,
    /** Truststore configuration */
    val trustStore: TruststoreConfig,
    /** Component client assertion generator */
    private val componentClientAssertionGenerator: IDSClientAssertionGenerator,
    /** Http Client generator */
    private val httpClientGenerator: HttpClientGenerator,
    /** IDS Certificate Identifier */
    private val idsCertificateIdentifier: IDSCertificateIdentifier
) {
    /** In-memory token cache, for reuse when still valid */
    private val cache =
        object : ConcurrentHashMap<String, Pair<Date, String>>() {
            fun cached(key: String): String? {
                return super.get(key)?.let {
                    if (it.first.before(Date())) {
                        LOG.info("Token expired for $key (${it.first} < ${Date()}")
                        remove(key)
                        null
                    } else {
                        it.second
                    }
                }
            }
        }

    /**
     * Abstract class that verifies a Dynamic Attribute Token and provides (if successful) a Claims
     * object
     */
    abstract fun verifyToken(token: String): Claims

    /**
     * Method to acquire a Dynamic Attribute Token (DAT) from a Dynamic Attribute Provisioning
     * Service (DAPS)
     */
    open fun acquireToken(audience: String, disableCache: Boolean = false): String {
        val cachedToken = cache.cached(audience)
        if (!disableCache && cachedToken != null) {
            return cachedToken
        }
        val clientAssertion = componentClientAssertionGenerator.generate(audience)
        val parameters =
            mutableListOf(
                BasicNameValuePair("grant_type", "client_credentials"),
                BasicNameValuePair(
                    "client_assertion_type",
                    "urn:ietf:params:oauth:client-assertion-type:jwt-bearer"),
                BasicNameValuePair("client_assertion", clientAssertion),
                BasicNameValuePair("scope", IDS_CONNECTOR_ATTRIBUTES_ALL))

        dapsConfig.dynamicClaims?.let { dynamicClaims ->
            dynamicClaims.transportCertsSha256?.let { transportCertsSha256 ->
                val certificate = CertificateFactory.getInstance("X.509")
                    .generateCertificates(File(transportCertsSha256.certLocation).inputStream())
                    .filterIsInstance<X509Certificate>()
                    .firstOrNull()
                certificate?.let {
                    val sha256 = idsCertificateIdentifier.retrieveSha256(it)
                    parameters.add(BasicNameValuePair("claims", JSONObject()
                        .put("access_token", JSONObject()
                            .put("transportCertsSha256", JSONObject()
                                .put("value", JSONArray(listOf(sha256))))
                        )
                        .toString())
                    )
                }
            }
        }

        val httpPost = HttpPost(dapsConfig.tokenUrl ?: (dapsConfig.url + "/token"))
        httpPost.entity = UrlEncodedFormEntity(parameters)
        try {
            httpClientGenerator.get().execute(httpPost).use { response ->
                val responseString = String(response.entity.content.readAllBytes())
                LOG.debug(responseString)
                return try {
                    val tokenObject = JSONObject(responseString)

                    if (tokenObject.has("access_token")) {
                        tokenObject.getString("access_token").also {
                            val claims = verifyToken(it)
                            cache[audience] = claims.expiration to it
                        }
                    } else {
                        LOG.error("Did not receive access_token: {}", tokenObject.toString())
                        throw TokenManagerException(
                            "Did not receive access token from DAPS: $responseString")
                    }
                } catch (e: Exception) {
                    throw TokenManagerException(
                        "Received malformed message from DAPS: $responseString", e)
                }
            }
        } catch (e: IOException) {
            LOG.warn("Could not request token at DAPS:", e)
            throw TokenManagerException("Could not request token at DAPS", e)
        }
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(TokenManagerService::class.java)

        private const val IDS_CONNECTOR_ATTRIBUTES_ALL = "idsc:IDS_CONNECTOR_ATTRIBUTES_ALL"
    }
}
