/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.tokenmanager.tokenmanagerserviceimpl

import io.jsonwebtoken.Claims
import io.jsonwebtoken.impl.DefaultClaims
import nl.tno.ids.configuration.http.HttpClientGenerator
import nl.tno.ids.configuration.keystore.KeystoreGenerator
import nl.tno.ids.configuration.model.DapsConfig
import nl.tno.ids.configuration.model.KeystoreConfig
import nl.tno.ids.configuration.model.TruststoreConfig
import nl.tno.ids.configuration.model.TruststoreType
import nl.tno.ids.configuration.truststore.AcceptAllX509TrustManager
import nl.tno.ids.configuration.truststore.MergedX509TrustManager
import nl.tno.ids.tokenmanager.IDSCertificateIdentifier
import nl.tno.ids.tokenmanager.TokenManagerService
import nl.tno.ids.tokenmanager.clientassertion.IDSClientAssertionGenerator
import org.jose4j.http.Get
import org.jose4j.jwa.AlgorithmConstraints
import org.jose4j.jwk.HttpsJwks
import org.jose4j.jws.AlgorithmIdentifiers
import org.jose4j.jwt.consumer.JwtConsumerBuilder
import org.jose4j.keys.resolvers.HttpsJwksVerificationKeyResolver
import org.jose4j.keys.resolvers.VerificationKeyResolver
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.net.URLEncoder
import java.security.KeyStore
import javax.annotation.PostConstruct
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

/**
 * IDS DAPS (v2) Token Manager Service Conforming to the IDS standard with JWKS token verification
 */
@Component
class IDSATokenManagerService(
    /** DAPS configuration */
    dapsConfig: DapsConfig,
    /** Keystore configuration */
    keystore: KeystoreConfig,
    /** Truststore configuration */
    trustStore: TruststoreConfig,
    /** Component client assertion generator */
    componentClientAssertionGenerator: IDSClientAssertionGenerator,
    /** Http Client generator */
    httpClientGenerator: HttpClientGenerator,
    /** Java Keystore generator */
    private val keystoreGenerator: KeystoreGenerator,
    /** Accept All trust manager, used if type is ACCEPT_ALL */
    private val acceptAllX509TrustManager: AcceptAllX509TrustManager,
    /** IDS Certificate Identifier */
    idsCertificateIdentifier: IDSCertificateIdentifier
) :
    TokenManagerService(dapsConfig, keystore, trustStore, componentClientAssertionGenerator, httpClientGenerator, idsCertificateIdentifier) {
    /** JWKS Key Resolver */
    val jwksKeyResolvers: MutableMap<String, VerificationKeyResolver> = mutableMapOf()

    /** Create SSL Socket Factory for the JWKS Key Resolver to be able to retrieve the DAPS JWKS */
    private fun createSslSocketFactory(): SSLSocketFactory {
        val trustManagers =
            when (trustStore.type) {
                TruststoreType.PEM -> {
                    val pemTruststore = keystoreGenerator.createTruststore(
                        trustStore.pem?.chain
                            ?: throw Exception("trustStore.pem.chain must be provided")
                    )
                    val pem = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).apply {
                        init(pemTruststore)
                    }.trustManagers.first() as X509TrustManager
                    val system = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).apply {
                        init(null as KeyStore?)
                    }.trustManagers.first() as X509TrustManager
                    arrayOf(MergedX509TrustManager(pem, system))
                }
                TruststoreType.ACCEPT_ALL -> {
                    arrayOf(acceptAllX509TrustManager)
                }
                else -> null
            }
        val sslContext = SSLContext.getInstance("TLS")
        sslContext.init(null, trustManagers, null)
        return sslContext.socketFactory
    }

    /** Setup JWKS Key Resolver configuration */
    @PostConstruct
    private fun postConstruct() {
        val getInstance = Get().apply {
            setSslSocketFactory(createSslSocketFactory())
        }
        // Concatenate additonalDapsConfig and dapsConfig
        val jwksUrls = dapsConfig.additionalDaps + dapsConfig

        // Add the jwks from each jwk url to the jwksKeyResolvers
        jwksUrls.forEach {daps ->
            val httpsJwks = HttpsJwks(daps.jwksUrl ?: "${daps.url}/.well-known/jwks.json")
            httpsJwks.setSimpleHttpGet(getInstance)

            // create new jwks key resolver, selects jwk based on key ID in jwt header
            jwksKeyResolvers[daps.issuerUrl ?: daps.url.substringBefore("/v2")] = HttpsJwksVerificationKeyResolver(httpsJwks)
        }

    }

    /** Verify token based on standard JWKS flows */
    override fun verifyToken(token: String): Claims {
        // Get token issuer
        val issuer = parseIssuer(token)

        val jwksKeyResolver = jwksKeyResolvers.computeIfAbsent(issuer) {
            val jwks = HttpsJwks((dapsConfig.jwksUrl ?: "${dapsConfig.url}/.well-known/jwks.json") + "?issuer=${URLEncoder.encode(it, Charsets.UTF_8)}")
            HttpsJwksVerificationKeyResolver(jwks)
        }

        // create validation requirements
        val jwtConsumer =
            JwtConsumerBuilder()
                .setRequireExpirationTime() // has expiration time
                .setAllowedClockSkewInSeconds(30) // leeway in validation time
                .setRequireSubject() // has subject
                .setExpectedAudience(true, "IDS_Connector", "idsc:IDS_CONNECTORS_ALL")
                .setExpectedIssuer(true, null)
                .setVerificationKeyResolver(jwksKeyResolver) // get decryption key from jwks
                .setJweAlgorithmConstraints(
                    AlgorithmConstraints(
                        AlgorithmConstraints.ConstraintType.PERMIT,
                        AlgorithmIdentifiers.RSA_USING_SHA256))
                .build()
        val claims = jwtConsumer.processToClaims(token)
        return DefaultClaims(claims.claimsMap)
    }

    /** Get issuer of JWT */
    private fun parseIssuer(token: String): String {
        val jwtConsumerWithoutVerification = JwtConsumerBuilder()
            .setSkipSignatureVerification()
            .setSkipDefaultAudienceValidation()
            .build()

        val jwtClaims = jwtConsumerWithoutVerification.processToClaims(token)
        return jwtClaims.issuer
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(IDSATokenManagerService::class.java)
    }
}
