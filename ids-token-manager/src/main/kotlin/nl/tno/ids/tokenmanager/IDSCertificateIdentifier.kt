/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.tokenmanager

import org.bouncycastle.asn1.ASN1OctetString
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier
import org.bouncycastle.asn1.x509.Extension
import org.bouncycastle.asn1.x509.SubjectKeyIdentifier
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.security.cert.X509Certificate
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * Helper class to extract the IDS certificate identifier from an certificate. Used parts are the
 * Authority Key Identifier (AKI) and Subject Key Identifier (SKI). The formatting is:
 * {{AKI}}:keyid:{{SKI}}
 */
@Component
class IDSCertificateIdentifier {
    companion object {
        private val LOG = LoggerFactory.getLogger(IDSCertificateIdentifier::class.java)
    }

    /** Retrieve Identifier from X509Certificate object */
    fun retrieve(cert: X509Certificate): String {
        // Get AuthorityKeyIdentifier (ASN1 object id 2.5.29.35)
        val akiOid = Extension.authorityKeyIdentifier.id
        val rawAuthorityKeyIdentifier = cert.getExtensionValue(akiOid)
        val akiOc = ASN1OctetString.getInstance(rawAuthorityKeyIdentifier)
        val aki = AuthorityKeyIdentifier.getInstance(akiOc.octets)
        val authorityKeyIdentifier = aki.keyIdentifier

        // Get SubjectKeyIdentifier (ASN1 object id 2.5.29.14)
        val skiOid = Extension.subjectKeyIdentifier.id
        val rawSubjectKeyIdentifier = cert.getExtensionValue(skiOid)
        val ski0c = ASN1OctetString.getInstance(rawSubjectKeyIdentifier)
        val ski = SubjectKeyIdentifier.getInstance(ski0c.octets)
        val subjectKeyIdentifier = ski.keyIdentifier
        val akiResult = encodeHexString(authorityKeyIdentifier).uppercase(Locale.getDefault())
        val skiResult = encodeHexString(subjectKeyIdentifier).uppercase(Locale.getDefault())
        if (LOG.isDebugEnabled) {
            LOG.debug("AKI: $akiResult")
            LOG.debug("SKI: $skiResult")
        }
        val certificateId = skiResult + "keyid:" + akiResult.substring(0, akiResult.length - 1)
        if (LOG.isDebugEnabled) {
            LOG.debug("Connector ID: $certificateId")
        }
        return certificateId
    }

    /** Retrieve SHA256 for transport certificates */
    fun retrieveSha256(cert: X509Certificate): String {
        val skiOid = Extension.subjectKeyIdentifier.id
        val rawSubjectKeyIdentifier = cert.getExtensionValue(skiOid)
        val ski0c = ASN1OctetString.getInstance(rawSubjectKeyIdentifier)
        val ski = SubjectKeyIdentifier.getInstance(ski0c.octets)
        val subjectKeyIdentifier = ski.keyIdentifier
        return encodeHexString(subjectKeyIdentifier, false)
    }

    /**
     * Convert byte to hexadecimal chars without any dependencies to libraries.
     * @param num Byte to get hexadecimal representation for
     * @return The hexadecimal representation of the given byte value
     */
    private fun byteToHex(num: Int): CharArray {
        val hexDigits = CharArray(2)
        hexDigits[0] = Character.forDigit(num shr 4 and 0xF, 16)
        hexDigits[1] = Character.forDigit(num and 0xF, 16)
        return hexDigits
    }

    /** Lookup table for encodeHexString() */
    private val hexLookup = ConcurrentHashMap<Byte, CharArray>()

    /**
     * Encode a byte array to a hex string
     * @param byteArray Byte array to get hexadecimal representation for
     * @param colons Boolean indicating whether colons should be added inbetween hex string
     * @return Hexadecimal representation of the given bytes
     */
    private fun encodeHexString(byteArray: ByteArray, colons: Boolean = true): String {
        val sb = StringBuilder()
        for (b in byteArray) {
            sb.append(hexLookup.computeIfAbsent(b) { num: Byte -> byteToHex(num.toInt()) })
            if (colons) {
                sb.append(':')
            }
        }
        return sb.toString()
    }
}
