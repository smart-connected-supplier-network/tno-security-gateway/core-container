rootProject.name = "core-container"

include(":ids-main")
include(":ids-configuration")

include(":camel-multipart-processor")
include(":camel-daps-processor")

include(":ids-artifact-manager")
include(":ids-clearing")
include(":ids-resource-manager")
include(":ids-token-manager")
include(":ids-security")
include(":ids-selfdescription")
include(":ids-test-extensions")
include(":ids-route-manager")
include(":ids-workflow-manager")
include(":ids-orchestration-manager")
include(":ids-pef")