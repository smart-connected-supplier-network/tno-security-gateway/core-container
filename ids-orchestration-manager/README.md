# IDS Orchestration Manager

This module contains the Orchestration Manager for orchestrating containers alongside the core container (e.g. Data Apps).

Currently supported container environments:
- Kubernetes

This module is actively used by the [`ids-workflow-manager` module](../ids-workflow-manager) to start, stop, and manage containers for workflows.
