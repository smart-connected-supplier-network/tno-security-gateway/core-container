plugins {
    kotlin("plugin.spring")
    kotlin("plugin.serialization")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation(project(":ids-security"))

    implementation("io.fabric8:kubernetes-client:${LibraryVersions.K8s.fabric8}")
    implementation("com.github.fkorotkov:k8s-kotlin-dsl:${LibraryVersions.K8s.k8sKotlin}") {
        exclude("io.fabric8")
    }

    implementation(project(":ids-configuration"))

    // Model serialization support
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${LibraryVersions.Kotlin.kotlinxSerialization}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    testImplementation("io.fabric8:kubernetes-server-mock:${LibraryVersions.K8s.fabric8}")
    testImplementation(project(":ids-test-extensions"))
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-opt-in=kotlinx.serialization.ExperimentalSerializationApi"
    }
}