/*
 * Copyright (C) 2021 TNO
 * 
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.orchestrationmanager

import com.fkorotkov.kubernetes.*
import com.fkorotkov.kubernetes.apps.*
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.client.KubernetesClient
import io.fabric8.kubernetes.client.server.mock.EnableKubernetesMockClient
import nl.tno.ids.orchestrationmanager.models.ContainerModel
import nl.tno.ids.orchestrationmanager.models.HealthProbe
import nl.tno.ids.orchestrationmanager.models.ImageModel
import nl.tno.ids.testextensions.DapsTestExtension
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith

/**
 * Kubernetes Orchestration Manager test using Kubernetes Mock client
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@EnableKubernetesMockClient(crud=true)
@ExtendWith(DapsTestExtension::class)
class KubernetesOrchestrationManagerTest {
    /** Kubernetes Mock client */
    private lateinit var client: KubernetesClient
    /** Kubernetes Orchestration Manager */
    private lateinit var idsKubernetesOrchestrationManager: KubernetesOrchestrationManager
    /** Container Model used across test methods */
    private lateinit var containerModel: ContainerModel

    /** Orchestration Manager configuration */
    private val orchestrationManagerConfig: OrchestrationManagerConfig = OrchestrationManagerConfig(
        masterUrl = null,
        pullSecretName = "ids-pull-secret",
        namespace = "default",
        mocked = true
    )

    /** Setup context before each test method  */
    @BeforeEach
    fun setup() {
        idsKubernetesOrchestrationManager = KubernetesOrchestrationManager(orchestrationManagerConfig, this.client)
        containerModel = ContainerModel("nginx", ImageModel("nginx", "1.14.2"), listOf("8080"))
    }

    /** Deploy test container */
    @Test
    @Order(1)
    fun testDeployment() {
        idsKubernetesOrchestrationManager.launchContainer(containerModel).join()
        Assertions.assertEquals(1, idsKubernetesOrchestrationManager.containers.size)

        println(idsKubernetesOrchestrationManager.getContainerLog("nginx", 10).toString())
        idsKubernetesOrchestrationManager.containerStatus("nginx")

        idsKubernetesOrchestrationManager.stopContainer("nginx")
        Assertions.assertEquals(0, idsKubernetesOrchestrationManager.containers.size)
    }

    /** Deploy test container via the REST controller */
    @Test
    @Order(2)
    fun testOrchestrationManagerController() {
        val orchestrationManagerController = OrchestrationManagerController(idsKubernetesOrchestrationManager)

        orchestrationManagerController.addContainer(containerModel).join()
        Assertions.assertDoesNotThrow { orchestrationManagerController.getContainer(containerModel.name) }
        Assertions.assertEquals(1, idsKubernetesOrchestrationManager.containers.size)
    }

    /** Test restoring state after startup */
    @Test
    @Order(3)
    fun testStateRestoration() {
        val deployment = newDeployment {
            metadata {
                name = "test"
                labels =
                    mapOf("app.kubernetes.io/managed-by" to "ids-orchestration-manager")
            }
            spec {
                strategy { type = "Recreate" }
                replicas = 1
                selector {
                    matchLabels = mapOf("app.kubernetes.io/name" to containerModel.name)
                }
                template {
                    metadata {
                        labels = mapOf("app.kubernetes.io/name" to containerModel.name)
                    }
                    spec {
                        containers =
                            listOf(
                                newContainer {
                                    name = "app"
                                    image = containerModel.image.name
                                    ports =
                                        containerModel.ports.map { port ->
                                            newContainerPort { containerPort = port.toInt() }
                                        }
                                    env =
                                        containerModel.environment.map {
                                            newEnvVar {
                                                name = it.key
                                                value = it.value
                                            }
                                        }
                                    volumeMounts =
                                        listOf(
                                            newVolumeMount {
                                                name = containerModel.image.name
                                                mountPath = containerModel.configMountPath
                                            })
                                    tty = true
                                    val probe = newProbe {
                                        httpGet =
                                            newHTTPGetAction {
                                                path = containerModel.healthEndpoint
                                                port =
                                                    IntOrString(
                                                        containerModel.ports.firstOrNull()?.toInt()
                                                            ?: 8080)
                                                initialDelaySeconds =
                                                    containerModel.initialDelaySeconds
                                                periodSeconds = 3
                                            }
                                    }
                                    when(containerModel.healthProbe) {
                                        HealthProbe.STARTUP -> {
                                            startupProbe = probe
                                        }
                                        HealthProbe.LIVENESS -> {
                                            livenessProbe = probe
                                        }
                                        HealthProbe.READINESS -> {
                                            readinessProbe = probe
                                        }
                                    }
                                })
                        volumes =
                            listOf(
                                newVolume {
                                    name = containerModel.image.name
                                    configMap { name = containerModel.image.name }
                                })
                    }
                }
            }
        }
        client.apps().deployments().resource(deployment).create()
        idsKubernetesOrchestrationManager.restoreState()
        Assertions.assertEquals(1, idsKubernetesOrchestrationManager.containers.size)
    }
}