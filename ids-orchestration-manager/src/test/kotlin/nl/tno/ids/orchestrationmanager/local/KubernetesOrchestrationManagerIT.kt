/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.orchestrationmanager.local

import nl.tno.ids.orchestrationmanager.KubernetesOrchestrationManager
import nl.tno.ids.orchestrationmanager.models.ConfigurationEntry
import nl.tno.ids.orchestrationmanager.models.ContainerModel
import nl.tno.ids.orchestrationmanager.models.ImageModel
import nl.tno.ids.testextensions.DapsTestExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.core.io.Resource
import org.springframework.test.context.TestPropertySource

/**
 * Kubernetes Orchestration Manager Integration Test
 *
 * Requires active Kubernetes configuration on this machine (e.g. via KUBECONFIG environment variable)
 */
@SpringBootTest(classes = [KubernetesOrchestrationManagerIT.Application::class])
@TestPropertySource(properties = ["spring.config.location = classpath:properties.yaml"])
@ExtendWith(DapsTestExtension::class)
class KubernetesOrchestrationManagerIT {
    /** Test data app configuration */
    @Value("classpath:openapi-config.yaml") var openapiConfig: Resource? = null

    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Kubernetes Orchestration Manager */
    @Autowired lateinit var idsKubernetesOrchestrationManager: KubernetesOrchestrationManager

    /** Deploy test data app with the orchestration manager  */
    @Test
    fun testDeployment() {

        val containerName = "open-api-data-app"
        val containerConfig =
            ContainerModel(
                containerName,
                ImageModel("registry.ids.smart-connected.nl/openapi-data-app", "IM4"),
                listOf("8080"),
                listOf(
                    ConfigurationEntry(
                        "application.yaml", openapiConfig?.file?.readText() ?: "dummy:dummy")),
                configMountPath = "/app/resources")
        // Launch a container
        val t = System.currentTimeMillis()
        idsKubernetesOrchestrationManager.launchContainer(containerConfig).join()
        println("Starting container took: ${System.currentTimeMillis()-t} milliseconds")
        Assertions.assertEquals(1, idsKubernetesOrchestrationManager.containers.size)

        // Try to launch the same container twice, should fail, number of containers should remain
        // at 1.
        idsKubernetesOrchestrationManager.launchContainer(containerConfig).join()
        Assertions.assertEquals(1, idsKubernetesOrchestrationManager.containers.size)

        // Wait for the container to start up
        idsKubernetesOrchestrationManager.waitForContainerReady(containerName).join()

        // Prindt log, info and status.
        println(idsKubernetesOrchestrationManager.getContainerLog(containerName, 10)?.readText())
        println(idsKubernetesOrchestrationManager.getContainerInfo(containerName))
        println(idsKubernetesOrchestrationManager.containerStatus(containerName))

        // Delete non existing container.
        Assertions.assertEquals(null, idsKubernetesOrchestrationManager.stopContainer("ngdqwinx"))

        // Delete container.
        idsKubernetesOrchestrationManager.stopContainer(containerName)
        Assertions.assertEquals(0, idsKubernetesOrchestrationManager.containers.size)
    }
}
