/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.orchestrationmanager

import com.fkorotkov.kubernetes.*
import com.fkorotkov.kubernetes.apps.*
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.api.model.Pod
import io.fabric8.kubernetes.client.KubernetesClient
import io.fabric8.kubernetes.client.KubernetesClientException
import nl.tno.ids.orchestrationmanager.models.*
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import java.io.Reader
import java.util.concurrent.CompletableFuture
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

/** Kubernetes Orchestration Manager implementation */
@Component
@ConditionalOnProperty(
    prefix = "orchestration-manager-config", name = ["enableKubernetes"], havingValue = "true")
class KubernetesOrchestrationManager(
    /** Autowired Orchestration Manager Configuration */
    val config: OrchestrationManagerConfig,

    /** Autowired Kubernetes Client */
    val client: KubernetesClient
) : OrchestrationManager {
    companion object {
        private val LOG = LoggerFactory.getLogger(KubernetesOrchestrationManager::class.java)
    }

    /** Kubernetes labels used for all deployments of this Orchestration Manager */
    val commonLabels =
        mapOf("app.kubernetes.io/managed-by" to "ids-orchestration-manager") +
            (getKubernetesInstanceName()?.let {
                mapOf("app.kubernetes.io/created-by" to it)
                mapOf("app.kubernetes.io/instance" to it)
            }
                ?: emptyMap())

    /** Set of currently running containers that are managed by this Orchestration Manager */
    override val containers: MutableSet<ContainerInstance> = mutableSetOf()

    /** Restore state of the orchestration manager by retrieving data from the Kubernetes cluster */
    @PostConstruct
    fun restoreState() {

        LOG.info("Restoring previously started containers state")
        val orchestrationManagerConfigMaps = client.configMaps().withLabel("app.kubernetes.io/managed-by", "ids-orchestration-manager").list().items


        client.apps().deployments().withLabel("app.kubernetes.io/managed-by", "ids-orchestration-manager").list().items.forEach { deployment ->
            deployment.spec.template.spec.containers.forEach { container ->

                var mountPath: String? = null

                val configuration = container.volumeMounts.flatMap { mount ->
                    if (mountPath == null) {
                        mountPath = mount.mountPath
                    }
                    val configMaps = orchestrationManagerConfigMaps.filter { it.metadata.name == mount.name }
                    configMaps.flatMap { configMap ->
                        configMap.data.map { configEntry ->
                            ConfigurationEntry(configEntry.key, configEntry.value.trimIndent())
                        }
                    }
                }

                val healthProbePath: String
                val healthProbeType: HealthProbe
                val initialDelaySeconds: Int
                if(container.startupProbe.httpGet != null) {
                    healthProbePath = container.startupProbe.httpGet.path
                    initialDelaySeconds = container.startupProbe.initialDelaySeconds
                    healthProbeType = HealthProbe.STARTUP
                }
                else if(container.livenessProbe.httpGet != null) {
                    healthProbePath = container.livenessProbe.httpGet.path
                    initialDelaySeconds = container.livenessProbe.initialDelaySeconds
                    healthProbeType = HealthProbe.LIVENESS
                }
                else  {
                    healthProbePath = container.readinessProbe.httpGet.path
                    initialDelaySeconds = container.readinessProbe.initialDelaySeconds
                    healthProbeType = HealthProbe.READINESS
                }

                val containerModel = ContainerModel(
                    name = deployment.metadata.name,
                    image = ImageModel(
                        name = container.image.split(":")[0],
                        tag = if (container.image.split(":").size > 1) container.image.split(":")[1] else "latest"
                    ),
                    ports = container.ports.map { port ->
                        port.containerPort.toString()
                    },
                    configuration = configuration,
                    configMountPath = mountPath ?: "/config",
                    environment = container.env.associate { envVar ->
                        Pair(envVar.name, envVar.value)
                    },
                    healthEndpoint =  healthProbePath,
                    initialDelaySeconds = initialDelaySeconds,
                    healthProbe = healthProbeType
                )

                containers.add(ContainerInstance(containerModel, client.namespace, configuration.map { configurationEntry ->
                    "$mountPath/${configurationEntry.filename}"
                }))
            }
        }
    }

    /** Clean up running containers before destroying this Orchestration Manager */
    @PreDestroy
    fun preDestroy() {
        LOG.info("Gracefully shutting and removing orchestrated containers")
        if (containers
            .toMutableSet()
            .mapNotNull {
                LOG.info("Stopping ${it.containerModel.name}")
                stopContainer(it.containerModel.name)
            }
            .isNotEmpty()) {
            Thread.sleep(5000)
        }
    }

    /**
     * Launch a new container based on a ContainerModel
     *
     * Deploys a ConfigMap, Deployment, and Service on the Kubernetes cluster. Returns a a future
     * with a ContainerInstance if the deployment was successful
     */
    @Async
    override fun launchContainer(
        containerModel: ContainerModel,
        waitForReady: Boolean
    ): CompletableFuture<ContainerInstance?> {
        val imageName = "${containerModel.image.name}${if (containerModel.image.tag == "") "" else ":${containerModel.image.tag}"}"

        val configMapName = "${containerModel.name}-config"

        if (client.apps().deployments().list().items.any { deployment ->
            deployment.metadata.name == containerModel.name
        }) {
            LOG.error(
                "Unable to deploy container: a deployment with name ${containerModel.name} already exists.")
            return CompletableFuture.completedFuture(null)
        }

        val configMapSpec = generateConfigMap(containerModel, configMapName)
        val deploymentSpec = generateDeployment(containerModel, imageName, configMapName)
        val serviceSpec = generateService(containerModel)
        return CompletableFuture.supplyAsync {
            client.configMaps().resource(configMapSpec).createOrReplace()
            client.apps().deployments().resource(deploymentSpec).create()
            client.services().resource(serviceSpec).create()

            while (waitForReady && !containerHealthy(containerModel.name)) {
                LOG.debug("Waiting for deployment ${containerModel.name} to get ready..")
                Thread.sleep(500)
            }

            ContainerInstance(
                    containerModel = containerModel,
                    namespace = client.namespace,
                    volumes =
                        containerModel.configuration.map {
                            "${containerModel.configMountPath}/${it.filename}"
                        })
                .also { containers.add(it) }
        }
            .orTimeout(config.timeout, TimeUnit.SECONDS)
            .exceptionally { exception ->
                when (exception) {
                    is TimeoutException -> LOG.error("Timeout exceeded. Could not start container.")
                    else -> LOG.error("Error starting container: ${exception.message}", exception)
                }
                // Do cleanup, delete container and service.
                stopContainer(containerModel.name)
                null
            }
    }

    /** Generate the ConfigMap specification with configuration for the Container */
    private fun generateConfigMap(containerModel: ContainerModel, configMapName: String) =
        newConfigMap {
        metadata {
            name = configMapName
            labels = commonLabels + mapOf("app.kubernetes.io/name" to containerModel.name)
        }
        data =
            containerModel.configuration.associate { configEntry ->
                Pair(configEntry.filename, configEntry.content)
            }
    }

    /** Generate the Deployment specification for the ContainerModel */
    private fun generateDeployment(
        containerModel: ContainerModel,
        imageName: String,
        configMapName: String
    ) = newDeployment {
        metadata {
            name = containerModel.name
            labels =
                commonLabels + mapOf("app.kubernetes.io/name" to containerModel.name) + commonLabels
        }
        spec {
            strategy { type = "Recreate" }
            replicas = 1
            selector {
                matchLabels = mapOf("app.kubernetes.io/name" to containerModel.name) + commonLabels
            }
            template {
                metadata {
                    labels = mapOf("app.kubernetes.io/name" to containerModel.name) + commonLabels
                }
                spec {
                    imagePullSecrets =
                        listOf(
                            newLocalObjectReference {
                                name = containerModel.image.pullSecretName ?: config.pullSecretName
                            })
                    containers =
                        listOf(
                            newContainer {
                                name = "app"
                                image = imageName
                                imagePullPolicy = config.pullPolicy
                                ports =
                                    containerModel.ports.map { port ->
                                        newContainerPort { containerPort = port.toInt() }
                                    }
                                env =
                                    containerModel.environment.map {
                                        newEnvVar {
                                            name = it.key
                                            value = it.value
                                        }
                                    }
                                volumeMounts =
                                    listOf(
                                        newVolumeMount {
                                            name = configMapName
                                            mountPath = containerModel.configMountPath
                                        })
                                tty = true
                                val probe = newProbe {
                                    httpGet =
                                        newHTTPGetAction {
                                            path = containerModel.healthEndpoint
                                            port =
                                                IntOrString(
                                                    containerModel.ports.firstOrNull()?.toInt()
                                                        ?: 8080)
                                            initialDelaySeconds =
                                                containerModel.initialDelaySeconds
                                            periodSeconds = 3
                                        }
                                }
                                when(containerModel.healthProbe) {
                                    HealthProbe.STARTUP -> {
                                        startupProbe = probe
                                    }
                                    HealthProbe.LIVENESS -> {
                                        livenessProbe = probe
                                    }
                                    HealthProbe.READINESS -> {
                                        readinessProbe = probe
                                    }
                                }
                            })
                    volumes =
                        listOf(
                            newVolume {
                                name = configMapName
                                configMap { name = configMapName }
                            })
                }
            }
        }
    }

    /** Generate the Service specification linked to the Deployment */
    private fun generateService(containerModel: ContainerModel) = newService {
        metadata {
            name = containerModel.name
            labels = commonLabels + mapOf("app.kubernetes.io/name" to containerModel.name)
        }
        spec {
            type = "ClusterIP"
            selector =
                mapOf("app.kubernetes.io/name" to containerModel.name) +
                    commonLabels.filterKeys { it == "app.kubernetes.io/instance" }
            ports =
                containerModel.ports.map { portNumber ->
                    newServicePort {
                        port = portNumber.toInt()
                        targetPort = IntOrString(portNumber.toInt())
                        name = "http"
                        protocol = "TCP"
                    }
                }
        }
    }

    /** Stop the container with a given name and return the corresponding ContainerInstance */
    override fun stopContainer(name: String): ContainerInstance? {
        client.apps().deployments().withName(name).delete()
        // Also stop any service associated with this container (deployment).
        deleteService(name)
        deleteConfigMap("$name-config")

        return containers.find { it.containerModel.name == name }?.also { containers.remove(it) }
            ?: run {
                LOG.warn(
                    "Cannot delete deployment with name $name because no deployment with that name exists.")
                null
            }
    }

    /** Safely delete a Service from the Kubernetes cluster */
    private fun deleteService(name: String) {
        try {
            client.services().withName(name).delete()
        } catch (e: KubernetesClientException) {
            LOG.warn("Cannot delete service with name $name: ${e.message}")
        }
    }

    /** Safely delete a ConfigMap from the Kubernetes cluster */
    private fun deleteConfigMap(name: String) {
        try {
            client.configMaps().withName(name).delete()
        } catch (e: KubernetesClientException) {
            LOG.warn("Cannot delete config map with name $name: ${e.message}")
        }
    }

    /** Get the deployed Pods for a given Deployment */
    private fun getPodsForDeployment(name: String): List<Pod> {
        return client
            .pods()
            .withLabelSelector(client.apps().deployments().withName(name).get()?.spec?.selector)
            ?.list()
            ?.items
            ?: emptyList()
    }

    /** Get a Reader for the logs of the first Pod in a Deployment */
    override fun getContainerLog(name: String, numTailingLines: Int): Reader? {
        return getPodsForDeployment(name).firstOrNull()?.metadata?.name?.let { podName ->
            client
                .pods()
                .inNamespace(config.namespace)
                .withName(podName)
                ?.tailingLines(numTailingLines)
                ?.logReader
        }
    }

    /** Get the current Container Info */
    override fun getContainerInfo(name: String): Map<String, String?>? {
        val infoMap = mutableMapOf<String, String?>()
        val deployment =
            client.apps().deployments().inNamespace(config.namespace).withName(name).get()
        if (deployment != null) {
            infoMap["creationTimestamp"] = deployment.metadata?.creationTimestamp
            infoMap["nodeName"] = deployment.spec?.template?.spec?.nodeName
            infoMap["podName"] = deployment.metadata?.generateName
        }

        return infoMap
    }

    /**
     * Get the status of the Pods for a given Container/Deployment
     *
     * TODO: Extract relevant information
     */
    override fun containerStatus(name: String): Map<String, String> {
        return getPodsForDeployment(name).associate { it.metadata.name to it.status.toString() } +
            mapOf(
                "deployment" to
                    (client.apps().deployments().withName(name).get().status?.toString()
                        ?: "unknown"))
    }

    /**
     * Check whether the container is healthy
     *
     * Returns true when status can't be found, e.g. when using Kubernetes Mock Server
     */
    override fun containerHealthy(name: String): Boolean {
        if (config.mocked) return true
        return client.apps().deployments().withName(name).get().status?.conditions?.any { condition ->
            condition.type == "Available" && condition.status == "True"
        } ?: false
    }

    /**
     * Wait for the Container to be healthy, by providing a CompletableFuture with a given timeout
     */
    override fun waitForContainerReady(name: String, timeout: Long?): CompletableFuture<Boolean> {
        return CompletableFuture.supplyAsync {
            while (!containerHealthy(name)) {
                Thread.sleep(1000)
            }
            true
        }
            .orTimeout(timeout ?: config.timeout, TimeUnit.SECONDS)
            .exceptionally { false }
    }

    /**
     * If instance is not set in the config,this method gets the Helm instance name from the
     * containing Kubernetes cluster
     */
    private fun getKubernetesInstanceName(): String? {
        val podName: String = System.getenv("HOSTNAME") ?: return null

        // Get instance name from pod labels
        return client
            .pods()
            .withName(podName)
            ?.get()
            ?.metadata
            ?.labels
            ?.get("app.kubernetes.io/name")
    }
}
