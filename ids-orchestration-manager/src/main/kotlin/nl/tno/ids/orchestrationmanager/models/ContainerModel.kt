/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.orchestrationmanager.models

import kotlinx.serialization.Serializable

/** Container Model specifying the relevant information for deploying a container */
@Serializable
data class ContainerModel(
    /** Container Name */
    val name: String,

    /** Container (Docker) Image */
    val image: ImageModel,

    /** List of ports to be exposed */
    val ports: List<String>,

    /** Additional configuration entries to be mounted on the container */
    val configuration: List<ConfigurationEntry> = emptyList(),

    /** Location in the container for mounting configuration */
    val configMountPath: String = "/config",

    /** Map of environment variables that will be set for the container */
    val environment: Map<String, String> = emptyMap(),

    /** Health endpoint for the container */
    val healthEndpoint: String = "/health",

    /** Initial delay before health checks start */
    val initialDelaySeconds: Int = 3,

    /** Type of Health probe */
    val healthProbe: HealthProbe = HealthProbe.STARTUP
)

/**
 * Health probe types
 */
enum class HealthProbe {
    STARTUP, LIVENESS, READINESS
}

/** Configuration Entry specification */
@Serializable
data class ConfigurationEntry(
    /** Filename to be used for this entry */
    val filename: String,

    /** Configuration content */
    val content: String,
)

/** (Docker) Image specification */
@Serializable
data class ImageModel(
    /** Image Name */
    val name: String,

    /** Image Tag */
    val tag: String = "latest",

    /**
     * Required Image Pull Secret
     *
     * If not set, the global pull secret of OrchestrationManagerConfig will be used
     */
    val pullSecretName: String? = null
)
