/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.orchestrationmanager

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/** Orchestration Manager Configuration */
@Component
@ConfigurationProperties(prefix = "orchestration-manager-config")
data class OrchestrationManagerConfig(
    /** Enable Kubernetes Orchestration Manager */
    var enableKubernetes: Boolean = false,

    /**
     * Kubernetes cluster master URL
     */
    var masterUrl: String? = null,

    /** Client certificate file. */
    var clientCertificate: String? = null,

    /** Client key file. */
    var clientKey: String? = null,

    /** Certificate Authority data */
    var certificateAuthorityData: String? = null,

    /**
     * Global pull-secret, used when no explicit pull-secret is given for a container
     *
     * When null and no explicit pull-secret is given for a container, no pull-secret is applied
     */
    var pullSecretName: String? = null,

    /**
     * Kubernetes Image Pull Policy
     *
     * Should be one of: Always, IfNotPresent, Never
     */
    var pullPolicy: String = "IfNotPresent",

    /**
     * Kubernetes Namespace to use for resources, defaults to the current namespace (if in a Pod) or
     * to "default"
     */
    var namespace: String? = null,

    /** Timeout (in seconds) for Kubernetes Completable Futures */
    var timeout: Long = 60 * 15,

    /** Flag for usage of Kubernetes Mock Server */
    var mocked: Boolean = false
)
