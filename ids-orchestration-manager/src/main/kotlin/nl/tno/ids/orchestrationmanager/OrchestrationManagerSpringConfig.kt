/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.orchestrationmanager

import io.fabric8.kubernetes.client.*
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.FileNotFoundException
import java.net.URL

/** Spring Configuration class for providing relevant Beans for the Orchestration Manager */
@Configuration
class OrchestrationManagerSpringConfig(var orchestrationManagerConfig: OrchestrationManagerConfig) {
    companion object {
        private val LOG = LoggerFactory.getLogger(OrchestrationManagerSpringConfig::class.java)
    }

    /**
     * Fabric8 Kubernetes Client for interacting with a Kubernetes cluster
     *
     * Automatically selects the current namespace if the current connector instance is deployed on
     * Kubernetes
     */
    @Bean
    @ConditionalOnProperty(
        prefix = "orchestration-manager-config", name = ["enableKubernetes"], havingValue = "true")
    fun kubernetesClient(): KubernetesClient? {
        return try {
            val namespace =
                orchestrationManagerConfig.namespace
                    ?: run {
                        try {
                            URL("file:///var/run/secrets/kubernetes.io/serviceaccount/namespace")
                                .readText()
                        } catch (e: FileNotFoundException) {
                            LOG.warn(
                                "No explicit Kubernetes namespace set and the current context is not in a Kubernetes cluster, defaulting namespace to \"default\"")
                            "default"
                        }
                    }
            if (orchestrationManagerConfig.masterUrl != null) {
                val config =
                    ConfigBuilder()
                        .withMasterUrl(orchestrationManagerConfig.masterUrl)
                        .withCaCertData(orchestrationManagerConfig.certificateAuthorityData)
                        .withClientCertData(orchestrationManagerConfig.clientCertificate)
                        .withClientKeyData(orchestrationManagerConfig.clientKey)
                        .withNamespace(namespace)
                        .build()
                KubernetesClientBuilder().withConfig(config).build()
            } else {
                KubernetesClientBuilder().withConfig(
                    ConfigBuilder().withNamespace(namespace).build()
                ).build()
            }
        } catch (e: KubernetesClientException) {
            LOG.warn("Could not start Kubernetes Client")
            null
        }
    }
}
