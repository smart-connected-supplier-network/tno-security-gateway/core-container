/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package nl.tno.ids.orchestrationmanager

import kotlinx.serialization.Serializable
import nl.tno.ids.configuration.model.SecurityRoles
import nl.tno.ids.orchestrationmanager.models.ContainerInstance
import nl.tno.ids.orchestrationmanager.models.ContainerModel
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.http.HttpStatus
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.concurrent.CompletableFuture

@Serializable
data class ContainerWithStatus(
    val containerInfo: ContainerInstance,
    val healthy: Boolean
)

/**
 * Rest Controller for interacting with the Orchestration Manager via the HTTP API
 */
@RestController
@RequestMapping("/api/orchestration")
@Secured(SecurityRoles.ORCHESTRATION_MANAGER)
@ConditionalOnProperty(
    prefix = "orchestration-manager-config", name = ["enableKubernetes"], havingValue = "true")
class OrchestrationManagerController(
    private val orchestrationManager: OrchestrationManager,
) {
    companion object {
        val LOG: Logger = LoggerFactory.getLogger(OrchestrationManagerController::class.java)
    }

    /**
     * Retrieve all running containers managed by the OrchestrationManager
     */
    @GetMapping(
        produces = ["application/json"]
    )
    @Secured(SecurityRoles.ORCHESTRATION_READER)
    fun listContainers(): CompletableFuture<List<ContainerWithStatus>>
    {
        return CompletableFuture.supplyAsync {
            orchestrationManager.containers.map {
                ContainerWithStatus(it, orchestrationManager.containerHealthy(it.containerModel.name))
            }
        }
    }
    /**
     * Retrieve detailed information for a specific container
     */
    @GetMapping(
        value = ["{containerName}"],
        produces = ["application/json"]
    )
    @Secured(SecurityRoles.ORCHESTRATION_READER)
    fun getContainer(@PathVariable containerName: String): ContainerWithStatus {
        return orchestrationManager.containers.firstOrNull { container ->
            container.containerModel.name == containerName
        }?.let { container ->
            ContainerWithStatus(container, orchestrationManager.containerHealthy(containerName))
        } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Container not found")
    }

    /**
     * Add a new container using the given container configuration.
     */
    @PostMapping(
        consumes = ["application/json"]
    )
    fun addContainer(@RequestBody containerParameters: ContainerModel): CompletableFuture<ContainerInstance?> {

        LOG.info("Adding Container: $containerParameters")
        return orchestrationManager.launchContainer(containerParameters)
    }

    /**
     * Delete a container from the OrchestrationManager
     */
    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @DeleteMapping("{containerName}")
    fun stopContainer(@PathVariable containerName: String): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            orchestrationManager.stopContainer(containerName)?.let { "" }
                ?: run { throw ResponseStatusException(HttpStatus.NOT_FOUND, "Container not found") }
        }
    }
}