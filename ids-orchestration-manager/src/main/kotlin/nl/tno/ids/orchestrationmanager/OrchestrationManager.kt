/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.orchestrationmanager

import java.io.Reader
import java.util.concurrent.CompletableFuture
import nl.tno.ids.orchestrationmanager.models.ContainerInstance
import nl.tno.ids.orchestrationmanager.models.ContainerModel

/**
 * Orchestration Manager interface. Implement using a container manager of choice
 *
 * @author TNO
 */
interface OrchestrationManager {
    /**
     * Containers that this manager manages.
     * @return Set of IDSContainers
     */
    val containers: Set<ContainerInstance>

    /**
     * Launches a named container with the specified image name, exposing the given port range
     * @param containerModel Container Model with the required specification of the container that
     * should be launched
     * @return The created IDSContainer if successful, or null
     */
    fun launchContainer(
        containerModel: ContainerModel,
        waitForReady: Boolean = true
    ): CompletableFuture<ContainerInstance?>

    /**
     * Stops container with the given name
     * @param name Name of the container to stop
     * @return The stopped IDSContainer if successful, or null
     */
    fun stopContainer(name: String): ContainerInstance?

    /**
     * Return container log
     * @param name Name of the container
     * @param numTailingLines Number of lines, from the end of the log, to return
     * @return Reader containing the logs if successful, or null
     */
    fun getContainerLog(name: String, numTailingLines: Int): Reader?

    /**
     * Return container info
     * @param name Name of the container
     * @return A string containing container info and metadata, or null if the container does not
     * exist.
     *
     * TODO: Create a data model for the Container info
     */
    fun getContainerInfo(name: String): Map<String, String?>?

    /**
     * Check if the container with the given name exists.
     * @param name Name of the container
     * @return True of the container exists, false if it does not.
     */
    fun containerExists(name: String): Boolean =
        containers.any { container -> container.containerModel.name == name }

    /**
     * Returns string representation of container status.
     * @param name Name of the container
     * @return Detailed string representation of container status
     */
    fun containerStatus(name: String): Map<String, String>

    /**
     * Check if container is healthy
     * @param name Name of the container
     * @return True if container is healthy, false otherwise
     */
    fun containerHealthy(name: String): Boolean

    /**
     * Wait till a container is ready, with a maximum timeout.
     * @param name Name of the container
     * @param timeout Nullable timeout for the future
     * @return True if container is ready, false otherwise
     */
    fun waitForContainerReady(name: String, timeout: Long? = null): CompletableFuture<Boolean>
}
