/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.routemanager

import nl.tno.ids.configuration.infomodel.process
import nl.tno.ids.configuration.model.*
import org.apache.camel.ExtendedCamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.impl.engine.DefaultResourceResolvers.MemResolver
import org.apache.camel.language.constant.ConstantLanguage.constant
import org.apache.camel.model.ProcessorDefinition
import org.apache.camel.model.RouteDefinition
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

/** Camel XML Route generator for converting Connector Route model to Apache Camel XML routes */
@Component
class CamelRoutesGenerator(
    @Value("\${camel.port:8080}") private val camelPort: String,
    /** Camel HTTPS flag */
    @Value("\${routes.https:false}") private val https: Boolean
) {
    private var routeCount = 0

    /** Generate XML InputStream for a list of Routes */
    fun generate(routeList: List<Route>, context: ExtendedCamelContext): List<RouteDefinition> {
        return routeList.mapNotNull {
            when (it) {
                is IDSCPIngressRoute -> generateRoute(it, context)
                is IDSCPEgressRoute -> generateRoute(it, context)
                is HTTPSIngressRoute -> generateRoute(it, context)
                is HTTPSEgressRoute -> generateRoute(it, context)
                is CustomRoute -> generateRoute(it, context)
            }
        }
    }

    /** Load CustemRoute xml directly into the Camel context */
    private fun generateRoute(route: CustomRoute, context: ExtendedCamelContext): RouteDefinition? {
        val routes = """
            <routes xmlns="http://camel.apache.org/schema/spring">
            ${route.xml}
            </routes>
        """.trimIndent()
        context.routesLoader.loadRoutes(
            MemResolver().createResource("routes.xml", routes)
        )
        return null
    }

    /** Generate Route definition for IDSCPIngressRoutes */
    private fun generateRoute(route: IDSCPIngressRoute, context: ExtendedCamelContext): RouteDefinition {
        if (route.id == null) {
            route.id = "route-${routeCount++}-idscp-in"
        }
        return object : RouteBuilder() {
            override fun configure() {
                from(
                    constructUri(
                        base = "idscp2server://0.0.0.0:${route.port ?: 9292}/",
                        "dapsSslContextParameters" to "#dapsSslContextParameters",
                        "transportSslContextParameters" to "#transportSslContextParameters",
                        "tlsClientHostnameVerification" to "${route.tlsClientHostnameVerification}",
                        "useIdsMessages" to "true"
                    )
                ).routeId(route.id)
                    .process("IDSCPInputProcessor")
                    .process("multiPartOutputProcessor")
                    .to(constructUri(route.dataApp, "throwExceptionOnFailure" to "false"))
                    .process("multiPartInputProcessor")
                    .process("IDSCPOutputProcessor")

            }
        }.run {
            configureRoutes(context)
            routeCollection.routes.first()
        }
    }


    /** Generate Route definition for IDSCPEgressRoutes */
    private fun generateRoute(route: IDSCPEgressRoute, context: ExtendedCamelContext): RouteDefinition {
        if (route.id == null) {
            route.id = "route-${routeCount++}-idscp-out"
        }
        return object : RouteBuilder() {
            override fun configure() {
                if (route.restOverIDSCP) {
                    from(
                        constructUri(
                            base = "jetty:http${if (https) "s" else ""}://0.0.0.0:${route.listenPort ?: camelPort}/${route.endpoint}",
                            "mapHttpMessageBody" to "false",
                            "enableMultipartFilter" to "false",
                            "sslContextParameters" to if (https) "#idsSSLContextParameters" else ""
                        )
                    ).routeId(route.id)
                        .toD(
                            constructUri(
                                base = "idscp2client://\${in.headers.${route.forwardHeader}}",
                                "awaitResponse" to "true",
                                "responseTimeout" to "40000",
                                "dapsSslContextParameters" to "#dapsSslContextParameters",
                                "transportSslContextParameters" to "#transportSslContextParameters",
                                "copyHeadersRegex" to "\${in.headers.copyHttpHeaders}"
                            )
                        )
                } else {
                    from(
                        constructUri(
                            "jetty:http${if (https) "s" else ""}://0.0.0.0:${route.listenPort ?: camelPort}/${route.endpoint}",
                            "mapHttpMessageBody" to "false",
                            "enableMultipartFilter" to "false",
                            "sslContextParameters" to if (https) "#idsSSLContextParameters" else ""
                        )
                    ).routeId(route.id)
                        .process("authProcessor")
                        .egressPreProcessing(route.dapsInject, route.clearing)
                        .process("IDSCPOutputProcessor")
                        .toD(
                            constructUri(
                                "idscp2client://\${in.headers.${route.forwardHeader}}",
                                "dapsSslContextParameters" to "#dapsSslContextParameters",
                                "transportSslContextParameters" to "#transportSslContextParameters",
                                "awaitResponse" to "true",
                                "responseTimeout" to "40000",
                                "useIdsMessages" to "true"
                            )
                        )
                        .process("IDSCPInputProcessor")
                        .process("multiPartOutputProcessor")
                }
            }
        }.run {
            configureRoutes(context)
            routeCollection.routes.first()
        }
    }

    /** Generate Route definition for HTTPSIngressRoutes */
    private fun generateRoute(route: HTTPSIngressRoute, context: ExtendedCamelContext): RouteDefinition {
        if (route.id == null) {
            route.id = "route-${routeCount++}-https-in"
        }
        return object : RouteBuilder() {
            override fun configure() {
                from(
                    constructUri(
                        "jetty:http${if (https) "s" else ""}://0.0.0.0:${route.port ?: camelPort}/${route.endpoint}",
                        "mapHttpMessageBody" to "false",
                        "enableMultipartFilter" to "false${route.parameters}",
                        "sslContextParameters" to if (https) "#idsSSLContextParameters" else ""
                    )
                ).routeId(route.id)
                    .ingressPreProcessing(route.dapsVerify, route.clearing)
                    .ingressPolicyEnforcement(route.policyEnforcement, route.delegatedPolicyNegotiation)
                    .process("multiPartOutputProcessor")
                    .setHeader("Response-Type", constant("sync"))
                    .additionalProcessing(route.preProcessing)
                    .toD(
                        constructUri(
                            route.dataApp,
                            "bridgeEndpoint" to "true",
                            "copyHeaders" to "false",
                            "throwExceptionOnFailure" to "false"
                        )
                    )
                    .additionalProcessing(route.postProcessing)
                    .ingressPostProcessing(route.dapsVerify, route.clearing)
                    .onException(Exception::class.java)
                    .useOriginalMessage()
                    .handled(true)
                    .process("errorHandlerProcessor")
            }
        }.run {
            configureRoutes(context)
            routeCollection.routes.first()
        }
    }

    /** Generate Route definition for HTTPSEgressRoutes */
    private fun generateRoute(route: HTTPSEgressRoute, context: ExtendedCamelContext): RouteDefinition {
        if (route.id == null) {
            route.id = "route-${routeCount++}-https-out"
        }
        return object : RouteBuilder() {
            override fun configure() {
                from(
                    constructUri(
                        "jetty:http${if (https) "s" else ""}://0.0.0.0:${route.listenPort ?: camelPort}/${route.endpoint}",
                        "mapHttpMessageBody" to "false",
                        "enableMultipartFilter" to "false",
                        "sslContextParameters" to if (https) "#idsSSLContextParameters" else ""
                    )
                )
                    .routeId(route.id)
                    .process("authProcessor")
                    .egressPreProcessing(route.dapsInject, route.clearing)
                    .process("multiPartOutputProcessor")
                    .additionalProcessing(route.preProcessing)
                    .process("dynamicEndpointProcessor")
                    .toD("\${in.headers.${route.forwardHeader}}")
                    .egressPostProcessing(route.dapsInject, route.clearing)
                    .onException(Exception::class.java)
                    .useOriginalMessage()
                    .handled(true)
                    .process("errorHandlerProcessor")

            }
        }.run {
            configureRoutes(context)
            routeCollection.routes.first()
        }
    }

    /** Inject generic ingress pre-processing processors */
    private fun ProcessorDefinition<*>.ingressPreProcessing(
        dapsVerify: Boolean,
        clearing: Boolean
    ): ProcessorDefinition<*> {
        return process("multiPartInputProcessor")
            .process(dapsVerify, "dapsTokenVerifier")
            .process(clearing, "ingressClearing")
    }

    /** Inject generic ingress post-processing processors */
    private fun ProcessorDefinition<*>.ingressPostProcessing(dapsVerify: Boolean, clearing: Boolean): ProcessorDefinition<*> {
        return process(dapsVerify || clearing, "multiPartInputProcessor")
            .process(dapsVerify, "dapsTokenInjector")
            .process(clearing, "egressClearing")
            .process(dapsVerify || clearing, "multiPartOutputProcessor")
    }

    /** Inject generic egress pre-processing processors */
    private fun ProcessorDefinition<*>.egressPreProcessing(
        dapsInject: Boolean,
        clearing: Boolean
    ): ProcessorDefinition<*> {
        return process("multiPartInputProcessor")
            .process(dapsInject, "dapsTokenInjector")
            .process(clearing, "egressClearing")
    }

    /** Inject generic egress post-processing processors */
    private fun ProcessorDefinition<*>.egressPostProcessing(dapsVerify: Boolean, clearing: Boolean): ProcessorDefinition<*> {
        return process(dapsVerify || clearing, "multiPartInputProcessor")
            .process(dapsVerify, "dapsTokenVerifier")
            .process(clearing, "ingressClearing")
            .process(dapsVerify || clearing, "multiPartOutputProcessor")
    }

    /** Inject custom processing processors */
    private fun ProcessorDefinition<*>.additionalProcessing(processors: List<String>): ProcessorDefinition<*> {
        var processor = this
        processors.forEach { processor = processor.process(it) }
        return processor
    }

    /** Inject ingress PEF processors */
    private fun ProcessorDefinition<*>.ingressPolicyEnforcement(
        enabled: Boolean,
        delegatedNegotiation: Boolean
    ): ProcessorDefinition<*> {
        if (!enabled) return this
        val delegated = if (delegatedNegotiation) {
            setHeader("PEF_STOP_ROUTE", constant("false"))
        } else {
            process("negotiationProcessor")
        }
        return delegated
            .setHeader("PEF_STAGE", constant("PRE"))
            .setHeader("PEF_MODE", constant("INGRESS"))
            .process("pefProcessor")
    }

    /** Construct URI with query parameters */
    private fun constructUri(base: String, vararg parameters: Pair<String, String>): String {
        val parameterString = when {
            parameters.isEmpty() -> ""
            else -> {
                "?" + parameters.filter { it.second.isNotEmpty() }.joinToString(separator = "&") { "${it.first}=${it.second}" }
            }
        }
        return "$base$parameterString"
    }
}