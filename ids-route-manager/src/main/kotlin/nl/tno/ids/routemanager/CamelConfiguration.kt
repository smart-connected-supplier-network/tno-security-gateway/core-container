package nl.tno.ids.routemanager

import io.micrometer.core.instrument.Clock
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import io.prometheus.client.CollectorRegistry
import org.apache.camel.CamelContext
import org.apache.camel.component.jetty.JettyHttpComponent
import org.apache.camel.component.micrometer.MicrometerConstants
import org.apache.camel.component.micrometer.messagehistory.MicrometerMessageHistoryFactory
import org.apache.camel.component.micrometer.routepolicy.MicrometerRoutePolicyFactory
import org.apache.camel.spring.boot.CamelContextConfiguration
import org.eclipse.jetty.server.handler.ErrorHandler
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Camel metrics configuration
 *
 * Allows to extract metrics via Spring actuator in Prometheus format
 */
@Configuration
class CamelMetricsConfiguration {
    /** Prometheus collector registry */
    @Bean
    fun collectorRegistry(): CollectorRegistry {
        return CollectorRegistry()
    }
    /** Prometheus configuration */
    @Bean
    fun prometheusConfig(): PrometheusConfig {
        return PrometheusConfig.DEFAULT
    }

    /** Micrometer Prometheus registry */
    @Bean(name = [MicrometerConstants.METRICS_REGISTRY_NAME, "prometheusMeterRegistry"])
    fun prometheusMeterRegistry(
        prometheusConfig: PrometheusConfig, collectorRegistry: CollectorRegistry, clock: Clock
    ): PrometheusMeterRegistry {
        return PrometheusMeterRegistry(prometheusConfig, collectorRegistry, clock)
    }

    /** Camel Micrometer route policy & message history configuration */
    @Bean
    fun camelContextConfiguration(): CamelContextConfiguration {
        return object : CamelContextConfiguration {
            override fun beforeApplicationStart(camelContext: CamelContext) {
                camelContext.addRoutePolicyFactory(MicrometerRoutePolicyFactory())
                camelContext.messageHistoryFactory = MicrometerMessageHistoryFactory()
            }

            override fun afterApplicationStart(camelContext: CamelContext) {
                val jetty = camelContext.getComponent("jetty", JettyHttpComponent::class.java)
                jetty.isSendServerVersion = false
                jetty.errorHandler = ErrorHandler().apply {
                    isShowServlet = false
                    isShowStacks = false
                }
            }
        }
    }
}