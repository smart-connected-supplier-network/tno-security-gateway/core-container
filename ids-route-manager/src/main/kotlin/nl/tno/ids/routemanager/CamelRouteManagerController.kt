/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.routemanager

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import nl.tno.ids.configuration.model.Route
import nl.tno.ids.configuration.model.SecurityRoles
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.util.concurrent.CompletableFuture

/** Rest Controller for interacting with the Camel Route Manager via the HTTP API */
@RestController
@RequestMapping("/api/routes")
@Secured(SecurityRoles.ROUTE_MANAGER)
class CamelRouteManagerController(
    /** Camel route manager */
    private val camelRouteManager: CamelRouteManager
) {
    companion object {
        val LOG: Logger = LoggerFactory.getLogger(CamelRouteManagerController::class.java)
    }

    /** KotlinX Json serializer */
    private val json = Json { coerceInputValues = true }

    /** Retrieve all routes managed by the CamelRouteManager */
    @GetMapping(produces = ["application/json"])
    @Secured(SecurityRoles.ROUTE_READER)
    fun listRoutes(): CompletableFuture<String> {
        return CompletableFuture.supplyAsync { Json.encodeToString(camelRouteManager.getRoutes()) }
    }

    /** Retrieve detailed information for a specific route */
    @GetMapping(value = ["{routeId}"], produces = ["application/json"])
    @Secured(SecurityRoles.ROUTE_READER)
    fun getRoute(@PathVariable routeId: String): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            camelRouteManager.getRoute(routeId)?.let { Json.encodeToString(it) }
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Route not found")
        }
    }

    /** Add a new route based on route configuration class */
    @PostMapping(consumes = ["application/json"])
    fun addRoute(@RequestBody routeString: String): CompletableFuture<String> {
        LOG.info("Add route: $routeString")
        return CompletableFuture.supplyAsync {
            val route =
                try {
                    json.decodeFromString<Route>(routeString)
                } catch (e: Exception) {
                    LOG.warn("Could not deserialize JSON: ${e.message}", e)
                    throw ResponseStatusException(
                        HttpStatus.BAD_REQUEST, "Could not deserialize JSON: ${e.message}")
                }
            camelRouteManager.addRoute(route)
            ""
        }
    }

    /** Delete a route from the Route manager and the Camel instance */
    @DeleteMapping("{routeId}")
    fun deleteRoute(@PathVariable routeId: String): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            if (camelRouteManager.removeRoute(routeId)) {
                ""
            } else {
                throw ResponseStatusException(HttpStatus.NOT_FOUND, "Route not found")
            }
        }
    }
}
