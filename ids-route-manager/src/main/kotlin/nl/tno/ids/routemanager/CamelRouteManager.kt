/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.routemanager

import io.micrometer.core.instrument.MeterRegistry
import kotlinx.serialization.Serializable
import nl.tno.ids.configuration.model.Route
import nl.tno.ids.configuration.model.RouteConfig
import org.apache.camel.CamelContext
import org.apache.camel.ExtendedCamelContext
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.model.Model
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.util.concurrent.TimeUnit
import javax.annotation.PostConstruct

/** Generic route information message data class */
@Serializable
data class RouteMessage(
    val id: String,
    val uptime: Long,
    val status: String,
    val stats: RouteStats?
)

/** Detailed route information message data class */
@Serializable
data class RouteDefinitionMessage(
    val id: String,
    val uptime: Long,
    val status: String,
    val stats: RouteStats?,
    val content: String
)

/** Route statistics & metrics */
@Serializable
data class RouteStats(
    val processed: Long,
    val failed: Long,
    val inflight: Long,
    val mean: Long,
    val min: Long,
    val max: Long
)

/** Camel Route Manager for managing Camel routes and retrieving route statistics */
@Component
class CamelRouteManager(
    /** Camel context */
    private val context: CamelContext,
    /** Camel Routes Generator */
    private val camelRoutesGenerator: CamelRoutesGenerator,
    /** Route configuration */
    private val routeConfig: RouteConfig,
    /** Micrometer Meter registry */
    private val meterRegistry: MeterRegistry
) {
    /** Camel Model object for route interactions */
    private val model: Model = context.getExtension(Model::class.java)

    /** Camel Extended Context for XML (de)serialization */
    private val extendedContext: ExtendedCamelContext =
        context.adapt(ExtendedCamelContext::class.java)

    /** Configure initial routes configured in application properties */
    @PostConstruct
    fun initialRouteSetup() {
        configureRoutes(routeConfig.allRoutes)
    }

    /**
     * Add list of Routes to the Camel context by first converting them to XML and then to
     * RoutesDefinition
     */
    fun configureRoutes(routes: List<Route>) {
        val routeDefinitions = camelRoutesGenerator.generate(routes, extendedContext)
        model.addRouteDefinitions(routeDefinitions)
        LOG.info("Configured routes:\n${
            routeDefinitions.joinToString("\n") {
                extendedContext
                    .modelToXMLDumper
                    .dumpModelAsXml(context, it)
                    .replace("\\r?\\n([ \t]*\\r?\\n)?".toRegex(RegexOption.MULTILINE), "\n")
            }
        }")
    }

    /** Get list of active routes, with statistics */
    fun getRoutes(): List<RouteMessage> {
        return context.routes.map {
            RouteMessage(
                it.id,
                it.uptimeMillis,
                context.routeController.getRouteStatus(it.id)?.name ?: "",
                getRouteStats(it.id))
        }
    }

    /** Get list of active routes, with statistics */
    fun getFullRoutes(): List<RouteDefinitionMessage> {
        return context.routes.mapNotNull {
            getRoute(it.id)
        }
    }

    /** Get detailed route information, with statistics */
    fun getRoute(id: String): RouteDefinitionMessage? {
        return model.getRouteDefinition(id)?.let {
            val xml =
                extendedContext
                    .modelToXMLDumper
                    .dumpModelAsXml(context, it)
                    .replace("\\r?\\n([ \t]*\\r?\\n)?".toRegex(RegexOption.MULTILINE), "\n")

            RouteDefinitionMessage(
                id,
                context.getRoute(id)?.uptimeMillis ?: -1,
                context.routeController.getRouteStatus(id)?.name ?: "unknown",
                getRouteStats(id),
                xml)
        }
    }

    /** Add single Route to the Camel context */
    fun addRoute(route: Route) {
        configureRoutes(listOf(route))
    }

    /** Add route from RouteBuilder object to Camel context */
    fun addRoute(routeBuilder: RouteBuilder) {
        context.addRoutes(routeBuilder)
    }

    /** Remove route from Camel context */
    fun removeRoute(id: String): Boolean {
        return try {
            context.routeController?.stopRoute(id)
            context.removeRoute(id)
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    /**
     * Get route statistics by converting RouteStatDump to RouteStats
     */
    fun getRouteStats(id: String): RouteStats? {
        val processed = meterRegistry.find("CamelExchangesSucceeded").tag("routeId", id).counter()?.count()?.toLong() ?: 0
        val failed = meterRegistry.find("CamelExchangesFailed").tag("routeId", id).counter()?.count()?.toLong() ?: 0
        val total = meterRegistry.find("CamelExchangesTotal").tag("routeId", id).counter()?.count()?.toLong() ?: 0
        val timer = meterRegistry.find("CamelRoutePolicy").tag("routeId", id).tag("failed", "false").timer()
        val mean = timer?.mean(TimeUnit.MILLISECONDS)?.toLong() ?: 0
        val max = timer?.max(TimeUnit.MILLISECONDS)?.toLong() ?: 0
        return RouteStats(
            processed = processed,
            failed = failed,
            inflight = total - processed - failed,
            mean = mean,
            min = 0,
            max = max
        )
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(CamelRouteManager::class.java)
    }
}
