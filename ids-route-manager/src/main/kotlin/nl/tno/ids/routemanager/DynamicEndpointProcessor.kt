/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.routemanager

import nl.tno.ids.configuration.Constants
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

/**
 * Camel Processor for managing parameters for dynamic endpoints focusing on the specific Camel HTTP
 * component parameters.
 */
@Component("dynamicEndpointProcessor")
class DynamicEndpointProcessor : Processor {

    /**
     * Process the incoming message if it does contain a Forward-To header
     *
     * The bridgeEndpoint=true parameter is required for our Camel setup The
     * sslContextParameters=#idsSSLContextParameters parameters is required when using HTTPS forward
     * addresses to make sure that non-standard TLS certificates are accepted if they are in the
     * truststore configuration
     */
    override fun process(exchange: Exchange) {
        if (exchange.getIn().headers.containsKey(Constants.Headers.forwardTo)) {
            var forwardTo = exchange.getIn().getHeader(Constants.Headers.forwardTo, String::class.java)
            forwardTo +=
                (if (forwardTo.contains("?")) "&" else "?") +
                    "bridgeEndpoint=true&copyHeaders=false&throwExceptionOnFailure=false"
            if (forwardTo.startsWith("https://")) {
                forwardTo += "&sslContextParameters=#idsSSLContextParameters"
            }
            LOG.debug("Forwarding to: {}", forwardTo)
            exchange.getIn().removeHeader(Constants.Headers.forwardTo)
            exchange.getIn().setHeader(Constants.Headers.forwardTo, forwardTo)
        }
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(DynamicEndpointProcessor::class.java)
    }
}
