/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.routemanager

import nl.tno.ids.configuration.model.HTTPSIngressRoute
import nl.tno.ids.testextensions.DapsTestExtension
import org.apache.camel.Processor
import org.apache.camel.builder.RouteBuilder
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.HttpClients
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.TestPropertySource
import kotlin.random.Random

/**
 * Test Camel route manager
 */
@SpringBootTest(classes = [CamelRouteManagerTest.Application::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = [
    "spring.config.location = classpath:application.yaml",
    "camel.port=46286"
])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class CamelRouteManagerTest {
    /** Application context with dummy processor beans */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application {
        /** Dummy error handler processor */
        @get:Bean("errorHandlerProcessor")
        val errorHandlerProcessor = Processor { println("Empty processor") }

        /** Dummy multipart input processor */
        @get:Bean("multiPartInputProcessor")
        val multiPartInputProcessor = Processor { println("Empty processor") }

        /** Dummy multipart output processor */
        @get:Bean("multiPartOutputProcessor")
        val multiPartOutputProcessor = Processor { println("Empty processor") }

        /** Dummy DAPS token verifier processor */
        @get:Bean("dapsTokenVerifier")
        val dapsTokenVerifier = Processor { println("Empty processor") }

        /** Dummy DAPS token injector processor */
        @get:Bean("dapsTokenInjector")
        val dapsTokenInjector = Processor { println("Empty processor") }

        /** Dummy clearing processor */
        @get:Bean("clearing") val clearing = Processor { println("Empty processor") }
    }

    /** Camel route manager */
    @Autowired private lateinit var camelRouteManager: CamelRouteManager

    /** Test addition and removal of routes */
    @Test
    @Order(1)
    fun testRoutes() {
        camelRouteManager.addRoute(
            HTTPSIngressRoute().apply {
                id = "test-in"
                port = 32123
                dataApp = "https://dataapp/router"
            })
        Assertions.assertEquals(1, camelRouteManager.getRoutes().count())
        println(camelRouteManager.getFullRoutes())
        camelRouteManager.removeRoute("test-in")
        Assertions.assertEquals(0, camelRouteManager.getRoutes().count())
    }

    /** Test route metrics */
    @Test
    @Order(2)
    fun testMetrics() {
        camelRouteManager.addRoute(
            object : RouteBuilder() {
                override fun configure() {
                    from("jetty:http://0.0.0.0:32124")
                        .process { Thread.sleep(10) }
                        .process {
                            if (Random.nextBoolean()) {
                                it.message.body = "Test"
                            } else {
                                throw Exception("Test exception")
                            }
                        }
                        .process { Thread.sleep (10) }
                }
            })

        for (i in 1..5) {
            HttpClients.createDefault().execute(HttpPost("http://localhost:32124")).use {
                println(it.statusLine.statusCode)
            }
        }

        Assertions.assertEquals(
            5, camelRouteManager.getRoutes()[0].stats?.let { it.processed + it.failed })
    }
}
