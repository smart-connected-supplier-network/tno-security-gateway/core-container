# IDS Route Manager

The IDS Route Manager module manages all of the dynamic Camel routes in the core container.

The routes originate either from the Spring Configuration properties (`ids.routes`), or from the [`ids-workflow-manager` module](../ids-workflow-manager).

The `CamelRoutesGenerator` generates Apache Camel RouteDefinitions from `Route` objects from the configuration model ([`ids-configuration` module](../ids-configuration)).

The `CamelRouteManager` has support for both configuration based routes as the Java-DSL represented routes (in the form of an `RouteBuilder` object)