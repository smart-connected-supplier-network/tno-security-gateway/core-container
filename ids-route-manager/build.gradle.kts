plugins {
    kotlin("plugin.spring")
    kotlin("plugin.serialization")
}

dependencies {
    // Camel Spring Boot integration
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")
    implementation(project(":ids-security"))

    implementation(project(":ids-configuration"))
    implementation(project(":ids-clearing"))

    implementation("org.apache.camel.springboot:camel-micrometer-starter")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("io.micrometer:micrometer-registry-prometheus:1.9.0")

    implementation("org.apache.camel:camel-management")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${LibraryVersions.Kotlin.kotlinxSerialization}")
    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    testImplementation("org.apache.camel.springboot:camel-http-starter")
    implementation("org.apache.camel.springboot:camel-jetty-starter")
    testImplementation(project(":ids-test-extensions"))
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-opt-in=kotlinx.serialization.ExperimentalSerializationApi"
    }
}