package nl.tno.ids.main

import nl.tno.ids.configuration.model.SecurityRoles
import nl.tno.ids.security.ApiKeyConfig
import nl.tno.ids.security.AuthManager
import nl.tno.ids.security.SecurityConfig
import nl.tno.ids.security.UserConfig
import nl.tno.ids.testextensions.DapsTestExtension
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.test.context.TestPropertySource
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

/**
 * Test class validating API security
 */
@SpringBootTest(
    classes = [SecurityTest.Application::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@TestPropertySource(
    properties =
    [
        "spring.config.location = classpath:application-security.yaml",
        "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration",
        "artifacts.location=./resources",
        "security.enabled=true"]
)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class SecurityTest {
    /** Port Spring listens on */
    @LocalServerPort
    private var port: Int = 0

    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Test RestTemplate for sending HTTP requests */
    @Autowired private lateinit var restTemplate: TestRestTemplate
    /** Security configuration */
    @Autowired private lateinit var securityConfig: SecurityConfig
    /** Authentication manager */
    @Autowired private lateinit var authManager: AuthManager

    /** Test request without authentication, should return a 403 HTTP code */
    @Test
    @Order(1)
    fun testNoAuth() {
        restTemplate.getForEntity("http://localhost:$port/api/artifacts/provider", String::class.java).let {
            Assertions.assertEquals(403, it.statusCodeValue, "Request with no token should fail")
        }
    }

    /** Test request with API key, should return a 200 HTTP code */
    @Test
    @Order(2)
    fun testApiKey() {
        restTemplate.exchange(
            "http://localhost:$port/api/artifacts/provider",
            HttpMethod.GET,
            HttpEntity<String>(HttpHeaders().apply {
                setBearerAuth(securityConfig.apiKeys.first().key)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
        }
    }

    /** Test request with user credentials, first requests a token, secondly requests a resource */
    @Test
    @Order(3)
    fun testUser() {
        restTemplate.postForEntity(
            "http://localhost:$port/api/auth/signin",
            HttpEntity<MultiValueMap<String, String>>(LinkedMultiValueMap<String, String>().apply {
                add("username", "admin")
                add("password", "admin")
            }, HttpHeaders().apply {
                contentType = MediaType.MULTIPART_FORM_DATA
            }),
            Map::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
            Assertions.assertTrue(response.body?.containsKey("token") == true)
            adminToken = response.body?.get("token") as String
        }
        restTemplate.exchange(
            "http://localhost:$port/api/artifacts/provider",
            HttpMethod.GET,
            HttpEntity<String>(HttpHeaders().apply {
                setBearerAuth(adminToken)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
        }
    }

    /** Test Authentication Manager API */
    @Test
    @Order(4)
    fun testAuthManagerApi() {
        restTemplate.exchange(
            "http://localhost:$port/api/auth/users",
            HttpMethod.GET,
            HttpEntity<String>(HttpHeaders().apply {
                setBearerAuth(adminToken)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
        }
        restTemplate.exchange(
            "http://localhost:$port/api/auth/apikeys",
            HttpMethod.GET,
            HttpEntity<String>(HttpHeaders().apply {
                setBearerAuth(adminToken)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
        }
        restTemplate.exchange(
            "http://localhost:$port/api/auth/users",
            HttpMethod.POST,
            HttpEntity(UserConfig(
                id = "reader",
                password = BCryptPasswordEncoder().encode("test"),
                roles = setOf(SecurityRoles.READER)
            ), HttpHeaders().apply {
                setBearerAuth(adminToken)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
            Assertions.assertTrue(authManager.getUsers().any { it.id == "reader" })
        }
        restTemplate.exchange(
            "http://localhost:$port/api/auth/users/reader",
            HttpMethod.PUT,
            HttpEntity(UserConfig(
                id = "reader",
                password = null,
                roles = setOf(SecurityRoles.READER, SecurityRoles.ARTIFACT_PROVIDER_MANAGER)
            ), HttpHeaders().apply {
                setBearerAuth(adminToken)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
            Assertions.assertTrue(authManager.getUsers().any { it.id == "reader" && it.roles.contains(SecurityRoles.ARTIFACT_PROVIDER_MANAGER) })
        }
        restTemplate.exchange(
            "http://localhost:$port/api/auth/users/reader",
            HttpMethod.DELETE,
            HttpEntity<String>(HttpHeaders().apply {
                setBearerAuth(adminToken)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
            Assertions.assertTrue(authManager.getUsers().none { it.id == "reader" })
        }

        restTemplate.exchange(
            "http://localhost:$port/api/auth/apikeys",
            HttpMethod.POST,
            HttpEntity(ApiKeyConfig(
                id = "data-app-1",
                key = "APIKEY-DATA_APP_1",
                roles = setOf(SecurityRoles.DATA_APP)
            ), HttpHeaders().apply {
                setBearerAuth(adminToken)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
            Assertions.assertTrue(authManager.getApiKeys().any { it.id == "data-app-1" })
        }
        restTemplate.exchange(
            "http://localhost:$port/api/auth/apikeys/data-app-1",
            HttpMethod.PUT,
            HttpEntity(ApiKeyConfig(
                id = "data-app-1",
                key = "APIKEY-DATA_APP_1",
                roles = setOf(SecurityRoles.DATA_APP, SecurityRoles.WORKFLOW_MANAGER)
            ), HttpHeaders().apply {
                setBearerAuth(adminToken)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
            Assertions.assertTrue(authManager.getApiKeys().any { it.id == "data-app-1" && it.roles.contains(SecurityRoles.WORKFLOW_MANAGER) })
        }
        restTemplate.exchange(
            "http://localhost:$port/api/auth/apikeys/data-app-1",
            HttpMethod.DELETE,
            HttpEntity<String>(HttpHeaders().apply {
                setBearerAuth(adminToken)
            }),
            String::class.java
        ).let { response ->
            Assertions.assertEquals(200, response.statusCodeValue, "Request with API Key should pass")
            Assertions.assertTrue(authManager.getApiKeys().none { it.id == "data-app-1" })
        }
    }

    companion object {
        /** Admin user token */
        private lateinit var adminToken: String
    }
}