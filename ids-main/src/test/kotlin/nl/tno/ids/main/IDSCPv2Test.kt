/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.main

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.http.HttpHeader
import com.github.tomakehurst.wiremock.http.HttpHeaders
import de.fraunhofer.iais.eis.RequestMessageBuilder
import de.fraunhofer.iais.eis.ResultMessageBuilder
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.defaults
import nl.tno.ids.configuration.infomodel.isResponseTo
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.IDSCPEgressRoute
import nl.tno.ids.configuration.model.IDSCPIngressRoute
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.routemanager.CamelRouteManager
import nl.tno.ids.testextensions.DapsTestExtension
import org.apache.camel.test.spring.junit5.CamelSpringBootTest
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.test.context.TestPropertySource
import java.net.URI

@CamelSpringBootTest
@SpringBootTest(classes = [ConnectorMain::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = [
    "spring.config.location = classpath:application-idscp.yaml",
    "camel.port=46285"
])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class IDSCPv2Test {
    @Autowired private lateinit var restTemplate: TestRestTemplate

    @Autowired private lateinit var routeManager: CamelRouteManager

    @Test
    @Order(0)
    fun testIDSCP() {
        Thread.sleep(2000)
        routeManager.addRoute(IDSCPIngressRoute().apply {
            id = "idscp-ingress"
            port = serverPort
            dataApp = "http://localhost:$wireMockPort/dataapp"
            tlsClientHostnameVerification = true
        })
        routeManager.addRoute(IDSCPEgressRoute().apply {
            id = "idscp-egress"
            listenPort = clientListenPort
        })

        var i = 0
        while (routeManager.getRoute("idscp-ingress")?.status != "Started" || routeManager.getRoute("idscp-egress")?.status != "Started") {
            Thread.sleep(500)
            if (i == 240) {
                throw Exception("IDSCP Camel routes not started within 120 seconds")
            }
            i++
        }

        val requestMessage = RequestMessageBuilder()
            ._issuerConnector_(URI("urn:scsn:ids:connectors:SCSN-Integration-Test"))
            ._senderAgent_(URI("urn:scsn:ids:participants:TNO"))
            ._recipientConnector_(URI("urn:scsn:ids:connectors:SCSN-Integration-Test"))
            ._recipientAgent_(URI("urn:scsn:ids:participants:TNO"))
            .build()
            .defaults()

        val resultMessage = ResultMessageBuilder()
            .build()
            .isResponseTo(requestMessage)

        val resultHttpMessage = resultMessage.toMultiPartMessage()
        wireMock.stubFor(
            WireMock.post("/dataapp")
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(200)
                        .withBody(
                            resultHttpMessage.toString()
                        )
                        .withHeaders(HttpHeaders(resultHttpMessage.httpHeaders.map { HttpHeader(it.key, it.value) }))
                )
        )

        val response = restTemplate.postForEntity(
            "http://localhost:$clientListenPort/idscp_out",
            HttpEntity(
                requestMessage.toMultiPartMessage(),
                org.springframework.http.HttpHeaders().apply { set(Constants.Headers.forwardTo, "localhost:$serverPort") }),
            MultiPartMessage::class.java)

        Assertions.assertEquals(200, response.statusCodeValue)
        Assertions.assertEquals(resultMessage.id, response.body?.header?.id)
    }

    companion object {
        private const val wireMockPort = 46237
        private const val serverPort = 40181
        private const val clientListenPort = 34638
        private lateinit var wireMock: WireMockServer

        @JvmStatic
        @BeforeAll
        fun setupWireMockDataApp() {
            wireMock = WireMockServer(wireMockPort).apply { start() }
        }
    }
}
