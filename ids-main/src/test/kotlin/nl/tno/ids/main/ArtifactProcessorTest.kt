/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.main

import de.fraunhofer.iais.eis.*
import nl.tno.ids.artifacts.ArtifactConfiguration
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.testextensions.DapsTestExtension
import nl.tno.ids.tokenmanager.TokenManagerService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.ComponentScan
import org.springframework.core.io.ByteArrayResource
import org.springframework.test.context.TestPropertySource
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.util.UriUtils
import java.io.File
import java.net.URI
import java.nio.charset.Charset
import java.util.*

/**
 * Artifact Processor Test class for testing artifacts combined with policy enforcement
 */
@SpringBootTest(
    classes = [ArtifactProcessorTest.Application::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
    properties =
        [
            "spring.config.location = classpath:application-artifact.yaml",
            "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration",
            "artifacts.location=./resources",
            "camel.port=46284"
        ])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class ArtifactProcessorTest {
    /** Port Spring listens on */
    @LocalServerPort private var port: Int = 0

    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Test RestTemplate for sending HTTP requests */
    @Autowired private lateinit var restTemplate: TestRestTemplate
    /** Artifact configuration */
    @Autowired private lateinit var artifactConfiguration: ArtifactConfiguration
    /** Token manager service for requesting tokens */
    @Autowired private lateinit var tokenManagerService: TokenManagerService

    /** Add a resource */
    @Test
    @Order(0)
    fun addResource() {
        dataResource =
            restTemplate.postForObject(
                "http://localhost:$port/api/artifacts/provider",
                LinkedMultiValueMap<String, Any>().apply {
                    add(
                        "artifact",
                        FileNameAwareByteArrayResource(
                            "testartifact.pdf", "Test Artifact".toByteArray()))
                    add("title", "Test Artifact")
                    add("description", "Unit Test Artifact")
                    add(
                        "contractOffer",
                        ContractOfferBuilder()
                            ._permission_(PermissionBuilder()._action_(Action.READ).build())
                            .build())
                },
                DataResource::class.java)
        Assertions.assertEquals("Test Artifact", dataResource.title[0].value)
        Assertions.assertEquals(
            13, (dataResource.representation[0].instance[0] as Artifact).byteSize.toInt())
    }

    /** Start contract negotiation process with a ContractRequestMessage */
    @Test
    @Order(1)
    fun contractRequest() {
        val contractRequest =
            ContractRequestBuilder()
                ._consumer_(URI("urn:scsn:ids:participants:TNO"))
                ._provider_(URI("urn:scsn:ids:participants:TNO"))
                ._permission_(
                    PermissionBuilder()
                        ._target_(dataResource.representation.first().instance.first().id)
                        ._action_(Action.READ)
                        .build())
                .build()
        val contractRequestMessage =
            ContractRequestMessageBuilder()
                ._issuerConnector_(URI("urn:scsn:ids:connectors:SCSN-Integration-Test"))
                ._recipientConnector_(URI("urn:scsn:ids:connectors:SCSN-Integration-Test"))
                ._senderAgent_(URI("urn:scsn:ids:participants:TNO"))
                ._recipientAgent_(URI("urn:scsn:ids:participants:TNO"))
                .buildWithDefaults(tokenManagerService.acquireToken(""))
                .toMultiPartMessage(
                    payload = contractRequest.toJsonLD(), contentType = "application/ld+json")

        val contractResponse =
            restTemplate.postForObject(
                "http://localhost:46284/router/artifacts/${
            UriUtils.encode(dataResource.id.toString(), Charset.defaultCharset())
        }",
                contractRequestMessage,
                MultiPartMessage::class.java)

        Assertions.assertTrue(contractResponse.header is ContractAgreementMessage)
        contractAgreementMessage = contractResponse.header
        contractAgreement = Serialization.fromJsonLD(contractResponse.payload?.asString())
    }

    /** Finalize contract negotiation process with a ContractAgreementMessage */
    @Test
    @Order(2)
    fun contractAgreement() {
        val contractAgreementMessage =
            ContractAgreementMessageBuilder()
                ._correlationMessage_(contractAgreementMessage.id)
                ._issuerConnector_(URI("urn:scsn:ids:connectors:SCSN-Integration-Test"))
                ._recipientConnector_(URI("urn:scsn:ids:connectors:SCSN-Integration-Test"))
                ._senderAgent_(URI("urn:scsn:ids:participants:TNO"))
                ._recipientAgent_(URI("urn:scsn:ids:participants:TNO"))
                .buildWithDefaults(tokenManagerService.acquireToken(""))
                .toMultiPartMessage(
                    payload = contractAgreement.toJsonLD(), contentType = "application/ld+json")

        val agreementResponse =
            restTemplate.postForEntity(
                "http://localhost:46284/router/artifacts/${
            UriUtils.encode(dataResource.id.toString(), Charset.defaultCharset())
        }",
                contractAgreementMessage,
                MultiPartMessage::class.java)

        Assertions.assertTrue(agreementResponse.statusCodeValue in 200..299)
    }

    /** Retrieve the artifact based on the previously agreed contract */
    @Test
    @Order(3)
    fun retrieveArtifact() {
        val multiPartMessage =
            ArtifactRequestMessageBuilder()
                ._requestedArtifact_(dataResource.representation.first().instance.first().id)
                ._transferContract_(contractAgreement.id)
                ._issuerConnector_(URI("urn:scsn:ids:connectors:SCSN-Integration-Test"))
                ._recipientConnector_(URI("urn:scsn:ids:connectors:SCSN-Integration-Test"))
                ._senderAgent_(URI("urn:scsn:ids:participants:TNO"))
                ._recipientAgent_(URI("urn:scsn:ids:participants:TNO"))
                .buildWithDefaults(tokenManagerService.acquireToken(""))
                .toMultiPartMessage()

        val response =
            restTemplate.postForObject(
                "http://localhost:46284/router/artifacts/${
            UriUtils.encode(dataResource.id.toString(), Charset.defaultCharset())
        }",
                multiPartMessage,
                MultiPartMessage::class.java)
        Assertions.assertEquals(
            "Test Artifact",
            String(Base64.getDecoder().decode(response.payload?.asString()?.trim())))
        Assertions.assertEquals(
            "application/pdf", response.payload?.httpHeaders?.get("Content-Type"))
    }

    /** Clean-up resources */
    @Test
    @Order(Int.MAX_VALUE)
    fun cleanUpResources() {
        File(artifactConfiguration.location).deleteRecursively()
    }

    companion object {
        /** Data Resource state that is maintained over test methods */
        private lateinit var dataResource: DataResource
        /** ContractAgreementMessage state that is maintained over test methods */
        private lateinit var contractAgreementMessage: Message
        /** ContractAgreement state that is maintained over test methods */
        private lateinit var contractAgreement: ContractAgreement
    }
}

/**
 * Helper class for ByteArray resources with configured filename
 */
class FileNameAwareByteArrayResource(
    private val fileName: String,
    byteArray: ByteArray,
    description: String? = null
) : ByteArrayResource(byteArray, description) {
    override fun getFilename(): String {
        return fileName
    }
}
