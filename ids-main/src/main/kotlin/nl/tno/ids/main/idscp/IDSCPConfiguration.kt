/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.main.idscp

import de.fhg.aisec.ids.camel.idscp2.Utils
import nl.tno.ids.configuration.keystore.KeystoreGenerator
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.DapsConfig
import nl.tno.ids.configuration.model.KeystoreConfig
import nl.tno.ids.configuration.model.TruststoreConfig
import org.apache.camel.support.jsse.KeyManagersParameters
import org.apache.camel.support.jsse.KeyStoreParameters
import org.apache.camel.support.jsse.SSLContextParameters
import org.apache.camel.support.jsse.TrustManagersParameters
import org.slf4j.LoggerFactory
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.FileOutputStream
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.security.KeyStore
import java.util.*
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

/** PKCS#12 configuration data class for temporary (generated) PKCS#12 store */
data class Pkcs12Configuration(val resource: String, val password: String)

/**
 * IDSCPv2 Configuration class defining beans for use with IDSCP as well as a PKCS#8 to PKCS#12
 * conversion
 *
 * Will only be configured when the property idscp.enabled is set to true
 */
@Configuration
@ConditionalOnProperty(name = ["idscp.enabled"], havingValue = "true")
class IDSCPConfiguration(
    private val connectorInfo: ConnectorInfo,
    private val dapsConfig: DapsConfig,
    private val keystoreConfig: KeystoreConfig,
    private val truststoreConfig: TruststoreConfig,
    private val keystoreGenerator: KeystoreGenerator
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(IDSCPConfiguration::class.java)
    }

    /** Password character pool for random password generation */
    private val charPool: List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')

    /** Generate random alphanumeric string for use in temporary PCKS#12 keystore */
    private fun randomPassword(n: Int = 20) =
        (1..n)
            .map { kotlin.random.Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("")

    /** SSL Context Parameters that will be used for the TLS encryption on the transport level */
    @Bean("transportSslContextParameters")
    fun transportSslContextParameters(
        pkcs12Configuration: Pkcs12Configuration
    ): SSLContextParameters {
        val keyStoreParameters =
            KeyStoreParameters().apply {
                resource = pkcs12Configuration.resource
                password = pkcs12Configuration.password
            }
        return SSLContextParameters().apply {
            keyManagers =
                KeyManagersParameters().apply {
                    keyStore = keyStoreParameters
                    keyPassword = pkcs12Configuration.password
                }
            trustManagers = TrustManagersParameters().apply { keyStore = keyStoreParameters }
        }
    }

    /**
     * SSL Context Parameters that will be used for creating client assertions for communication
     * with the DAPS
     */
    @Bean("dapsSslContextParameters")
    fun dapsSslContextParameters(pkcs12Configuration: Pkcs12Configuration): SSLContextParameters {
        val keyStoreParameters =
            KeyStoreParameters().apply {
                resource = pkcs12Configuration.resource
                password = pkcs12Configuration.password
            }
        return SSLContextParameters().apply {
            keyManagers =
                KeyManagersParameters().apply {
                    keyStore = keyStoreParameters
                    keyPassword = pkcs12Configuration.password
                }
            trustManagers = TrustManagersParameters().apply { keyStore = keyStoreParameters }
        }
    }

    /**
     * Generate PKCS#12 keystore from a PEM certificate and PKCS#8 private key.
     *
     * Stores the store on the temp (OS) file directory.
     *
     * @return Pkcs12Configuration with the location and the password of the PKCS#12 store
     */
    @Bean
    fun generatePkcs12Keystore(): Pkcs12Configuration {
        return keystoreConfig.pem?.let { pem ->
            val certChain =
                truststoreConfig.pem?.let {
                    listOf(pem.cert) + it.chain
                } ?: listOf(pem.cert)
            val password = randomPassword()
            val keystore = keystoreGenerator.createKeystore(certChain, pem.key, "PKCS12", "1", password)
            certChain.forEach { certificate ->
                keystore.setCertificateEntry(certificate.subjectX500Principal.name, certificate)
            }
            addDefaultRootCaCertificates(keystore)
            val tempFile = Files.createTempFile(null, ".p12")
            keystore.store(FileOutputStream(tempFile.toFile()), password.toCharArray())
            LOG.info("Created temporary PCKS#12 keystore: $tempFile")
            Pkcs12Configuration(tempFile.toString(), password)
        }
            ?: throw Exception("Bad configuration: PEM keystore configuration required for IDSCP")
    }

    /** Add the default/system trusted certificates to the custom keystore */
    private fun addDefaultRootCaCertificates(keyStore: KeyStore) {
        val trustManagerFactory =
            TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(null as KeyStore?)
        trustManagerFactory.trustManagers.filterIsInstance<X509TrustManager>().forEach { tm ->
            tm.acceptedIssuers.forEach { acceptedIssuer ->
                keyStore.setCertificateEntry(acceptedIssuer.subjectX500Principal.name, acceptedIssuer)
            }
        }
    }

    /** Configure IDSCPv2 specific library static values */
    @Bean
    fun configureIdscp2(): CommandLineRunner {
        return CommandLineRunner {
            Utils.connectorUrlProducer = { connectorInfo.idsid }
            Utils.maintainerUrlProducer = { connectorInfo.maintainer }
            Utils.dapsUrlProducer = { dapsConfig.url.substringBefore("/v2") }
            Utils.infomodelVersion = "4.2.7"
        }
    }

    /** Base64 encoding helper function */
    private fun base64Decode(base64: String): String {
        return String(
            Base64.getMimeDecoder().decode(base64.toByteArray(StandardCharsets.US_ASCII)),
            StandardCharsets.US_ASCII)
    }

    /** Base64 encoding helper function */
    private fun base64Encode(string: String): String {
        return Base64.getEncoder()
            .withoutPadding()
            .encodeToString(string.toByteArray(StandardCharsets.US_ASCII))
    }
}
