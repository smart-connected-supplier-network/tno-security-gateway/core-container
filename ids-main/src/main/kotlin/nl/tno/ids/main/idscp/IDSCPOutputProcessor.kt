package nl.tno.ids.main.idscp

import de.fhg.aisec.ids.camel.idscp2.Constants
import de.fraunhofer.iais.eis.Message
import nl.tno.ids.configuration.serialization.Serialization
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.springframework.stereotype.Component

/**
 * IDSCP Output processor
 */
@Component("IDSCPOutputProcessor")
class IDSCPOutputProcessor : Processor {
    /** Process incoming message and move header from Constants.Headers.idsHeader to IDSCP2_HEADER */
    override fun process(exchange: Exchange) {
        if (exchange.message.headers.containsKey(nl.tno.ids.configuration.Constants.Headers.idsHeader)) {
            val header = exchange.message.getHeader(nl.tno.ids.configuration.Constants.Headers.idsHeader, String::class.java)

            exchange.message.setHeader(Constants.IDSCP2_HEADER, Serialization.fromJsonLD<Message>(header))
            exchange.message.removeHeader(nl.tno.ids.configuration.Constants.Headers.idsHeader)
        } else {
            throw IDSCPException("Message does not contain parsed IDS message")
        }
    }
}