/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.main.sslcontextparameters

import nl.tno.ids.configuration.keystore.KeystoreGenerator
import nl.tno.ids.configuration.model.KeystoreConfig
import nl.tno.ids.configuration.model.TruststoreConfig
import nl.tno.ids.configuration.model.TruststoreType
import nl.tno.ids.configuration.truststore.AcceptAllX509TrustManager
import nl.tno.ids.configuration.truststore.MergedX509TrustManager
import nl.tno.ids.security.SecurityConfig
import org.apache.camel.support.jsse.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.security.KeyStore
import java.security.KeyStoreException
import java.security.NoSuchAlgorithmException
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

/** Apache Camel SSL Context Parameters Configuration for trust & keystores */
@Configuration
class SSLContextParametersConfiguration(
    private val keystoreGenerator: KeystoreGenerator,
    private val acceptAllX509TrustManager: AcceptAllX509TrustManager
) {

    /** SSLContextParameters Bean that can be used in Camel routes */
    @Bean("idsSSLContextParameters")
    fun generate(
        keystoreConfig: KeystoreConfig,
        truststoreConfig: TruststoreConfig,
        securityConfig: SecurityConfig
    ): SSLContextParameters { // Setup keystore manager
        return SSLContextParameters().apply {
            keyManagers = keyManagersParameters(keystoreConfig)
            trustManagers = trustManagersParameters(truststoreConfig)
            secureSocketProtocols = SecureSocketProtocolsParameters().apply {
                secureSocketProtocol = securityConfig.tlsVersions
            }
            if (securityConfig.cipherSuites.isNotEmpty()) {
                cipherSuites = CipherSuitesParameters().apply {
                    cipherSuites.cipherSuite = securityConfig.cipherSuites
                }
            }
        }
    }

    /** KeystoreConfig translation to KeyManagersParameters */
    private fun keyManagersParameters(keystoreConfig: KeystoreConfig): KeyManagersParameters {
        val keyManagersParameters = KeyManagersParameters()
        when (keystoreConfig.type) {
            KeystoreConfig.Type.PEM -> {
                keystoreConfig.pem?.let { pem ->
                    val keyStore = keystoreGenerator.createKeystore(listOf(pem.cert), pem.key)
                    val keyStoreParameters: KeyStoreParameters =
                        object : KeyStoreParameters() {
                            override fun createKeyStore(): KeyStore {
                                return keyStore
                            }
                        }
                    keyManagersParameters.keyPassword = ""
                    keyManagersParameters.keyStore = keyStoreParameters
                }
            }
        }
        return keyManagersParameters
    }

    /** TruststoreConfig translation to TrustManagersParameters */
    private fun trustManagersParameters(truststoreConfig: TruststoreConfig): TrustManagersParameters {
        val trustManagersParameters = TrustManagersParameters()
        when (truststoreConfig.type) {
            TruststoreType.PEM -> {
                truststoreConfig.pem?.let { pem ->
                    val trustStore = keystoreGenerator.createTruststore(pem.chain)
                    trustManagersParameters.trustManager = createTrustManager(trustStore)
                }
            }
            TruststoreType.ACCEPT_ALL ->
                trustManagersParameters.trustManager = acceptAllX509TrustManager
            else -> {}
        }
        return trustManagersParameters
    }

    /**
     * TrustManager builder from default/system truststore and additional trusted CA certificates
     */
    private fun createTrustManager(keystore: KeyStore): TrustManager? {
        try {
            var tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            // Using null here initialises the TMF with the default trust store.
            tmf.init(null as KeyStore?)
            // Get hold of the default trust manager
            val defaultTm = tmf.trustManagers.filterIsInstance<X509TrustManager>().first()
            tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
            tmf.init(keystore)
            // Get hold of the default trust manager
            val additionalTm = tmf.trustManagers.filterIsInstance<X509TrustManager>().first()
            // Wrap it in your own class.
            return MergedX509TrustManager(defaultTm, additionalTm)
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: KeyStoreException) {
            e.printStackTrace()
        }
        return null
    }
}
