package nl.tno.ids.main.healthcheck

import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.RouteConfig
import nl.tno.ids.security.SecurityConfig
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Config validation class that is executed at the startup of the connector
 */
@Configuration
class ConfigValidation(
    private val securityConfig: SecurityConfig,
    private val routeConfig: RouteConfig,
    private val connectorInfo: ConnectorInfo
) {

    /**
     * Command Line Runner bean that checks the security httpsOnly flag in combination with the configuration for the endpoints.
     * Throws a runtime exception in case an endpoint is configured with http, which ensures the
     * connector won't start.
     */
    @Bean("Configvalidate")
    fun validate(): CommandLineRunner {
        return CommandLineRunner {
            if (System.getenv("CI") != null) {
                val allExternalAccessUrlsHttps = connectorInfo.accessUrl.all { it.protocol == "https" }
                val isHttpsOnly = securityConfig.httpsOnly
                if (isHttpsOnly && routeConfig.https && allExternalAccessUrlsHttps) {
                    throw RuntimeException("Trying to start the container with HTTPS only, while not all endpoints are HTTPS.")
                }
            }
        }
    }
}