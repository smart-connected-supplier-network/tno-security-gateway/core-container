/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.main

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import java.security.Security

/** Primary Spring Boot Application */
@SpringBootApplication(exclude = [MongoAutoConfiguration::class, UserDetailsServiceAutoConfiguration::class])
@ConfigurationPropertiesScan("nl.tno.ids")
@ComponentScan("nl.tno.ids")
class ConnectorMain

/**
 * Runnable main function, including setting the Tomcat property to allow encoded slashes in paths
 * for URI based path parameters as well as force certificate revocation (CRL & OCSP).
 */
fun main(args: Array<String>) {
    System.setProperty("org.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH", "true")
    System.setProperty("com.sun.net.ssl.checkRevocation", "true")
    System.setProperty("com.sun.security.enableCRLDP", "true")
    Security.setProperty("ocsp.enable", if (System.getenv().containsKey("ENABLE_OCSP")) System.getenv()["ENABLE_OCSP"] else "true")

    runApplication<ConnectorMain>(*args)
}
