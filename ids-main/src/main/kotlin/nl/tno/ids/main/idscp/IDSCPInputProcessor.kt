package nl.tno.ids.main.idscp

import de.fhg.aisec.ids.camel.idscp2.Constants
import de.fraunhofer.iais.eis.Message
import nl.tno.ids.configuration.infomodel.toJsonLD
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.springframework.stereotype.Component

/**
 * IDSCP Input processor
 */
@Component("IDSCPInputProcessor")
class IDSCPInputProcessor : Processor {
    /** Process incoming message and move header from IDSCP2_HEADER to Constants.Headers.idsHeader */
    override fun process(exchange: Exchange) {
        if (exchange.message.headers.containsKey(Constants.IDSCP2_HEADER)) {
            val header = exchange.message.getHeader(Constants.IDSCP2_HEADER, Message::class.java)
            exchange.message.setHeader(nl.tno.ids.configuration.Constants.Headers.idsHeader, header.toJsonLD())
            exchange.message.removeHeader(Constants.IDSCP2_HEADER)
        } else {
            throw IDSCPException("Message received over IDSCP does not contain IDS message header")
        }
    }
}