package nl.tno.ids.main.timevalidation

import org.apache.commons.net.ntp.NTPUDPClient
import org.apache.commons.net.ntp.TimeStamp
import org.springframework.boot.CommandLineRunner
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.Retryable
import java.net.InetAddress
import kotlin.math.abs

/**
 * NTP Offset validation class that is executed at the startup of the connector
 */
@Configuration
class NTPOffsetValidation {

    /**
     * Command Line Runner bean that detects the difference in System time and NTP time
     * Throws a runtime exception in case the difference is too large, which ensures the
     * connector won't start.
     */
    @Bean("NTPOffsetValidate")
    @Retryable(value = [java.lang.Exception::class], maxAttempts = 5, backoff = Backoff(delay = 100))
    fun validate(): CommandLineRunner {
        return CommandLineRunner {
            if (System.getenv("CI") != null) {
                val client = NTPUDPClient()
                val inetAddress = InetAddress.getByName("pool.ntp.org")
                val timeInfo = client.getTime(inetAddress)
                timeInfo.computeDetails()
                val systemNtpTime = TimeStamp.getCurrentTime()
                val currentTime = System.currentTimeMillis()
                val atomicNtpTime = TimeStamp.getNtpTime(currentTime + (timeInfo.offset ?: 0))
                println("System time: $systemNtpTime ${systemNtpTime.toDateString()} ${systemNtpTime.time}")
                println("Atomic time: $atomicNtpTime ${atomicNtpTime.toDateString()} ${atomicNtpTime.time}")
                val difference = abs(systemNtpTime.time - atomicNtpTime.time)
                if (difference > 60000) {
                    throw RuntimeException("NTP Time difference of $difference milliseconds, please verify the System clock")
                }
            }
        }
    }
}