/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.main.errorhandling

import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.RejectionMessage
import de.fraunhofer.iais.eis.RejectionMessageBuilder
import de.fraunhofer.iais.eis.RejectionReason
import nl.tno.ids.camel.daps.TokenVerificationException
import nl.tno.ids.camel.multipart.MultiPartInputProcessor
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.configuration.multipart.MultiPartParseException
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.tokenmanager.TokenManagerService
import org.apache.camel.CamelExecutionException
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.net.URI

/** Error context for extracting relevant information from an exchange */
data class ErrorContext(
    var issuerConnector: URI,
    var senderAgent: URI,
    var correlationId: URI,
    var dynamicAttributeToken: String
)

/**
 * Camel error handling processor that will be called in case of an exception during Camel routes
 */
@Component("errorHandlerProcessor")
class ErrorHandlerProcessor(
    val connectorInfo: ConnectorInfo,
    val tokenManagerService: TokenManagerService,
    val multiPartInputProcessor: MultiPartInputProcessor
) : Processor {

    /**
     * Parse incoming message, determine ErrorContext and provide either an IDS-based or plain error
     * response
     */
    override fun process(exchange: Exchange) {
        val caused = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable::class.java)
        val routeId = exchange.getProperty(Exchange.FAILURE_ROUTE_ID, String::class.java)
        when (caused) {
            is CamelExecutionException -> {
                if (caused.cause != null) {
                    LOG.warn(
                        "Error in route $routeId: {}: {}",
                        caused.cause!!.javaClass.name,
                        caused.cause!!.message)
                } else {
                    LOG.warn(
                        "Error in route $routeId: {}: {}", caused.javaClass.name, caused.message)
                }
            }
            else ->
                LOG.warn("Error in route $routeId: {}: {}", caused.javaClass.name, caused.message)
        }

        LOG.debug("Exception in Camel Route", caused)
        try {
            multiPartInputProcessor.process(exchange)
        } catch (e: Exception) {
            plainError(exchange, caused)
            return
        }
        val errorContext = parseErrorContext(exchange)
        try {
            val header = rejectionMessageBuilder(errorContext, caused)
            val multiPartMessage =
                MultiPartMessage(
                    header,
                    JSONObject()
                        .put("status", "Error")
                        .put("type", caused.javaClass.simpleName)
                        .put("origin", "Core Container")
                        .put("reason", caused.message)
                        .toString(),
                    contentType = "application/json")
            exchange.message.body = multiPartMessage.toString()
            exchange.message.headers.clear()
            exchange.message.headers["Content-Type"] = multiPartMessage.httpHeaders["Content-Type"]
        } catch (e: Exception) {
            plainError(exchange, caused)
            return
        }
    }

    /**
     * Parse the incoming IDS-Header and retrieve relevant information to be used in the error
     * handling
     */
    private fun parseErrorContext(exchange: Exchange): ErrorContext {
        return try {
            val idsHeader: String = exchange.getIn().getHeader(Constants.Headers.idsHeader, String::class.java)
            val header = Serialization.fromJsonLD<Message>(idsHeader)
            ErrorContext(
                header.issuerConnector,
                header.senderAgent,
                header.id,
                tokenManagerService.acquireToken(header.issuerConnector.toString()))
        } catch (e: Exception) {
            LOG.warn("Error in parsing incoming header")
            ErrorContext(URI("urn:ids:unknown"), URI("urn:ids:unknown"), URI("urn:ids:unknown"), "")
        }
    }

    /** Create a IDS rejection message corresponding to the context and the exception */
    private fun rejectionMessageBuilder(
        errorContext: ErrorContext,
        caused: Throwable?,
    ): RejectionMessage {
        return RejectionMessageBuilder()
            ._issuerConnector_(connectorInfo.idsid)
            ._recipientConnector_(arrayListOf(errorContext.issuerConnector))
            ._correlationMessage_(errorContext.correlationId)
            ._rejectionReason_(
                when (caused) {
                    is MultiPartParseException -> RejectionReason.MALFORMED_MESSAGE
                    is TokenVerificationException -> RejectionReason.NOT_AUTHENTICATED
                    else -> RejectionReason.INTERNAL_RECIPIENT_ERROR
                })
            ._senderAgent_(connectorInfo.maintainer)
            ._recipientAgent_(arrayListOf(errorContext.senderAgent))
            .buildWithDefaults(errorContext.dynamicAttributeToken)
    }

    /** Send a plain JSON response in case the caller did not provide an IDS request */
    fun plainError(exchange: Exchange, caused: Throwable) {
        exchange.message.body =
            JSONObject()
                .put("status", "Error")
                .put("type", caused.javaClass.simpleName)
                .put("origin", "Core Container")
                .put("reason", caused.message)
                .toString()
        exchange.message.headers.clear()
        exchange.message.headers[Exchange.HTTP_RESPONSE_CODE] =
            when (caused) {
                is MultiPartParseException -> 400
                is TokenVerificationException -> 401
                else -> 500
            }
        exchange.message.headers["Content-Type"] = "application/json"
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(ErrorHandlerProcessor::class.java)
    }
}
