package nl.tno.ids.main.idscp

class IDSCPException: Exception {
    constructor(message: String) : super(message)
    constructor(message: String, caused: Throwable) : super(message, caused)
}
