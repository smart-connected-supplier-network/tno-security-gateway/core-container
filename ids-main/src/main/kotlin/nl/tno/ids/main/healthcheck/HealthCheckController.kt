/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.main.healthcheck

import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.buildJsonObject
import kotlinx.serialization.json.put
import nl.tno.ids.artifacts.ArtifactConfiguration
import nl.tno.ids.configuration.model.*
import nl.tno.ids.security.SecurityConfig
import nl.tno.ids.selfdescription.BrokerRegistrationService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

/**
 * Health Check Controller, for indicating the health of this application to orchestrators (e.g.
 * Kubernetes)
 */
@RestController
@RequestMapping("health")
class HealthCheckController(private val brokerRegistrationService: BrokerRegistrationService,
                            private val securityConfig: SecurityConfig,
                            private val connectorInfo: ConnectorInfo,
                            private val routeConfig: RouteConfig,
                            private val truststoreConfig: TruststoreConfig,
                            private val artifactConfiguration: ArtifactConfiguration
) {

    /**
     * Simple health endpoint that returns 200 when this controller is loaded and the broker
     * registration was successful iff the autoRegistration configuration is enabled
     */
    @GetMapping
    fun health(): ResponseEntity<JsonObject> {
        val allExternalAccessUrlsHttps = connectorInfo.accessUrl.all { it.protocol == "https" }
        val json = buildJsonObject {
            put("description", "This page returns a few statuses to check the security of the configuration " +
                    "of the deployment. For maximum security, all booleans should be set to true.")
            put("routeConfiguration", buildJsonObject {
                // Connector on TLS
                put("routesUsingTLS", routeConfig.https)
                // No custom camel routes
                put("noCustomRoutes", routeConfig.allRoutes.none { it is CustomRoute})
                // Daps check for routes
                put("camelRoutesViaDapsVerify", routeConfig.ingress.http.all { it.dapsVerify })
                // IDSCP with hostname verification
                put("CamelRoutesIDSCPIngressWithHostnameVerification", routeConfig.ingress.idscp.all { it.tlsClientHostnameVerification})
            })
            put("Artifacts", buildJsonObject {
                // Artifact encryption when saved to disk
                put("encryptionAtRest", artifactConfiguration.encryptAtRest)
                // Secure key configured for encryption
                put("secureAesKey", artifactConfiguration.aesEncryptionKey !== "7syl0HCT+Bz8qTEssWupjyjgC9JGFEyxHwS4nNrl1Gw=")
            })
            // External access URL https
            put("allExternalAccessUrlsHttps", allExternalAccessUrlsHttps)
            // Security configuration with users
            put("securityEnabled", securityConfig.enabled)
            // Truststore not on ACCEPT_ALL
            put("LimitedTrustStore", truststoreConfig.type !== TruststoreType.ACCEPT_ALL)
            // Enable OCSP
            put("OCSP", if (System.getenv().containsKey("ENABLE_OCSP")) System.getenv()["ENABLE_OCSP"].toBoolean() else true)
            // API keys should have a minimal length of 32 characters.
            put("APIKeysHaveMinimumOf32Chars", securityConfig.apiKeys.all { it.key.length >= 32})
            // TLS version should be 1.3.
            put("TLSVersion1.3Only", securityConfig.tlsVersions.none { it in listOf("TLSv1.0", "TLSv1.1", "TLSv1.2")})
            // Optionally, enable more security measures.
            put("Optional", buildJsonObject {
                // Enforce policies on all camel routes.
                put("policyEnforcementOnCamelRoutes", routeConfig.ingress.http.all{ it.policyEnforcement })
            })
        }

        return if (brokerRegistrationService.registered) {
            ResponseEntity.ok(json)
        } else {
            ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(json)
        }
    }
}
