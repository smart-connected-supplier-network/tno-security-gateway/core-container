# IDS Main

This module collects all of the submodules and provides the main class for execution.

This module also contains some configuration and beans that require other modules to work:
- `errorhandling`: Apache Camel processor that will be used when an exception is thrown in one of the exchange routes.
- `healthcheck`: Health check endpoint for orchestration (e.g. Kubernetes) purposes
- `idscp`: IDSCPv2 configuration, used to configure `de.fhg.aisec.ids:camel-idscp2`
- `sslcontextparameters`: `SSLContextParameters` provider for usage in apache Camel routes, based on the Truststore configuration

# Google Container Tools Jib Docker builds

This module contains the configuration for the Docker image build process by using the [Jib](https://github.com/GoogleContainerTools/jib) library. This is located in the `build.gradle.kts` file.

For local builds, the `jibDockerBuild` task can be used to build the image using Docker on the same machine (e.g. `./gradlew jibDockerBuild` (in the root of the repository)). For other builds the `jib` task can be used, that automatically uploads the Docker image to the registry (requires `REGISTRY_USERNAME` & `REGISTRY_PASSWORD` environment variables, or being logged in to the registry via the Docker CLI).