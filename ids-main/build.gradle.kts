plugins {
    application
    id("org.springframework.boot")
    id("com.google.cloud.tools.jib") version LibraryVersions.jib
    kotlin("plugin.spring")
    jacoco
    kotlin("plugin.serialization")
}

dependencies {
    // Camel Spring Boot integration
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")

    // Spring Boot
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-security")

    // Camel components
    implementation("org.apache.camel.springboot:camel-rest-starter")
    implementation("org.apache.camel.springboot:camel-http-starter")
    implementation("org.apache.camel.springboot:camel-jetty-starter")
    implementation("org.apache.camel.springboot:camel-zipkin-starter")

    implementation("org.apache.camel:camel-metrics")

    implementation("org.springframework.retry:spring-retry:${LibraryVersions.Spring.retry}")

    // Sub Projects
    api(project(":ids-configuration"))

    api(project(":camel-daps-processor"))
    api(project(":camel-multipart-processor"))

    api(project(":ids-artifact-manager"))
    api(project(":ids-orchestration-manager"))
    api(project(":ids-resource-manager"))
    api(project(":ids-route-manager"))
    api(project(":ids-security"))
    api(project(":ids-selfdescription"))
    api(project(":ids-token-manager"))
    api(project(":ids-workflow-manager"))

    implementation("commons-net:commons-net:${LibraryVersions.commonsNet}")

    // IDSCP v2
    implementation("de.fhg.aisec.ids:camel-idscp2:${LibraryVersions.IDS.idscp2}") {
        implementation("com.google.guava:guava:${LibraryVersions.Test.guava}")
    }

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${LibraryVersions.Kotlin.kotlinxSerialization}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.apache.camel:camel-test-spring-junit5")
    testImplementation("com.github.tomakehurst:wiremock-jre8:${LibraryVersions.Test.wiremock}")
    testImplementation("com.google.guava:guava:${LibraryVersions.Test.guava}")
    testImplementation("org.bitbucket.b_c:jose4j:${LibraryVersions.Jwt.jose4j}")
    testImplementation(project(":ids-test-extensions"))
}

jib {
    setAllowInsecureRegistries(true)

    from {
        image = "eclipse-temurin:21-jre"
        platforms {
            platform {
                architecture = "amd64"
                os = "linux"
            }
            platform {
                architecture = "arm64"
                os = "linux"
            }
        }
    }

    // Resulting image name & auth for registry
    to {
        val imageName = System.getenv().getOrDefault("IMAGE_NAME", "docker.nexus.dataspac.es/core-container")
        val imageTag = System.getenv().getOrDefault("IMAGE_TAG", "develop").replace('/', '-')
        val imageTags = mutableSetOf(imageTag)
        if (System.getenv().containsKey("CI") && System.getenv().containsKey("CI_COMMIT_SHORT_SHA")) {
            imageTags += "${imageTag}-${System.getenv("CI_COMMIT_SHORT_SHA")}"
        }
        image = imageName
        tags = imageTags
    }

    // Container parameters, fine-tuning the default JVM & Docker parameters
    container {
        jvmFlags = listOf("-Xms64m", "-Xmx1024m")
        ports = listOf("8080/tcp", "8081/tcp")
        creationTime.set("USE_CURRENT_TIMESTAMP")
        ports = listOf("8080", "8081", "8082")
    }
}
