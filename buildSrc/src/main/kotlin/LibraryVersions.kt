/**
 * Library versions used across the build.gradle.kts files
 */
object LibraryVersions {
    object Kotlin {
        const val kotlin = "1.9.22"
        const val api = "1.9"
        const val jackson = "2.15.3"
        const val kotlinxSerialization = "1.6.2"
        const val kaml = "0.56.0"
    }
    object Spring {
        const val boot = "2.7.18"
        const val framework = "5.3.31"
        const val cloud = "2021.0.9"
        const val retry = "1.3.4"
    }
    object Camel {
        const val camel = "3.18.8"
        const val spring = "3.18.8"
    }
    object Jwt {
        const val jjwt = "0.11.5"
        const val jose4j = "0.9.4"
        const val bouncyCastle = "1.77"
    }
    object IDS {
        const val idscp2 = "0.8.0"
        const val infomodel = "4.2.7"
        const val infomodelSerializer = "4.2.8"
    }
    object Test {
        const val wiremock = "2.35.0"
        const val mongoJava = "1.44.0"
        const val guava = "33.0.0-jre"
    }
    object K8s {
        const val fabric8 = "6.8.0"
        const val k8sKotlin = "3.0.1"
    }

    const val mongodb = "4.6.1"
    const val json = "20231013"
    const val hibernate = "8.0.1.Final"
    const val httpclient = "4.5.14"
    const val commonsConfiguration = "1.10"
    const val commonsCompress = "1.25.0"
    const val commonsNet = "3.9.0"
    const val snakeYaml = "2.0"
    const val okhttp = "4.12.0"
    const val spotless = "6.20.0"
    const val javax = "6.0.0"
    const val jib = "3.3.2"
}
