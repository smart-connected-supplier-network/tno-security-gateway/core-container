plugins {
    kotlin("plugin.spring")
}

dependencies {
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")

    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("jakarta.servlet:jakarta.servlet-api:${LibraryVersions.javax}")

    implementation(project(":ids-configuration"))

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:${LibraryVersions.Kotlin.jackson}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation(project(":ids-test-extensions"))
}