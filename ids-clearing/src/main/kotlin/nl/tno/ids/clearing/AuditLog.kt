package nl.tno.ids.clearing

import nl.tno.ids.configuration.model.KeystoreConfig
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.security.PrivateKey
import java.security.Signature
import java.security.cert.X509Certificate
import java.util.*
import java.util.function.Predicate
import java.util.function.UnaryOperator

/**
 * Audit trail loggable interface, that requires a toString function that allows for a flag to indicate
 * whether the output is logged to standard out or not
 */
interface AuditTrailLoggable {
    fun toString(stdOut: Boolean): String
}

/**
 * An auditEntry logs a piece of data with a timestamp.
 * The entry is signed together with the signature of the previous entry, using the given private key.
 * Every entry is therefore linked to all the previous entries.
 */
data class AuditEntry(
    val timestamp: Long,
    val data: AuditTrailLoggable,
    val previousSignature: String = "",
    val currentSignature: String = ""
) {
    /** Compute the signature of an audit entry linking it to the previous audit entry by including its hash in the signature */
    fun computeSignature(previousHash: String, privateKey: PrivateKey): AuditEntry {
        val withLinkedHash = this.copy(previousSignature = previousHash)
        val signature = Signature.getInstance("SHA256withRSA").run {
            initSign(privateKey)
            update(withLinkedHash.toString().encodeToByteArray())
            Base64.getEncoder().encodeToString(sign())
        }
        return withLinkedHash.copy(currentSignature = signature)
    }
    /** Validate the signature to ensure it still matches the audit entry */
    fun validateSignature(publicKey: X509Certificate, signature: String?): Boolean {
        return Signature.getInstance("SHA256withRSA").run {
            initVerify(publicKey.publicKey)
            update(copy(currentSignature = "").toString().encodeToByteArray())
            verify((signature ?: currentSignature).encodeToByteArray())
        }
    }

    /** Create a string representation of the audit entry, either for usage in std out or in an audit file */
    fun toString(stdOut: Boolean): String {
        return "timestamp=$timestamp; ${data.toString(stdOut)}"
    }
}

/**
 * Access control audit entry of an decision made by the Policy Decision Point 
 */
data class AccessControlAuditEntry(
    val consumer: String,
    val provider: String,
    val senderAgent: String?,
    val recipientAgent: String?,
    val contractId: String,
    val decision: String
): AuditTrailLoggable {
    override fun toString(stdOut: Boolean): String {
        return "Access Control: consumer=$consumer, provider=$provider${senderAgent?.let { ", senderAgent=$it"}}${recipientAgent?.let { ", recipientAgent=$it"}}, contractId=$contractId, decision=$decision"
    }
}

/**
 * Data access audit entry of successful access to a resource with the given identifier
 */
data class DataAccessAuditEntry(
    val consumer: String,
    val provider: String,
    val senderAgent: String?,
    val recipientAgent: String?,
    val contractId: String?,
    val identifier: String,
): AuditTrailLoggable {
    override fun toString(stdOut: Boolean): String {
        return "Data Access: consumer=$consumer, provider=$provider${senderAgent?.let { ", senderAgent=$it"}}${recipientAgent?.let { ", recipientAgent=$it"}}, contractId=$contractId, identifier=$identifier"
    }
}

/**
 * Configuration change audit entry indicating some interaction with the management API to either retrieve or change configuration
 */
data class ConfigurationChangeAuditEntry(
    val responsible: String,
    val category: String,
    val operation: String,
    val responseCode: Int
): AuditTrailLoggable {
    override fun toString(stdOut: Boolean): String {
        return "Configuration Interaction: responsible=$responsible, category=$category, operation=$operation, responseCode=$responseCode"
    }
}

/**
 * Auditlog contains an ordered collection of AuditEntries and can be used to log AuditEntries.
 */
@Component
class AuditLog(
    keystoreConfig: KeystoreConfig
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(AuditLog::class.java)
    }

    fun getLog() : List<AuditEntry> {
        return auditLog
    }

    // For additional safety, disable most methods that alter the list.
    private val auditLog = object : ArrayList<AuditEntry>() {
        override fun set(index: Int, element: AuditEntry): AuditEntry {
            throw UnsupportedOperationException()
        }

        override fun add(index: Int, element: AuditEntry) {
            throw UnsupportedOperationException()
        }

        override fun remove(element: AuditEntry): Boolean {
            throw UnsupportedOperationException()
        }

        override fun removeAll(elements: Collection<AuditEntry>): Boolean {
            throw UnsupportedOperationException()
        }

        override fun removeAt(index: Int): AuditEntry {
            throw UnsupportedOperationException()
        }

        override fun removeIf(filter: Predicate<in AuditEntry>): Boolean {
            throw UnsupportedOperationException()
        }

        override fun removeRange(fromIndex: Int, toIndex: Int) {
            throw UnsupportedOperationException()
        }

        override fun clear() {
            throw UnsupportedOperationException()
        }

        override fun addAll(index: Int, elements: Collection<AuditEntry>): Boolean {
            throw UnsupportedOperationException()
        }

        override fun retainAll(elements: Collection<AuditEntry>): Boolean {
            throw UnsupportedOperationException()
        }

        override fun replaceAll(operator: UnaryOperator<AuditEntry>) {
            throw UnsupportedOperationException()
        }

        override fun sort(c: Comparator<in AuditEntry>?) {
            throw UnsupportedOperationException()
        }

    }

    private var previousSignature = ""

    private val privateKey = keystoreConfig.pem?.key ?: throw Exception()
    /** Create a new audit entry for a policy decision */
    @Synchronized
    fun logAccessControl(consumer: String, provider: String, senderAgent: String?, recipientAgent: String?, contractId: String, decision: String) {
        logAuditEntry(
            AuditEntry(
                System.currentTimeMillis(), AccessControlAuditEntry(
                consumer, provider, senderAgent, recipientAgent, contractId, decision
                )
            )
        )
    }

    /** Create a new audit entry for the successful retrieval of a resource */
    @Synchronized
    fun logDataAccess(consumer: String, provider: String, senderAgent: String?, recipientAgent: String?, contractId: String?, identifier: String) {
        logAuditEntry(
            AuditEntry(
                System.currentTimeMillis(), DataAccessAuditEntry(
                consumer, provider, senderAgent, recipientAgent, contractId, identifier
                )
            )
        )
    }

    /** Create a new audit entry for interaction with the management API */
    @Synchronized
    fun logConfigurationChange(responsible: String, category: String, operation: String, responseCode: Int) {
        logAuditEntry(
            AuditEntry(
                System.currentTimeMillis(), ConfigurationChangeAuditEntry(
                responsible, category, operation, responseCode
                )
            )
        )
    }

    /** Add an audit entry to the log and compute the corresponding signatures */
    @Synchronized
    private fun logAuditEntry(auditEntry: AuditEntry) {
        val signedAuditEntry = auditEntry.computeSignature(previousSignature, privateKey)
        LOG.info(signedAuditEntry.toString(true))
        auditLog.add(signedAuditEntry)
        previousSignature = signedAuditEntry.currentSignature
    }
}
