package nl.tno.ids.clearing

import de.fraunhofer.iais.eis.Message
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.serialization.Serialization
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.springframework.stereotype.Component
import java.io.IOException
import java.io.InputStream

/**
 * Abstract clearing processor that logs at most 10240 characters of the payload of the message
 */
abstract class ClearingProcessor(private val clearingLog: ClearingLog) : Processor {
    abstract val direction: ClearingDirection

    override fun process(exchange: Exchange) {
        val idsHeader =
            exchange.message.getHeader(Constants.Headers.idsHeader, String::class.java)
                ?: return
        val header: Message =
            try {
                Serialization.fromJsonLD(idsHeader)
            } catch (e: IOException) {
                return
            }
        val body = exchange.message.body
        val logBody = if (body is InputStream) {
            if (body.markSupported()) {
                body.readNBytes(10240).decodeToString().also {
                    body.reset()
                }
            } else {
                val bytes = body.readAllBytes()
                exchange.message.body = bytes.inputStream()
                bytes.decodeToString(0, bytes.size.coerceAtMost(10240))
            }
        } else {
            body
        }
        clearingLog.clearIds(ClearingContext.DATA_APP, direction, header, logBody)
    }
}

/**
 * Ingress clearing processor intended for usage on ingress routes before processing
 */
@Component("ingressClearing")
class IngressClearingProcessor(clearingLog: ClearingLog) : ClearingProcessor(clearingLog) {
    override val direction = ClearingDirection.INGRESS
}

/**
 * Egress clearing processor intended for usage on egress routes before sending the message to external connectors
 */
@Component("egressClearing")
class EgressClearingProcessor(clearingLog: ClearingLog) : ClearingProcessor(clearingLog) {
    override val direction = ClearingDirection.EGRESS
}