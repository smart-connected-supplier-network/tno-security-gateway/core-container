package nl.tno.ids.clearing

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.tno.ids.configuration.model.SecurityRoles
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import javax.annotation.security.RolesAllowed

/**
 * Clearing REST Controller for retrieving clearing messages via the API
 */
@RestController
@RequestMapping("api/clearing")
@RolesAllowed(SecurityRoles.ADMIN)
class ClearingController(
    /** Local clearing handler */
    private val clearingLog: ClearingLog
) {
    /** ObjectMapper for serializing clearing messages */
    private val objectMapper = jacksonObjectMapper()

    /** Retrieve clearing entries with optional from & to timestamps (unix epoch ms) */
    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getClearingEntries(@RequestParam(required = false) from: Long?, @RequestParam(required = false) to: Long?): String {
        return objectMapper.writeValueAsString(clearingLog.get(from, to).reversed())
    }

    /** Retrieve filtered clearing entries with based on filter parameters */
    @GetMapping("filter", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getFilteredClearingEntries(@RequestParam properties: MutableMap<String, String>): String {
        val context = properties.remove("context")?.let { context ->
            ClearingContext.values().find { it.name.equals(context, ignoreCase = true) }
        }
        val direction = properties.remove("direction")?.let { direction ->
            ClearingDirection.values().find { it.name.equals(direction, ignoreCase = true) }
        }
        return objectMapper.writeValueAsString(clearingLog.filter(
            ClearingFilter(context, direction, properties)
        ))
    }

}