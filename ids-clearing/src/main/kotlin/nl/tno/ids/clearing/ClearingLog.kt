package nl.tno.ids.clearing

import de.fraunhofer.iais.eis.Message
import org.springframework.stereotype.Component
import java.net.URI
import java.util.concurrent.ConcurrentSkipListMap
import kotlin.random.Random

/** Enum indicating the message direction */
enum class ClearingDirection {
    INGRESS, EGRESS
}

/** Enum indicating the message context */
enum class ClearingContext {
    DESCRIPTION,
    ARTIFACT,
    POLICY_NEGOTIATION,
    DATA_APP
}

/** Data class holding all relevant information for clearing */
data class ClearingMessage(
    val context: ClearingContext,
    val direction: ClearingDirection,
    val header: Message?,
    val body: Any?,
    val linkedClearing: URI? = null,
    val properties: Map<String, String?>? = null,
    val timestamp: Long = System.currentTimeMillis()
)

/** Data class holding filter parameters for clearing messages */
data class ClearingFilter(
    val context: ClearingContext? = null,
    val direction: ClearingDirection? = null,
    val properties: Map<String, String?>? = null
)

/**
 * Clearing handling class that stores clearing in-memory
 */
@Component
class ClearingLog {
    /** In-memory storage for clearing messages */
    private val idsClearing: ConcurrentSkipListMap<Long, ClearingMessage> = ConcurrentSkipListMap()

    /** Clear IDS message */
    fun clearIds(context: ClearingContext, direction: ClearingDirection, header: Message? = null, body: Any? = null, linkedClearing: ClearingMessage? = null, properties: Map<String, String?>? = null): ClearingMessage {
        val timestamp = System.currentTimeMillis()
        val message = ClearingMessage(context, direction, header, body, linkedClearing?.header?.id, properties, timestamp)
        idsClearing[timestamp * 1000 + Random.nextInt(0, 999)] = message
        return message
    }

    /** Retrieve clearing messages within a time window, masks token values */
    fun get(from: Long? = null, to: Long? = null): Collection<ClearingMessage> {
        return when {
            from == null -> idsClearing.values
            to == null -> idsClearing.tailMap(from).values
            else -> idsClearing.subMap(from, to).values
        }.map {
            it.header?.securityToken?.tokenValue = "..."
            it
        }
    }

    /** Retrieve clearing messages based on filter, masks token values */
    fun filter(filter: ClearingFilter): Collection<ClearingMessage> {
        return idsClearing.values.filter { clearingMessage ->
            filter.context?.let { clearingMessage.context == it } ?: true
                    && filter.direction?.let { clearingMessage.direction == it } ?: true
                    && filter.properties?.let {
                        it.all { property -> clearingMessage.properties?.get(property.key) == property.value }
            } ?: true
        }.map {
            it.header?.securityToken?.tokenValue = "..."
            it
        }
    }
}