package nl.tno.ids.clearing

import de.fraunhofer.iais.eis.RequestMessageBuilder
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.testextensions.DapsTestExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.TestPropertySource
import java.net.URI

/**
 * Test class for clearing functionality
 */
@SpringBootTest(classes = [ClearingLogTest.Application::class])
@TestPropertySource(
    properties = ["spring.config.location = classpath:application.yaml"])
@ExtendWith(DapsTestExtension::class)
class ClearingLogTest {
    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Clearing handler */
    @Autowired private lateinit var clearingLog: ClearingLog

    /** Test clearing of messages */
    @Test
    fun testClearing() {
        val header = RequestMessageBuilder()
            ._issuerConnector_(URI("urn:ids:test"))
            ._recipientConnector_(URI("urn:ids:test"))
            ._senderAgent_(URI("urn:ids:test"))
            ._recipientAgent_(URI("urn:ids:test"))
            .buildWithDefaults()

        val clearingMessage = clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.INGRESS, header, "Test", properties = mapOf("testProperty" to "test"))
        val clearingMessage2 = clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.EGRESS, header, "Test2", clearingMessage, properties = mapOf("testProperty" to "test2"))

        Assertions.assertEquals(2, clearingLog.get().size)
        Assertions.assertTrue(clearingLog.get().contains(clearingMessage))
        Assertions.assertTrue(clearingLog.get().contains(clearingMessage2))
    }
}