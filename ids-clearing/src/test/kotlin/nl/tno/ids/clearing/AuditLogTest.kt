package nl.tno.ids.clearing

import nl.tno.ids.configuration.model.KeystoreConfig
import nl.tno.ids.testextensions.DapsTestExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.TestPropertySource

@SpringBootTest(classes = [AuditLogTest.Application::class])
@TestPropertySource(
    properties = ["spring.config.location = classpath:application.yaml"])
@ExtendWith(DapsTestExtension::class)
class AuditLogTest {

    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Clearing handler */
    @Autowired
    private lateinit var clearingLog: ClearingLog

    @Autowired
    private lateinit var keyStoreConfig: KeystoreConfig

    @Autowired
    private lateinit var auditLog: AuditLog

    @Test
    fun testAuditLog() {
        auditLog.logAccessControl("urn:ids:test", "urn:ids:test",
            "urn:ids:test", "urn:ids:test", "urn:ids:test",
        "ALLOW")

        auditLog.logDataAccess("urn:ids:test", "urn:ids:test",
            "urn:ids:test", "urn:ids:test",
            "urn:ids:test", "urn:ids:test")

        auditLog.logConfigurationChange("urn:ids:test", "testCategory", "testOperation", 200)

        Assertions.assertTrue(auditLog.getLog().size == 3)
    }
}