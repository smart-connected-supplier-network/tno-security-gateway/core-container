# IDS Clearing

The IDS clearing module is responsible for clearing ingress and egress IDS messages.

Currently, the clearing is done in-memory, with the possibility to retrieve the clearing logs via an API endpoint.

Not all envisioned functionality is currently implemented, some of the features that are likely to be added soon are:
* Persistent storage of clearing
* Integration of the IDS Clearing House
* Integration of admin interactions with the Core Container, to log configuration changes or access to sensitive information
