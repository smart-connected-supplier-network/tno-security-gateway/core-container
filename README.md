# TNO Core Container

The _Core Container_ is the core component of the IDS ecosystem.

The generic features of this core container:
- Message routing based on [Apache Camel](https://camel.apache.org/)
- Modular approach based on [Spring Boot](https://spring.io/projects/spring-boot)
- Preliminary Data App support
- IDS Messaging support
- Embedded Artifact handling
- Embedded Policy Enforcement Framework
- Embedded Container Orchestration support
- Embedded Workflow Management support

## How to report issues
If you have suggestions to improve the Core Container, you can submit a Gitlab issue. Your issue will be worked on as soon as possible. In case you encounter a bug, please also raise an issue and you will get a response within 5 business days. In case of a Security flaw, please also raise a Gitlab issue. Security flaws are dealt with as soon as possible, often this will be on the same business day as the report, or the next. One of the project administrators will assess the report, assess the severity and take actions accordingly. They create a Vulnerability in Gitlab under Secure>Vulnerability report> + Submit vulnerability. Vulnerabilities with the severity Unknown, info, Low or Medium will be fixed in the following release. This means that a Gitlab issue will be raised to fix the issue, which will be added to the workload of one of the project maintainers. For a vulnerability High or Critical, a release will be made to fix the issue as soon as possible. The issue will also be on the top of the list of tasks of the project maintainers, to make sure the issue is resolved as soon as possible. As soon as the issue is merged with the main branch, and a new release is created, the Vulnerability will be marked as resolved. 

## How to build

The build pipeline of this repository is built up using Gradle and is intended to be built into a Docker image.

To build execute (automatically pushes the image to the repository):
```shell
./gradlew clean assemble ciTest jib
```

To build the image for local usage (requires a Docker daemon to be reachable):
```shell
./gradlew clean assemble ciTest jibDockerBuild
```

To execute the integration and local tests (requires a Docker daemon to be reachable):
```shell
./gradlew clean assemble test
```

> _NOTE: The Spotless plugin is used to automatically set the copyright headers and formatting of files_
> In case of errors during Spotless Check tasks, run the following task to automatically fix the errors:
> ```
> ./gradlew spotlessApply
> ```

## How to deploy
The primary way of deploying the Core Container is via Kubernetes and Helm. For the Helm chart see [Connector Helm Chart](https://ci.tno.nl/gitlab/ids/helm-charts/connector).

In the Gitlab CI pipeline, Docker images will be automatically be built with the following format:
```
registry.ids.smart-connected.nl/core-container:{GIT_BRANCH_NAME}
registry.ids.smart-connected.nl/core-container:{GIT_BRANCH_NAME}-{BUILT_TIME}
```
> _Note: The format for `BUILT_TIME` is yyyyMMddHHmm_

## Modules
This repository is composed of several sub-modules responsible for parts of the core container:
- `camel-daps-processor` [README](./camel-daps-processor): Apache Camel processors for handling DAPS interactions
- `camel-multipart-processor` [README](./camel-multipart-processor): Apache Camel processors for handling HTTP Mime Multipart requests/response
- `ids-artifact-manager` [README](./ids-artifact-manager): Embedded Artifact handler
- `ids-clearing` [README](./ids-clearing): Clearing support for ingress & egress messages
- `ids-configuration` [README](./ids-configuration): Spring Configuration & Beans used by the other modules
- `ids-main` [README](./ids-main): Main application class & configuration/beans that require one of the other modules
- `ids-orchestration-manager` [README](./ids-orchestration-manager): Orchestrator for managing containers next to the Core Container
- `ids-pef` [README](./ids-pef): Embedded Policy Enforcement Framework
- `ids-resource-manager` [README](./ids-resource-manager): Resource metadata manager
- `ids-route-manager` [README](./ids-route-manager): Apache Camel dynamic route manager
- `ids-selfdescription` [README](./ids-selfdescription): IDS Information Model Self-Description handling and publishing (to Broker)
- `ids-security` [README](./ids-security): Security module that secures internal endpoints of the Core Container
- `ids-token-manager` [README](./ids-token-manager): Dynamic Attribute Provisioning Service (DAPS) interaction provider
- `ids-workflow-manager` [README](./ids-workflow-manager): Embedded Workflow manager