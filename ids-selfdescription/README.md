# IDS Self Description

The IDS Self Description module is responsible for constructing the self description of the connector and interact with the Broker to publish the self description.

## Self Description Generator

The `SelfDescriptionGenerator` component constructs the self description metadata. The sources used for this are:
- _Spring Configuration properties_: The information in the `ids.info.*` properties is used to construct the generic information about the connector and who is owner of the connector.
- _Resource Manager Service_: The complete resource catalog exposed by the connector is managed by the [`ids-resource-manager` module](../ids-resource-manager)

When receiving the `SelfDescriptionReloadEvent`, the `SelfDescriptionGenerator` will generate the up-to-date self description. After the self description is generated, the `SelfDescriptionChangeEvent` is triggered. 

## Broker Registration Service

The `BrokerRegistrationService` will publish the connector's metadata at the configured broker. For this to work, the `ids.broker.autoRegister` property should be `true`.
> _Note: Manual registration of the connector is not yet supported_

The `BrokerRegistrationService` will try the initial Broker registration in a retryable fashion (configurable via `ids.broker.registrationMaxRetries` & `ids.broker.registrationBackoffPeriod`).

To make sure the self description is updated and the Broker knows the connector is still available, the `BrokerRegistrationService` will periodically publish its self description (configurable via `broker.brokerHealthCheckInterval`).

## Description Processor

The `DescriptionProcessor` provides the functionality of handling incoming `DescriptionRequestMessage`s. By either providing the full self description, or a specific portion if the `requestedElement` property is filled in the request message. The processor is used in the Camel routes for the self description.

## Description Controller

The `DescriptionController` provides an API to send a `DescriptionRequest` to another connector. Primary used in combination with the `ArtifactConsumerController` from the [`ids-artifact-manager` module](../ids-artifact-manager), for retrieving `ContractOffer`s provided with resources in the self description. 