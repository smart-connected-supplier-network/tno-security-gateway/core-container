/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.selfdescription

import de.fraunhofer.iais.eis.*
import nl.tno.ids.clearing.ClearingLog
import nl.tno.ids.clearing.ClearingContext
import nl.tno.ids.clearing.ClearingDirection
import nl.tno.ids.clearing.ClearingMessage
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildAsResponseTo
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.serialization.Serialization
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.json.JSONArray
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.IOException

/** Apache Camel Processor to handle DescriptionRequest messages */
@Component
class DescriptionProcessor(
    /** Self description generator */
    private val selfDescriptionGenerator: SelfDescriptionGenerator,
    /** Local clearing handler */
    private val clearingLog: ClearingLog
) : Processor {
    companion object {
        private val LOG = LoggerFactory.getLogger(DescriptionProcessor::class.java)
    }

    /** Process incoming exchange */
    override fun process(exchange: Exchange) {
        val idsHeader =
            exchange.message.getHeader(Constants.Headers.idsHeader, String::class.java)
                ?: throw Exception("No IDS-Header found!")
        val header: Message =
            try {
                Serialization.fromJsonLD(idsHeader)
            } catch (e: IOException) {
                LOG.warn("Cannot parse header: {}", idsHeader, e)
                throw Exception("Error in parsing IDS header")
            }
        when (header) {
            is DescriptionRequestMessage -> {
                if (header.requestedElement == null) {
                    val ingressClearing = clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.INGRESS, header)
                    putIDSMessageOnExchange(
                        exchange,
                        DescriptionResponseMessageBuilder().buildAsResponseTo(header),
                        selfDescriptionGenerator.fullConnector.toJsonLD(),
                        ingressClearing
                    )
                } else {
                    val ingressClearing = clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.INGRESS, header, properties = mapOf("requestedElement" to header.requestedElement?.toString()))
                    val jsonObject = JSONObject(selfDescriptionGenerator.fullConnector.toJsonLD())
                    val element = findElement(header.requestedElement.toString(), jsonObject)
                    if (element == null) {
                        putIDSMessageOnExchange(
                            exchange,
                            RejectionMessageBuilder()
                                ._rejectionReason_(RejectionReason.NOT_FOUND)
                                .buildAsResponseTo(header),
                            clearingMessage = ingressClearing)
                    } else {
                        putIDSMessageOnExchange(
                            exchange,
                            DescriptionResponseMessageBuilder().buildAsResponseTo(header),
                            element
                                .put(
                                    "@context",
                                    JSONObject()
                                        .put("ids", "https://w3id.org/idsa/core/")
                                        .put("idsc", "https://w3id.org/idsa/code/"))
                                .toString(2),
                            ingressClearing)
                    }
                }
            }
            else -> {
                val ingressClearing = clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.INGRESS, header)
                putIDSMessageOnExchange(
                    exchange,
                    RejectionMessageBuilder()
                        ._rejectionReason_(RejectionReason.MESSAGE_TYPE_NOT_SUPPORTED)
                        .buildAsResponseTo(header),
                    clearingMessage = ingressClearing
                )
            }
        }
    }

    /**
     * Find specific element with given elementId in the full JSON Object tree.
     *
     * TODO: Possibly change to breadth-first search instead of depth-first search or ID-caching for
     * performance
     * ```
     *       reasons in case of large catalogs
     * ```
     */
    private fun findElement(elementId: String, json: JSONObject): JSONObject? {
        if (json.optString("@id") == elementId) return json

        json.keySet().forEach {
            val result =
                when (val child = json[it]) {
                    is JSONArray -> findElement(elementId, child)
                    is JSONObject -> findElement(elementId, child)
                    else -> null
                }
            if (result != null) return result
        }
        return null
    }

    /** Find specific element with fiven elementId in the full JSON Array */
    private fun findElement(elementId: String, json: JSONArray): JSONObject? {
        json.forEach {
            val result =
                when (it) {
                    is JSONArray -> findElement(elementId, it)
                    is JSONObject -> findElement(elementId, it)
                    else -> null
                }
            if (result != null) return result
        }
        return null
    }

    /** Set the correct headers that are assumed by the MultipartOutputProcessor */
    private fun putIDSMessageOnExchange(
        exchange: Exchange,
        header: Message,
        payload: String? = null,
        clearingMessage: ClearingMessage
    ) {
        payload?.let { exchange.message.body = payload }
        clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.EGRESS, header, payload, clearingMessage)
        exchange.message.headers =
            mapOf("Content-Type" to "application/ld+json", Constants.Headers.idsHeader to header.toJsonLD())
    }
}
