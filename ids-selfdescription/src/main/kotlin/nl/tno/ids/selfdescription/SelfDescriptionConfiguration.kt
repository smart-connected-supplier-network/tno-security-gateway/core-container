package nl.tno.ids.selfdescription

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated

/**
 * Security Configuration properties to enable the security setup and provide initial API keys and Users
 */
@Component
@ConfigurationProperties(prefix = "selfdescription")
@Validated
class SelfDescriptionConfiguration {
    /** Enable local multipart description endpoint */
    var localEndpoint: Boolean = true
    /** Enable local plain description endpoint */
    var localPlainEndpoint: Boolean = false
}