/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.selfdescription

import de.fraunhofer.iais.eis.ConnectorUnavailableMessageBuilder
import de.fraunhofer.iais.eis.ConnectorUpdateMessageBuilder
import de.fraunhofer.iais.eis.RejectionMessage
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.events.SelfDescriptionChangeEvent
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.BrokerConfig
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.RouteConfig
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.security.AuthManager
import org.json.JSONException
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.retry.annotation.Backoff
import org.springframework.retry.annotation.EnableRetry
import org.springframework.retry.annotation.Retryable
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.HttpServerErrorException
import org.springframework.web.client.RestTemplate
import org.springframework.web.client.UnknownHttpStatusCodeException
import java.net.URI
import java.util.concurrent.ConcurrentHashMap

/**
 * Broker Registration Exception
 */
class BrokerRegistrationException(message: String, cause: Throwable? = null) :
    Exception(message, cause)

/**
 * Broker Registration data class containing information on the last time the registration is updated
 */
data class BrokerRegistration(
    val brokerId: String,
    val brokerAddress: String,
    val lastUpdated: Long
)

/**
 * Broker Registration Service for registering at the IDS Broker with configurable maximum retries
 * and backoff periods
 */
@Service
@EnableScheduling
@EnableRetry
class BrokerRegistrationService(
    /** Self Description generator */
    private val selfDescriptionGenerator: SelfDescriptionGenerator,
    /** Broker configuration */
    private val brokerConfig: BrokerConfig,
    /** Connector info configuration */
    private val connectorInfo: ConnectorInfo,
    /** Route configuration */
    private val routeConfig: RouteConfig,
    /** Rest template for sending HTTP requests */
    private val restTemplate: RestTemplate,
    /** Authentication manager */
    private val authManager: AuthManager
) : ApplicationListener<SelfDescriptionChangeEvent> {
    /**
     * Boolean to indicate whether the initial Broker registration was successful (if auto
     * registration is enabled)
     */
    var registered = !brokerConfig.autoRegister

    /**
     * Active Broker registrations
     */
    val registrations: MutableMap<URI, BrokerRegistration> = ConcurrentHashMap()

    /** Self Description Change Event handler that triggers re-registration at the Broker */
    override fun onApplicationEvent(event: SelfDescriptionChangeEvent) {
        initialBrokerRegistration()
    }

    /**
     * Execute the initial Broker registration, is scheduled to run once after a fixed delay of 10
     * seconds. Has configurable retry configuration, primarily used in cases a Broker and Connector
     * start up at the same time.
     */
    @Scheduled(
        initialDelayString = "\${broker.brokerInitialDelay:10000}",
        fixedDelay = Long.MAX_VALUE)
    @Retryable(
        value = [BrokerRegistrationException::class],
        maxAttemptsExpression = "\${broker.registrationMaxRetries:10}",
        backoff =
            Backoff(
                delayExpression = "\${broker.registrationBackoffPeriod:5000}", multiplier = 2.0))
    fun initialBrokerRegistration() {
        if (brokerConfig.autoRegister) {
            try {
                exchangeConnectorUpdateMessage()
            } catch (e: Exception) {
                LOG.info("Broker Registration failed: ${e.message}")
                LOG.debug("", e)
                when (e) {
                    is BrokerRegistrationException -> throw e
                    else ->
                        throw BrokerRegistrationException("Initial Broker Registration Failed", e)
                }
            }
        }
    }

    /**
     * Execute the Broker Health check at a configurable interval (in ms) to indicate to the Broker
     * that this connector is still running.
     *
     * TODO: Re-registration process should be improved, now the full self-description is shared on
     * an interval
     * ```
     *       for performance reasons this should be converted towards a "simple" notification message / health check
     *       that the connector is still online
     * ```
     */
    @Scheduled(
        initialDelayString = "\${broker.brokerHealthCheckInterval:3600000}",
        fixedRateString = "\${broker.brokerHealthCheckInterval:3600000}")
    @Retryable(
        value = [BrokerRegistrationException::class],
        maxAttempts = 3,
        backoff = Backoff(delay = 60000))
    fun brokerHealthCheckInterval() {
        if (brokerConfig.autoRegister) {
            try {
                exchangeConnectorUpdateMessage()
            } catch (e: Exception) {
                LOG.info("Broker Health check failed: ${e.message}")
                LOG.debug("", e)
                when (e) {
                    is BrokerRegistrationException -> throw e
                    else -> throw BrokerRegistrationException("Broker Health check failed", e)
                }
            }
        }
    }

    /**
     * Exchange Connector update message with a Broker and either throw a BrokerRegistrationException in case of errors
     * or return without exception if the registration was successful
     */
    fun exchangeConnectorUpdateMessage(providedBrokerId: URI? = null, providedBrokerAddress: String? = null) {
        val brokerId = providedBrokerId ?: brokerConfig.id
        val brokerAddress = providedBrokerAddress ?: brokerConfig.address.toString()
        val connectorUpdateMessage =
            ConnectorUpdateMessageBuilder()
                ._issuerConnector_(connectorInfo.idsid)
                ._recipientConnector_(arrayListOf(brokerId))
                ._senderAgent_(connectorInfo.maintainer)
                ._recipientAgent_(arrayListOf(brokerId))
                ._affectedConnector_(connectorInfo.idsid)
                .buildWithDefaults()
                .toMultiPartMessage(
                    selfDescriptionGenerator.fullConnector.toJsonLD(),
                    httpHeaders = mapOf(
                        Constants.Headers.forwardTo to brokerAddress,
                        Constants.Headers.authorization to "Bearer ${authManager.internalApiKey.key}"
                    ),
                    contentType = "application/ld+json")

        LOG.debug(
            "Executing Connector update message: ${routeConfig.defaultEgressUrl} -> $brokerAddress")
        try {
            val response =
                restTemplate.postForEntity(
                    routeConfig.defaultEgressUrl,
                    connectorUpdateMessage,
                    MultiPartMessage::class.java)
            when (response.body?.header) {
                is RejectionMessage -> {
                    val reason = response.body?.payload?.asString()?.run {
                        try {
                            JSONObject(this).let { reason ->
                                "${reason["type"]} (${reason["origin"]}) -> ${reason["reason"]}"
                            }
                        } catch (e: JSONException) {
                            this
                        }
                    }
                    throw BrokerRegistrationException(
                        "Broker registration failed: $reason")
                }
                else -> {
                    registered = true
                    LOG.info("Successful Broker registration")
                    registrations[brokerId] = BrokerRegistration(
                        brokerId = brokerId.toString(),
                        brokerAddress = brokerAddress,
                        lastUpdated = System.currentTimeMillis()
                    )
                }
            }
        } catch (e: HttpClientErrorException) {
            throw BrokerRegistrationException("Client Error ${e.statusCode}")
        } catch (e: HttpServerErrorException) {
            throw BrokerRegistrationException("Server Error ${e.statusCode}")
        } catch (e: UnknownHttpStatusCodeException) {
            throw BrokerRegistrationException("Unknown Error ${e.rawStatusCode} ${e.statusText}")
        }
    }

    /**
     * Exchange Connector unavailable message with a Broker and either throw a BrokerRegistrationException in case of errors
     * or return without exception if the exchange was successful
     */
    fun exchangeConnectorUnavailableMessage(providedBrokerId: URI? = null, providedBrokerAddress: String? = null, affectedConnector: URI? = null) {
        val brokerId = providedBrokerId ?: brokerConfig.id
        val brokerAddress = providedBrokerAddress ?: brokerConfig.address.toString()
        val connectorUnavailableMessage =
            ConnectorUnavailableMessageBuilder()
                ._issuerConnector_(connectorInfo.idsid)
                ._recipientConnector_(arrayListOf(brokerId))
                ._senderAgent_(connectorInfo.maintainer)
                ._recipientAgent_(arrayListOf(brokerId))
                ._affectedConnector_(affectedConnector ?: connectorInfo.idsid)
                .buildWithDefaults()
                .toMultiPartMessage(
                    selfDescriptionGenerator.fullConnector.toJsonLD(),
                    httpHeaders = mapOf(
                        Constants.Headers.forwardTo to brokerAddress,
                        Constants.Headers.authorization to "Bearer ${authManager.internalApiKey.key}"
                    ),
                    contentType = "application/ld+json")

        LOG.debug(
            "Executing Connector unavailable message: ${routeConfig.defaultEgressUrl} -> $brokerAddress")
        try {
            val response =
                restTemplate.postForEntity(
                    routeConfig.defaultEgressUrl,
                    connectorUnavailableMessage,
                    MultiPartMessage::class.java)
            when (response.body?.header) {
                is RejectionMessage -> {
                    val reason = response.body?.payload?.asString()?.run {
                        try {
                            JSONObject(this).let { reason ->
                                "${reason["type"]} (${reason["origin"]}) -> ${reason["reason"]}"
                            }
                        } catch (e: JSONException) {
                            this
                        }
                    }
                    throw BrokerRegistrationException(
                        "Broker deregistration failed: $reason")
                }
                else -> {
                    LOG.info("Successful Broker deregistration")
                    registrations.remove(brokerId)
                }
            }
        } catch (e: HttpClientErrorException) {
            throw BrokerRegistrationException("Client Error ${e.statusCode}")
        } catch (e: HttpServerErrorException) {
            throw BrokerRegistrationException("Server Error ${e.statusCode}")
        } catch (e: UnknownHttpStatusCodeException) {
            throw BrokerRegistrationException("Unknown Error ${e.rawStatusCode} ${e.statusText}")
        }
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(BrokerRegistrationService::class.java)
    }
}
