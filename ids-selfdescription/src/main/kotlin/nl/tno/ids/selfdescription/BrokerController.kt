package nl.tno.ids.selfdescription

import de.fraunhofer.iais.eis.ArtifactRequestMessageBuilder
import nl.tno.ids.clearing.ClearingContext
import nl.tno.ids.clearing.ClearingDirection
import nl.tno.ids.clearing.ClearingLog
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.BrokerConfig
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.RouteConfig
import nl.tno.ids.configuration.model.SecurityRoles
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.security.AuthManager
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.io.InputStream
import java.net.URI

/** Broker Rest Controller for sending update & unavailable message towards Broker(s) */
@RestController
@RequestMapping("api/broker")
@Secured(SecurityRoles.DESCRIPTION_MANAGER)
class BrokerController(
    /** Broker registration service that handles the communication with Broker(s) */
    private val brokerRegistrationService: BrokerRegistrationService,
    /** Connector info configuration */
    private val connectorInfo: ConnectorInfo,
    /** Broker configuration */
    private val brokerConfig: BrokerConfig,
    /** Rest template for sending HTTP requests */
    private val restTemplate: RestTemplate,
    /** Authentication manager */
    private val authManager: AuthManager,
    /** Route configuration */
    private val routeConfig: RouteConfig,
    /** Local clearing handler */
    private val clearingLog: ClearingLog
) {

    /**
     * Retrieve the active Broker registrations of this connector
     */
    @GetMapping("registrations")
    fun getBrokerRegistrations(): Collection<BrokerRegistration> {
        return brokerRegistrationService.registrations.values
    }

    /**
     * Manually update the registration of this connector at the specified Broker or the default configured Broker
     */
    @PostMapping("update")
    fun connectorUpdate(
        @RequestParam(required = false) brokerId: URI?,
        @RequestParam(required = false) brokerAddress: String?
    ): ResponseEntity<String> {
        return try {
            brokerRegistrationService.exchangeConnectorUpdateMessage(brokerId, brokerAddress)
            ResponseEntity.ok("")
        } catch (e: BrokerRegistrationException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Indicate the unavailability of this connector at the specified Broker or the default configured Broker
     */
    @PostMapping("unavailable")
    fun connectorUnavailable(
        @RequestParam(required = false) brokerId: URI?,
        @RequestParam(required = false) brokerAddress: String?,
        @RequestParam(required = false) affectedConnector: URI?
    ): ResponseEntity<String> {
        return try {
            brokerRegistrationService.exchangeConnectorUnavailableMessage(brokerId, brokerAddress, affectedConnector)
            ResponseEntity.ok("")
        } catch (e: BrokerRegistrationException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Retrieve list of federated Brokers from the main configured broker
     */
    @GetMapping("federated-brokers")
    fun federatedBrokers(
        @RequestParam(required = false) connectorId: String?,
        @RequestParam(required = false) agentId: String?,
        @RequestParam(required = false) accessUrl: String?,
        @RequestParam(required = false) artifactId: String?
    ): ResponseEntity<InputStream> {
        val brokerId = connectorId?.let { URI(it) } ?: brokerConfig.id
        val brokerParticipantId = agentId?.let { URI(it) } ?: brokerId
        val brokerAccessUrl = accessUrl ?: brokerConfig.address.toString()
        val requestMessage = ArtifactRequestMessageBuilder()
            ._issuerConnector_(connectorInfo.idsid)
            ._recipientConnector_(brokerId)
            ._senderAgent_(connectorInfo.curator)
            ._recipientAgent_(brokerParticipantId)
            ._requestedArtifact_(URI(artifactId ?: "urn:ids:broker:federatedBrokers"))
            .buildWithDefaults()
        val egressClearing = clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.EGRESS, requestMessage)

        val response = restTemplate.postForEntity(
            routeConfig.defaultEgressUrl,
            requestMessage.toMultiPartMessage(
                httpHeaders = mapOf(
                    Constants.Headers.forwardTo to brokerAccessUrl,
                    Constants.Headers.authorization to "Bearer ${authManager.internalApiKey.key}"
                )
            ),
            MultiPartMessage::class.java
        )

        clearingLog.clearIds(
            ClearingContext.DESCRIPTION,
            ClearingDirection.INGRESS,
            response.body?.header,
            linkedClearing = egressClearing
        )

        return ResponseEntity.status(response.statusCode)
            .apply {
                response.body?.payload?.httpHeaders?.forEach {
                    header(it.key, it.value)
                }
            }
            .contentType(MediaType.APPLICATION_JSON)
            .body(response.body?.payload?.inputStream())
    }
}