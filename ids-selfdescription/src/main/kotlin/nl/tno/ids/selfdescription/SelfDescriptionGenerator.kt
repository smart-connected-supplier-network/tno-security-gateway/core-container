/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.selfdescription

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.configuration.events.SelfDescriptionChangeEvent
import nl.tno.ids.configuration.events.SelfDescriptionReloadEvent
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.KeystoreConfig
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.resourcemanager.ResourceManagerService
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.ApplicationListener
import org.springframework.lang.Nullable
import org.springframework.stereotype.Component
import java.security.cert.X509Certificate

/**
 * Self-Description Generator component that provides an up-to-date self-description to be used by
 * other components in the connector (e.g. broker registration, self-description endpoint)
 */
@Component
class SelfDescriptionGenerator(
    /** Connector Info configuration containing the basic information of the connector */
    @Nullable private val connectorInfo: ConnectorInfo?,
    /** Resource Manager Service for populating the catalogs of the self-description */
    private val resourceManagerService: ResourceManagerService,
    /** Spring Event Publisher to notify other components of a change in the self-description */
    private val applicationEventPublisher: ApplicationEventPublisher,
    /** Keystore configuration for sharing the public key of the connector in the self-description */
    private val keystoreConfig: KeystoreConfig
) : ApplicationListener<SelfDescriptionReloadEvent> {
    /** Minimal Connector instance without catalogs */
    lateinit var minimalConnector: Connector

    /** Full Connector instance */
    lateinit var fullConnector: Connector

    /** List of endpoints used in the connector */
    private var connectorEndpoints: List<ConnectorEndpoint> = emptyList()

    init {
        generate()
    }

    /** Self Description Reload Event handler, triggers generating the self descriptions */
    override fun onApplicationEvent(event: SelfDescriptionReloadEvent) {
        LOG.info("Received SelfDescription Reload Event from ${event.source::class.simpleName}")
        generate()
        applicationEventPublisher.publishEvent(SelfDescriptionChangeEvent(this))
    }

    /** Generate minimal & full Connector instances */
    private fun generate() {
        val connectorBuilder =
            connectorInfo?.idsid?.let { TrustedConnectorBuilder(it) } ?: TrustedConnectorBuilder()

        connectorInfo?.let { info ->
            connectorBuilder._title_(
                info.titles
                    .map { valueAndLanguage: String? -> TypedLiteral(valueAndLanguage) }
                    .toCollection(ArrayList()))

            connectorBuilder._description_(
                info.descriptions
                    .map { valueAndLanguage: String? -> TypedLiteral(valueAndLanguage) }
                    .toCollection(ArrayList()))

            connectorEndpoints =
                info.accessUrl.map { accessUrl ->
                    ConnectorEndpointBuilder()._accessURL_(accessUrl.toURI()).build()
                }
            connectorBuilder
                ._hasEndpoint_(arrayListOf(*connectorEndpoints.toTypedArray()))
                ._hasDefaultEndpoint_(connectorEndpoints[0])

            connectorBuilder
                ._outboundModelVersion_("4.2.7")
                ._inboundModelVersion_(arrayListOf("4.0.0", "4.1.0", "4.1.2", "4.2.0", "4.2.1", "4.2.2", "4.2.3", "4.2.4", "4.2.5", "4.2.6", "4.2.7"))
                ._curator_(info.curator)
                ._maintainer_(info.maintainer)
                ._securityProfile_(info.securityProfile)
                ._publicKey_(PublicKeyBuilder()
                    ._keyType_(parseKeyType(keystoreConfig.pem?.cert))
                    ._keyValue_(keystoreConfig.pem?.cert?.encoded)
                    .build())
        }
            ?: TrustedConnectorBuilder()

        minimalConnector =
            connectorBuilder
                ._resourceCatalog_(arrayListOf(ResourceCatalogBuilder().build()))
                .build()
        fullConnector = connectorBuilder._resourceCatalog_(buildCatalogWithResources()).build()

        // Builder classes don't use inheritance, so this block is used to convert an ids:TrustedConnector class to
        // an ids:BaseConnector class to allow for a single builder pattern to be used.
        if (connectorInfo?.securityProfile == SecurityProfile.BASE_SECURITY_PROFILE) {
            minimalConnector = Serialization.fromJsonLD(minimalConnector.toJsonLD().replace("ids:TrustedConnector", "ids:BaseConnector"))
            fullConnector = Serialization.fromJsonLD(fullConnector.toJsonLD().replace("ids:TrustedConnector", "ids:BaseConnector"))
        }
    }

    /** Parse type of Certificate into Information Model object */
    private fun parseKeyType(cert: X509Certificate?): KeyType {
        return cert?.sigAlgName?.run {
            when {
                contains("ECDSA") -> KeyType.ECDSA
                contains("DSA") -> KeyType.DSA
                contains("RSA") -> KeyType.RSA
                contains("ED25519") -> KeyType.ED25519
                else -> throw IllegalArgumentException()
            }
        } ?: throw IllegalArgumentException()
    }

    /** Build up catalogs, from both container configurations as the resourceManagers resources */
    private fun buildCatalogWithResources(): ArrayList<out ResourceCatalog> {
        return ArrayList(resourceManagerService.getResourceCatalogs())
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(SelfDescriptionGenerator::class.java)
    }
}
