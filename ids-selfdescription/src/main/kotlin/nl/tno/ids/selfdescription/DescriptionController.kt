/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.selfdescription

import de.fraunhofer.iais.eis.*
import nl.tno.ids.clearing.ClearingContext
import nl.tno.ids.clearing.ClearingDirection
import nl.tno.ids.clearing.ClearingLog
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.BrokerConfig
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.RouteConfig
import nl.tno.ids.configuration.model.SecurityRoles
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.security.AuthManager
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.net.URI

/** Description Rest Controller for requesting metadata from other connectors (or a Broker) */
@RestController
@RequestMapping("api/description")
@Secured(SecurityRoles.DESCRIPTION_READER)
class DescriptionController(
    /** Connector info configuration */
    private val connectorInfo: ConnectorInfo,
    /** Broker configuration */
    private val brokerConfig: BrokerConfig,
    /** Rest template for sending HTTP requests */
    private val restTemplate: RestTemplate,
    /** Authentication manager */
    private val authManager: AuthManager,
    /** Route configuration */
    private val routeConfig: RouteConfig,
    /** Local clearing handler */
    private val clearingLog: ClearingLog
) {

    /**
     * Construct a DescriptionRequestMessage and forward it to the corresponding connector
     *
     * Returns either the HTTP Mime Multipart response, or the JSON-LD body if the "Accept" header
     * is set to application/ld+json or application/json
     */
    @GetMapping
    fun descriptionRequest(
        @RequestHeader(required = false) accept: String?,
        @RequestParam connectorId: String,
        @RequestParam(required = false) agentId: String?,
        @RequestParam accessUrl: String,
        @RequestParam(required = false) requestedElement: String?,
        @RequestParam(required = false) validate: Boolean?
    ): ResponseEntity<Any> {
        try {
            val descriptionRequestMessage =
                DescriptionRequestMessageBuilder()
                    ._issuerConnector_(connectorInfo.idsid)
                    ._recipientConnector_(URI(connectorId))
                    ._senderAgent_(connectorInfo.curator)
                    ._recipientAgent_(URI(agentId ?: connectorId))
                    .apply { requestedElement?.let { _requestedElement_(URI(it)) } }
                    .buildWithDefaults()

            val egressClearing =
                clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.EGRESS, descriptionRequestMessage)

            val response = restTemplate.postForEntity(
                routeConfig.defaultEgressUrl,
                descriptionRequestMessage.toMultiPartMessage(
                    httpHeaders = mapOf(
                        Constants.Headers.forwardTo to accessUrl,
                        Constants.Headers.authorization to "Bearer ${authManager.internalApiKey.key}"
                    )
                ),
                MultiPartMessage::class.java
            )

            clearingLog.clearIds(
                ClearingContext.DESCRIPTION,
                ClearingDirection.INGRESS,
                response.body?.header,
                linkedClearing = egressClearing
            )

            return if (accept?.startsWith("application/") == true && accept.endsWith("json")) {
                when (val header = response.body?.header) {
                    is RejectionMessage -> {
                        ResponseEntity.badRequest().body(header.rejectionReason.comment.first().value)
                    }
                    is DescriptionResponseMessage -> {
                        if (validate == true) {
                            val json = response.body?.payload?.inputStream()?.readAllBytes()?.decodeToString()
                            try {
                                Serialization.fromJsonLD<ModelClass>(json)
                                ResponseEntity.ok()
                                    .header("Content-Type", "application/ld+json")
                                    .body(json)
                            } catch (e: Exception) {
                                ResponseEntity.badRequest().body(e.message)
                            }
                        } else {
                            ResponseEntity.ok()
                                .header("Content-Type", "application/ld+json")
                                .body(response.body?.payload?.inputStream())
                        }
                    }
                    else -> {
                        ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("")
                    }
                }
            } else if (accept?.startsWith("multipart") == true) {
                ResponseEntity.status(response.statusCode)
                    .apply {
                        response.headers.contentType?.let { header("Content-Type", it.toString()) }
                    }
                    .body(response.body)
            } else {
                ResponseEntity.status(response.statusCode)
                    .apply {
                        response.body?.payload?.httpHeaders?.forEach {
                            header(it.key, it.value)
                        }
                    }
                    .body(response.body?.payload?.inputStream())
            }
        } catch (e: Exception) {
            return ResponseEntity.badRequest().body(e.message)
        }
    }
    /**
     * Construct a QueryMessage and forward it to the corresponding connector or the default Broker that is configured
     */
    @PostMapping("query")
    fun queryMessage(
        @RequestHeader(required = false) accept: String?,
        @RequestParam(required = false) connectorId: String?,
        @RequestParam(required = false) agentId: String?,
        @RequestParam(required = false) accessUrl: String?,
        @RequestParam(required = false) queryLanguage: String?,
        @RequestParam(required = false) queryScope: String?,
        @RequestParam(required = false) recipientScope: String?,
        @RequestBody query: String
    ): ResponseEntity<Any> {
        val brokerId = connectorId?.let { URI(it) } ?: brokerConfig.id
        val brokerParticipantId = agentId?.let { URI(it) } ?: brokerId
        val brokerAccessUrl = accessUrl ?: brokerConfig.address.toString()

        val queryMessage = QueryMessageBuilder()
            ._issuerConnector_(connectorInfo.idsid)
            ._recipientConnector_(brokerId)
            ._senderAgent_(connectorInfo.curator)
            ._recipientAgent_(brokerParticipantId)
            ._queryLanguage_(parseEnum<QueryLanguage>(queryLanguage) ?: QueryLanguage.SPARQL)
            ._queryScope_(parseEnum<QueryScope>(queryScope))
            ._recipientScope_(parseEnum<QueryTarget>(recipientScope) ?: QueryTarget.BROKER)
            .buildWithDefaults()

        val egressClearing = clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.EGRESS, queryMessage)

        val response = restTemplate.postForEntity(
            routeConfig.defaultEgressUrl,
            queryMessage.toMultiPartMessage(
                payload = query,
                httpHeaders = mapOf(
                    Constants.Headers.forwardTo to brokerAccessUrl,
                    Constants.Headers.authorization to "Bearer ${authManager.internalApiKey.key}"
                )
            ),
            MultiPartMessage::class.java)

        clearingLog.clearIds(ClearingContext.DESCRIPTION, ClearingDirection.INGRESS, response.body?.header, linkedClearing = egressClearing)

        return if (accept?.startsWith("multipart") == true) {
            ResponseEntity.status(response.statusCode)
                .apply {
                    response.headers.contentType?.let { header("Content-Type", it.toString()) }
                }
                .body(response.body)
        } else {
            ResponseEntity.status(response.statusCode)
                .apply {
                    response.body?.payload?.httpHeaders?.forEach {
                        header(it.key, it.value)
                    }
                }
                .body(response.body?.payload?.inputStream())
        }
    }

    private inline fun <reified T: Enum<T>> parseEnum(value: String?): T? {
        return value?.let {
            try {
                java.lang.Enum.valueOf(T::class.java, value)
            } catch (e: IllegalArgumentException) {
                null
            }
        }
    }
}
