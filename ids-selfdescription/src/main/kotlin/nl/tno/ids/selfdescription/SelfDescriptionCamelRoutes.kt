/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.selfdescription

import nl.tno.ids.configuration.infomodel.toJsonLD
import org.apache.camel.builder.RouteBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/** Static Self Description Camel routes */
@Configuration
class SelfDescriptionCamelRoutes(
    /** Self description generator */
    private val selfDescriptionGenerator: SelfDescriptionGenerator,
    /** Description Camel processor */
    private val descriptionProcessor: DescriptionProcessor,
    /** Camel port modifier */
    @Value("\${camel.port:8080}") private val camelPort: String,
    /** Camel HTTPS flag */
    @Value("\${routes.https:false}") private val https: Boolean
) {

    /** HTTP Mime Multipart Self Description route */
    @Bean
    @ConditionalOnProperty(
        prefix = "selfdescription", name = ["localEndpoint"], havingValue = "true", matchIfMissing = true)
    fun selfDescription(): RouteBuilder {
        return object : RouteBuilder() {
            override fun configure() {
                from("jetty:http${if (https) "s" else ""}://0.0.0.0:$camelPort/selfdescription?httpMethodRestrict=POST&enableMultipartFilter=false${if (https) "&sslContextParameters=#idsSSLContextParameters" else ""}")
                    .routeId("selfdescription")
                    .process("multiPartInputProcessor")
                    .process("dapsTokenVerifier")
                    .process(descriptionProcessor)
                    .process("dapsTokenInjector")
                    .process("multiPartOutputProcessor")
            }
        }
    }

    /**
     * Plain HTTP GET Self Description route
     *
     * Used only for debugging purposes
     */
    @Bean
    @ConditionalOnProperty(
        prefix = "selfdescription", name = ["localPlainEndpoint"], havingValue = "true", matchIfMissing = true)
    fun selfDescriptionPlain(): RouteBuilder {
        return object : RouteBuilder() {
            override fun configure() {
                from(
                    "jetty:http${if (https) "s" else ""}://0.0.0.0:$camelPort/selfdescription?httpMethodRestrict=GET&enableMultipartFilter=false${if (https) "&sslContextParameters=#idsSSLContextParameters" else ""}")
                    .routeId("selfdescriptionPlain")
                    .process { exchange ->
                        exchange.message.body = selfDescriptionGenerator.fullConnector.toJsonLD()
                        exchange.message.headers = mapOf("Content-Type" to "application/ld+json")
                    }
            }
        }
    }
}
