/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.selfdescription

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.VerificationException
import com.github.tomakehurst.wiremock.client.WireMock.*
import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.resourcemanager.ResourceManagerService
import nl.tno.ids.testextensions.DapsTestExtension
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.TestPropertySource
import java.math.BigInteger
import java.net.URI

/**
 * Broker Registration test with mocked Broker
 */
@SpringBootTest(classes = [BrokerRegistrationTest.Application::class])
@TestPropertySource(
    properties =
        [
            "spring.config.location = classpath:application.yaml",
            "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration"])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class BrokerRegistrationTest {
    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Broker registration service */
    @Autowired private lateinit var brokerRegistrationService: BrokerRegistrationService

    /** Resource manager service */
    @Autowired private lateinit var resourceManagerService: ResourceManagerService

    /** Verify initial self description publication */
    @Test
    @Order(0)
    fun testInitialSelfDescription() {
        Thread.sleep(2500)
        wireMock.verify(moreThanOrExactly(1), postRequestedFor(urlEqualTo("/https_out")))
        Assertions.assertTrue(brokerRegistrationService.registered)
    }

    /** Verify changed self description on resource change */
    @Test
    @Order(1)
    fun testChangedSelfDescription() {
        Thread.sleep(5000)
        resourceManagerService.addResource(
            "urn:ids:connector:catalog-1",
            DataResourceBuilder(URI("urn:ids:resources:resource1"))
                ._sovereign_(URI("urn:ids:test"))
                ._representation_(
                    RepresentationBuilder()
                        ._mediaType_(IANAMediaTypeBuilder()._filenameExtension_("csv").build())
                        ._instance_(
                            ArtifactBuilder()
                                ._byteSize_(BigInteger("7000"))
                                ._fileName_("lungPhotos-2020.csv")
                                ._creationDate_(DateUtil.now())
                                .build())
                        .build())
                ._title_(TypedLiteral("mnist", "en"))
                ._description_(TypedLiteral("(28,28,15000)", "SHAPE"))
                .build())
        Thread.sleep(1000)
        for (i in 0..20) {
            try {
                wireMock.verify(moreThanOrExactly(2), postRequestedFor(urlEqualTo("/https_out")))
                return
            } catch (e: VerificationException) {
                Thread.sleep(1000)
            }
        }
        wireMock.verify(moreThanOrExactly(2), postRequestedFor(urlEqualTo("/https_out")))
    }

    companion object {
        /** WireMock server */
        private lateinit var wireMock: WireMockServer

        /** Setup WireMock server with Broker response mocks */
        @JvmStatic
        @BeforeAll
        fun setupWireMock() {
            wireMock = WireMockServer(43391).apply { start() }

            val brokerMessage =
                MessageProcessedNotificationMessageBuilder()
                    ._issuerConnector_(URI("urn:ids:broker"))
                    ._recipientConnector_(arrayListOf(URI("urn:ids:connector")))
                    ._senderAgent_(URI("urn:ids:brokerParticipant"))
                    ._recipientAgent_(arrayListOf(URI("urn:ids:connectorParticipant")))
                    ._correlationMessage_(URI("urn:ids:unknown"))
                    .buildWithDefaults()
                    .toMultiPartMessage()
            wireMock.stubFor(
                post("/https_out")
                    .withHeader(Constants.Headers.authorization, containing("Bearer APIKEY-"))
                    .willReturn(
                        aResponse()
                            .withStatus(200)
                            .withBody(brokerMessage.toString())
                            .withHeader("Content-Type", brokerMessage.httpHeaders["Content-Type"])))
        }
    }
}
