plugins {
    kotlin("plugin.spring")
}

dependencies {
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")

    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation(project(":ids-security"))

    implementation(project(":ids-configuration"))
    implementation(project(":ids-clearing"))
    implementation(project(":ids-resource-manager"))

    // Spring Retry support
    implementation("org.springframework.retry:spring-retry:${LibraryVersions.Spring.retry}")
    implementation("org.springframework:spring-aspects:${LibraryVersions.Spring.framework}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.apache.camel.springboot:camel-jetty-starter")

    // Wiremock
    testImplementation("com.github.tomakehurst:wiremock-jre8:${LibraryVersions.Test.wiremock}")
    testImplementation("com.google.guava:guava:${LibraryVersions.Test.guava}")
    testImplementation(project(":ids-test-extensions"))
}