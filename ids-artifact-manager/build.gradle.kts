plugins {
    kotlin("plugin.spring")
}

dependencies {
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")

    implementation(project(":ids-security"))

    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("jakarta.servlet:jakarta.servlet-api:${LibraryVersions.javax}")

    implementation(project(":ids-configuration"))
    implementation(project(":ids-clearing"))
    implementation(project(":ids-resource-manager"))
    implementation(project(":ids-pef"))
    implementation(project(":ids-selfdescription"))

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.apache.camel:camel-test-spring-junit5")
    testImplementation("org.apache.camel.springboot:camel-jetty-starter")
    testImplementation(project(":camel-multipart-processor"))
    testImplementation(project(":ids-test-extensions"))
}