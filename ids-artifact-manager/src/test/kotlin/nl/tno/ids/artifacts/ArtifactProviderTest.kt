/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.artifacts

import de.fraunhofer.iais.eis.Artifact
import de.fraunhofer.iais.eis.DataResource
import nl.tno.ids.testextensions.DapsTestExtension
import org.apache.camel.builder.RouteBuilder
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.ComponentScan
import org.springframework.core.io.ByteArrayResource
import org.springframework.http.RequestEntity
import org.springframework.test.context.TestPropertySource
import org.springframework.util.LinkedMultiValueMap
import org.springframework.web.util.UriUtils
import java.io.File
import java.nio.charset.Charset

/**
 * Test clas for artifact provider controller
 */
@SpringBootTest(
    classes = [ArtifactProviderTest.Application::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
    properties =
        [
            "spring.config.location = classpath:application.yaml",
            "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration",
            "artifacts.location=./resources"])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class ArtifactProviderTest {
    /** Spring RANDOM_PORT injection */
    @LocalServerPort private var port: Int = 0

    /** Application Context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Spring TestRestTemplate for creating API requests */
    @Autowired private lateinit var restTemplate: TestRestTemplate
    /** Artifact configuration */
    @Autowired private lateinit var artifactConfiguration: ArtifactConfiguration
    /** Artifact Routebuilder, only to make sure it is loaded */
    @Qualifier("artifactRoute") @Autowired private lateinit var artifactRoute: RouteBuilder

    /** Add an artifact via the API */
    @Test
    @Order(0)
    fun addResource() {
        val response =
            restTemplate.exchange(
                RequestEntity.post("http://localhost:$port/api/artifacts/provider")
                    .body(
                        LinkedMultiValueMap<String, Any>().apply {
                            add(
                                "artifact",
                                FileNameAwareByteArrayResource(
                                    "testartifact.txt", "Test Artifact".toByteArray()))
                            add("title", "Test Artifact")
                            add("description", "Unit Test Artifact")
                        }),
                DataResource::class.java)

        Assertions.assertTrue(response.statusCodeValue in 200..299)

        dataResource = response.body
        Assertions.assertEquals("Test Artifact", dataResource!!.title[0].value)
        Assertions.assertEquals(
            13, (dataResource!!.representation[0].instance[0] as Artifact).byteSize.toInt())
    }

    /** Update the artifact via the API */
    @Test
    @Order(1)
    fun updateArtifact() {
        val response =
            restTemplate.exchange(
                RequestEntity.put(
                    "http://localhost:$port/api/artifacts/provider/${UriUtils.encode(dataResource!!.id.toString(), Charset.defaultCharset())
                    }")
                    .body(
                        LinkedMultiValueMap<String, Any>().apply {
                            add(
                                "artifact",
                                FileNameAwareByteArrayResource(
                                    "testartifact.txt", "Test Artifact Update".toByteArray()))
                        }),
                DataResource::class.java)

        Assertions.assertTrue(response.statusCodeValue in 200..299)
        dataResource = response.body
        Assertions.assertEquals("Test Artifact", dataResource!!.title[0].value)
        Assertions.assertEquals(
            20, (dataResource!!.representation[0].instance[0] as Artifact).byteSize.toInt())
    }

    /** Retrieve the artifacts metadata */
    @Test
    @Order(2)
    fun getArtifactMetadata() {
        val response =
            restTemplate.exchange(
                RequestEntity.get(
                    "http://localhost:$port/api/artifacts/provider/${UriUtils.encode(dataResource!!.id.toString(), Charset.defaultCharset())
                    }").build(),
                DataResource::class.java)

        Assertions.assertTrue(response.statusCodeValue in 200..299)
        dataResource = response.body
        Assertions.assertEquals("Test Artifact", dataResource!!.title[0].value)
        Assertions.assertEquals(
            20, (dataResource!!.representation[0].instance[0] as Artifact).byteSize.toInt())
    }

    /** Retrieve the actual artifact data */
    @Test
    @Order(3)
    fun getArtifactData() {
        val response =
            restTemplate.exchange(
                RequestEntity.get(
                    "http://localhost:$port/api/artifacts/provider/${UriUtils.encode(dataResource!!.id.toString(), Charset.defaultCharset())
                    }/data").build(),
                String::class.java)

        Assertions.assertTrue(response.statusCodeValue in 200..299)

        Assertions.assertEquals("attachment; filename=\"testartifact.txt\"", response.headers["Content-Disposition"]?.first())
        Assertions.assertEquals("Test Artifact Update", response.body)
    }

    /** Clean-up all artifacts */
    @Test
    @Order(Int.MAX_VALUE)
    fun cleanUpResources() {
        File(artifactConfiguration.location).deleteRecursively()
    }

    companion object {
        private var dataResource: DataResource? = null

        @JvmStatic
        @BeforeAll
        fun encodedSlash() {
            System.setProperty("org.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH", "true")
        }
    }
}

/**
 * Helper class for ByteArray resources with configured filename
 */
class FileNameAwareByteArrayResource(
    private val fileName: String,
    byteArray: ByteArray,
    description: String? = null
) : ByteArrayResource(byteArray, description) {
    override fun getFilename(): String {
        return fileName
    }
}
