/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.artifacts

import de.fraunhofer.iais.eis.ArtifactRequestMessageBuilder
import de.fraunhofer.iais.eis.DataResource
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.testextensions.DapsTestExtension
import org.apache.camel.builder.RouteBuilder
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.ComponentScan
import org.springframework.http.RequestEntity
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.TestPropertySource
import org.springframework.util.LinkedMultiValueMap
import java.io.File
import java.net.URI
import java.util.*

/**
 * Test class for artifact consumption
 */
@SpringBootTest(
    classes = [ArtifactConsumerTest.Application::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
    properties =
        [
            "spring.config.location = classpath:application.yaml",
            "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration",
            "artifacts.location=./resources",
            "camel.port=48211"
        ])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_CLASS)
@ExtendWith(DapsTestExtension::class)
class ArtifactConsumerTest {
    /** Spring RANDOM_PORT injection */
    @LocalServerPort private var port: Int = 0

    /** Application Context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Spring TestRestTemplate for creating API requests */
    @Autowired private lateinit var restTemplate: TestRestTemplate
    /** Artifact configuration */
    @Autowired private lateinit var artifactConfiguration: ArtifactConfiguration
    /** Artifact Routebuilder, only to make sure it is loaded */
    @Qualifier("artifactRoute") @Autowired private lateinit var artifactRoute: RouteBuilder

    /** Initialize the artifact manager with an artifact */
    @Test
    @Order(0)
    fun addResource() {
        dataResource = restTemplate.exchange(
            RequestEntity.post("http://localhost:$port/api/artifacts/provider")
                .body(
                    LinkedMultiValueMap<String, Any>().apply {
                        add(
                            "artifact",
                            FileNameAwareByteArrayResource(
                                "testartifact.txt", "Test Artifact".repeat(1000).toByteArray()))
                        add("title", "Test Artifact")
                        add("description", "Unit Test Artifact")
                    }),
            DataResource::class.java).body
        Assertions.assertNotNull(dataResource)
    }

    @Test
    @Order(1)
    fun validateEncryptionAtRest() {
        val storedFile = File("./resources/${dataResource!!.id.toString().replace("\\W+".toRegex(), "")}/data").readBytes().decodeToString()
        Assertions.assertNotEquals("Test Artifact", storedFile)
    }

    /** Retrieve the resource directly on the camel route */
    @Test
    @Order(2)
    fun retrieveResource() {
        val response = restTemplate.postForEntity(
            "http://localhost:48211/router/artifacts",
            ArtifactRequestMessageBuilder()
                ._issuerConnector_(URI("urn:ids:test"))
                ._recipientConnector_(URI("urn:ids:test"))
                ._senderAgent_(URI("urn:ids:test"))
                ._recipientAgent_(URI("urn:ids:test"))
                ._requestedArtifact_(dataResource!!.id)
                .buildWithDefaults()
                .toMultiPartMessage(),
            MultiPartMessage::class.java
        )
        Assertions.assertEquals(Base64.getEncoder().encodeToString("Test Artifact".repeat(1000).toByteArray()), response.body?.payload?.asString())
    }

    /** Clean-up all artifacts */
    @Test
    @Order(Int.MAX_VALUE)
    fun cleanUpResources() {
        File(artifactConfiguration.location).deleteRecursively()
    }

    companion object {
        private var dataResource: DataResource? = null

        @JvmStatic
        @BeforeAll
        fun encodedSlash() {
            System.setProperty("org.apache.tomcat.util.buf.UDecoder.ALLOW_ENCODED_SLASH", "true")
        }
    }
}
