/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.artifacts

import de.fraunhofer.iais.eis.*
import nl.tno.ids.clearing.*
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildAsResponseTo
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.selfdescription.DescriptionProcessor
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

/** Camel Artifact processor, handles Artifact requests inside an Camel Processor */
@Component("artifactProcessor")
class ArtifactProcessor(
    /** Artifact Manager Service for handling artifact IO */
    private val artifactManagerService: ArtifactManagerService,
    /** Connector Info for filling in the right metadata */
    private val connectorInfo: ConnectorInfo,
    /** Description Processor for handling Description requests */
    private val descriptionProcessor: DescriptionProcessor,
    /** Local clearing handler */
    private val clearingLog: ClearingLog,
    /** Audit log service */
    private val auditLog: AuditLog
) : Processor {
    companion object {
        private val LOG = LoggerFactory.getLogger(ArtifactProcessor::class.java)
    }

    /** Process function that handles ArtifactRequestMessages */
    override fun process(exchange: Exchange) {
        val idsHeader =
            exchange.message.getHeader(Constants.Headers.idsHeader, String::class.java)?.let {
                Serialization.fromJsonLD<Message>(it)
            }
                ?: run {
                    LOG.info("No IDS-Header found!")
                    setRejectionMessage(exchange, null, RejectionReason.MALFORMED_MESSAGE)
                    return
                }
        when(idsHeader) {
            is ArtifactRequestMessage -> processArtifactRequest(idsHeader, exchange)
            is DescriptionRequestMessage -> descriptionProcessor.process(exchange)
            is RequestMessageImpl -> processRequestMessage(idsHeader, exchange)
            else -> {
                val ingressClearing = clearingLog.clearIds(ClearingContext.ARTIFACT, ClearingDirection.INGRESS, idsHeader)
                LOG.info("Artifact Processor is only capable of handling ArtifactRequestMessages or offloading to the DescriptionProcessor")
                setRejectionMessage(exchange, idsHeader, RejectionReason.MESSAGE_TYPE_NOT_SUPPORTED, ingressClearing)
            }
        }
    }

    /** Process ArtifactRequestMessage */
    private fun processArtifactRequest(
        idsHeader: ArtifactRequestMessage,
        exchange: Exchange
    ) {
        val ingressClearing = clearingLog.clearIds(
            ClearingContext.ARTIFACT,
            ClearingDirection.INGRESS,
            idsHeader,
            properties = mapOf("artifactId" to idsHeader.requestedArtifact.toString())
        )
        fetchArtifact(idsHeader.requestedArtifact, idsHeader, exchange, ingressClearing)
    }

    /** Process RequestMessage, introduced for compatibility with the DSC */
    private fun processRequestMessage(
        idsHeader: RequestMessageImpl,
        exchange: Exchange
    ) {
        val target = idsHeader.properties.filterKeys { it.contains("target") }.values.filterIsInstance(Map::class.java)
            .firstOrNull()?.values?.firstOrNull() as String?
        val ingressClearing = clearingLog.clearIds(
            ClearingContext.ARTIFACT,
            ClearingDirection.INGRESS,
            idsHeader,
            properties = mapOf("artifactId" to target)
        )
        target?.let {
            fetchArtifact(URI(target), idsHeader, exchange, ingressClearing)
        } ?: setRejectionMessage(exchange, idsHeader, RejectionReason.MALFORMED_MESSAGE, ingressClearing)
    }

    /** Fetch the artifact from the manager service and construct IDS response message */
    private fun fetchArtifact(
        target: URI,
        idsHeader: Message,
        exchange: Exchange,
        ingressClearing: ClearingMessage
    ) = artifactManagerService.getArtifactData(target)?.let { inputStream ->
        auditLog.logDataAccess(
            idsHeader.issuerConnector.toString(),
            idsHeader.recipientConnector.first().toString(),
            idsHeader.senderAgent?.toString(),
            idsHeader.recipientAgent?.firstOrNull()?.toString(),
            idsHeader.transferContract?.toString(),
            target.toString()
        )
        val contentType =
            artifactManagerService
                .getArtifactMetadataFromArtifact(target)
                ?.representation
                ?.firstOrNull()
                ?.mediaType
                ?.filenameExtension
                ?.let { Files.probeContentType(Paths.get("file.$it")) }
                ?: "text/plain"
        val responseHeader = ArtifactResponseMessageBuilder()
            .buildAsResponseTo(idsHeader, connectorInfo)
        exchange.message.removeHeaders("*")
        exchange.message.setHeader(Constants.Headers.idsHeader, responseHeader.toJsonLD())
        exchange.message.setHeader("Content-Type", contentType)
        exchange.message.body = Base64.getEncoder().encodeToString(inputStream.readAllBytes())
        clearingLog.clearIds(
            ClearingContext.ARTIFACT,
            ClearingDirection.EGRESS,
            responseHeader,
            linkedClearing = ingressClearing,
            properties = mapOf("artifactId" to target.toString())
        )
    } ?: run {
        LOG.info("Artifact $target can't be found")
        setRejectionMessage(exchange, idsHeader, RejectionReason.NOT_FOUND, ingressClearing)
    }

    /** Set the output of the exchange to a RejectionMessage with the given reason */
    private fun setRejectionMessage(
        exchange: Exchange,
        header: Message?,
        rejectionReason: RejectionReason,
        clearingMessage: ClearingMessage? = null
    ) {
        val responseHeader = RejectionMessageBuilder()
            ._rejectionReason_(rejectionReason)
            .buildAsResponseTo(header, connectorInfo)
        exchange.message.removeHeaders("*")
        exchange.message.setHeader(Constants.Headers.idsHeader, responseHeader.toJsonLD())
        clearingLog.clearIds(ClearingContext.ARTIFACT, ClearingDirection.EGRESS, responseHeader, linkedClearing = clearingMessage, properties = clearingMessage?.properties)
    }
}
