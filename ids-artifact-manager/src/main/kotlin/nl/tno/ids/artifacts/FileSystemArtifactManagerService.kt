/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.artifacts

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.KeystoreConfig
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.pef.pap.PolicyAdministrationPoint
import nl.tno.ids.resourcemanager.ResourceManagerService
import org.apache.commons.io.FileUtils
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.stereotype.Service
import org.springframework.web.util.UriUtils
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.math.BigInteger
import java.net.URI
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import javax.crypto.Cipher
import javax.crypto.CipherInputStream
import javax.crypto.spec.SecretKeySpec




/**
 * File-System Artifact Manager Service, uses the local file system for persisting Artifacts.
 *
 * Stores the content of an artifact in a "data" file and the metadata in a "metadata.json" file on
 * a folder created for each artifact
 */
@Service
@ConditionalOnProperty(name = ["artifacts.enabled"], havingValue = "true", matchIfMissing = true)
class FileSystemArtifactManagerService(
    /** Connector Info used for metadata and prefixes */
    private val connectorInfo: ConnectorInfo,
    /** Artifact Configuration, providing the used location on the file system */
    private val artifactConfiguration: ArtifactConfiguration,
    /** Resource Manager Service for updating the resource metadata as used in self descriptions */
    private val resourceManagerService: ResourceManagerService,
    /** Policy Administration Point, for storing contract offers for artifacts */
    private val policyAdministrationPoint: PolicyAdministrationPoint,
    /** Keystore configuration for encryption at rest */
    private val keystoreConfig: KeystoreConfig
) : ArtifactManagerService {
    /** URI delimiter for new Artifact identifiers, depends on the structure of the IDS ID */
    private val uriDelimiter = if (connectorInfo.idsid.scheme == "urn") ":" else "/"

    /** Resource Catalog ID as used in the Resource Manager Serivce */
    private val catalogId = "${connectorInfo.idsid}${uriDelimiter}resources"

    /** Cache of Artifact metadata files, for quick retrieval */
    private val artifactMetadataCache: MutableMap<String, DataResource> = ConcurrentHashMap()

    private val encryptionKey: SecretKeySpec?

    init {
        readExistingState()
        encryptionKey = if (artifactConfiguration.encryptAtRest) {
            val decodedEncryptionKey = Base64.getDecoder().decode(artifactConfiguration.aesEncryptionKey)
            SecretKeySpec(decodedEncryptionKey, "AES")
        } else {
            null
        }
    }

    /** Read the existing file structure on the given location to synchronize an earlier state */
    private fun readExistingState() {
        File(artifactConfiguration.location)
            .listFiles(File::isDirectory)
            ?.mapNotNull {
                if (File(it, "metadata.json").exists()) {
                    Serialization.fromJsonLD<DataResource>(File(it, "metadata.json").readText())
                } else {
                    null
                }
            }
            ?.let { resources ->
                resources.forEach {
                    it.contractOffer?.forEach { contractOffer ->
                        policyAdministrationPoint.insertOffer(contractOffer)
                    }
                }
                resourceManagerService.replaceResource(catalogId, ArrayList(resources), true)
                resources.forEach { artifactMetadataCache[it.id.toString()] = it }
            }
    }

    /** Get metadata of all artifacts */
    override fun getArtifacts(): Collection<DataResource> {
        return artifactMetadataCache.values
    }

    /** Get artifact metadata */
    override fun getArtifactMetadata(resourceId: String): DataResource? {
        return artifactMetadataCache[resourceId]
    }

    /** Get artifact metadata */
    override fun getArtifactMetadata(resourceId: URI): DataResource? {
        return getArtifactMetadata(resourceId.toString())
    }

    /** Get artifact metadata */
    override fun getArtifactMetadataFromArtifact(artifactId: String): DataResource? {
        return artifactMetadataCache.values.find { resource ->
            resource.representation.any { representation ->
                representation.instance.any { instance -> instance.id.toString() == artifactId }
            }
        } ?: artifactMetadataCache[artifactId]
    }

    /** Get artifact metadata */
    override fun getArtifactMetadataFromArtifact(artifactId: URI): DataResource? {
        return getArtifactMetadataFromArtifact(artifactId.toString())
    }

    /** Get the artifact contents as InputStream */
    override fun getArtifactData(artifactId: String): InputStream? {
        val resource =
            artifactMetadataCache.values.find { resource ->
                resource.representation.any { representation ->
                    representation.instance.any { instance -> instance.id.toString() == artifactId }
                }
            }
                ?: artifactMetadataCache[artifactId]
                ?: return null

        val dataFile =
            File(
                "${artifactConfiguration.location}/${resource.id.toString().replace("\\W+".toRegex(), "")}/data")
        return if (dataFile.exists()) {
            if (artifactConfiguration.encryptAtRest) {
                val decryptCipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
                decryptCipher.init(Cipher.DECRYPT_MODE, encryptionKey)
                CipherInputStream(dataFile.inputStream(), decryptCipher)
            } else {
                dataFile.inputStream()
            }
        } else {
            null
        }
    }

    /** Get the artifact contents as InputStream */
    override fun getArtifactData(artifactId: URI): InputStream? {
        return getArtifactData(artifactId.toString())
    }

    /** Scope a ContractOffer by setting the target field in all the rules */
    private fun scopeContractOffer(contractOffer: ContractOffer, artifactId: String) {
        (contractOffer.permission + contractOffer.prohibition + contractOffer.obligation).forEach {
            rule ->
            if (rule.target == null) {
                rule.target = URI(artifactId)
            }
        }
    }

    /** Add a new artifact */
    override fun addArtifact(
        artifactId: String?,
        filename: String,
        data: InputStream,
        title: String,
        description: String,
        contractOffer: ContractOffer?
    ): DataResource {
        try {
            if (artifactId != null && artifactMetadataCache.containsKey(artifactId)) {
                throw ArtifactException("Artifact with id $artifactId already exists")
            }
            val resourceId =
                artifactId
                    ?: "${connectorInfo.idsid}${uriDelimiter}resources${uriDelimiter}${UUID.randomUUID()}"
            val artifactFolder =
                File(
                        "${artifactConfiguration.location}/${resourceId.replace("\\W+".toRegex(), "")}")
                    .apply { mkdirs() }
            val byteSize = if (artifactConfiguration.encryptAtRest) {
                val streamLength = data.available()
                val encryptCipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
                encryptCipher.init(Cipher.ENCRYPT_MODE, encryptionKey)
                val cipherInputStream = CipherInputStream(data, encryptCipher)
                FileUtils.copyInputStreamToFile(cipherInputStream, File(artifactFolder, "data"))
                streamLength.toBigInteger()
            } else {
                FileUtils.copyInputStreamToFile(data, File(artifactFolder, "data"))
                File(artifactFolder, "data").length().toBigInteger()
            }

            return persistMetadata(
                artifactFolder,
                resourceId,
                null,
                byteSize,
                filename,
                title,
                description,
                contractOffer,
                true)
        } catch (e: IOException) {
            throw ArtifactException("IO Exception while handling Artifact", e)
        }
    }

    /** Update an existing artifact */
    override fun updateArtifact(
        artifactId: String,
        filename: String?,
        data: InputStream?,
        title: String?,
        description: String?,
        contractOffer: ContractOffer?
    ): DataResource {
        try {
            val artifactFolder =
                File(
                    "${artifactConfiguration.location}/${artifactId.replace("\\W+".toRegex(), "")}")
            if (!artifactFolder.exists()) {
                throw ArtifactException(
                    "Artifact with identifier $artifactId does not exist on the filesystem")
            }
            val originalResource = Serialization.fromJsonLD<DataResourceImpl>(File(artifactFolder, "metadata.json").readText())
            val newByteSize = data?.let {
                if (artifactConfiguration.encryptAtRest) {
                    val streamLength = data.available()
                    val encryptCipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
                    encryptCipher.init(Cipher.ENCRYPT_MODE, encryptionKey)
                    val cipherInputStream = CipherInputStream(data, encryptCipher)
                    FileUtils.copyInputStreamToFile(cipherInputStream, File(artifactFolder, "data"))
                    streamLength.toBigInteger()
                } else {
                    FileUtils.copyInputStreamToFile(data, File(artifactFolder, "data"))
                    File(artifactFolder, "data").length().toBigInteger()
                }
            } ?: (originalResource.representation.firstOrNull()?.instance?.firstOrNull() as
                    ArtifactImpl?)
                ?.byteSize
            ?: BigInteger("-1")
            val newTitle = title ?: originalResource.title.firstOrNull()?.value ?: artifactId
            val newDescription =
                description ?: originalResource.description.firstOrNull()?.value ?: artifactId

            val newFileName =
                filename
                    ?: (originalResource.representation.firstOrNull()?.instance?.firstOrNull() as
                            ArtifactImpl?)
                        ?.fileName
                        ?: "unknown"

            val newVersion = try {
                originalResource.version?.toInt()?.let {i -> "${i+1}"} ?: "1"
            } catch (e: NumberFormatException) {
                "1"
            }

            if (contractOffer != null) {
                originalResource.contractOffer.firstOrNull()?.let {
                    policyAdministrationPoint.deleteOffer(it.id.toString())
                }
            }

            return persistMetadata(
                artifactFolder,
                originalResource.id.toString(),
                originalResource
                    .representation
                    .firstOrNull()
                    ?.instance
                    ?.firstOrNull()
                    ?.id
                    .toString(),
                newByteSize,
                newFileName,
                newTitle,
                newDescription,
                contractOffer ?: originalResource.contractOffer.firstOrNull(),
                contractOffer != null,
                newVersion
            )
        } catch (e: IOException) {
            throw ArtifactException("IO Exception while handling Artifact", e)
        }
    }

    /**
     * Persist metadata to the file storage and share the Resource metadata with the
     * ResourceManagerService
     */
    private fun persistMetadata(
        artifactFolder: File,
        resourceId: String,
        artifactId: String?,
        byteSize: BigInteger,
        filename: String,
        title: String,
        description: String,
        contractOffer: ContractOffer?,
        persistOffer: Boolean,
        version: String = "1",
        keywords: List<String> = emptyList(),
        license: String = "http://www.apache.org/licenses/LICENSE-2.0"
    ): DataResource {
        val artifact =
            ArtifactBuilder(
                    URI.create(
                        artifactId
                            ?: "${connectorInfo.idsid}${uriDelimiter}artifacts${uriDelimiter}${UUID.randomUUID()}"))
                ._byteSize_(byteSize)
                ._fileName_(filename)
                .build()
        if (persistOffer && contractOffer != null) {
            scopeContractOffer(contractOffer, artifact.id.toString())
            policyAdministrationPoint.insertOffer(contractOffer)
        }
        val fileType = "https://www.iana.org/assignments/media-types/${Files.probeContentType(Paths.get("./${filename}"))}"
        val dataResource =
            DataResourceBuilder(URI(resourceId))
                ._sovereign_(connectorInfo.curator)
                ._title_(TypedLiteral(title, "en"))
                ._description_(TypedLiteral(description, "en"))
                ._keyword_(keywords.ifEmpty { description.split(" ") }.map { TypedLiteral(it.lowercase(Locale.getDefault()), "en") })
                ._version_(version)
                ._publisher_(connectorInfo.curator)
                ._standardLicense_(URI(license))
                ._language_(Language.EN)
                ._created_(DateUtil.now())
                ._resourceEndpoint_(
                    ConnectorEndpointBuilder()
                        ._accessURL_(URI.create("http://internal"))
                        ._path_(
                            "/artifacts/${UriUtils.encode(resourceId, Charset.defaultCharset())}")
                        .build())
                ._representation_(
                    RepresentationBuilder()
                        ._mediaType_(
                            IANAMediaTypeBuilder(URI(fileType))
                                ._filenameExtension_(filename.substringAfterLast('.'))
                                .build())
                        ._instance_(artifact)
                        .build())
                .build()
        contractOffer?.let { dataResource.contractOffer = arrayListOf(contractOffer) }
        FileUtils.writeStringToFile(
            File(artifactFolder, "metadata.json"),
            dataResource.toJsonLD(),
            Charset.defaultCharset())

        resourceManagerService.replaceResource(catalogId, dataResource)
        artifactMetadataCache[resourceId] = dataResource
        return dataResource
    }

    /** Delete an artifact, also in the PolicyAdministrationPoint and ResourceManagerService */
    override fun deleteArtifact(artifactId: String): Boolean {
        val artifactIdStripped = artifactId.replace("\\W+".toRegex(), "")
        val artifactFolder = File("${artifactConfiguration.location}/${artifactIdStripped}")
        if (artifactFolder.exists()) {
            artifactFolder.deleteRecursively()
        }
        artifactMetadataCache.remove(artifactId)?.contractOffer?.firstOrNull()?.let {
            policyAdministrationPoint.deleteOffer(it.id.toString())
        }
        return resourceManagerService.deleteResource(catalogId, artifactId)
    }
}
