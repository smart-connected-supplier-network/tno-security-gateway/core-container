/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.artifacts

import de.fraunhofer.iais.eis.Artifact
import de.fraunhofer.iais.eis.ContractOffer
import de.fraunhofer.iais.eis.DataResource
import nl.tno.ids.configuration.model.SecurityRoles
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.InputStreamResource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.util.UriUtils
import java.nio.charset.Charset

/**
 * Artifact Provider Controller, REST controller for interacting with an ArtifactManagerService.
 * Intended for internal use only and not for inter-connector communication
 */
@RestController
@RequestMapping("api/artifacts/provider")
@Secured(SecurityRoles.ARTIFACT_PROVIDER_MANAGER)
class ArtifactProviderController {
    /**
     * Optional ArtifactManagerService, is optional as this means the API remains the same
     * regardless of the presence of an ArtifactManagerService
     */
    @Autowired(required = false) private var artifactManagerService: ArtifactManagerService? = null

    /** Get metadata of all artifacts */
    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    @Secured(SecurityRoles.ARTIFACT_PROVIDER_READER)
    fun getArtifacts(): Collection<DataResource> {
        return artifactManagerService?.getArtifacts()
            ?: throw ResponseStatusException(
                HttpStatus.METHOD_NOT_ALLOWED, "Artifact Manager Service is disabled")
    }

    /** Add a new artifact */
    @PostMapping(
        consumes = [MediaType.MULTIPART_FORM_DATA_VALUE], produces = ["application/ld+json"])
    fun addArtifact(
        @RequestPart artifact: MultipartFile,
        @RequestPart title: String,
        @RequestPart description: String,
        @RequestPart(required = false) artifactId: String?,
        @RequestPart(required = false) contractOffer: ContractOffer?
    ): DataResource {
        return artifactManagerService?.run {
            addArtifact(
                artifactId = artifactId,
                filename = artifact.originalFilename ?: "artifact.txt",
                data = artifact.inputStream,
                title = title,
                description = description,
                contractOffer = contractOffer)
        }
            ?: throw ResponseStatusException(
                HttpStatus.METHOD_NOT_ALLOWED, "Artifact Manager Service is disabled")
    }

    /** Retrieve artifact metadata */
    @GetMapping(
        path = ["{artifactId}"],
        produces = ["application/ld+json"]
    )
    fun getArtifactMetadata(@PathVariable artifactId: String): DataResource {
        return artifactManagerService?.run {
            val parsedArtifactId = UriUtils.decode(artifactId, Charset.defaultCharset())
            getArtifactMetadataFromArtifact(parsedArtifactId)
                ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Artifact $parsedArtifactId not found")
        }
            ?: throw ResponseStatusException(
                HttpStatus.METHOD_NOT_ALLOWED, "Artifact Manager Service is disabled")
    }

    /** Retrieve artifact data */
    @GetMapping(
        path = ["{artifactId}/data"],
        produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE]
    )
    fun getArtifactData(@PathVariable artifactId: String): ResponseEntity<InputStreamResource> {
        return artifactManagerService?.run {
            val parsedArtifactId = UriUtils.decode(artifactId, Charset.defaultCharset())
            getArtifactData(parsedArtifactId)?.let {
                val metadata = getArtifactMetadataFromArtifact(parsedArtifactId) ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Artifact $artifactId not found")
                val filename = (metadata.representation.first().instance.first() as Artifact).fileName
                ResponseEntity.ok()
                    .header("Content-Disposition", "attachment; filename=\"$filename\"")
                    .body(InputStreamResource(it))
            } ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "Artifact $parsedArtifactId not found")
        }
            ?: throw ResponseStatusException(
                HttpStatus.METHOD_NOT_ALLOWED, "Artifact Manager Service is disabled")
    }

    /** Update an existing artifact */
    @PutMapping(
        path = ["{artifactId}"],
        consumes = [MediaType.MULTIPART_FORM_DATA_VALUE],
        produces = ["application/ld+json"])
    fun updateArtifact(
        @PathVariable artifactId: String,
        @RequestPart(required = false) artifact: MultipartFile?,
        @RequestPart(required = false) title: String?,
        @RequestPart(required = false) description: String?,
        @RequestPart(required = false) contractOffer: ContractOffer?
    ): DataResource {
        return artifactManagerService?.run {
            updateArtifact(
                artifactId = UriUtils.decode(artifactId, Charset.defaultCharset()),
                filename = artifact?.originalFilename,
                data = artifact?.inputStream,
                title = title,
                description = description,
                contractOffer = contractOffer
            )
        }
            ?: throw ResponseStatusException(
                HttpStatus.METHOD_NOT_ALLOWED, "Artifact Manager Service is disabled")
    }

    /** Delete an artifact */
    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @DeleteMapping(path = ["{artifactId}"])
    fun deleteArtifact(@PathVariable artifactId: String): ResponseEntity<String> {
        return artifactManagerService?.run {
            if (deleteArtifact(UriUtils.decode(artifactId, Charset.defaultCharset()))) {
                ResponseEntity.ok().build()
            } else {
                ResponseEntity.notFound().build()
            }
        }
            ?: throw ResponseStatusException(
                HttpStatus.METHOD_NOT_ALLOWED, "Artifact Manager Service is disabled")
    }
}
