/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.artifacts

import de.fraunhofer.iais.eis.ContractOffer
import de.fraunhofer.iais.eis.DataResource
import java.io.InputStream
import java.net.URI

/** Artifact Exception, for encapsulating errors related to Artifact handling */
class ArtifactException : Exception {
    constructor(s: String) : super(s)
    constructor(s: String, t: Throwable) : super(s, t)
}

/**
 * Artifact Manager Service interface for generic IO interaction with an artifact manager service
 */
interface ArtifactManagerService {
    /** Get metadata of all artifacts */
    fun getArtifacts(): Collection<DataResource>

    /** Get artifact metadata, based on the non-URI identifier of the resource */
    fun getArtifactMetadata(resourceId: String): DataResource?

    /** Get artifact metadata, based on the URI identifier of the resource */
    fun getArtifactMetadata(resourceId: URI): DataResource?

    /** Get artifact metadata, based on the non-URI identifier of the artifact */
    fun getArtifactMetadataFromArtifact(artifactId: String): DataResource?

    /** Get artifact metadata, based on the URI identifier of the artifact */
    fun getArtifactMetadataFromArtifact(artifactId: URI): DataResource?

    /** Get artifact data as InputStream, based on the non-URI identifier of the resource */
    fun getArtifactData(artifactId: URI): InputStream?

    /** Get artifact data as InputStream, based on the URI identifier of the resource */
    fun getArtifactData(artifactId: String): InputStream?

    /** Add a new artifact */
    fun addArtifact(
        artifactId: String? = null,
        filename: String,
        data: InputStream,
        title: String,
        description: String,
        contractOffer: ContractOffer? = null
    ): DataResource

    /** Update an existing artifact */
    fun updateArtifact(
        artifactId: String,
        filename: String? = null,
        data: InputStream? = null,
        title: String? = null,
        description: String? = null,
        contractOffer: ContractOffer? = null
    ): DataResource

    /** Delete an artifact */
    fun deleteArtifact(artifactId: String): Boolean
}
