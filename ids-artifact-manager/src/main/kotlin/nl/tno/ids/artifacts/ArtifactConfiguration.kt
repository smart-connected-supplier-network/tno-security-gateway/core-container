/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.artifacts

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/** Artifact Configuration */
@Component
@ConfigurationProperties(prefix = "artifacts")
data class ArtifactConfiguration(
    /** Whether automatic artifact handling should be enabled */
    var enabled: Boolean = true,
    /** File location where artifacts will be stored */
    var location: String = "/resources",
    /** Whether DAPS should be enabled for artifacts */
    var dapsValidation: Boolean = true,
    /** Whether policy enforcement should be enabled for artifacts */
    var policyEnforcement: Boolean = true,
    /** Whether encryption at rest should be anabled for artifacts */
    var encryptAtRest: Boolean = true,
    /** AES encryption key, Base64 encoded */
    var aesEncryptionKey: String = "7syl0HCT+Bz8qTEssWupjyjgC9JGFEyxHwS4nNrl1Gw="
)
