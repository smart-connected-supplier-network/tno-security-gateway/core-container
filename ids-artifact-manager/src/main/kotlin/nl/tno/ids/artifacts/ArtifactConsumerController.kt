/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.artifacts

import de.fraunhofer.iais.eis.*
import nl.tno.ids.clearing.ClearingContext
import nl.tno.ids.clearing.ClearingDirection
import nl.tno.ids.clearing.ClearingLog
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildAsResponseTo
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.RouteConfig
import nl.tno.ids.configuration.model.SecurityRoles
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.pef.pap.PolicyAdministrationPoint
import nl.tno.ids.security.AuthManager
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.net.URI
import java.util.*

data class ErrorMessage(
    val status: String,
    val location: String,
    val reason: String,
    val explanation: String? = null
)

@RestController
@RequestMapping("api/artifacts/consumer")
class ArtifactConsumerController(
    /** Connector Info for filling in the right metadata */
    private val connectorInfo: ConnectorInfo,
    /** Autowired RestTemplate for making REST calls */
    private val restTemplate: RestTemplate,
    /** Policy Administration Point for storing agreed contracts */
    private val policyAdministrationPoint: PolicyAdministrationPoint,
    /** Auth manager for internal API key */
    private val authManager: AuthManager,
    /** Route config for finding the default egress */
    private val routeConfig: RouteConfig,
    /** Local clearing handler */
    private val clearingLog: ClearingLog
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(ArtifactConsumerController::class.java)
    }

    /** Artifact Manager Service for managing artifact storage & retrieval */
    @Autowired(required = false) private var artifactManagerService: ArtifactManagerService? = null
    /**
     * Retrieve artifact from a remote connector
     * Builds an ArtifactRequest IDS message to request an artifact from a remote connector with a known access url
     * with the specified connector Id. The respective remote IDS participant can optionally be specified.
     * An identifier of a contract agreement can also optionally be specified.
     */
    @GetMapping("artifact")
    @Secured(SecurityRoles.ARTIFACT_CONSUMER)
    fun retrieveArtifact(
        @RequestParam artifact: String,
        @RequestParam connectorId: String,
        @RequestParam(required = false) agentId: String?,
        @RequestParam accessUrl: String,
        @RequestParam(required = false) transferContract: String?
    ): ResponseEntity<Any> {
        val artifactRequestMessage =
            ArtifactRequestMessageBuilder()
                ._issuerConnector_(connectorInfo.idsid)
                ._recipientConnector_(URI(connectorId))
                ._senderAgent_(connectorInfo.curator)
                ._recipientAgent_(URI(agentId ?: connectorId))
                ._requestedArtifact_(URI(artifact))
                .apply { transferContract?.let { _transferContract_(URI(it)) } }
                .buildWithDefaults()

        val egressClearing = clearingLog.clearIds(ClearingContext.ARTIFACT, ClearingDirection.EGRESS, artifactRequestMessage)
        val artifactRequestResponse =
            restTemplate.postForObject(
                routeConfig.defaultEgressUrl,
                artifactRequestMessage.toMultiPartMessage(
                    httpHeaders = mapOf(
                        Constants.Headers.forwardTo to accessUrl,
                        Constants.Headers.authorization to "Bearer ${authManager.internalApiKey.key}"
                    )),
                MultiPartMessage::class.java)
                ?: return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ErrorMessage("error", "Artifact Request", "Did not receive a correct answer from the remote entity"))

        clearingLog.clearIds(ClearingContext.ARTIFACT, ClearingDirection.INGRESS, artifactRequestResponse.header, linkedClearing = egressClearing)

        return when (val artifactRequestResponseHeader = artifactRequestResponse.header) {
            is ArtifactResponseMessage ->
                ResponseEntity.ok()
                    .header(
                        "Content-Type",
                        artifactRequestResponse.payload?.httpHeaders?.get("Content-Type")
                            ?: "application/octet-stream")
                    .body(
                        artifactRequestResponse.payload?.inputStream()?.let {
                            Base64.getDecoder().wrap(it)
                        }
                            ?: "")
            is RejectionMessage ->
                ResponseEntity.badRequest()
                    .body(ErrorMessage("error", "Artifact Request", artifactRequestResponseHeader.rejectionReason.toString(), artifactRequestResponse.payload?.asString()))
            else ->
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ErrorMessage("error", "Artifact Request", "Unexpected Artifact Request response"))
        }
    }

    /**
     * Request a Contract from a remote Connector with the specified connector id
     * and access URL based on the provided Contract Offer. Optionally the agent id of the remote IDS participant
     * can be specified.
     */
    @PostMapping(
        "contractRequest",
        consumes =
            [MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_FORM_URLENCODED_VALUE],
        produces = ["application/ld+json", MediaType.APPLICATION_JSON_VALUE])
    @Secured(SecurityRoles.ARTIFACT_CONSUMER)
    fun contractRequest(
        @RequestParam connectorId: String,
        @RequestParam(required = false) agentId: String?,
        @RequestParam contractOffer: ContractOffer,
        @RequestParam accessUrl: String,
    ): ResponseEntity<Any> {
        LOG.debug("Received contract offer: \n${contractOffer.toJsonLD()}")
        val contractRequest = buildContractRequest(contractOffer)
        LOG.debug("Created contract request: \n${contractRequest.toJsonLD()}")
        val contractRequestMessage =
            ContractRequestMessageBuilder()
                ._issuerConnector_(connectorInfo.idsid)
                ._recipientConnector_(URI(connectorId))
                ._senderAgent_(connectorInfo.curator)
                ._recipientAgent_(URI(agentId ?: connectorId))
                .buildWithDefaults()
        val egressClearing = clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.EGRESS, contractRequestMessage, contractRequest.toJsonLD())

        val contractRequestResponse =
            restTemplate.postForObject(
                routeConfig.defaultEgressUrl,
                contractRequestMessage.toMultiPartMessage(
                        payload = contractRequest.toJsonLD(),
                        contentType = "application/ld+json",
                        httpHeaders = mapOf(
                            Constants.Headers.forwardTo to accessUrl,
                            Constants.Headers.authorization to "Bearer ${authManager.internalApiKey.key}"
                        )
                    ),
                MultiPartMessage::class.java)
                ?: return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ErrorMessage("error", "Contract Request", "Did not receive a correct answer from the remote entity"))

        clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.INGRESS, contractRequestResponse.header, linkedClearing = egressClearing)

        return when (val contractRequestResponseHeader = contractRequestResponse.header) {
            is ContractAgreementMessage ->
                sendContractAgreement(
                    contractRequestResponseHeader, contractRequestResponse, accessUrl)
            is ContractRejectionMessage ->
                ResponseEntity.badRequest()
                    .body(ErrorMessage("error", "Contract Request", contractRequestResponseHeader.contractRejectionReason.value))
            is RejectionMessage ->
                ResponseEntity.badRequest()
                    .body(ErrorMessage("error", "Contract Request", contractRequestResponseHeader.rejectionReason.toString(), contractRequestResponse.payload?.asString()))
            else ->
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ErrorMessage("error", "Contract Request", "Unexpected Contract Request response"))
        }
    }

    /** Exchange Contract Agreement with the provider to formally agree with the contract */
    private fun sendContractAgreement(
        contractRequestResponseHeader: ContractAgreementMessage,
        contractRequestResponse: MultiPartMessage,
        accessUrl: String
    ): ResponseEntity<Any> {
        val contractAgreementString = contractRequestResponse.payload?.asString()
        val contractAgreement = Serialization.fromJsonLD<ContractAgreement>(contractAgreementString)
        policyAdministrationPoint.insertContract(contractAgreement)
        val contractAgreementMessage =
            ContractAgreementMessageBuilder()
                .buildAsResponseTo(contractRequestResponseHeader, connectorInfo)

        val egressClearing = clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.EGRESS, contractAgreementMessage, contractAgreement.toJsonLD())

        val contractAgreementResponse =
            restTemplate.postForObject(
                routeConfig.defaultEgressUrl,
                contractAgreementMessage.toMultiPartMessage(
                    payload = contractAgreementString,
                    contentType = "application/ld+json",
                    httpHeaders = mapOf(
                        Constants.Headers.forwardTo to accessUrl,
                        Constants.Headers.authorization to "Bearer ${authManager.internalApiKey.key}"
                    )
                ),
                MultiPartMessage::class.java)
                ?: return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ErrorMessage("error", "Contract Agreement", "Did not receive a correct answer from the remote entity"))
        clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.INGRESS, contractAgreementResponse.header, linkedClearing = egressClearing)

        return when (val contractAgreementResponseHeader = contractAgreementResponse.header) {
            is MessageProcessedNotificationMessage ->
                ResponseEntity.ok()
                    .header("Content-Type", "application/ld+json")
                    .body(contractRequestResponse.payload?.inputStream() ?: "")
            is ContractRejectionMessage ->
                ResponseEntity.badRequest()
                    .body(ErrorMessage("error", "Contract Agreement", contractAgreementResponseHeader.contractRejectionReason.value))
            is RejectionMessage ->
                ResponseEntity.badRequest()
                    .body(ErrorMessage("error", "Contract Agreement", contractAgreementResponseHeader.rejectionReason.toString(), contractAgreementResponse.payload?.asString()))
            else ->
                ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(ErrorMessage("error", "Contract Agreement", "Unexpected Contract Request response"))
        }
    }

    /** Build a ContractRequest based upon a ContractOffer */
    fun buildContractRequest(contractOffer: ContractOffer): ContractRequest =
        ContractRequestBuilder()
            ._contractStart_(contractOffer.contractStart ?: xmlGregorianCalendar())
            ._contractEnd_(contractOffer.contractEnd ?: xmlGregorianCalendar(Calendar.MONTH, 3))
            ._contractDate_(contractOffer.contractDate ?: xmlGregorianCalendar())
            ._contractAnnex_(contractOffer.contractAnnex)
            ._contractDocument_(contractOffer.contractDocument)
            ._consumer_(connectorInfo.idsid)
            ._permission_(
                contractOffer.permission.map {
                    it.assignee = arrayListOf(connectorInfo.idsid)
                    (it.postDuty + it.preDuty).forEach { duty ->
                        duty.assignee = arrayListOf(connectorInfo.idsid)
                    }
                    it
                })
            ._prohibition_(
                contractOffer.prohibition.map {
                    it.assignee = arrayListOf(connectorInfo.idsid)
                    it
                })
            ._obligation_(
                contractOffer.obligation.map {
                    it.assignee = arrayListOf(connectorInfo.idsid)
                    it
                })
            .build()

    /** Create an XML Gregorian Calendar offset by a Calendar offset with a given amount */
    private fun xmlGregorianCalendar(field: Int? = null, amount: Int? = null) =
        Calendar.getInstance()
            .apply {
                time = Date()
                if (field != null && amount != null) {
                    add(field, amount)
                }
            }
            .time
            .run { DateUtil.asXMLGregorianCalendar(this) }
}
