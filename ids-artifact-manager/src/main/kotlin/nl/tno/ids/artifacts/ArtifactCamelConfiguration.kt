/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.artifacts

import nl.tno.ids.configuration.infomodel.process
import org.apache.camel.builder.RouteBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/** Spring Configuration for providing a Camel RouteBuilder bean for Artifact interactions */
@Configuration
class ArtifactCamelConfiguration(
    /** Artifact configuration */
    private val artifactConfiguration: ArtifactConfiguration,
    /** Camel port modifier */
    @Value("\${camel.port:8080}") private val camelPort: String,
    /** Camel HTTPS flag */
    @Value("\${routes.https:false}") private val https: Boolean
) {

    /**
     * Artifact handling Camel RouteBuilder
     */
    @Bean("artifactRoute")
    @ConditionalOnProperty(
        name = ["artifacts.enabled"], havingValue = "true", matchIfMissing = true)
    fun artifactCamelRoute() =
        object : RouteBuilder() {
            override fun configure() {
                LoggerFactory.getLogger(ArtifactCamelConfiguration::class.java).info("Creating artifacts route")
                from(
                        "jetty:http${if (https) "s" else ""}://0.0.0.0:$camelPort/router/artifacts?enableMultipartFilter=false&matchOnUriPrefix=true${if (https) "&sslContextParameters=#idsSSLContextParameters" else ""}")
                    .routeId("artifacts")
                    .process("multiPartInputProcessor")
                    .process(artifactConfiguration.dapsValidation, "dapsTokenVerifier")
                    .process(artifactConfiguration.policyEnforcement) {
                        process("negotiationProcessor")
                            .setHeader("PEF_STAGE", simple("PRE"))
                            .setHeader("PEF_MODE", simple("INGRESS"))
                            .setHeader("DESCRIPTION", simple("ALLOW"))
                            .process("pefProcessor")
                    }
                    .process("artifactProcessor")
                    .process(artifactConfiguration.dapsValidation, "dapsTokenInjector")
                    .process("multiPartOutputProcessor")
            }
        }
}
