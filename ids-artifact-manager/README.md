# IDS Artifact Manager

The IDS Artifact Manager module is a self-contained module to support simple data exchanges without additional Data Apps.

## Spring REST Controllers
The Artifact Manager contains 2 internal API controllers for administration of artifacts and for requesting artifacts.

### Consumer

The Consumer controller offers the possibility to request artifacts from other IDS connectors.
Also, the consumer controller is capable of starting the Contract Negotiation process, based on a predefined ContractOffer (e.g. stored next to the artifact metadata in the Broker)

### Provider

The Provider controller offers the possibility to manage artifacts published by the connector, by means of CRUD requests.

Next to the actual artifact, an ContractOffer can be provided to limit the access rights to the artifact. This ContractOffer will be scoped to reflect the uploaded artifact, by setting the `target` property of all rules in the ContractOffer to the identifier of the artifact.

## Artifact exchange process

The sequence diagram below shows the generic process for the interaction of users with the Artifact Manager. Containing the processes to upload a new artifact, perform the contract negotiation, and downloading the artifact.
```mermaid
sequenceDiagram
    actor C as Consumer
    participant CC as Consumer Connector
    participant PC as Provider Connector
    actor P as Provider
    P->>PC: /api/artifacts/provider<br />With optional ContractOffer
    activate P
    activate PC
    PC->>PC: Store artifact
    PC-->>P: 
    deactivate P
    deactivate PC
    
    C->>CC: /api/artifacts/consumer/contractRequest
    activate C
    activate CC
    CC->>PC: ContractRequestMessage
    activate PC
    alt Contract Request accepted
    PC-->>CC: ContractAgreementMessage
    CC-->>C: ContractAgreement
    else Contract Request denied
    PC-->>CC: ContractRejectionMessage
    CC-->>C: ContractRejection
    end
    C->>CC: /api/artifacts/consumer/artifact
    CC->>PC: ArtifactRequestMessage
    PC-->>CC: ArtifactResponseMessage
    deactivate PC
    CC-->>C: Artifact
    deactivate C
    deactivate CC
```
