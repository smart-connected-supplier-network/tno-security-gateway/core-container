package nl.tno.ids.testextensions

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.options
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.security.Keys
import org.jose4j.jwk.RsaJsonWebKey
import org.junit.jupiter.api.extension.*
import java.security.KeyPair
import java.security.interfaces.RSAPublicKey
import java.util.*

class DapsTestExtension : BeforeAllCallback, AfterAllCallback, ParameterResolver {
    private lateinit var wireMock: WireMockServer
    private val federatedKeyPair = Keys.keyPairFor(SignatureAlgorithm.RS256)

    override fun beforeAll(context: ExtensionContext) {
        System.setProperty(
            "keystore.pem.key",
            "LS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tCk1JSUV2QUlCQURBTkJna3Foa2lHOXcwQkFRRUZBQVNDQktZd2dnU2lBZ0VBQW9JQkFRQ21qdHR4YmhZY3RGUGYKbmNGVG1HZnlBUnY4YVQxejRKZGMxcjFFZW43SjJXN3NGVGRrQVpwK1BCa2Jsa09NcWlRSnRYK25LTy81TFBlRgpWb2x0ZWllZ1BFR1dVVTgvVGgwSkF4TlAxV2RWWWlCbjNhdUhxRGI5NFhlVGUxcmhoY0VUbW1aaXF1SWlmQWxqCjRxYTMyMU9SLzZrczZiZmJKZVB4TkN3SitsUk1MTGNsY2d2ZzRENFBWbUgzeTgzNUFXQ0ZJemNNNDRBMlJXWGwKZElDMUNvVXl5UnFDbDBPeGt6eXZsanBvV2l1RnhqSThyRk9HRCtiR1Q0eG5jZ3lEalpJUmt3UXNkVElHaFozbQp0V0theldCVmgxQ1pWc1dOU05xcGVOSm5uZ200My8zc2FpclI1aVZlVWQrQ09CK3ExZ01JSTZGSFJjZnhxNDVICjdWVS9jU1V2QWdNQkFBRUNnZ0VBWDlwUEI5Um0vVlg1L3ZMMjAza2VPUFBTZTB6RmhPNTNyWUlYb2xVc3FsVnIKdElueE9xaUJsS3haZHJyZlhSQjlkT2p5OGV4cFRtbEdlWUgzelAxRDJMOVVyNEZUcmdieVJzRnlWN3Rrb0lxOApRNG9hc1JOcHNtb1RPdGorMEZMOWJwNm8xWmVxVkZZMXdUb3E0QnhrMTl6ZUsyN0YxR1RWN1hZczhnRVNpbE16CnVNQm9zN29DNjFVVE80aEhPT2xCL05hcm5Qc1Y4eW1yQ0RMcFgvQUFWeDZzUHdTT2kyTXJRQ3pTNkNBS3N0eHgKcEpFNWQxUXBRSUZhNVdTMGxmeEUzQ2NGWS9NREF5L2M2RzRuOTJmMUxmdmNGMk5RYWxyWVNJejA1eFBNbUl5dApRTUtZY2o2N2UweUQ4RmdsUnFDR2lkQlBycUp2RktkQ24vb0wxWGV1Z1FLQmdRRFZJNy9FeWl4dDY0K3B3dUYxCnFhZTJNTVZUQVd5c0s3VFVtVHZvTEUyakU5Zm5Xem5UK28zRUVZdldFdm5Kamx5S2tZSFJTcENSR3RaTGc2RjkKZm5sR2ZpK0thME5BSWlsK1YxakRQSHFiTkx4UzFrdkc1U1dkT2hWcXduVkMyaGFkblJDclFCNkF6UnlvTVJ4RApoWnlMcmJLZWZqYmdUZ3lmMjczTzJPaWtjUUtCZ1FESURTQXpCaEZveFA2aHVONTlIaCtnSWt3VHdBeXRqZGJEClo0eFlBWGM5clhYeEhPRW9zSzhJcm1KeVR6aHozYTRZN3NjbTdCbFR2TlVHaG5tSFVXdUpjc2dxWDgzbk5RRDYKcVNreWZEZGZGWXV3bXh2aU1TU2JVeXUxczdVQ29DdGdZYlJIMmZOc3JadHFONy80N3R0OXZyQW0rOTZDNnZTMApON01ZdDNpem53S0JnRmJhZElEcXFlUTVBNWYzOUF3UWtHa3J3cDBxalhBemQwTFRGcGhyeFU5WS9MZ29jbzR4CklhZnVSbjk3eUJJY1V4V2NhYnd4bUd1bzlmNWJxMmtLS3IxUkdtcGJmMUlaR2JPRVFJVitsYSs5YzZpRjd1Y2oKNVlvR2EwZlA2aHpONWoxZ0RiVGJNaExtZ25EL1lqY1pzVXN5QXdHWENTR3FiSGFRQXU3UmNTdmhBb0dBTkRqWQo4dmplaW84Q09ZMWM1eVI1OVA5L0JKalp0bVNVYjZZVVNOVDVINnlzWVNpOHpqWnhPcjdyREJRS0ZKVUY0WlJUCmdLR1phdmFjVlh1OU5jblhYS1lPQXFISHlsZFh6aGRpbmI4M1pTYm10RStKVTk2b1E3UE9jVHhjUURjM200MWwKMW12YTA3L3VGaWcvZ09nNWUvWFBYaDNaMUNzcGZmSWNXcE5xZXRrQ2dZQXZPMWREMkVrUDkwRmJCSUpKTTFGRApCK0l6THQwQ01wVVkxbnlvb2I4TUdBbnErdXVYWDBKeWYrL3prUE1UZTdJTllXTlRCU1dCMGFuQTA5ZzlwWmtNCmh3bDczVHhCZkpTc05Eb1N1c04wUUhJZXkxVWN4WjVFOXFWbEhNOWJ4cGltQUNoOGI5RE9KL1FqQWpNMFV4SE0KMG14RVRmOHhtTGdLNjQwWkFzT25vZz09Ci0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS0="
        )
        System.setProperty(
            "keystore.pem.cert",
            "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUVRekNDQXl1Z0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREIrTVFzd0NRWURWUVFHRXdKT1RERVMKTUJBR0ExVUVDQk1KUjNKdmJtbHVaMlZ1TVJJd0VBWURWUVFIRXdsSGNtOXVhVzVuWlc0eEREQUtCZ05WQkFvVApBMVJPVHpFWU1CWUdBMVVFQ3hNUFJHRjBZU0JGWTI5emVYTjBaVzF6TVI4d0hRWURWUVFERXhaSlJGTWdWR1Z6CmRDQkVaWFpwWTJVZ1UzVmlJRU5CTUI0WERUSXpNRGN3TlRBNE5EUTFPVm9YRFRJMU1EY3dOVEE0TkRRMU9Wb3cKZ1l3eEN6QUpCZ05WQkFZVEFrNU1NUkl3RUFZRFZRUUlFd2xIY205dWFXNW5aVzR4RWpBUUJnTlZCQWNUQ1VkeQpiMjVwYm1kbGJqRU1NQW9HQTFVRUNoTURWRTVQTVJnd0ZnWURWUVFMRXc5RVlYUmhJRVZqYjNONWMzUmxiWE14CkxUQXJCZ05WQkFNVEpIVnlianBwWkhNNmRITm5PbU52Ym01bFkzUnZjbk02VkdWemRFTnZibTVsWTNSdmNqQ0MKQVNJd0RRWUpLb1pJaHZjTkFRRUJCUUFEZ2dFUEFEQ0NBUW9DZ2dFQkFLYU8yM0Z1Rmh5MFU5K2R3Vk9ZWi9JQgpHL3hwUFhQZ2wxeld2VVI2ZnNuWmJ1d1ZOMlFCbW40OEdSdVdRNHlxSkFtMWY2Y283L2tzOTRWV2lXMTZKNkE4ClFaWlJUejlPSFFrREUwL1ZaMVZpSUdmZHE0ZW9OdjNoZDVON1d1R0Z3Uk9hWm1LcTRpSjhDV1BpcHJmYlU1SC8KcVN6cHQ5c2w0L0UwTEFuNlZFd3N0eVZ5QytEZ1BnOVdZZmZMemZrQllJVWpOd3pqZ0RaRlplVjBnTFVLaFRMSgpHb0tYUTdHVFBLK1dPbWhhSzRYR01qeXNVNFlQNXNaUGpHZHlESU9Oa2hHVEJDeDFNZ2FGbmVhMVlwck5ZRldIClVKbFd4WTFJMnFsNDBtZWVDYmpmL2V4cUt0SG1KVjVSMzRJNEg2cldBd2dqb1VkRngvR3Jqa2Z0VlQ5eEpTOEMKQXdFQUFhT0J2RENCdVRBSkJnTlZIUk1FQWpBQU1Bc0dBMVVkRHdRRUF3SUY0REFkQmdOVkhTVUVGakFVQmdncgpCZ0VGQlFjREFRWUlLd1lCQlFVSEF3SXdIUVlEVlIwT0JCWUVGSWltUDNuL1AxZWlWSStaaXZpa01yeC95YktTCk1COEdBMVVkSXdRWU1CYUFGT3dqM0FQc2JIVzdEUlEzdEI0Q2w3dnBwTm5CTUVBR0ExVWRFUVE1TURlSEJIOEEKQUFHQ0NXeHZZMkZzYUc5emRJSWtkWEp1T21sa2N6cDBjMmM2WTI5dWJtVmpkRzl5Y3pwVVpYTjBRMjl1Ym1WagpkRzl5TUEwR0NTcUdTSWIzRFFFQkN3VUFBNElCQVFDa2pxRmdwMkZSRncyVEl6YVhIR2pKZWZ2SjFlbkVIcHFZCjgwM1UremxlemVrRHRueXpQR1FvYmYyQjZzVU5kTkJiQTlaZ2F1Z2R6Y1NhT2VjUmNvV0I5VDAxbVpkYlZ1L2IKRXV4dWVnMXVEanVhbE9jUmFKUm9jNjVHZjF2YjVrKzBSMmZKZWU1Z3YxZW81NUhrdGVFR0kwL0FjNFdhTG8xQgp3VURrandvM2MvYllCS3Y1NTlrV1ZyODhNc1o3SUpCSVlJUnVDcnJZTjNkeTBuTEs2UTdjbktBajlmVXlQUU9PClB2cmtNb1htRXA4SFZ2cWFKL2FVbyt6MmdCL3VVMU1sR0JYamFOa3RvK040TlduWkthMnJTRjRKdlkwQ1lYUTUKMUE1dmx0NUVBYjY5K3VRRnd2OEw4dUhLUGdtbFNYbGsxV2s0UFVZYXdDa2lTQXNWZmZrYwotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0tCg=="
        )
        System.setProperty(
            "truststore.pem.chain",
            "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUVGRENDQXZ5Z0F3SUJBZ0lCQURBTkJna3Foa2lHOXcwQkFRc0ZBREI0TVFzd0NRWURWUVFHRXdKT1RERVMKTUJBR0ExVUVDQk1KUjNKdmJtbHVaMlZ1TVJJd0VBWURWUVFIRXdsSGNtOXVhVzVuWlc0eEREQUtCZ05WQkFvVApBMVJPVHpFWU1CWUdBMVVFQ3hNUFJHRjBZU0JGWTI5emVYTjBaVzF6TVJrd0Z3WURWUVFERXhCSlJGTWdWR1Z6CmRDQlNiMjkwSUVOQk1CNFhEVEl6TURjd05UQTRORFExT0ZvWERUTXhNRGN3TlRBNE5EUTFPRm93ZURFTE1Ba0cKQTFVRUJoTUNUa3d4RWpBUUJnTlZCQWdUQ1VkeWIyNXBibWRsYmpFU01CQUdBMVVFQnhNSlIzSnZibWx1WjJWdQpNUXd3Q2dZRFZRUUtFd05VVGs4eEdEQVdCZ05WQkFzVEQwUmhkR0VnUldOdmMzbHpkR1Z0Y3pFWk1CY0dBMVVFCkF4TVFTVVJUSUZSbGMzUWdVbTl2ZENCRFFUQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0MKZ2dFQkFNdzkzT0dPTnZZdGJiMThSYUxDcE4zK1ZhRWZvSCs4cm1sRFZiUzhickxoelE3MllwWGJ3OFFyOHBHOApDbDdoUkl5MlVlV0FlT1NNdHRGcGVMQURYeXJnYkFNKzN3MlhwNmR3OC9BRUhJUmViVnNvQnIzVW5sVWdHMXJpCkRoZGhZMHJ6aUpJQ00vQnV4Rk5OdFlTeHBDR0FXVk9wZHRXUmxpbUpwclliMlQrYmZnTmlYQjBEN241RERaV1YKTU1lRnF5UUVXUHpjOGRqSkF0a1J1a29EdG9mZndEOXFZWCtJQmE5Zm54aU5tZllueU9jcGlGbmoxbFB1emxJMgpweWZSYW9Lc09oMEw4Y1JxMHNTM3hZcUxtSjhFUWlBQ0s1SXRXcGsvdnVVNUdZejFIRFVyck9HVi9xdFhoaENEClllRUhBREhQL3JEcDRkL1Z5U3hTT0h5TWowc0NBd0VBQWFPQnFEQ0JwVEFNQmdOVkhSTUVCVEFEQVFIL01Bc0cKQTFVZER3UUVBd0lCUmpBZEJnTlZIU1VFRmpBVUJnZ3JCZ0VGQlFjREFRWUlLd1lCQlFVSEF3SXdIUVlEVlIwTwpCQllFRkErWWFhUTNNQTFlVXUwbVdua2RxZDI3b1JKNk1COEdBMVVkSXdRWU1CYUFGQStZYWFRM01BMWVVdTBtCldua2RxZDI3b1JKNk1Da0dBMVVkRVFRaU1DQ0NDV3h2WTJGc2FHOXpkSUlUY205dmRDNWpZUzVsZUdGdGNHeGwKTG1OdmJUQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FRRUFxU0taYXQ0cmxkM2NXQ1hhOXY2eHZpcVFZMXBieWR3dgpGMHFBQXBzUTdxaExhSG9IdW9LcFBzSVRxNTUzdVFQbmFYZWRmWjNzOStRa0RYQkt4TEt4VVA1YTNyd0E3cXM1CjF6UjRubjJqOHdWV1UvRU9USkhJOTIxK1lKalZkYytTbVh6ZWdtVDNaeko4cEoyQmFtcmdiamxFd3ZxbHJPQUEKZElIUlBEQWVucEp2cFppdStXMThjYXB5Q2ROWlVaK0pJK2tCUU82YWdxWGlQWUxlYVg3VHhweW5BVENFR1RWZQoyR2kvem9ScmxwN2k0b2cwU0lURTAyL29rQ2NWTGxFVHZsSFVaM0RkKzVUWmFQc3hyMW0wVVd5azAyUGdGcTVSCnh1eUpkMUNGNWZqUHhDQ0d1U1dSNGI2MGRaNVZTYzE3QmkrWTlvQU9Vbi80SDBMQktWeE5odz09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0KCi0tLS0tQkVHSU4gQ0VSVElGSUNBVEUtLS0tLQpNSUlFSVRDQ0F3bWdBd0lCQWdJQkFEQU5CZ2txaGtpRzl3MEJBUXNGQURCNE1Rc3dDUVlEVlFRR0V3Sk9UREVTCk1CQUdBMVVFQ0JNSlIzSnZibWx1WjJWdU1SSXdFQVlEVlFRSEV3bEhjbTl1YVc1blpXNHhEREFLQmdOVkJBb1QKQTFST1R6RVlNQllHQTFVRUN4TVBSR0YwWVNCRlkyOXplWE4wWlcxek1Sa3dGd1lEVlFRREV4QkpSRk1nVkdWegpkQ0JTYjI5MElFTkJNQjRYRFRJek1EY3dOVEE0TkRRMU9Gb1hEVEkzTURjd05UQTRORFExT0Zvd2dZUXhDekFKCkJnTlZCQVlUQWs1TU1SSXdFQVlEVlFRSUV3bEhjbTl1YVc1blpXNHhFakFRQmdOVkJBY1RDVWR5YjI1cGJtZGwKYmpFTU1Bb0dBMVVFQ2hNRFZFNVBNUmd3RmdZRFZRUUxFdzlFWVhSaElFVmpiM041YzNSbGJYTXhKVEFqQmdOVgpCQU1USEVsRVV5QlVaWE4wSUZCaGNuUnBZMmx3WVc1MGN5QlRkV0lnUTBFd2dnRWlNQTBHQ1NxR1NJYjNEUUVCCkFRVUFBNElCRHdBd2dnRUtBb0lCQVFEcjNwTVVnRW4zK1hSOU1HU2E0R0hHMG9Gd2dnYitZU2NIWkVQaDl0T2oKUkRibEZxTmJoVjUyOGIxZWVYUDFqS3dkcGp0UHI2SHlrT2VuTXpIa1l2WjZ4NEtyUXJVQmpIQVZ1dGdCNHpxNgoyT2ZzVDVvcHlSRnlJV3EyMzVYZllsSHI5WFVrQzA0dkxEVHBIK1ZIQTZoejVhcE85S0dSVkhJeExiVTZyUEhyClU1d0JteldPM1BSbWlBK3UwUVNnNDBBejFyWC9ldXI3eVFSZkhDbHh6cVd2UnB1UEptVUlqZEdHVGF3TTVacGkKMjhaOHdma3BnRVUyTmVLSUFBZnhHVllibHl0R3M2alNGQUpRWWJheTRkeFM5TmxscEV6NVNCTCtVZjNRdHN6Qgo4bXlyMUxDbjBqd3ptZ29HOFFOc0tXRUx4cVh5VkgzSlZOYmxEY0UwSVAyeEFnTUJBQUdqZ2Fnd2dhVXdEQVlEClZSMFRCQVV3QXdFQi96QUxCZ05WSFE4RUJBTUNBVVl3SFFZRFZSMGxCQll3RkFZSUt3WUJCUVVIQXdFR0NDc0cKQVFVRkJ3TUNNQjBHQTFVZERnUVdCQlQySGhxTWE3aGs3ZFoyaU45bFpLQmFObWFqSURBZkJnTlZIU01FR0RBVwpnQlFQbUdta056QU5YbEx0SmxwNUhhbmR1NkVTZWpBcEJnTlZIUkVFSWpBZ2dnbHNiMk5oYkdodmMzU0NFM0p2CmIzUXVZMkV1WlhoaGJYQnNaUzVqYjIwd0RRWUpLb1pJaHZjTkFRRUxCUUFEZ2dFQkFCZ3docm5xMHRvMDZsc0IKY3E3L3lpUFA4SldwaWFmcVRSbW9hd0tzc3pkN1BnV2g1MHM0SEZMckNJY040VC93NW4velgxcitwUlZCeWJpdgp4eE1BdGU2SnN3aDZPNi9qU0FjQmFNYWdCbkgyMXVLdndjQXpXdmVaS01La1NoS24rbzkxZlh3WGM5akpGa2lBCjFMKzd2U2JJZ1pEajNoWllRWTRFblJDMmRDU2lDUEZpRHJhM0pkeFBwVVJoVkc2b08xbUlWaitTZHR0dGE3SzgKc3IreW82NjBrREc1S25QMTA5TzFyMHlYdWZybEo0ZnhCa3Fmd3o1dHF3aWsxR2c0WmZvUnZkNjMvWDV5Y1k0NQpnWjRmdnRHSFZvZEtpd3prd1pmSDNKRTRKQnJTWmNLNXp1UjlOZDhtZ3JpdllXS0J4Vkd6R3hJaXpYNHJVaklGCnl1TlowaVk9Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0KCi0tLS0tQkVHSU4gQ0VSVElGSUNBVEUtLS0tLQpNSUlFR2pDQ0F3S2dBd0lCQWdJQkFEQU5CZ2txaGtpRzl3MEJBUXNGQURCNE1Rc3dDUVlEVlFRR0V3Sk9UREVTCk1CQUdBMVVFQ0JNSlIzSnZibWx1WjJWdU1SSXdFQVlEVlFRSEV3bEhjbTl1YVc1blpXNHhEREFLQmdOVkJBb1QKQTFST1R6RVlNQllHQTFVRUN4TVBSR0YwWVNCRlkyOXplWE4wWlcxek1Sa3dGd1lEVlFRREV4QkpSRk1nVkdWegpkQ0JTYjI5MElFTkJNQjRYRFRJek1EY3dOVEE0TkRRMU9Gb1hEVEkzTURjd05UQTRORFExT0Zvd2ZqRUxNQWtHCkExVUVCaE1DVGt3eEVqQVFCZ05WQkFnVENVZHliMjVwYm1kbGJqRVNNQkFHQTFVRUJ4TUpSM0p2Ym1sdVoyVnUKTVF3d0NnWURWUVFLRXdOVVRrOHhHREFXQmdOVkJBc1REMFJoZEdFZ1JXTnZjM2x6ZEdWdGN6RWZNQjBHQTFVRQpBeE1XU1VSVElGUmxjM1FnUkdWMmFXTmxJRk4xWWlCRFFUQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQCkFEQ0NBUW9DZ2dFQkFLMHNGaUZlWEZKbmNhOXp0MFNKb3hGbzZZUnc0Q012V3U0ZjRGb3pRSm9YV1F6dysrd3QKM1JTcndBMDRUKzNoMjFleVlxT2JSRlJ1QXB6SkZvQWZCUTgvOTg0UVFzUzVOMlRVQ1IxRGRJTzdBVEhxbEdJdQo3eG0xVHVpblREL2Q3VU56RW11cEZNemEzUWxMNFk0VDRBUjFxMEVidXJScUU2b3FYRFZRSTRHblduS0lXc0MrClF6b1J0Mml4VFRxR1RyRkkxNlY2bnFYbGpXQVdSYXpPdWhJOHAxNWdUK050MmNIaTZvU01UT3dmRUM2VWJmaFIKcU1QaWtCK242MlFUTVA2azRCbnBSVXBBc28yelZwMmFrOEp4WFNxcXltWFh4YVlhem1pbytwQUJjT2dPWGtTcApFa3ZPQVIyRURqL1k0Z09CMXV6Uy9HN1JLK2hOVlRQeWR0Y0NBd0VBQWFPQnFEQ0JwVEFNQmdOVkhSTUVCVEFECkFRSC9NQXNHQTFVZER3UUVBd0lCUmpBZEJnTlZIU1VFRmpBVUJnZ3JCZ0VGQlFjREFRWUlLd1lCQlFVSEF3SXcKSFFZRFZSME9CQllFRk93ajNBUHNiSFc3RFJRM3RCNENsN3ZwcE5uQk1COEdBMVVkSXdRWU1CYUFGQStZYWFRMwpNQTFlVXUwbVdua2RxZDI3b1JKNk1Da0dBMVVkRVFRaU1DQ0NDV3h2WTJGc2FHOXpkSUlUY205dmRDNWpZUzVsCmVHRnRjR3hsTG1OdmJUQU5CZ2txaGtpRzl3MEJBUXNGQUFPQ0FRRUFLZFRFMWVpeEpjS2dhVnV2bVI2bGVVcnQKQW5EVkk4NlRIZmtENzl1T3VNMkNRWHpycHNjTUpob1ZXS0hsMG5zWk5JWnVxY3g3VzFOd3hqUGhpaDIrMzRzWgplWURNYzdwZmZZQm13WGNFOXF1Vm1zZ3dxVVdIdGVlYTVqdWFSdlJ1dk8xM2ZyMjQwNzlPb2MwM1Y3a1hzYU9zClU4REhPTXZTczJwRjVwYkFQS3BxcTh0SUhRTm9taDQwalhGMTByQVFMZGpXWHMrTUhKU1Z6TjJBelJPWUJKVUgKNU5pVTdMbTY5d1ZQcmtaaTgrbG5zMTFEV0hhcS9xZUk5Q0xzaEVnNENlQ1FvNGF4QXBpR3JpZGF3QWRzZXUxLwprMFVYRUlCTmdpTWJIT3Z2VmxvYkRPaTNaa3dVZm5pRGIzQjEwQk0rTGxzQ0w3T0RNbVRxYldmbDFqa0I5UT09Ci0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0K"
        )

        wireMock = WireMockServer(options().dynamicPort()).apply { start() }

        System.setProperty("daps.url", "http://localhost:${wireMock.port()}/v2")
        val keyPair = Keys.keyPairFor(SignatureAlgorithm.RS256)
        wireMock.stubFor(
            WireMock.post(WireMock.urlPathMatching("(/v2)?/token"))
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(200)
                        .withBody(
                            "{\"access_token\":\"${
                                Jwts.builder()
                                    .setIssuer("http://localhost:${wireMock.port()}")
                                    .setSubject("test")
                                    .setAudience("idsc:IDS_CONNECTORS_ALL")
                                    .setHeaderParam("kid", "default")
                                    .setExpiration(Date(System.currentTimeMillis() + 3600 * 1000))
                                    .setNotBefore(Date())
                                    .addClaims(
                                        mapOf(
                                            "@context" to "https://w3id.org/idsa/contexts/context.jsonld",
                                            "@type" to "ids:DatPayload",
                                            "scopes" to listOf("idsc:IDS_CONNECTOR_ATTRIBUTES_ALL"),
                                            "referringConnector" to "urn:ids:test",
                                            "securityProfile" to "idsc:TRUST_SECURITY_PROFILE",
                                        )
                                    )
                                    .signWith(keyPair.private, SignatureAlgorithm.RS256)
                                    .compact()
                            }\"}"
                        )
                )
        )
        wireMock.stubFor(
            WireMock.get(WireMock.urlPathMatching("(/v2)?/.well-known/jwks.json"))
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(200)
                        .withBody(
                            "{\"keys\": [${
                                RsaJsonWebKey(keyPair.public as RSAPublicKey).apply {
                                    keyId = "default"
                                }.toJson()
                            }]}"
                        )
                )
        )
        wireMock.stubFor(
            WireMock.get(WireMock.urlPathMatching("(/v2)?/.well-known/jwks.json.*"))
                .withQueryParam("issuer", WireMock.equalTo("https://federated.daps.com"))
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(200)
                        .withBody(
                            "{\"keys\": [${
                                RsaJsonWebKey(federatedKeyPair.public as RSAPublicKey).apply {
                                    keyId = "default"
                                }.toJson()
                            }]}")))
    }

    override fun afterAll(context: ExtensionContext?) {
        wireMock.stop()
    }

    override fun supportsParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Boolean {
        return parameterContext.parameter.type.equals(KeyPair::class.java)
    }

    override fun resolveParameter(parameterContext: ParameterContext, extensionContext: ExtensionContext): Any {
        return federatedKeyPair
    }
}