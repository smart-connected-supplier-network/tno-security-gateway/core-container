# IDS Test Extensions

The IDS Test Extensions module is providing JUnit extensions for common tasks performed in multiple tests.

Currently the following extensions are available:
- _DAPS Test Extensions_: Provides WireMock setup and DAPS configuration for local DAPS token requests & responses.
