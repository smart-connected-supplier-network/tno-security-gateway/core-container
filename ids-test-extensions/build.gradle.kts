plugins {
    kotlin("plugin.spring")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-test")
    implementation("com.github.tomakehurst:wiremock-jre8:${LibraryVersions.Test.wiremock}")
    implementation("com.google.guava:guava:${LibraryVersions.Test.guava}")
    implementation("org.bitbucket.b_c:jose4j:${LibraryVersions.Jwt.jose4j}")
    api("io.jsonwebtoken:jjwt-api:${LibraryVersions.Jwt.jjwt}")
    implementation("io.jsonwebtoken:jjwt-impl:${LibraryVersions.Jwt.jjwt}")
    runtimeOnly("io.jsonwebtoken:jjwt-jackson:${LibraryVersions.Jwt.jjwt}")

}