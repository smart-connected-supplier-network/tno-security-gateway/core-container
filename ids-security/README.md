# IDS Security

The IDS Security module is responsible for the security of non-IDS communication. Which includes:
- the management API
- the Data App &#8594; Core Container API

Authentication for these resources is done by either API key authentication or by User credential authentication. For User credential authentication, a token is requested at the `/auth/signin` which can be used for requests to the API.

## Security Roles

Access to resources of the API is role-based, the following roles are currently supported:

- **ADMIN**: Administrator role for the primary administrator that has full access and inherits all of the other roles
- **DATA_APP**: Data App role that can be given to Data Apps for managing the resources that they offer
- **READER**: Reader role that is allowed to read all resources in the core container but not modify them
- **ARTIFACT_PROVIDER_MANAGER**: Artifact Provider Manager role that is allowed to administrate provided artifact
- **ARTIFACT_PROVIDER_READER**: Artifact Provider Reader role that is allowed to list provided artifacts
- **ARTIFACT_CONSUMER**: Artifact Consumer role that is allowed to request artifacts from other connectors in the network
- **ORCHESTRATION_MANAGER**: Orchestration Manager role that is allowed to orchestrate containers
- **ORCHESTRATION_READER**: Orchestration Reader role that is allowed to list orchestrated containers
- **PEF_MANAGER**: Policy Enforcement Manager role that is allowed to initiate new contracts and contract negotiations
- **PEF_READER**: Policy Enforcement Reader role that is allowed to list agreed upon contracts and contract offers
- **RESOURCE_MANAGER**: Resource Manager role that is allowed to modify the resources provided by the core container and data apps
- **RESOURCE_READER**: Resource Reader role that is allowed to list resources provided by the core container and data apps
- **ROUTE_MANAGER**: Route Manager role that is allowed to modify Camel routes offered by the Core Container
- **ROUTE_READER**: Route Reader role that is allowed to list Camel routes offered by the Core Container
- **DESCRIPTION_READER**: Description Reader role that is allowed to send Description Request messages to other connectors in the network
- **WORKFLOW_MANAGER**: Workflow Manager role that is allowed to initiate new workflows
- **WORKFLOW_READER**: Workflow Reader role that is allowed to list workflows and show the results and status of the workflow

## Controller Configuration

Spring controllers can limit the access to endpoints by providing the `@Secured` annotation, which requires a list of roles that have access to the endpoints.
This can be done both on class-level (indicating that for each of the methods these roles are required) or on method-level, these options are combined so you'd either need access on class-level or on method-level.

The `SecurityRoles` class provides constant values that can be used in the annotations:
```kotlin
@RestController
@Secured(SecurityRoles.ARTIFACT_PROVIDER_MANAGER)
class Controller
```
```kotlin
@GetMapping
@Secured(SecurityRoles.ARTIFACT_PROVIDER_READER)
fun get()
```

## External authentication provider

The external authentication provider gives the ability to use the users and API keys defined in the core container to be used in external services.

The provider is exposed on the management API under `/api/auth/external/` as well as via a camel route under `/external-auth/`, the latter one is the most convenient to use to secure other services on ingress level.

Primary use case for this is to use it in conjunction with an Ingress controller via the following Kubernetes annotations for the Nginx Ingress controller:
```yaml
nginx.ingress.kubernetes.io/auth-url: "https://$host/external-auth/auth"
nginx.ingress.kubernetes.io/auth-signin: "https://$host/external-auth/signin?rd=$escaped_request_uri"
```

The default behaviour is to grant access if the user or API key exists. Optionally, restrict the roles a user or API key must be assigned to get access via a comma separated query parameter, e.g. to restrict access to users or API keys that hold the `ROLE_ADMIN` or `ROLE_DATA_APP` roles provide the following URLs:

```yaml
nginx.ingress.kubernetes.io/auth-url: "https://$host/external-auth/auth?roles=ROLE_ADMIN,ROLE_DATA_APP"
nginx.ingress.kubernetes.io/auth-signin: "https://$host/external-auth/signin?rd=$escaped_request_uri&roles=ROLE_ADMIN,ROLE_DATA_APP"
```

The `$host` nginx variable is used here, which requires the service that you'd want to secure and the core container to be on the same domain and the core container to be available on the root of that domain.
If this is not the case, replace `$host` with the actual domain of the core container ingress.

_**NOTE**_: Do **not** use these annotations on the ingress of the core container itself, since that would also block the requests to login.