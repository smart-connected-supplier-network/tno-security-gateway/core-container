package nl.tno.ids.security

import nl.tno.ids.configuration.Constants
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.AuthenticationException
import org.springframework.web.bind.annotation.*
import java.util.*
import javax.servlet.http.HttpServletRequest


/**
 * Authentication endpoint RestController for handling sign-in requests of users
 */
@RestController
@RequestMapping("/api/auth")
class AuthController(
    /** Authentication manager holding users & API keys */
    private val authManager: AuthManager,
    /** Bearer token provider, used for creating tokens */
    private val bearerTokenProvider: BearerTokenProvider
) {
    /**
     * Sign-in endpoint for user login based on username and password
     */
    @PostMapping("/signin")
    fun signin(@RequestParam username: String, @RequestParam password: String): ResponseEntity<*> {
        try {
            val user = authManager.getUser(username, password)

            return if (user != null) {
                if (user.locked) {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).body("User account locked")
                }
                val token = bearerTokenProvider.createToken(user.id, user.roles)
                val model: MutableMap<Any, Any?> = HashMap()
                model["username"] = username
                model["token"] = token
                model["roles"] = user.roles
                if (user.nextPasswordChangeBefore != null) {
                    model["nextPasswordChangeBefore"] = user.nextPasswordChangeBefore
                }
                ResponseEntity.ok(model)
            } else {
                return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid username/password supplied")
            }
        } catch (e: AuthenticationException) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Invalid username/password supplied")
        }
    }

    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @PostMapping("/signout")
    fun signout(@RequestParam username: String, request: HttpServletRequest): ResponseEntity<String> {
        bearerTokenProvider.invalidateTokens(username, request)
        return ResponseEntity.ok("")
    }

    /**
     * Basic authentication endpoint, only used for providing external basic authorization (e.g. for use in ingress
     * routes)
     */
    @GetMapping(value = ["/basic", "/basic/{role}"])
    fun basic(@RequestHeader(Constants.Headers.authorization, required = false) authorization: String?,
              @PathVariable(required = false) role: String?): ResponseEntity<*> {
        if (authorization?.startsWith("Basic ") == true) {
            val (username, password) = Base64.getDecoder()
                .decode(authorization.substring(6)).decodeToString()
                .split(":").let {
                    Pair(it.first(), it.last())
                }
            val user = authManager.getUser(username, password)

             if (user != null && !user.locked &&(role == null || user.roles.contains(role))) {
                 return ResponseEntity.ok("")
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED)
            .header("WWW-Authenticate", "Basic realm=\"TSG Core Container\"")
            .body("")
    }
}