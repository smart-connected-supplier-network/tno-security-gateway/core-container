package nl.tno.ids.security

import nl.tno.ids.clearing.AuditLog
import nl.tno.ids.configuration.model.SecurityRoles
import org.springframework.context.annotation.Bean
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.SecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.web.filter.GenericFilterBean
import org.springframework.web.util.ContentCachingRequestWrapper
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Spring Web Security Adapter for optionally enabling Security by means of users or API keys
 *
 * When security is disabled, all requests assume the ADMIN role is present to remain compatible with the
 * @Security annotations in Spring Controllers
 */
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
class WebSecurityConfig(
    /** Bearer token provider */
    private val bearerTokenProvider: BearerTokenProvider,
    /** Security configuration */
    private val securityConfig: SecurityConfig,
    /** Audit log service */
    private val auditLog: AuditLog
) {

    /**
     * Configure HTTP Security
     *
     * This disables Cross Site Request Forgery (CSRF) and Cross-Origin Resource Sharing (CORS), these might be enabled
     * in the future. The /api/auth/ and /health endpoints are always accessible, and for any other request the
     * BearerTokenConfigurer handles the correct authentication.
     */
    @Bean
    fun configure(http: HttpSecurity): SecurityFilterChain {
        if (securityConfig.enabled) {
            http.csrf().disable().cors()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/auth/**", "/health").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore({ request, response, chain ->
                    chain.doFilter(ContentCachingRequestWrapper(request as HttpServletRequest), response)
                }, UsernamePasswordAuthenticationFilter::class.java)
                .addFilterAfter({ request, response, chain ->
                    val httpRequest = request as ContentCachingRequestWrapper
                    if (httpRequest.method == "GET") {
                        chain.doFilter(request, response)
                    } else {
                        val category = httpRequest.requestURI.substring(request.contextPath.length)
                        if (category.startsWith("/api")) {
                            val change = "${httpRequest.method}: ${httpRequest.contentAsByteArray.take(1024).toByteArray().decodeToString()}"
                            chain?.doFilter(request, response)
                            val httpResponse = response as HttpServletResponse
                            auditLog.logConfigurationChange(
                                SecurityContextHolder.getContext().authentication?.name ?: "unknown",
                                category,
                                change,
                                httpResponse.status
                            )
                        }
                    }
                }, UsernamePasswordAuthenticationFilter::class.java)
                .apply(BearerTokenConfigurer(bearerTokenProvider))

        } else {
            http.csrf().disable().cors()
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/api/auth/**", "/health").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilterBefore(object : GenericFilterBean() {
                    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {
                        SecurityContextHolder.getContext().authentication = UsernamePasswordAuthenticationToken("guest", "",
                            SecurityRoles.getRelatedRoles(SecurityRoles.ADMIN).map { relatedRole -> SimpleGrantedAuthority(relatedRole) } + SimpleGrantedAuthority("APIKEY"))

                        chain?.doFilter(request, response)
                    }

                }, UsernamePasswordAuthenticationFilter::class.java)
        }
        return http.build()
    }

}