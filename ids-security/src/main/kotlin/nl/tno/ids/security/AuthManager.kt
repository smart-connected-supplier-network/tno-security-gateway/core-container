package nl.tno.ids.security

import org.apache.commons.lang3.RandomStringUtils
import org.slf4j.LoggerFactory
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import java.util.concurrent.ConcurrentHashMap

data class LoginAttempts(
    val attempts: List<Long>,
    val lockedUntil: Long?
)

/**
 * Authentication Manager holding the state of API keys and User tokens
 */
@Component
class AuthManager(
    /** Security configuration */
    private val securityConfig: SecurityConfig
) {
    /**
     * In-memory storage of active API keys
     */
    private val apiKeys = ConcurrentHashMap<String, ApiKeyConfig>()

    /**
     * In-memory storage of active users
     */
    private val users = ConcurrentHashMap<String, UserConfig>()

    /**
     * Failed login attempts
     */
    private val failedLoginAttempts = ConcurrentHashMap<String, LoginAttempts>()

    /**
     * Internal API key used for internal API interaction
     */
    final val internalApiKey = ApiKeyConfig("internal", "APIKEY-${generatePassword()}", roles = setOf("ROLE_INTERNAL"))

    companion object {
        private val LOG = LoggerFactory.getLogger(AuthManager::class.java)
    }

    /** Initial state configuration */
    init {
        securityConfig.apiKeys.forEach {
            apiKeys[it.key] = it.copy(roles = it.roles.map { role -> "ROLE_$role" }.toSet())
        }

        apiKeys["internal"] = internalApiKey

        securityConfig.users.forEach {
            users[it.id] = it.copy(roles = it.roles.map { role -> "ROLE_$role" }.toSet())
        }

        if(securityConfig.enabled && securityConfig.users.none {
            it.roles.contains("ADMIN")
        }) {
            // Generate an admin user.
            LOG.info("No Admin user detected, generating one.")
            val password = generatePassword()
            val bcryptEncodedPassword = BCryptPasswordEncoder().encode(password)

            users["admin"] = UserConfig(
                id = "admin",
                password = bcryptEncodedPassword,
                roles = setOf("ROLE_ADMIN")
            )
            if (securityConfig.passwordRotation) {
                users["admin"]?.nextPasswordChangeBefore = System.currentTimeMillis() + securityConfig.rotationPeriod
            }
            LOG.info("""
               
               An admin account has been generated:
               
               username: admin
               password: $password   
                           
               Keep this password secure!
               
               
           """.trimIndent())
        }
    }

    /** Generate random password */
    private fun generatePassword(passwordLength: Int = 30): String {
        val upperCaseLetters: String = RandomStringUtils.random(passwordLength, 65, 90, true, true)
        val lowerCaseLetters: String = RandomStringUtils.random(passwordLength, 97, 122, true, true)

        val password = (upperCaseLetters + lowerCaseLetters).toCharArray()
        password.shuffle()

        return password.concatToString()
    }

    fun userExists(username: String): Boolean {
        return users.containsKey(username)
    }

    /**
     * Get user based on credentials
     */
    fun getUser(username: String, password: String): UserConfig? {
        failedLoginAttempts[username]?.let { loginAttempts ->
            if (loginAttempts.lockedUntil !== null && loginAttempts.lockedUntil > System.currentTimeMillis()) {
                return null
            }
        }
        return users[username]?.let { user ->
            if (BCryptPasswordEncoder().matches(password, user.password)) {
                if (user.locked && user.lockedUntil!! < System.currentTimeMillis()) {
                    failedLoginAttempts.remove(username)
                    val unlockedUser = user.copy(locked = false, lockedUntil = null)
                    users[username] = unlockedUser
                    unlockedUser
                } else {
                    user
                }
            } else {
                val loginAttempt = failedLoginAttempts[username] ?: LoginAttempts(listOf(), null)
                val attempts = loginAttempt.attempts.filter { attempt -> attempt > System.currentTimeMillis() - securityConfig.failedAttemptDelay } + System.currentTimeMillis()
                if (attempts.size >= securityConfig.maxFailedAttempts) {
                    val lockedUser = user.copy(locked = true, lockedUntil = System.currentTimeMillis() + securityConfig.accountLockDuration)
                    users[username] = lockedUser
                    lockedUser
//                    failedLoginAttempts[username] = LoginAttempts(attempts, System.currentTimeMillis() + securityConfig.accountLockDuration)
                } else {
                    failedLoginAttempts[username] = LoginAttempts(attempts, null)
                    null
                }
            }
        }
    }

    /**
     * Get API key details based on token
     */
    fun getApiKey(token: String): ApiKeyConfig? {
        return apiKeys[token]?.takeUnless { it.id == "internal" }
    }

    /**
     * Get Users
     */
    fun getUsers() = users.values.toList()

    /**
     * Get API Keys
     */
    fun getApiKeys() = apiKeys.values.filter { it.id != "internal" }.toList()

    /**
     * Update User
     */
    fun insertUser(user: UserConfig) {
        if (users.containsKey(user.id)) {
            throw Exception("User with id ${user.id} already exists")
        }
        if (securityConfig.passwordRotation) {
            user.nextPasswordChangeBefore = System.currentTimeMillis() + securityConfig.rotationPeriod
        }
        users[user.id] = user
    }

    /**
     * Update API Key
     */
    fun insertApiKey(apiKey: ApiKeyConfig) {
        if (apiKeys.containsKey(apiKey.key)) {
            throw Exception("API key ${apiKey.key} already exists")
        }
        if (securityConfig.passwordRotation) {
            apiKey.nextApiKeyChangeBefore = System.currentTimeMillis() + securityConfig.rotationPeriod
        }
        apiKeys[apiKey.key] = apiKey
    }

    /**
     * Update User
     */
    fun updateUser(user: UserConfig, updatedPassword: Boolean) {
        if (securityConfig.passwordRotation && updatedPassword) {
            user.nextPasswordChangeBefore = System.currentTimeMillis() + securityConfig.rotationPeriod
        }
        users[user.id] = user
    }

    /**
     * Update User
     */
    fun resetLockedUser(userId: String) {
        if (!users.containsKey(userId)) {
            throw Exception("User does not exist")
        }
        users[userId] = users[userId]!!.copy(locked = false, lockedUntil = null)
        failedLoginAttempts.remove(userId)
    }

    /**
     * Update API Key
     */
    fun updateApiKey(apiKey: ApiKeyConfig, updatedApiKey: Boolean) {
        if (securityConfig.passwordRotation && updatedApiKey) {
            apiKey.nextApiKeyChangeBefore = System.currentTimeMillis() + securityConfig.rotationPeriod
        }
        apiKeys[apiKey.key] = apiKey
    }

    /**
     * Delete User
     */
    fun deleteUser(userId: String): Boolean {
        return users.remove(userId) != null
    }

    /**
     * Delete API Key
     */
    fun deleteApiKey(apiKeyId: String): Boolean {
        return apiKeys.values.find { it.id == apiKeyId }?.let { apiKeys.remove(it.key) } != null
    }

}