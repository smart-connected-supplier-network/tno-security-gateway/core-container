package nl.tno.ids.security

import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.model.SecurityRoles
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Component
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import javax.crypto.spec.SecretKeySpec
import javax.servlet.http.HttpServletRequest

/**
 * Spring User object for API/GUI users
 */
class AdminUser(userId: String, roles: Set<String>) : User(
        userId,
        "",
        roles.flatMap { role ->
            SecurityRoles.getRelatedRoles(role)
                .map { relatedRole -> SimpleGrantedAuthority(relatedRole) }
        } + SimpleGrantedAuthority("USER"))
/**
 * Spring User object for API Keys
 */
class ApiKey(apiKeyId: String, roles: Set<String>) : User(
        apiKeyId,
        "",
        roles.flatMap { role ->
            SecurityRoles.getRelatedRoles(role)
                .map { relatedRole -> SimpleGrantedAuthority(relatedRole) }
        } + SimpleGrantedAuthority("APIKEY"))

/**
 * BearerTokenProvider that is able to resolve and validate API keys and JWTs of users
 */
@Component
class BearerTokenProvider(
    /** Authentication manager */
    private val authManager: AuthManager
) {
    companion object {
        /**
         * Secret key for JWT validation
         */
        private val secretKey = SecretKeySpec(UUID.randomUUID().toString().toByteArray(), "HmacSHA256")

        /**
         * User token validity period (1 hour)
         */
        private const val validityInMilliseconds: Long = 3600000L

        /**
         * SLF4J Logger
         */
        private val LOG = LoggerFactory.getLogger(BearerTokenProvider::class.java)
    }

    /**
     * In-memory storage of active tokens for Admin users of this connector
     */
    private val userTokenMap = ConcurrentHashMap<String, Pair<String, Set<String>>>()

    /**
     * JWTS Parser for parsing JWTs from API/GUI users
     */
    private val jwtsParser = Jwts.parserBuilder().setSigningKey(secretKey).build()

    /**
     * List of tokens that are blacklisted.
     */
    private val blacklistedTokens = ConcurrentHashMap<String, String>()
    /**
     * Resolve token from HttpServletRequest as String
     */
    fun resolveToken(request: HttpServletRequest): String? {
        val bearerToken = request.getHeader(Constants.Headers.authorization)
        return if (bearerToken == null || !bearerToken.startsWith("Bearer ")) {
            null
        } else {
            bearerToken.substring(7)
        }
    }

    /**
     * Validate token based on the secret key and JWT validity
     */
    fun validateToken(token: String): Boolean {
        return if (token.startsWith("APIKEY-")) {
            true
        } else {
            try {
                val claims = jwtsParser.parseClaimsJws(token)
                return !claims.body.expiration.before(Date())
            } catch (e: JwtException) {
                LOG.warn("Token validation error: {}", e.message)
                return false
            }
        }
    }

    /**
     * Resolve token to a Spring Authentication object
     */
    fun getAuthentication(token: String): Authentication? {
        return if (token.startsWith("APIKEY-")) {
            authManager.getApiKey(token)?.let {
                val adminUser = ApiKey(it.id, it.roles)
                UsernamePasswordAuthenticationToken(adminUser, "", adminUser.authorities)
            }
        } else {
            if (blacklistedTokens.containsKey(token)) {
                return null
            }
            userTokenMap[token]?.let {
                if (authManager.userExists(it.first)) {
                    val adminUser = AdminUser(it.first, it.second)
                    UsernamePasswordAuthenticationToken(adminUser, "", adminUser.authorities)
                } else {
                    null
                }
            }
        }
    }

    /**
     * Generate a token for a specific User of this Core Container
     */
    fun createToken(userId: String, roles: Set<String>): String? {
        val claims = Jwts.claims().setSubject(userId)
        claims["roles"] = roles
        val now = Date()
        val validity = Date(now.time + validityInMilliseconds)
        val token = Jwts.builder()
            .setClaims(claims)
            .setIssuedAt(now)
            .setExpiration(validity)
            .signWith(secretKey, SignatureAlgorithm.HS256)
            .compact()
        userTokenMap[token] = userId to roles
        return token
    }

    /**
     * Invalidate active tokens for user
     */
    fun invalidateTokens(userId: String, request: HttpServletRequest) {
        val token: String? = resolveToken(request)
        if (token != null) {
            blacklistedTokens[token] = userId
            LOG.info("Added {} to blacklisted tokens for {}",token,userId)
        }
        userTokenMap.filterValues { it.first === userId }.keys.forEach { token ->
            blacklistedTokens[token] = userId
            userTokenMap.remove(token)
        }

    }
}
