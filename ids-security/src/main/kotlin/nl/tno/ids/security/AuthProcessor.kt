package nl.tno.ids.security

import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.RejectionMessageBuilder
import de.fraunhofer.iais.eis.RejectionReason
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildAsResponseTo
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.SecurityRoles
import nl.tno.ids.tokenmanager.TokenManagerService
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.springframework.lang.Nullable
import org.springframework.stereotype.Component

/**
 * Apache Camel Authentication Processor for validating incoming requests.
 *
 * Requires an API key
 */
@Component("authProcessor")
class AuthProcessor(
    /** Security configuration */
    private val securityConfig: SecurityConfig,
    /** Authentication manager */
    private val authManager: AuthManager,
    /** Connector info configuration */
    private val connectorInfo: ConnectorInfo,
    /** Token manager service for optionally injecting DATs */
    @Nullable private val tokenManagerService: TokenManagerService?
): Processor {
    /**
     * Process the incoming exchange and validate the bearer token
     */
    override fun process(exchange: Exchange) {
        if (securityConfig.enabled) {
            val authorization = exchange.message.getHeader(Constants.Headers.authorization, String::class.java)
            when {
                authorization == null -> {
                    stopRoute(exchange, null, RejectionReason.NOT_AUTHENTICATED, 403)
                    return
                }
                !authorization.startsWith("Bearer ") -> {
                    stopRoute(exchange, null, RejectionReason.MALFORMED_MESSAGE, 403, "Authorization header must be a Bearer token")
                    return
                }
            }
            val token = authorization.substring(7)


            if (authManager.getApiKey(token)?.roles?.any { it == SecurityRoles.DATA_APP || it == SecurityRoles.INTERNAL } == false) {
                stopRoute(exchange, null, RejectionReason.NOT_AUTHORIZED, 403)
            }
        }
    }

    /**
     * Stop the current Camel exchange, send the exchange message directly back to the Camel
     * Consumer without further processing
     */
    private fun stopRoute(
        exchange: Exchange,
        message: Message?,
        rejectionReason: RejectionReason,
        statusCode: Int,
        payload: String? = null
    ) {
        val multiPartMessage =
            RejectionMessageBuilder()
                ._rejectionReason_(rejectionReason)
                .buildAsResponseTo(message, connectorInfo, tokenManagerService?.acquireToken(""))
                .toMultiPartMessage(payload = payload)
        exchange.message.removeHeaders("*")
        exchange.message.body = multiPartMessage.toString()
        exchange.message.setHeader("Content-Type", multiPartMessage.httpHeaders["Content-Type"])
        exchange.message.setHeader(Exchange.HTTP_RESPONSE_CODE, statusCode)
        exchange.isRouteStop = true
    }
}