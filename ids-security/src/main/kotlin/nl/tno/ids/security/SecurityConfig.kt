package nl.tno.ids.security

import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.Serializable
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

/**
 * API Key configuration with API key id, key and roles
 *
 * API Keys must start with "APIKEY-" to be accepted by the core container
 * Roles must be in the scope of nl.tno.configuration.model.SecurityRoles
 */
@Validated
@Serializable
@ConstructorBinding
data class ApiKeyConfig(
    @field:NotNull val id: String,
    @field:NotNull @field:Pattern(regexp = "APIKEY-.*", message = "Invalid API key configuration, API key must start with \"APIKEY-\"") val key: String,
    val roles: Set<String> = emptySet(),
    var nextApiKeyChangeBefore: Long = System.currentTimeMillis() + 90*24*60*60*1000L,

    )

/**
 * User Configuration with user id, password and roles
 *
 * Passwords must adhere to the BCrypt standard and are validated by Spring upon start of the Core Container
 * Roles must be in the scope of nl.tno.configuration.model.SecurityRoles
 */
@Validated
@Serializable
@ConstructorBinding
data class UserConfig(
    @field:NotNull val id: String,
    @field:NotNull
    @field:Pattern(regexp = "[\$]2[abxy]?[\$](?:0[4-9]|[12]\\d|3[01])[\$][./\\da-zA-Z]{53}", message = "Invalid password configuration, password must be in bcrypt format")
    val password: String?,
    val roles: Set<String> = emptySet(),
    @EncodeDefault
    val locked: Boolean = false,
    val lockedUntil: Long? = null,
    var nextPasswordChangeBefore: Long = System.currentTimeMillis() + 90*24*60*60*1000L,

    )

/**
 * Security Configuration properties to enable the security setup and provide initial API keys and Users
 */
@Component
@ConfigurationProperties(prefix = "security")
@Validated
class SecurityConfig {
    var enabled: Boolean = false
    var externalAuthProvider: Boolean = true
    var apiKeys: List<ApiKeyConfig> = emptyList()
    var users: List<UserConfig> = emptyList()
    var maxFailedAttempts: Int = 10
    var failedAttemptDelay: Long = 15*60*1000
    var accountLockDuration: Long = 15*60*1000
    var httpsOnly: Boolean = false
    var tlsVersions: List<String> = listOf("TLSv1.3")
    var cipherSuites: List<String> = emptyList()
    var passwordRotation: Boolean = false
    var rotationPeriod: Long = 90*24*60*60*1000L
}