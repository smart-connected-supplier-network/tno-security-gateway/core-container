package nl.tno.ids.security

import org.apache.camel.builder.RouteBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/** Spring Configuration for providing a Camel RouteBuilder bean for external authentication */
@Configuration
class ExternalAuthCamelConfiguration(
    /** Camel port modifier */
    @Value("\${camel.port:8080}") private val camelPort: String,
    @Value("\${server.port:8082}") private val springPort: String,
    /** Camel HTTPS flag */
    @Value("\${routes.https:false}") private val https: Boolean
) {

    /**
     * External authentication Camel RouteBuilder
     */
    @Bean("externalAuthRoute")
    @ConditionalOnExpression("\${security.enabled:true} and \${security.externalAuthProvider:true}")
    fun externalAuthCamelRoute() =
        object : RouteBuilder() {
            override fun configure() {
                LoggerFactory.getLogger(ExternalAuthCamelConfiguration::class.java).info("Creating external authentication route")
                from("jetty:http${if (https) "s" else ""}://0.0.0.0:$camelPort/external-auth?enableMultipartFilter=false&matchOnUriPrefix=true")
                    .routeId("external-auth")
                    .to("http://localhost:$springPort/api/auth/external/?throwExceptionOnFailure=false&bridgeEndpoint=true")
            }
        }
}