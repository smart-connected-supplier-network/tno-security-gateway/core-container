package nl.tno.ids.security

import org.springframework.security.config.annotation.SecurityConfigurerAdapter
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.web.DefaultSecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import javax.servlet.http.HttpServletResponse

/**
 * Jwt Configurer that acts as Spring SecurityConfigurerAdapter which adds a Bearer Token Filter to the Spring Security
 * filter chain.
 */
class BearerTokenConfigurer(
    /** Bearer token provider for resolving Bearer tokens */
    private val bearerTokenProvider: BearerTokenProvider
) : SecurityConfigurerAdapter<DefaultSecurityFilterChain?, HttpSecurity>() {

    /**
     * Add exception handling and Bearer Token Filter to Spring HttpSecurity structure
     */
    override fun configure(http: HttpSecurity) {
        http.exceptionHandling()
            .authenticationEntryPoint { _, response, _ ->
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token authentication failed")
            }
            .and()
            .addFilterBefore(BearerTokenFilter(bearerTokenProvider), UsernamePasswordAuthenticationFilter::class.java)
    }

}
