package nl.tno.ids.security

import nl.tno.ids.configuration.model.SecurityRoles
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression
import org.springframework.core.io.InputStreamResource
import org.springframework.http.*
import org.springframework.web.bind.annotation.*
import java.net.URI
import java.net.URLEncoder
import java.nio.charset.Charset
import java.time.Duration
import java.util.concurrent.ConcurrentHashMap
import javax.servlet.http.HttpServletRequest

/**
 * Session detail data class
 */
data class SessionDetail(
    val userId: String,
    val roles: Set<String>
)

/**
 * External Authentication Provider for providing SSO-like features to services alongside the core container
 */
@RestController
@RequestMapping("/api/auth/external")
@ConditionalOnExpression("\${security.enabled:true} and \${security.externalAuthProvider:true}")
class ExternalAuthProviderController(
    /** Authentication manager holding users & API keys */
    private val authManager: AuthManager
) {
    /** In-memory session storage */
    private val sessions = ConcurrentHashMap<String, SessionDetail>()

    /** Sign-in page */
    @GetMapping("/signin")
    fun signin(request: HttpServletRequest, @RequestParam(required = false) rd: String?, @RequestParam(required = false) roles: String?): ResponseEntity<Any> {
        return getSessionDetail(request)?.let { sessionDetail ->
            if (roles?.isNotEmpty() == true && roles.split(",").intersect(sessionDetail.roles).isEmpty()) {
                ResponseEntity.ok("Logged in as ${sessionDetail.userId} with roles ${sessionDetail.roles}, but one of ${roles.split(",")} roles required. <a href=\"logout\">Logout</a> and try logging in with a different user.")
            } else {
                rd?.let {
                    ResponseEntity.status(HttpStatus.FOUND).location(URI(rd)).build()
                } ?: ResponseEntity.ok("Logged in as ${sessionDetail.userId} with roles ${sessionDetail.roles}. <a href=\"logout\">Logout</a>.")
            }
        } ?: ResponseEntity
            .ok()
            .contentType(MediaType.TEXT_HTML)
            .body(
                InputStreamResource(
                    ExternalAuthProviderController::class.java.getResourceAsStream("/static/external-auth-ui.html")
                        ?: throw Exception("external-auth-ui.html not found")
                )
            )
    }

    /** Logout user by removing the session reference and the session cookie */
    @GetMapping("/logout")
    fun logout(request: HttpServletRequest): ResponseEntity<Any> {
        request.cookies?.find {
            it.name == "tsgSession"
        }?.let {
            sessions.remove(it.value)
        }
        val sessionDeleteCookie = ResponseCookie.from("tsgSession", "")
            .httpOnly(true)
            .secure(true)
            .sameSite("None")
            .path("/")
            .maxAge(0)
            .build()
        return ResponseEntity
            .status(HttpStatus.FOUND)
            .header(HttpHeaders.SET_COOKIE, sessionDeleteCookie.toString())
            .location(URI("signin"))
            .body("")
    }

    /** Login endpoint that creates a session cookie, redirects to the original URL when succeeded */
    @PostMapping("/login", consumes = [MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE])
    fun signin(@RequestParam username: String, @RequestParam password: String, @RequestParam(required = false) rd: String?, @RequestParam(required = false) roles: String?): ResponseEntity<Any> {
        return authManager.getUser(username, password)?.let { userConfig ->
            if (userConfig.locked) {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User account locked")
            }
            val userRoles = userConfig.roles.flatMap { SecurityRoles.getRelatedRoles(it) }.toSet()
            if (roles?.isNotEmpty() == true && roles.split(",").intersect(userRoles).isEmpty()) {
                return ResponseEntity.ok("Logged in as ${userConfig.id} with roles ${userRoles}, but one of ${roles.split(",")} roles required. <a href=\"logout\">Logout</a> and try logging in with a different user.")
            }
            val sessionId = RandomStringUtils.randomAlphanumeric(64)
            val sessionCookie = ResponseCookie.from("tsgSession", sessionId)
                .httpOnly(true)
                .secure(true)
                .sameSite("None")
                .path("/")
                .maxAge(Duration.ofDays(1))
                .build()
            sessions[sessionId] = SessionDetail(userConfig.id, userRoles)
            rd?.let {
                ResponseEntity
                    .status(HttpStatus.FOUND)
                    .header(HttpHeaders.SET_COOKIE, sessionCookie.toString())
                    .location(URI(rd)).build()
            } ?: ResponseEntity.ok("Logged in as ${userConfig.id} with roles $userRoles. <a href=\"logout\">Logout</a>.")
        } ?: ResponseEntity.status(HttpStatus.FOUND).location(URI("signin?rd=${URLEncoder.encode(rd, Charset.defaultCharset())}")).build()
    }

    /** Primary auth endpoint to check whether a user is logged in */
    @GetMapping("/auth")
    fun auth(request: HttpServletRequest, @RequestParam(required = false) roles: String?, @RequestHeader(HttpHeaders.AUTHORIZATION, required = false) authorization: String?): ResponseEntity<Any> {
        val bearerToken = authorization?.let {
            authManager.getApiKey(it.substring(7))
        }
        val sessionDetail = getSessionDetail(request)
        val authRoles = bearerToken?.roles ?: sessionDetail?.roles ?: emptySet()

        return when {
            roles?.isNotEmpty() == true && roles.split(",").intersect(authRoles).isEmpty() ->
                ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(
                    "Logged in as ${sessionDetail?.userId ?: "API key"} with roles ${authRoles}, but one of ${
                        roles.split(
                            ","
                        )
                    } roles required"
                )
            bearerToken != null -> ResponseEntity.ok("")
            sessionDetail != null -> ResponseEntity.ok("")
            else -> ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("")
        }
    }

    /** Get session detail via a cookie in the servlet request */
    private fun getSessionDetail(request: HttpServletRequest): SessionDetail? {
        return request.cookies?.find {
            it.name == "tsgSession"
        }?.let {
            sessions[it.value]
        }
    }
}