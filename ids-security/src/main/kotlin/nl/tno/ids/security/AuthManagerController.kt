package nl.tno.ids.security

import nl.tno.ids.configuration.model.SecurityRoles
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException

/**
 * Authentication Manager Controller for manipulating the users and API keys available in the Core Container
 */
@RestController
@RequestMapping("/api/auth")
@Secured(SecurityRoles.ADMIN)
class AuthManagerController(
    /** Authentication manager */
    private val authManager: AuthManager,
    private val securityConfig: SecurityConfig
) {
    /** Mandatory API key prefix regular expression */
    val apiKeyPattern = Regex("APIKEY-.*")
    /** BCrypt validation regular expression */
    val bCryptPasswordPattern = Regex("[\$]2[abxy]?[\$](?:0[4-9]|[12][0-9]|3[01])[\$][./0-9a-zA-Z]{53}")

    /**
     * List all roles
     */
    @GetMapping("roles")
    fun getRoles(): List<String> {
        return SecurityRoles::class.java.declaredFields.map { "ROLE_${it.name}" }.filter { it != "ROLE_INSTANCE" }
    }

    /**
     * List all users
     */
    @GetMapping("users")
    fun getUsers(): List<UserConfig> {
        return authManager.getUsers().map {
            it.copy(password = null)
        }
    }

    /**
     * Insert a new user
     */
    @PostMapping("users", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun insertUser(@RequestBody user: UserConfig) {
        when {
            user.password?.matches(bCryptPasswordPattern) == false ->
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid BCrypt password")
        }
        authManager.insertUser(user)
    }

    /**
     * Update an existing user
     */
    @PutMapping("users/{userId}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun updateUser(@PathVariable userId: String, @RequestBody user: UserConfig) {
        val bcryptPassword = user.password ?: authManager.getUsers().first { it.id == user.id }.password
        when {
            userId != user.id ->
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Path variable does not match identifier in body")
            bcryptPassword?.matches(bCryptPasswordPattern) == false ->
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid BCrypt password")
        }
        var updatedPassword = false
        if (user.password != null) {
            updatedPassword = true
        }
        authManager.updateUser(
            UserConfig(
                id = user.id,
                password = user.password ?: authManager.getUsers().first { it.id == user.id }.password,
                roles = user.roles,
                nextPasswordChangeBefore=user.nextPasswordChangeBefore
            ),
            updatedPassword
        )
    }

    /**
     * Reset user
     */
    @PutMapping("users/{userId}/reset")
    fun resetUser(@PathVariable userId: String) {
        authManager.resetLockedUser(userId)
    }

    /**
     * Delete a user
     */
    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @DeleteMapping("users/{userId}")
    fun deleteUser(@PathVariable userId: String) {
        if (authManager.getUsers().count() == 1) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Cannot delete the last admin user.")
        }
        if (!authManager.deleteUser(userId)) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }

    /**
     * List API keys
     */
    @GetMapping("apikeys")
    fun getApiKeys(): List<ApiKeyConfig> {
        return authManager.getApiKeys()
    }

    /**
     * Insert a new API key
     */
    @PostMapping("apikeys", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun insertApiKey(@RequestBody apiKey: ApiKeyConfig) {
        when {
            !apiKey.key.matches(apiKeyPattern) ->
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid API key, must start with \"APIKEY-\"")
        }
        authManager.insertApiKey(apiKey)
    }

    /**
     * Update an existing API key
     */
    @PutMapping("apikeys/{apiKeyId}", consumes = [MediaType.APPLICATION_JSON_VALUE])
    fun updateApiKey(@PathVariable apiKeyId: String, @RequestBody apiKey: ApiKeyConfig) {
        when {
            apiKeyId != apiKey.id ->
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Path variable does not match identifier in body")
            !apiKey.key.matches(apiKeyPattern) ->
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid API key, must start with \"APIKEY-\"")
        }
        var updatedApiKey = true
        if (securityConfig.passwordRotation
            && apiKey.key == authManager.getApiKeys().first { it.id == apiKey.id }.key) {
            updatedApiKey = false
        }
        if (updatedApiKey) {
            authManager.deleteApiKey(apiKeyId)
        }
        authManager.updateApiKey(apiKey, updatedApiKey)
    }

    /**
     * Delete an API key
     */
    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @DeleteMapping("apikeys/{apiKeyId}")
    fun deleteApiKey(@PathVariable apiKeyId: String) {
        if (!authManager.deleteApiKey(apiKeyId)) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }

}