package nl.tno.ids.security

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletRequest

/**
 * Spring Filter for extracting the relevant authentication information into a Spring Authentication object
 */
class BearerTokenFilter(
    /** Bearer token provider for resolving Bearer tokens */
    private val bearerTokenProvider: BearerTokenProvider
) : GenericFilterBean() {
    /** Filter servlet requests */
    override fun doFilter(request: ServletRequest?, response: ServletResponse?, chain: FilterChain?) {
        val token: String? = bearerTokenProvider.resolveToken(request as HttpServletRequest)

        if (token != null && bearerTokenProvider.validateToken(token)) {
            bearerTokenProvider.getAuthentication(token)?.let {
                SecurityContextHolder.getContext().authentication = it
            }
        }
        chain?.doFilter(request, response)
    }
}
