package nl.tno.ids.security
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

/**
 * Test class for testing the AuthManager
 */
class AuthManagerTest {
    /** Test authentication manager without configured admin user */
    @Test
    fun testWithoutConfiguredAdminUser() {
        val authManager = AuthManager(SecurityConfig().apply {
            enabled = true
        })

        Assertions.assertTrue(authManager.getUsers().any { it.id == "admin" && it.roles.contains("ROLE_ADMIN") })
    }

    /** Test authentication manager with configured admin user */
    @Test
    fun testWithConfiguredAdminUser() {
        val authManager = AuthManager(SecurityConfig().apply {
            enabled = true
            users = listOf(UserConfig("test-admin", "\$2a\$12\$Xx4uZNtOBu91N0.D1GBi6ePelIJbNT8UzDppN.fU0P4lomlG/BcCm", setOf("ADMIN")))
        })

        // Automatically generated admin account should not exist.
        Assertions.assertTrue(authManager.getUsers().none { it.id == "admin" && it.roles.contains("ROLE_ADMIN") })

        // test-admin account should exist.
        Assertions.assertTrue(authManager.getUsers().any { it.id == "test-admin" && it.roles.contains("ROLE_ADMIN") })
    }

    /** Test locking of user */
    @Test
    fun testLocking() {
        val authManager = AuthManager(SecurityConfig().apply {
            enabled = true
            users = listOf(UserConfig("test-admin", BCryptPasswordEncoder().encode("password"), setOf("ADMIN")))
            accountLockDuration = 100
        })
        Assertions.assertNotNull(authManager.getUser("test-admin", "password"))
        repeat(10) {
            authManager.getUser("test-admin", "wrong-password")
        }
        Assertions.assertEquals(true, authManager.getUser("test-admin", "password")?.locked)
        Thread.sleep(200)
        Assertions.assertNotNull(authManager.getUser("test-admin", "password"))
    }

    @Test
    fun testPasswordRotation() {
        val authManager = AuthManager(SecurityConfig().apply{
            enabled = true
            passwordRotation = true
            rotationPeriod = 90*24*60*60*1000L
        })
        authManager.insertUser(UserConfig("test-admin", BCryptPasswordEncoder().encode("password"), setOf("ADMIN")))
        Assertions.assertNotNull(authManager.getUser("test-admin", "password"))
        Assertions.assertNotNull(authManager.getUser("test-admin", "password")?.nextPasswordChangeBefore)
        Assertions.assertTrue(authManager.getUser("test-admin", "password")?.nextPasswordChangeBefore!! < System.currentTimeMillis() + 90*24*60*60*1000L)
    }

    @Test
    fun testApiKeyRotation() {
        val authManager = AuthManager(SecurityConfig().apply{
            enabled = true
            passwordRotation = true
            rotationPeriod = 90*24*60*60*1000L
        })
        authManager.insertApiKey(ApiKeyConfig("admin-api-key", "APIKEY-1234562390412390842039SADF", setOf("ADMIN")))
        Assertions.assertNotNull(authManager.getApiKey("APIKEY-1234562390412390842039SADF"))
        Assertions.assertNotNull(authManager.getApiKey("APIKEY-1234562390412390842039SADF")?.nextApiKeyChangeBefore)
    }
}