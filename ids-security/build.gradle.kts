plugins {
    kotlin("plugin.spring")
    kotlin("plugin.serialization")
}

dependencies {
    // Camel Spring Boot integration
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")
    implementation("org.springframework.boot:spring-boot-starter-web")
    api("org.springframework.boot:spring-boot-starter-security")

    implementation(project(":ids-clearing"))
    implementation(project(":ids-configuration"))
    implementation(project(":ids-token-manager"))

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${LibraryVersions.Kotlin.kotlinxSerialization}")
    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    testImplementation("org.apache.camel.springboot:camel-http-starter")
    testImplementation("org.apache.camel.springboot:camel-jetty-starter")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-opt-in=kotlinx.serialization.ExperimentalSerializationApi"
    }
}