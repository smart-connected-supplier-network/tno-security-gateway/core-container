# IDS Policy Enforcment Framework

This module contains the embedded Policy Enforcement Framework.

The Policy Enforcement Framework follows the XACML architecture:

<img src="https://upload.wikimedia.org/wikipedia/commons/f/f2/XACML_Architecture_%26_Flow.png" width="600">

# Policy Administration Point

The Policy Administration Point (PAP) offers a management API for CRUD operations for ContractOffers and ContractAgreements in the administration of policies.

The ContractOffers are used in the negotiation process to be able to automatically negotiate ContractAgreements.
The ContractAgreements are retrieved by the PDP at the moment a decision has to be made whether or not to allow requests. 

# Policy Enforcement Point

The Policy Enforcement Point (PEP) makes sure that policies are enforced for all of the messages that flow through the PEP. The PEP is an Apache Camel processor that can be included in Camel routes, which makes that the messages in that route will be enforced. 

# Policy Decision Point

The Policy Decision Point (PDP) makes the actual decision whether or not to allow a request, based on the context of the request. This includes the policy itself (from the PAP), environmental properties (from the PEP), additional attributes (from the PIP).

# Policy Information Point

The Policy Information Point (PIP) provides additional information that might be required for taking a decision. At this moment, just the times a contract/resource are used is stored in the PIP.

# Negotiation

The Negotiation Processor will actively intercept messages related to the negotiation process (`ContractRequestMessage`, `ContractOfferMessage`, `ContractAgreementMessage`, and `ContractRejectionMessage`) and handle the automated negotation process.

> Note: Currently automated negotiation only supports `ContractRequest`s that are similar or of narrower scope than the related `ContractOffer`