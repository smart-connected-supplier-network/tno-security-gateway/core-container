plugins {
    kotlin("plugin.spring")
    kotlin("plugin.serialization")
}

dependencies {
    // Camel Spring Boot integration
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")
    implementation(project(":ids-security"))

    implementation(project(":ids-configuration"))
    implementation(project(":ids-clearing"))
    implementation(project(":ids-route-manager"))
    implementation(project(":ids-token-manager"))

    // YAML Serialization support
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${LibraryVersions.Kotlin.kotlinxSerialization}")
    implementation("com.charleskorn.kaml:kaml:${LibraryVersions.Kotlin.kaml}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.apache.camel:camel-test-spring-junit5")
    testImplementation("org.apache.camel.springboot:camel-jetty-starter")
    testImplementation(project(":ids-test-extensions"))
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-opt-in=kotlinx.serialization.ExperimentalSerializationApi"
    }
}