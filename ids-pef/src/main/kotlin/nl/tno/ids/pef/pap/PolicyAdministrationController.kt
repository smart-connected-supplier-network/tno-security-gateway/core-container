/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef.pap

import de.fraunhofer.iais.eis.Contract
import de.fraunhofer.iais.eis.ContractOffer
import nl.tno.ids.configuration.infomodel.toJsonLD
import java.nio.charset.Charset
import nl.tno.ids.configuration.model.SecurityRoles
import nl.tno.ids.configuration.serialization.Serialization
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriUtils

/** RestController for interacting with a PolicyAdministrationPoint */
@RestController
@RequestMapping(value = ["/api/pap"])
@Secured(SecurityRoles.PEF_MANAGER)
class PolicyAdministrationController(
    /** Policy Administration Point for storing Contracts and Offers */
    private val policyAdministrationPoint: PolicyAdministrationPoint,
) {
    /** List all agreed contracts in JSON format */
    @GetMapping(value = ["/contracts"], produces = [MediaType.APPLICATION_JSON_VALUE])
    @Secured(SecurityRoles.PEF_READER)
    fun listContracts(): ResponseEntity<String> {
        return ResponseEntity.ok(policyAdministrationPoint.listContracts().toJsonLD())
    }

    /** Insert a contract agreement */
    @PostMapping(
        value = ["/contracts"],
        consumes = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"])
    fun insertContract(@RequestBody payload: String): ResponseEntity<String> {
        val contract = Serialization.fromJsonLD<Contract>(payload)
        return if (policyAdministrationPoint.insertContract(contract)) {
            ResponseEntity.ok("")
        } else {
            ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    /** Retrieve a Contract by identifier */
    @GetMapping(value = ["/contracts/{contractId}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    @Secured(SecurityRoles.PEF_READER)
    fun getContract(@PathVariable contractId: String): ResponseEntity<String> {
        return policyAdministrationPoint.getContract(
                UriUtils.decode(contractId, Charset.defaultCharset()))
            ?.let { ResponseEntity.ok(it.toJsonLD()) }
            ?: ResponseEntity.notFound().build()
    }

    /** Delete a Contract by identifier */
    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @DeleteMapping(value = ["/contracts/{contractId}"])
    fun deleteContract(@PathVariable contractId: String): ResponseEntity<String> {
        return if (policyAdministrationPoint.deleteContract(
            UriUtils.decode(contractId, Charset.defaultCharset()))) {
            ResponseEntity.ok("")
        } else {
            ResponseEntity.notFound().build()
        }
    }

    /** List all contract offers in JSON format */
    @GetMapping(value = ["/offers"], produces = [MediaType.APPLICATION_JSON_VALUE])
    @Secured(SecurityRoles.PEF_READER)
    fun listOffers(): ResponseEntity<String> {
        return ResponseEntity.ok(policyAdministrationPoint.listOffers().toJsonLD())
    }

    /** Insert a contract offer */
    @PostMapping(
        value = ["/offers"], consumes = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"])
    fun insertOffer(@RequestBody payload: String): ResponseEntity<String> {
        val offer = Serialization.fromJsonLD<ContractOffer>(payload)
        return if (policyAdministrationPoint.insertOffer(offer)) {
            ResponseEntity.ok("")
        } else {
            ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build()
        }
    }

    /** Retrieve a Contract offer by identifier */
    @GetMapping(value = ["/offers/{offerId}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    @Secured(SecurityRoles.PEF_READER)
    fun getOffer(@PathVariable offerId: String): ResponseEntity<String> {
        return policyAdministrationPoint.getOffer(
                UriUtils.decode(offerId, Charset.defaultCharset()))
            ?.let { ResponseEntity.ok(it.toJsonLD()) }
            ?: ResponseEntity.notFound().build()
    }

    /** Delete a Contract offer by identifier */
    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @DeleteMapping(value = ["/offers/{offerId}"])
    fun deleteOffer(@PathVariable offerId: String): ResponseEntity<String> {
        return if (policyAdministrationPoint.deleteOffer(
            UriUtils.decode(offerId, Charset.defaultCharset()))) {
            ResponseEntity.ok("")
        } else {
            ResponseEntity.notFound().build()
        }
    }
}
