/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef.pap

import de.fraunhofer.iais.eis.Contract
import de.fraunhofer.iais.eis.ContractOffer

/**
 * Policy Administration Point for managing contracts and contract offers used by the Policy
 * Enforcement Point & Policy Decision Point
 */
interface PolicyAdministrationPoint {
    /** List agreed contracts */
    fun listContracts(): List<Contract>

    /** Insert new ContractAgreement */
    fun insertContract(contract: Contract): Boolean

    /** Get a Contract by identifier */
    fun getContract(contractId: String): Contract?

    /** Delete a Contract by identifier */
    fun deleteContract(contractId: String): Boolean

    /** List contract offers */
    fun listOffers(): List<ContractOffer>

    /** Insert new ContractOffer */
    fun insertOffer(offer: ContractOffer): Boolean

    /** Get a ContractOffer by identifier */
    fun getOffer(offerId: String): ContractOffer?

    /** Delete a ContractOffer by identifier */
    fun deleteOffer(offerId: String): Boolean
}
