/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef.pap

import de.fraunhofer.iais.eis.Contract
import de.fraunhofer.iais.eis.ContractOffer
import nl.tno.ids.configuration.infomodel.toJsonLD
import org.slf4j.LoggerFactory
import java.util.concurrent.ConcurrentHashMap
import org.springframework.stereotype.Service

/** In-memory implementation of an PolicyAdministrationPoint */
@Service
class InMemoryPolicyAdministrationPoint : PolicyAdministrationPoint {
    companion object {
        private val LOG = LoggerFactory.getLogger(InMemoryPolicyAdministrationPoint::class.java)
    }
    /** Stored ContractAgreements */
    private val contracts: MutableMap<String, Contract> = ConcurrentHashMap()

    /** Stored ContractOffers */
    private val offers: MutableMap<String, ContractOffer> = ConcurrentHashMap()

    /** List agreed contracts */
    override fun listContracts(): List<Contract> {
        return contracts.values.toList()
    }

    /** Insert new ContractAgreement */
    override fun insertContract(contract: Contract): Boolean {
        LOG.info("Inserting contract ${contract.id}")
        LOG.debug(contract.toJsonLD())
        contracts[contract.id.toString()] = contract
        return true
    }

    /** Get a Contract by identifier */
    override fun getContract(contractId: String): Contract? {
        return contracts[contractId]
    }

    /** Delete a Contract by identifier */
    override fun deleteContract(contractId: String): Boolean {
        LOG.info("Deleting contract $contractId")
        return contracts.remove(contractId) != null
    }

    /** List contract offers */
    override fun listOffers(): List<ContractOffer> {
        return offers.values.toList()
    }

    /** Insert new ContractOffer */
    override fun insertOffer(offer: ContractOffer): Boolean {
        LOG.info("Inserting offer ${offer.id}")
        LOG.debug(offer.toJsonLD())
        offers[offer.id.toString()] = offer
        return true
    }

    /** Get a ContractOffer by identifier */
    override fun getOffer(offerId: String): ContractOffer? {
        return offers[offerId]
    }

    /** Delete a ContractOffer by identifier */
    override fun deleteOffer(offerId: String): Boolean {
        LOG.info("Delete offer $offerId")
        return offers.remove(offerId) != null
    }
}
