/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef.pep

import com.fasterxml.jackson.databind.ObjectMapper
import de.fraunhofer.iais.eis.*
import nl.tno.ids.clearing.ClearingContext
import nl.tno.ids.clearing.ClearingDirection
import nl.tno.ids.clearing.ClearingLog
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildAsResponseTo
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.pef.pdp.*
import nl.tno.ids.tokenmanager.TokenManagerService
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.slf4j.LoggerFactory
import org.springframework.lang.Nullable
import org.springframework.stereotype.Component

class PolicyEnforcementException(message: String) : Exception(message)

@Component("pefProcessor")
class PolicyEnforcementProcessor(
    /** Policy Decision Point for taking decisions on given contexts */
    private val policyDecisionPoint: PolicyDecisionPoint,
    /** Policy Decision Point Configuration */
    private val policyDecisionPointConfiguration: PolicyDecisionPointConfiguration,
    /** TokenManagerService for requesting DAPS tokens for Rejection Messages */
    @Nullable private val tokenManagerService: TokenManagerService?,
    /** ConnectorInfo for constructing Rejection Messages */
    @Nullable private val connectorInfo: ConnectorInfo?,
    /** Local clearing handler */
    private val clearingLog: ClearingLog
) : Processor {
    companion object {
        private val LOG = LoggerFactory.getLogger(PolicyEnforcementProcessor::class.java)
    }

    /** Process the incoming message and prepare the context for the Policy Decision Point */
    override fun process(exchange: Exchange) {
        if (!policyDecisionPointConfiguration.enabled) return
        val messageHeader =
            exchange.message.getHeader(Constants.Headers.idsHeader, String::class.java)?.let {
                Serialization.fromJsonLD<Message>(it)
            }
                ?: run {
                    LOG.info("No IDS-Header found!")
                    stopRoute(
                        exchange,
                        null,
                        RejectionReason.MALFORMED_MESSAGE,
                        400,
                        "Incorrect message structure, no IDS header found")
                    return
                }

        if (exchange.message.getHeader("DESCRIPTION", String::class.java) == "ALLOW" && messageHeader is DescriptionRequestMessage) {
            LOG.debug("Allowing description request messages")
            return
        }


        val payload = exchange.message.getBody(String::class.java)
        val stage = Stage.valueOf(exchange.message.getHeader("PEF_STAGE", String::class.java))
        val mode = Mode.valueOf(exchange.message.getHeader("PEF_MODE", String::class.java))

        // Purpose can be set manually by Data Apps, in the future this will be extracted from its metadata
        val purpose: String? = exchange.message.getHeader("PEF_PURPOSE", String::class.java)

        val contract = messageHeader.transferContract?.toString()
        val consumer = messageHeader.issuerConnector?.toString()
        val provider = messageHeader.recipientConnector?.getOrNull(0)?.toString()
        val contextParameters = ContextParameters(purpose = purpose)

        if (consumer.isNullOrBlank() || provider.isNullOrBlank()) {
            LOG.warn("Message not complete, consumer: $consumer, provider: $provider")
            clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.INGRESS, messageHeader, properties = mapOf("PEP" to "Incorrect message structure, consumer and/or provider are missing"))
            stopRoute(
                exchange,
                null,
                RejectionReason.MALFORMED_MESSAGE,
                400,
                "Incorrect message structure, consumer and/or provider are missing")
            return
        }
        if (contract.isNullOrBlank()) {
            if (policyDecisionPointConfiguration.allowEmptyContract) {
                return
            } else {
                clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.INGRESS, messageHeader, properties = mapOf("PEP" to "Disallowed due to missing contract reference"))
                stopRoute(
                    exchange,
                    null,
                    RejectionReason.BAD_PARAMETERS,
                    400,
                    "Contract reference required")
                return
            }
        }

        val requestParameters =
            mapParameters(messageHeader, payload)
                ?: run {
                    if (policyDecisionPointConfiguration.allowUnmappableMessage) {
                        return
                    } else {
                        stopRoute(
                            exchange,
                            null,
                            RejectionReason.BAD_PARAMETERS,
                            501,
                            "Can't map ${messageHeader.javaClass.name} to internal structure")
                        return
                    }
                }

        val pdpRequest =
            PolicyDecisionRequest(
                consumer = consumer,
                provider = provider,
                senderAgent = messageHeader.senderAgent.toString(),
                recipientAgent = messageHeader.recipientAgent.firstOrNull()?.toString() ?: connectorInfo?.curator.toString(),
                contractId = contract,
                requestParameters = requestParameters,
                contextParameters = contextParameters,
                mode = mode,
                stage = stage)

        if (policyDecisionPoint.decide(pdpRequest)) {
            LOG.info("ODRL Pdp: Access Allowed")
            exchange.message.setHeader("PEF_DECISION", "ALLOW")
        } else {
            LOG.info("Access not allowed, pdp request parameters $pdpRequest")
            clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.INGRESS, messageHeader, properties = mapOf("PEP" to "Access not allowed"))
            stopRoute(exchange, null, RejectionReason.NOT_AUTHORIZED, 403, "Access not allowed")
        }
    }

    /**
     * Map the header and payload to a RequestParameters object containing the relevant information
     * for policy evaluation
     */
    private fun mapParameters(header: Message, payload: String?): RequestParameters? {
        return when (header) {
            is InvokeOperationMessage -> {
                if (header.operationReference.toString().startsWith("https://ids.tno.nl/openapi")) {
                    val payloadMap = ObjectMapper().readValue(payload, HashMap::class.java)
                    RequestParameters(
                        resourceId = payloadMap["path"] as String?,
                        resourceType = "rest",
                        action =
                            when (payloadMap["method"]) {
                                "get" -> Action.READ.name
                                "head" -> Action.READ.name
                                "put" -> Action.WRITE.name
                                "post" -> Action.MODIFY.name
                                "patch" -> Action.MODIFY.name
                                "delete" -> Action.DELETE.name
                                else -> Action.READ.name
                            })
                } else {
                    RequestParameters(
                        resourceId = header.operationReference.toString(),
                        resourceType = header.operationReference.toString(),
                        action = Action.MODIFY.name)
                }
            }
            is ArtifactRequestMessage -> {
                RequestParameters(
                    resourceId = header.requestedArtifact.toString(),
                    resourceType = "artifact",
                    action = Action.READ.name)
            }
            is NotificationMessage -> {
                RequestParameters(
                    resourceId = null, resourceType = null, action = Action.NOTIFY.name)
            }
            is DescriptionRequestMessage -> {
                RequestParameters(
                    resourceId = header.requestedElement.toString(),
                    resourceType = null,
                    action = Action.READ.name)
            }
            is QueryMessage -> {
                RequestParameters(
                    resourceId = (header.queryScope ?: QueryScope.ALL).id.toString(),
                    resourceType = "query",
                    action = Action.READ.name)
            }
            else -> {
                LOG.warn("No mapping available for ${header::class.java.name}")
                null
            }
        }
    }

    /**
     * Stop the current Camel exchange, send the exchange message directly back to the Camel
     * Consumer without further processing
     */
    private fun stopRoute(
        exchange: Exchange,
        message: Message?,
        rejectionReason: RejectionReason,
        statusCode: Int,
        payload: String? = null
    ) {
        val stopRoute = exchange.message.getHeader("PEF_STOP_ROUTE", true, Boolean::class.java)
        if (stopRoute) {
            val multiPartMessage =
                RejectionMessageBuilder()
                    ._rejectionReason_(rejectionReason)
                    .buildAsResponseTo(message, connectorInfo, tokenManagerService?.acquireToken(""))
                    .toMultiPartMessage(payload = payload)
            exchange.message.removeHeaders("*")
            exchange.message.body = multiPartMessage.toString()
            exchange.message.setHeader("Content-Type", multiPartMessage.httpHeaders["Content-Type"])
            exchange.message.setHeader(Exchange.HTTP_RESPONSE_CODE, statusCode)
            exchange.isRouteStop = true
        } else {
            LOG.info("PEF denied request, forwarded to data app: $payload")
            exchange.message.setHeader("PEF_DECISION", "DENY")
        }
    }
}
