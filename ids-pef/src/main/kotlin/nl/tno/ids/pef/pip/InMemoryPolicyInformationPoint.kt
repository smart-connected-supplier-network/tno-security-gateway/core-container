/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef.pip

import java.util.concurrent.ConcurrentHashMap
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

/**
 * In-Memory Policy Information Point for providing additional information used for taking decisions
 */
@Service
class InMemoryPolicyInformationPoint : PolicyInformationPoint {
    companion object {
        private val LOG = LoggerFactory.getLogger(PolicyInformationPoint::class.java)
    }

    /** In-Memory map of contract usages */
    private val timesUsedMap: MutableMap<Pair<String, String>, Int> = ConcurrentHashMap()

    /** Retrieve the amount of usages for a given contract and action */
    override fun getContractTimesUsed(contractId: String, action: String?): Int {
        LOG.info("getContractTimesUsed: $contractId $action")
        return timesUsedMap[Pair(contractId, action)] ?: 0
    }

    /** Increment the amount of usages for a given contract and action */
    override fun incrementContractTimesUsed(contractId: String, action: String) {
        LOG.info("incrementContractTimesUsed: $contractId $action")
        timesUsedMap.inc(Pair(contractId, action))
    }

    /**
     * Helper extension function to increment a value in a mutable map, even if the key does not
     * exist yet
     */
    fun <T> MutableMap<T, Int>.inc(key: T, more: Int = 1) = merge(key, more, Int::plus)
}
