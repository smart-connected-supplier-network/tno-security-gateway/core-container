/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef.pdp

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

/** Policy Decision Point Configuration */
@Component
@ConfigurationProperties(prefix = "pef.pdp")
data class PolicyDecisionPointConfiguration(
    /** Boolean on whether Policy Decision is enabled */
    var enabled: Boolean = true,
    /** Default Policy in case no matching policy can be found */
    var defaultPolicy: DecisionPolicy = DecisionPolicy.DENY_UNLESS,
    /** Allow incoming messages without transfer contract */
    var allowEmptyContract: Boolean = false,
    /**
     * Allow incoming message that are not supported for automatic mapping to a Policy Decision
     * Request context
     */
    var allowUnmappableMessage: Boolean = false
)

/** Enum class for default Decision Policy */
enum class DecisionPolicy {
    DENY_UNLESS,
    ALLOW_UNLESS
}
