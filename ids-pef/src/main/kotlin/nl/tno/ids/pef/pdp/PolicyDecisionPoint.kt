/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef.pdp

import de.fraunhofer.iais.eis.Duty
import de.fraunhofer.iais.eis.Permission
import de.fraunhofer.iais.eis.Prohibition
import de.fraunhofer.iais.eis.Rule
import nl.tno.ids.clearing.AuditLog
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.pef.pap.PolicyAdministrationPoint
import nl.tno.ids.pef.pip.PolicyInformationPoint
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.net.URI

/** Policy Decision Enum, used for single constraints or in RuleDecisions */
enum class Decision {
    ALLOW,
    DENY,
    NOT_APPLICABLE
}

/** Policy Mode Enum, used for indicating whether the message is ingress or egress */
enum class Mode {
    INGRESS,
    EGRESS
}

/**
 * Policy Stage Enum, used for indicating whether the decision is taken prior to or following after
 * the intended request
 */
enum class Stage {
    PRE,
    POST
}

/** Combined decision for a complete Rule */
data class RuleDecision(
    /** Combined decision */
    val decision: Decision,
    /** Pre-Duties for the decision */
    val preDuties: List<Duty> = emptyList(),
    /** Post-Duties for the decision */
    val postDuties: List<Duty> = emptyList()
)

/** Request Parameters indicating the relevant matching parts in contracts */
data class RequestParameters(
    /** Resource identifier */
    val resourceId: String?,
    /** Resource type */
    val resourceType: String?,
    /** (To be) Performed action */
    val action: String?
)

/** Context Parameters providing additional context to a Decision Request */
data class ContextParameters(
    /** Purpose Context, e.g. for Data Apps serving a specific purpose */
    val purpose: String?
)

/** Policy Decision Request context */
data class PolicyDecisionRequest(
    /** Consumer identifier */
    val consumer: String,
    /** Provider identifier */
    val provider: String,
    /** Sender agent */
    val senderAgent: String?,
    /** Recipient agent */
    val recipientAgent: String?,
    /** Contract identifier */
    val contractId: String,
    /** Request Parameters with information on the requested resource */
    val requestParameters: RequestParameters,
    /** Additional context parameters that may influence the policy decision */
    val contextParameters: ContextParameters,
    /** Mode of the interaction */
    val mode: Mode,
    /** Stage of the interaction */
    val stage: Stage
)

/** Component responsible for taken the actual decision on whether a request is allowed or not */
@Component
class PolicyDecisionPoint(
    /** Policy Administration Point for storing Contracts and Offers */
    private val policyAdministrationPoint: PolicyAdministrationPoint,
    /** Policy Decision Point Configuration */
    private val policyDecisionPointConfiguration: PolicyDecisionPointConfiguration,
    /** Constraint Evaluation service */
    private val constraintEvaluation: ConstraintEvaluation,
    /** Policy Information Point service */
    private val policyInformationPoint: PolicyInformationPoint,
    /** Audit log service */
    private val auditLog: AuditLog
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(PolicyDecisionPoint::class.java)
    }

    private fun auditLogDecision(policyDecisionRequest: PolicyDecisionRequest, decision: String) {
        auditLog.logAccessControl(
            policyDecisionRequest.consumer,
            policyDecisionRequest.provider,
            policyDecisionRequest.senderAgent,
            policyDecisionRequest.recipientAgent,
            policyDecisionRequest.contractId,
            decision
        )
    }

    /** Take a decision based on the Policy Decision Request context */
    fun decide(policyDecisionRequest: PolicyDecisionRequest): Boolean {
        val contract = policyAdministrationPoint.getContract(policyDecisionRequest.contractId)
        if (contract == null) {
            LOG.warn("Contract ${policyDecisionRequest.contractId} can't be found")
            auditLogDecision(policyDecisionRequest, "Contract ${policyDecisionRequest.contractId} can't be found")
            return false
        }
        if (contract.provider.toString() != policyDecisionRequest.recipientAgent) {
            LOG.warn(
                "Contract ${policyDecisionRequest.contractId} does not belong to provider ${policyDecisionRequest.recipientAgent}")
            auditLogDecision(policyDecisionRequest, "Contract ${policyDecisionRequest.contractId} does not belong to provider ${policyDecisionRequest.recipientAgent}")
            return false
        }
        if (contract.consumer.toString() != policyDecisionRequest.senderAgent) {
            LOG.warn(
                "Contract ${policyDecisionRequest.contractId} does not belong to consumer ${policyDecisionRequest.senderAgent}")
            auditLogDecision(policyDecisionRequest, "Contract ${policyDecisionRequest.contractId} does not belong to consumer ${policyDecisionRequest.senderAgent}")
            return false
        }
        if (contract.contractStart.compare(DateUtil.now()) > 0) {
            LOG.warn(
                "Contract ${policyDecisionRequest.contractId} does not start until ${contract.contractStart.toXMLFormat()} (${
                    DateUtil.now().toXMLFormat()
                })")
            auditLogDecision(policyDecisionRequest, "Contract ${policyDecisionRequest.contractId} does not start until ${contract.contractStart.toXMLFormat()} (${
                DateUtil.now().toXMLFormat()
            })")
            return false
        }

        val prohibitions =
            contract.prohibition?.map { checkRule(it, policyDecisionRequest) }?.filter {
                it.decision != Decision.NOT_APPLICABLE
            }
        if (prohibitions?.any { it.decision == Decision.ALLOW } == true) {
            LOG.warn("Any of the prohibitions ($prohibitions) is set to ALLOW, denying access")
            auditLogDecision(policyDecisionRequest, "Any of the prohibitions ($prohibitions) is set to ALLOW, denying access")
            return false
        }

        val permissions =
            contract.permission?.map { checkRule(it, policyDecisionRequest) }?.filter {
                it.decision != Decision.NOT_APPLICABLE
            }

        if (permissions?.any { it.decision == Decision.DENY } == true) {
            LOG.warn("Any of the permissions ($permissions) is set to DENY, denying access")
            auditLogDecision(policyDecisionRequest, "Any of the permissions ($permissions) is set to DENY, denying access")
            return false
        } else {
            if (permissions.isNullOrEmpty() && prohibitions.isNullOrEmpty()) {
                if (policyDecisionPointConfiguration.defaultPolicy == DecisionPolicy.DENY_UNLESS) {
                    LOG.warn(
                        "No applicable rules can be found, and default decision policy is set to DENY_UNLESS")
                    auditLogDecision(policyDecisionRequest,  "No applicable rules can be found, and default decision policy is set to DENY_UNLESS")
                    return false
                } else {
                    LOG.warn(
                        "No applicable rules can be found, and default decision policy is set to ALLOW_UNLESS")
                }
            }
            policyDecisionRequest.requestParameters.action?.let {
                policyInformationPoint.incrementContractTimesUsed(
                    policyDecisionRequest.contractId, it)
            }
            permissions?.map { it.preDuties + it.postDuties }?.flatten()?.forEach { duty ->
                when (duty.action) {
                    else -> {
                        LOG.warn(
                            "This Policy Enforcement Framework does not support Duties, denying access")
                        auditLogDecision(policyDecisionRequest,  "This Policy Enforcement Framework does not support Duties, denying access")
                        return false
                    }
                }
            }
            auditLogDecision(policyDecisionRequest, "Access allowed")
            return true
        }
    }

    /**
     * Check the given rule on applicability and if it is applicable provide a decision on whether
     * it is allowed or denied
     */
    private fun checkRule(r: Rule, policyDecisionRequest: PolicyDecisionRequest): RuleDecision {
        r.target?.let { artifact ->
            if ((!artifact.toString().startsWith("urn:ids:regex:") || !artifact.toString().drop(14).toRegex().matches(policyDecisionRequest.requestParameters.resourceId!!)) && artifact.toString() != policyDecisionRequest.requestParameters.resourceId!!) {
                LOG.debug("Target mismatch: $artifact != ${policyDecisionRequest.requestParameters.resourceId}")
                return RuleDecision(Decision.NOT_APPLICABLE)
            }
        }

        r.assigner?.let { assigner ->
            if (assigner.isNotEmpty() && (policyDecisionRequest.recipientAgent == null || !assigner.contains(URI(policyDecisionRequest.recipientAgent)))) {
                LOG.debug("Assigner mismatch: $assigner != ${policyDecisionRequest.recipientAgent}")
                return RuleDecision(Decision.NOT_APPLICABLE)
            }
        }
        r.assignee?.let { assignee ->
            if (assignee.isNotEmpty() && (policyDecisionRequest.senderAgent == null || !assignee.contains(URI(policyDecisionRequest.senderAgent)))) {
                LOG.debug("Assignee mismatch: $assignee != ${policyDecisionRequest.recipientAgent}")
                return RuleDecision(Decision.NOT_APPLICABLE)
            }
        }

        return if (r.constraint
            .map { constraintEvaluation.checkConstraint(it, policyDecisionRequest) }
            .contains(Decision.DENY)) {
            RuleDecision(Decision.DENY)
        } else {
            when (r) {
                is Permission ->
                    RuleDecision(
                        Decision.ALLOW, r.preDuty ?: emptyList(), r.postDuty ?: emptyList())
                is Prohibition -> RuleDecision(Decision.ALLOW)
                else -> RuleDecision(Decision.NOT_APPLICABLE)
            }
        }
    }
}
