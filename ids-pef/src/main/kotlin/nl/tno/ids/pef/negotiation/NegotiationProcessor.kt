/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef.negotiation


import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.clearing.ClearingContext
import nl.tno.ids.clearing.ClearingDirection
import nl.tno.ids.clearing.ClearingLog
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildAsResponseTo
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.pef.pap.PolicyAdministrationPoint
import nl.tno.ids.pef.pep.PolicyEnforcementException
import nl.tno.ids.tokenmanager.TokenManagerService
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.net.URI
import java.util.*

/** Policy Negotiation Camel Processor that intercepts negotiation messages */
@Component("negotiationProcessor")
class NegotiationProcessor(
    /** Policy Administration Point for storing Contracts and Offers */
    private val policyAdministrationPoint: PolicyAdministrationPoint,
    /** Policy Negotiation Configuration */
    private val negotiationConfiguration: NegotiationConfiguration,
    /** Connector Information for injecting the right metadata in responses */
    private val connectorInfo: ConnectorInfo,
    /** Token Manager Service for requesting DAPS token */
    private val tokenManagerService: TokenManagerService,
    /** Local clearing handler */
    private val clearingLog: ClearingLog
) : Processor {
    companion object {
        private val LOG = LoggerFactory.getLogger(NegotiationProcessor::class.java)
    }

    /** In-memory map of ContractRequests that are sent out to other connectors */
    val contractRequestsInFlight: MutableMap<URI, ContractRequest> = mutableMapOf()

    /** In-memory map of ContractAgreements that are sent out to other connectors */
    val contractAgreementsInFlight: MutableMap<URI, ContractAgreement> = mutableMapOf()

    /** Process messages, switches based on the incoming IDS Header */
    override fun process(exchange: Exchange) {
        if (!negotiationConfiguration.enabled) return

        val messageHeader =
            exchange.message.getHeader(Constants.Headers.idsHeader, String::class.java)?.let {
                Serialization.fromJsonLD<Message>(it)
            }
                ?: run {
                    LOG.info("No IDS-Header found!")
                    throw PolicyEnforcementException(
                        "Incorrect message structure, no IDS header found")
                }

        val payload = exchange.message.getBody(String::class.java) ?: ""
        // Check whether the respective message is an ingress or egress message.
        if (messageHeader.issuerConnector == connectorInfo.idsid && messageHeader.recipientConnector.first() != connectorInfo.idsid) {
            LOG.info("Handling egress ${messageHeader.javaClass.name} negotiation message")
            when (messageHeader) {
                is ContractRequestMessage -> {
                    clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.EGRESS, messageHeader, payload)
                    contractRequestsInFlight[messageHeader.id] = Serialization.fromJsonLD(payload)
                }
                else -> return
            }
        } else {
            LOG.info("Handling ${messageHeader.javaClass.name} negotiation message")
            when (messageHeader) {
                is ContractRequestMessage ->
                    handleIngressContractRequest(exchange, messageHeader, payload)
                is ContractOfferMessage -> handleIngressContractOffer(exchange, messageHeader)
                is ContractAgreementMessage ->
                    handleIngressContractAgreement(exchange, messageHeader, payload)
                is ContractRejectionMessage ->
                    handleIngressContractRejection(exchange, messageHeader)
                else -> return
            }
        }
    }

    /**
     * Handle ingress ContractRequest message
     *
     * Tries to find a matching policy to the ContractRequest and sends a ContractAgreement or
     * ContractRejection as response
     */
    private fun handleIngressContractRequest(
        exchange: Exchange,
        header: ContractRequestMessage,
        payload: String
    ) {
        LOG.debug("Handling contract request")
        val ingressClearing = clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.INGRESS, header, payload)
        val contractRequest = Serialization.fromJsonLD<ContractRequest>(payload)
        if (findMatchingPolicy(
            contractRequest.permission, contractRequest.prohibition, contractRequest.obligation) ==
            null) {
            LOG.debug(
                "Could not find matching policy for: \n\n$payload\n\nAvailable offers:\n\n${policyAdministrationPoint.listOffers().joinToString(separator = "\n\n"){ it.toJsonLD() }}\n\n")
            val responseHeader = buildContractRejectionMessage(header, "Offer and request do not match")
            clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.EGRESS, responseHeader, linkedClearing = ingressClearing)
            stopRoute(
                exchange,
                responseHeader,
                403)
        } else {
            val contractAgreementMessage = buildContractAgreementMessage(header)
            val contractAgreement = buildContractAgreement(contractRequest, header)
            contractAgreementsInFlight[contractAgreementMessage.id] = contractAgreement
            clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.EGRESS, contractAgreementMessage, contractAgreement.toJsonLD(), linkedClearing = ingressClearing)
            LOG.debug("Contract agreement proposal with id ${contractAgreement.id} created")
            stopRoute(
                exchange,
                contractAgreementMessage,
                200,
                contractAgreement.toJsonLD(),
                "application/ld+json")
        }
    }

    /** Handle ingress ContractOffer, currently not supported and will be rejected */
    private fun handleIngressContractOffer(exchange: Exchange, header: ContractOfferMessage) {
        val ingressClearing = clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.INGRESS, header)
        val responseHeader = buildContractRejectionMessage(header, "Can't handle Contract counter offers")
        clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.EGRESS, responseHeader, linkedClearing = ingressClearing)
        stopRoute(
            exchange,
            responseHeader,
            405)
    }

    /**
     * Handle ingress ContractAgreement, only accept if an egress ContractAgreement is sent out
     * prior
     */
    private fun handleIngressContractAgreement(
        exchange: Exchange,
        header: ContractAgreementMessage,
        payload: String
    ) {
        LOG.debug("Handle contract agreement acceptation")
        val contractAgreement = Serialization.fromJsonLD<ContractAgreement>(payload)
        val ingressClearing = clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.INGRESS, header)
        val internalContractAgreement = contractAgreementsInFlight.remove(header.correlationMessage)
            ?: kotlin.run {
                contractAgreementsInFlight.entries.find { it.value.id == contractAgreement.id }?.let {
                    contractAgreementsInFlight.remove(it.key)
                }
            }
        when {
            internalContractAgreement == null -> {
                LOG.debug("Unexpected ContractAgreement, corresponding contract not found")
                val responseHeader = buildContractRejectionMessage(
                    header, "Unexpected ContractAgreement, corresponding contract not found"
                )
                clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.EGRESS, responseHeader, linkedClearing = ingressClearing)
                stopRoute(
                    exchange,
                    responseHeader,
                    404
                )
            }
            contractAgreement != internalContractAgreement -> {
                LOG.info("Modified ContractAgreement, received ContractAgreement differs from local ContractAgreement")
                LOG.info("Internal: ${internalContractAgreement.toJsonLD()}")
                LOG.info("Received: ${contractAgreement.toJsonLD()}")
                val responseHeader = buildContractRejectionMessage(
                    header,
                    "Modified ContractAgreement, received ContractAgreement differs from local ContractAgreement"
                )
                clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.EGRESS, responseHeader, linkedClearing = ingressClearing)
                stopRoute(
                    exchange,
                    responseHeader,
                    403)
            }
            else -> {
                LOG.debug("Contract agreement acceptation successfully processed")
                val responseHeader = buildMessageProcessedNotificationMessage(header)
                clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.EGRESS, responseHeader, linkedClearing = ingressClearing)
                stopRoute(exchange, responseHeader, 200)
                policyAdministrationPoint.insertContract(contractAgreement)
            }
        }
    }

    /** Handle ingress ContractRejection */
    private fun handleIngressContractRejection(
        exchange: Exchange,
        header: ContractRejectionMessage
    ) {
        clearingLog.clearIds(ClearingContext.POLICY_NEGOTIATION, ClearingDirection.INGRESS, header)
        LOG.warn(
            "Received ContractRejection from ${header.issuerConnector} with reason: ${header.contractRejectionReason.value}")
        exchange.isRouteStop = true
    }

    /**
     * Stop the current Camel exchange, send the exchange message directly back to the Camel
     * Consumer without further processing
     */
    private fun stopRoute(
        exchange: Exchange,
        message: Message,
        statusCode: Int,
        payload: String? = null,
        contentType: String? = null
    ) {
        val multiPartMessage = MultiPartMessage(message, payload, contentType = contentType)
        exchange.message.removeHeaders("*")
        exchange.message.body = multiPartMessage.toString()
        exchange.message.setHeader("Content-Type", multiPartMessage.httpHeaders["Content-Type"])
        exchange.message.setHeader(Exchange.HTTP_RESPONSE_CODE, statusCode)
        exchange.isRouteStop = true
    }

    /** XML Gregorian Calendar generator that adds a certain interval to the current time */
    private fun xmlGregorianCalendar(field: Int? = null, amount: Int? = null) =
        Calendar.getInstance()
            .apply {
                time = Date()
                if (field != null && amount != null) {
                    add(field, amount)
                }
            }
            .time
            .run { DateUtil.asXMLGregorianCalendar(this) }

    /** Build ContractRejectionMessage */
    private fun buildContractRejectionMessage(
        header: Message,
        reason: String
    ): ContractRejectionMessage {
        LOG.debug("Contract Rejection: $reason")
        return ContractRejectionMessageBuilder()
            ._contractRejectionReason_(TypedLiteral(reason, "en"))
            .buildAsResponseTo(
                header,
                connectorInfo,
                tokenManagerService.acquireToken(header.issuerConnector.toString()))
    }

    /** Build ContractAgreementMessage */
    private fun buildContractAgreementMessage(header: Message) =
        ContractAgreementMessageBuilder()
            .buildAsResponseTo(
                header,
                connectorInfo,
                tokenManagerService.acquireToken(header.issuerConnector.toString()))

    /** Build MessageProcessedNotificationMessage */
    private fun buildMessageProcessedNotificationMessage(header: Message) =
        MessageProcessedNotificationMessageBuilder()
            .buildAsResponseTo(
                header,
                connectorInfo,
                tokenManagerService.acquireToken(header.issuerConnector.toString()))

    /** Build ContractAgreement */
    private fun buildContractAgreement(
        contractRequest: ContractRequest,
        header: ContractRequestMessage
    ) =
        ContractAgreementBuilder()
            ._contractStart_(contractRequest.contractStart ?: xmlGregorianCalendar())
            ._contractEnd_(contractRequest.contractEnd ?: xmlGregorianCalendar(Calendar.MONTH, 3))
            ._contractDate_(contractRequest.contractDate ?: xmlGregorianCalendar())
            ._contractAnnex_(contractRequest.contractAnnex)
            ._contractDocument_(contractRequest.contractDocument)
            ._provider_(connectorInfo.curator)
            ._consumer_(header.senderAgent)
            ._permission_(
                contractRequest.permission.map {
                    it.assignee = arrayListOf(header.senderAgent)
                    it.assigner = arrayListOf(connectorInfo.curator)
                    (it.postDuty + it.preDuty).forEach { duty ->
                        duty.assignee = arrayListOf(header.senderAgent)
                        duty.assigner = arrayListOf(connectorInfo.curator)
                    }
                    it
                })
            ._prohibition_(
                contractRequest.prohibition.map {
                    it.assignee = arrayListOf(header.senderAgent)
                    it.assigner = arrayListOf(connectorInfo.curator)
                    it
                })
            ._obligation_(
                contractRequest.obligation.map {
                    it.assignee = arrayListOf(header.senderAgent)
                    it.assigner = arrayListOf(connectorInfo.curator)
                    it
                })
            .build()

    /**
     * Find a matching policy in the Offers of the PolicyAdministrationPoint, based on the
     * permissions, prohibitions, and obligations.
     */
    private fun findMatchingPolicy(
        permissions: List<Permission>,
        prohibitions: List<Prohibition>,
        obligations: List<Duty>
    ): ContractOffer? {
        return policyAdministrationPoint.listOffers().find { offer ->
            offer.permission.size == permissions.size &&
                offer.prohibition.size == prohibitions.size &&
                offer.obligation.size == obligations.size &&
                offer.permission.all { a -> permissions.any { b -> comparePermission(a, b) } } &&
                offer.prohibition.all { a -> prohibitions.any { b -> compareRule(a, b) } } &&
                offer.obligation.all { a -> obligations.any { b -> compareRule(a, b) } }
        }
    }

    /**
     * Compare two Permissions, is a bit more complex than compareRule due to the possible pre/post
     * duties
     *
     * The comparison also matches when the external permission is more specific than the internal
     * permission e.g. an internal permission does not contain an assignee but the external
     * permission does
     */
    private fun comparePermission(internal: Permission, external: Permission): Boolean {
        return internal.assignee.size == 0 ||
            internal.assignee == external.assignee && internal.assigner.size == 0 ||
            internal.assigner == external.assigner &&
                internal.target?.equals(external.target) ?: true &&
                internal.constraint.size == 0 ||
            internal.constraint == external.constraint && internal.action.size == 0 ||
            internal.action == external.action &&
                internal.assetRefinement?.equals(external.assetRefinement) ?: true &&
                internal.preDuty.size == 0 ||
            internal.preDuty.all { a -> external.preDuty.any { b -> compareRule(a, b) } } &&
                internal.postDuty.size == 0 ||
            internal.postDuty.all { a -> external.postDuty.any { b -> compareRule(a, b) } }
    }

    /**
     * Compare two Rules
     *
     * The comparison also matches when the external permission is more specific than the internal
     * permission e.g. an internal permission does not contain an assignee but the external
     * permission does
     */
    private fun compareRule(internal: Rule, external: Rule): Boolean {
        return internal.assignee.size == 0 ||
            internal.assignee == external.assignee && internal.assigner.size == 0 ||
            internal.assigner == external.assignee &&
                internal.target?.equals(external.target) ?: true &&
                internal.constraint.size == 0 ||
            internal.constraint == external.constraint && internal.action.size == 0 ||
            internal.action == external.action &&
                internal.assetRefinement?.equals(external.assetRefinement) ?: true
    }
}
