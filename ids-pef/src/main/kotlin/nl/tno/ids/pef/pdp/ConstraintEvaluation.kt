/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef.pdp

import de.fraunhofer.iais.eis.*
import nl.tno.ids.configuration.serialization.DateUtil
import javax.xml.datatype.DatatypeFactory
import nl.tno.ids.pef.pip.PolicyInformationPoint
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

/** Helper Object to evaluate a Constraint given a PolicyDecisionRequest context */
@Service
class ConstraintEvaluation(private val policyInformationPoint: PolicyInformationPoint) {
    companion object {
        /** Current Spatial Position of this current instance of the core container */
        private val location =
            System.getenv()["SPATIAL_POSITION"] ?: "http://dbpedia.org/resource/Netherlands"
        private val LOG = LoggerFactory.getLogger(ConstraintEvaluation::class.java)
    }

    /** Provide a Decision based on the AbstractConstraint and PolicyDecisionRequest context */
    fun checkConstraint(
        c: AbstractConstraint,
        policyDecisionRequest: PolicyDecisionRequest
    ): Decision {
        return when (c) {
            is Constraint -> checkConstraint(c, policyDecisionRequest)
            is LogicalConstraint -> {
                val and =
                    c.and.map { checkConstraint(it, policyDecisionRequest) }.all {
                        it == Decision.ALLOW
                    }
                val or =
                    c
                        .or
                        .map { checkConstraint(it, policyDecisionRequest) }
                        .ifEmpty { listOf(Decision.ALLOW) }
                        .any { it == Decision.ALLOW }
                val xor =
                    c
                        .xone
                        .map { checkConstraint(it, policyDecisionRequest) }
                        .ifEmpty { listOf(Decision.ALLOW) }
                        .count { it == Decision.ALLOW } == 1

                if (and && or && xor) {
                    Decision.ALLOW
                } else {
                    Decision.DENY
                }
            }
            else -> throw RuntimeException()
        }
    }

    /** Provide a Decision based on the Constraint and PolicyDecisionRequest context */
    private fun checkConstraint(
        c: Constraint,
        policyDecisionRequest: PolicyDecisionRequest
    ): Decision {
        LOG.debug("Verifying Constraints, Constraint: $c . PdpRequest $policyDecisionRequest")
        return when (c.leftOperand) {
            LeftOperand.COUNT -> checkCountConstraint(c, policyDecisionRequest)
            LeftOperand.PURPOSE -> checkPurposeConstraint(c, policyDecisionRequest)
            LeftOperand.POLICY_EVALUATION_TIME -> checkPolicyEvaluationTimeConstraint(c)
            LeftOperand.ABSOLUTE_SPATIAL_POSITION -> checkAbsoluteSpatialPosition(c)
            else -> Decision.DENY
        }
    }

    /** Check a constraint with LeftOperand COUNT against the PolicyDecisionRequest context */
    private fun checkCountConstraint(
        c: Constraint,
        policyDecisionRequest: PolicyDecisionRequest
    ): Decision {
        LOG.debug("Verifying Count constraint")
        val count =
            policyInformationPoint.getContractTimesUsed(
                policyDecisionRequest.contractId, policyDecisionRequest.requestParameters.action)
        val rightOperand = c.rightOperand.value.toInt()
        val decision =
            when (c.operator) {
                BinaryOperator.LT -> if (count < rightOperand) Decision.ALLOW else Decision.DENY
                BinaryOperator.LTEQ -> if (count <= rightOperand) Decision.ALLOW else Decision.DENY
                BinaryOperator.EQ -> if (count == rightOperand) Decision.ALLOW else Decision.DENY
                BinaryOperator.GT -> if (count > rightOperand) Decision.ALLOW else Decision.DENY
                BinaryOperator.GTEQ -> if (count < rightOperand) Decision.ALLOW else Decision.DENY
                else -> Decision.DENY
            }
        LOG.debug(
            "Decision = $decision, Count = $count, Operator = ${c.operator}, RightOperand $rightOperand")
        return decision
    }

    /** Check a constraint with LeftOperand PURPOSE against the PolicyDecisionRequest context */
    private fun checkPurposeConstraint(
        c: Constraint,
        policyDecisionRequest: PolicyDecisionRequest
    ): Decision {
        LOG.debug("Verifying Purpose constraint")
        val purpose = policyDecisionRequest.contextParameters.purpose
        val decision =
            when (c.operator) {
                BinaryOperator.IN -> {
                    val rightOperand =
                        c.rightOperand.value.drop(1).dropLast(1).split(",").map { it.trim() }
                    if (rightOperand.contains(purpose)) Decision.ALLOW else Decision.DENY
                }
                BinaryOperator.SAME_AS, BinaryOperator.EQ, BinaryOperator.EQUALS -> {
                    val rightOperand = c.rightOperand.value
                    if (rightOperand == purpose) Decision.ALLOW else Decision.DENY
                }
                else -> Decision.DENY
            }
        LOG.debug(
            "Decision = $decision, Purpose = $purpose, Operator = ${c.operator}, RightOperand ${c.rightOperand.value}")
        return decision
    }

    /**
     * Check a constraint with LeftOperand POLICY_EVALUATION_TIME against the PolicyDecisionRequest
     * context
     */
    private fun checkPolicyEvaluationTimeConstraint(c: Constraint): Decision {
        LOG.debug("Verifying Policy Evaluation Time constraint")
        val now = DateUtil.now()
        val rightOperand =
            DatatypeFactory.newInstance().newXMLGregorianCalendar(c.rightOperand.value)
        val decision =
            when (c.operator) {
                BinaryOperator.BEFORE ->
                    if (now.compare(rightOperand) <= 0) Decision.ALLOW else Decision.DENY
                BinaryOperator.AFTER ->
                    if (now.compare(rightOperand) >= 0) Decision.ALLOW else Decision.DENY
                else -> Decision.DENY
            }
        LOG.debug(
            "Decision = $decision, Now = $now, Operator = ${c.operator}, RightOperand $rightOperand")
        return decision
    }

    /**
     * Check a constraint with LeftOperand ABSOLUTE_SPATIAL_POSITION against the
     * PolicyDecisionRequest context
     *
     * Currently only supports the location being set by an environment variable, so the context in
     * which the core container is deployed in must be trusted
     */
    private fun checkAbsoluteSpatialPosition(c: Constraint): Decision {
        LOG.debug("Verifying Absolute Spatial Position constraint")
        val decision =
            when (c.operator) {
                BinaryOperator.IN -> {
                    val rightOperand =
                        c.rightOperand.value.drop(1).dropLast(1).split(",").map { it.trim() }
                    if (rightOperand.contains(location)) Decision.ALLOW else Decision.DENY
                }
                else -> Decision.DENY
            }
        LOG.debug(
            "Decision = $decision, Location = $location, Operator = ${c.operator}, RightOperand ${c.rightOperand.value}")
        return decision
    }
}
