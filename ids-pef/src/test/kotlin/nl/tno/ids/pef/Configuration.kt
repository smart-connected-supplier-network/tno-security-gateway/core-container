/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef

import io.jsonwebtoken.Claims
import nl.tno.ids.configuration.http.HttpClientGenerator
import nl.tno.ids.configuration.model.DapsConfig
import nl.tno.ids.configuration.model.KeystoreConfig
import nl.tno.ids.configuration.model.TruststoreConfig
import nl.tno.ids.tokenmanager.IDSCertificateIdentifier
import nl.tno.ids.tokenmanager.TokenManagerService
import nl.tno.ids.tokenmanager.clientassertion.IDSClientAssertionGenerator
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Primary

/**
 * Application context used across the test classes
 */
@SpringBootApplication
@ConfigurationPropertiesScan("nl.tno.ids")
@ComponentScan("nl.tno.ids")
//@ExtendWith(DapsTestExtension::class)
class Configuration {
//    /** Negotiation processor */
//    @Autowired private lateinit var negotiationProcessor: NegotiationProcessor
//    /** Policy Enforcement processor */
//    @Autowired private lateinit var policyEnforcementProcessor: PolicyEnforcementProcessor

    /** Token Manager service mock */
    @Bean
    @Primary
    fun tokenManagerService(
        dapsConfig: DapsConfig,
        keystore: KeystoreConfig,
        trustStore: TruststoreConfig,
        componentClientAssertionGenerator: IDSClientAssertionGenerator,
        httpClientGenerator: HttpClientGenerator,
        idsCertificateIdentifier: IDSCertificateIdentifier
    ): TokenManagerService {
        return object :
            TokenManagerService(
                dapsConfig,
                keystore,
                trustStore,
                componentClientAssertionGenerator,
                httpClientGenerator,
                idsCertificateIdentifier
            ) {
            override fun verifyToken(token: String): Claims {
                throw Exception()
            }

            override fun acquireToken(audience: String, disableCache: Boolean): String {
                return "NO_TOKEN"
            }
        }
    }

//    /** Negotiation test route */
//    @Bean
//    fun testNegotiationProcessorRoute() =
//        object : RouteBuilder() {
//            override fun configure() {
//                from("direct:negotiation-start?block=true").process(negotiationProcessor).to("mock:negotiation-result")
//            }
//        }
//
//    /** Enforcement test route */
//    @Bean
//    fun testPolicyEnforcementRoute() =
//        object : RouteBuilder() {
//            override fun configure() {
//                from("direct:pef-start?block=true").process(policyEnforcementProcessor).to("mock:pef-result")
//            }
//        }
}
