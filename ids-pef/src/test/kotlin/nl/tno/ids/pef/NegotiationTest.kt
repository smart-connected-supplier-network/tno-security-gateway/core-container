/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.RdfResource
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.pef.negotiation.NegotiationProcessor
import nl.tno.ids.pef.pap.PolicyAdministrationPoint
import nl.tno.ids.pef.pep.PolicyEnforcementProcessor
import nl.tno.ids.testextensions.DapsTestExtension
import org.apache.camel.CamelContext
import org.apache.camel.EndpointInject
import org.apache.camel.ExchangePattern
import org.apache.camel.ProducerTemplate
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.test.spring.junit5.CamelSpringBootTest
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.test.context.TestPropertySource
import java.net.URI
import java.util.concurrent.TimeUnit

/**
 * Test class for contract negotiation support
 */
@CamelSpringBootTest
@SpringBootTest(classes = [Configuration::class])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@TestPropertySource(properties = ["spring.config.location = classpath:application.yaml"])
@ExtendWith(DapsTestExtension::class)
@Timeout(15, unit = TimeUnit.SECONDS)
class NegotiationTest {
    /** Camel Producer Template for sending messages to routes */
    @Autowired private lateinit var template: ProducerTemplate

    /** Mock result endpoint */
    @EndpointInject("mock:negotiation-result") private lateinit var mockEndpoint: MockEndpoint

    /** Policy Administration Point */
    @Autowired private lateinit var policyAdministrationPoint: PolicyAdministrationPoint


    @Autowired private lateinit var camelContext: CamelContext

    /** Negotiation processor */
    @Autowired private lateinit var negotiationProcessor: NegotiationProcessor

    /** Setup route */
    @BeforeEach
    fun setupRoute() {
        if (camelContext.getRoute("negotiation-start") == null) {
            camelContext.addRoutes(
                object : RouteBuilder() {
                    override fun configure() {
                        from("direct:negotiation-start?block=true").id("negotiation-start").process(negotiationProcessor).to("mock:negotiation-result")
                    }
                }
            )
        }
    }

    /** Reset mock endpoint before each test */
    @BeforeEach
    private fun resetMock() {
        mockEndpoint.reset()
    }

    companion object {
        /** Cached result message */
        private lateinit var requestResultMultiPartMessage: MultiPartMessage
    }

    private fun <T> callWithRetry(retries: Int = 2, call: () -> T): T? {
        return try {
            call()
        } catch (e: Exception) {
            if (retries == 0) {
                throw e
            } else {
                Thread.sleep(5000)
                callWithRetry(retries-1, call)
            }
        }
    }

    /** Test inserting a contract offer into the PAP */
    @Test
    @Order(1)
    fun testInsertOffer() {
        policyAdministrationPoint.insertOffer(
            ContractOfferBuilder()
                ._permission_(
                    listOf(
                        PermissionBuilder()
                            ._action_(arrayListOf(Action.READ, Action.WRITE))
                            ._target_(URI("urn:ids:tno:artifact-1"))
                            ._constraint_(
                                arrayListOf<AbstractConstraint>(
                                    ConstraintBuilder()
                                        ._leftOperand_(LeftOperand.COUNT)
                                        ._operator_(BinaryOperator.LT)
                                        ._rightOperand_(RdfResource("2"))
                                        .build()))
                            .build(),
                        PermissionBuilder()
                            ._action_(arrayListOf(Action.READ, Action.WRITE))
                            ._target_(URI("urn:ids:tno:artifact-2"))
                            ._constraint_(
                                arrayListOf<AbstractConstraint>(
                                    ConstraintBuilder()
                                        ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                                        ._operator_(BinaryOperator.BEFORE)
                                        ._rightOperand_(RdfResource("2021-12-31T23:59:59Z"))
                                        .build()))
                            .build()))
                .build())
    }

    /** Contract Request handling */
    @Test
    @Order(2)
    fun testSendContractRequest() {
        mockEndpoint.expectedCount = 0
        val contractRequest =
            ContractRequestBuilder()
                ._consumer_(URI("urn:ids:consumer"))
                ._provider_(URI("urn:ids:provider"))
                ._permission_(
                    arrayListOf(
                        PermissionBuilder()
                            ._action_(arrayListOf(Action.READ, Action.WRITE))
                            ._target_(URI("urn:ids:tno:artifact-1"))
                            ._assignee_(URI("urn:ids:consumer"))
                            ._constraint_(
                                arrayListOf<AbstractConstraint>(
                                    ConstraintBuilder()
                                        ._leftOperand_(LeftOperand.COUNT)
                                        ._operator_(BinaryOperator.LT)
                                        ._rightOperand_(RdfResource("2"))
                                        .build()))
                            .build(),
                        PermissionBuilder()
                            ._action_(arrayListOf(Action.READ, Action.WRITE))
                            ._target_(URI("urn:ids:tno:artifact-2"))
                            ._assignee_(URI("urn:ids:consumer"))
                            ._constraint_(
                                arrayListOf<AbstractConstraint>(
                                    ConstraintBuilder()
                                        ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                                        ._operator_(BinaryOperator.BEFORE)
                                        ._rightOperand_(RdfResource("2021-12-31T23:59:59Z"))
                                        .build()))
                            .build()))
                .build()
        val contractRequestMessage =
            ContractRequestMessageBuilder()
                ._issuerConnector_(URI("urn:ids:consumer"))
                ._recipientConnector_(URI("urn:ids:provider"))
                ._senderAgent_(URI("urn:ids:agent:ConsumerA"))
                ._recipientAgent_(URI("urn:ids:agentProviderA"))
                .buildWithDefaults()

        val contractRequestResult =
            callWithRetry {
                template.sendBodyAndHeader(
                    "direct:negotiation-start",
                    ExchangePattern.InOut,
                    contractRequest.toJsonLD(),
                    Constants.Headers.idsHeader,
                    contractRequestMessage.toJsonLD())
            }

        requestResultMultiPartMessage = MultiPartMessage.parse(contractRequestResult as String)
        Assertions.assertTrue(requestResultMultiPartMessage.header is ContractAgreementMessage)
    }

    /** Contract Agreement handling */
    @Test
    @Order(3)
    fun testSendContractAgreement() {
        val startingAgreements = policyAdministrationPoint.listContracts().size
        val contractAgreement = Serialization.fromJsonLD<ContractAgreement>(requestResultMultiPartMessage.payload?.asString())

        val contractAgreementMessage =
            ContractAgreementMessageBuilder()
                ._issuerConnector_(URI("urn:ids:consumer"))
                ._recipientConnector_(URI("urn:ids:provider"))
                ._senderAgent_(URI("urn:ids:agent:ConsumerA"))
                ._recipientAgent_(URI("urn:ids:agentProviderA"))
                ._correlationMessage_(requestResultMultiPartMessage.header.id)
                .buildWithDefaults()

        val contractAgreementResult =
            callWithRetry {
                template.sendBodyAndHeader(
                    "direct:negotiation-start",
                    ExchangePattern.InOut,
                    contractAgreement.toJsonLD(),
                    Constants.Headers.idsHeader,
                    contractAgreementMessage.toJsonLD()
                )
            }

        val agreementResultMultiPartMessage = MultiPartMessage.parse(contractAgreementResult as String)

        Assertions.assertTrue(
            agreementResultMultiPartMessage.header is MessageProcessedNotificationMessage)
        Assertions.assertEquals(1, policyAdministrationPoint.listContracts().size-startingAgreements)
    }
}
