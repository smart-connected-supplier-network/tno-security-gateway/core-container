/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.pef

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.RdfResource
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.pef.negotiation.NegotiationProcessor
import nl.tno.ids.pef.pap.PolicyAdministrationPoint
import nl.tno.ids.pef.pep.PolicyEnforcementProcessor
import nl.tno.ids.testextensions.DapsTestExtension
import org.apache.camel.CamelContext
import org.apache.camel.EndpointInject
import org.apache.camel.ProducerTemplate
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.test.spring.junit5.CamelSpringBootTest
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.test.context.TestPropertySource
import java.net.URI
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Policy enforcement test validating correct rule handling
 */
@CamelSpringBootTest
@SpringBootTest(classes = [Configuration::class])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@TestPropertySource(properties = ["spring.config.location = classpath:application.yaml"])
@Timeout(15, unit = TimeUnit.SECONDS)
@ExtendWith(DapsTestExtension::class)
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PefRuleCheckTest {
    /** Camel Producer Template for sending messages to routes */
    @Autowired private lateinit var template: ProducerTemplate

    /** Mock result endpoint */
    @EndpointInject("mock:pef-result") private lateinit var mockEndpoint: MockEndpoint

    /** Policy Administration Point */
    @Autowired private lateinit var policyAdministrationPoint: PolicyAdministrationPoint

    @Autowired private lateinit var camelContext: CamelContext

    /** Policy Enforcement processor */
    @Autowired private lateinit var policyEnforcementProcessor: PolicyEnforcementProcessor


    /** Setup route */
    @BeforeEach
    fun setupRoute() {
        if (camelContext.getRoute("pef-start") == null) {
            camelContext.addRoutes(
                object : RouteBuilder() {
                    override fun configure() {
                        from("direct:pef-start?block=true").id("pef-start").process(policyEnforcementProcessor).to("mock:pef-result")
                    }
                }
            )
        }
    }

    /** Reset mock endpoint before each test */
    @BeforeEach
    private fun resetMock() {
        mockEndpoint.reset()
    }

    private fun <T> callWithRetry(retries: Int = 2, call: () -> T): T? {
        return try {
            call()
        } catch (e: Exception) {
            if (retries == 0) {
                throw e
            } else {
                Thread.sleep(5000)
                callWithRetry(retries-1, call)
            }
        }
    }

    /** Helper function to create artifact request messages */
    private fun createArtifactMessage(
        function: ArtifactRequestMessageBuilder.() -> Unit
    ): ArtifactRequestMessage {
        baseArtifactMessageBuilder.function()
        return baseArtifactMessageBuilder.buildWithDefaults()
    }

    /** Artifact Request message base */
    private val baseArtifactMessageBuilder =
        ArtifactRequestMessageBuilder()
            ._issuerConnector_(URI("urn:ids:consumer"))
            ._recipientConnector_(arrayListOf(URI("urn:ids:provider")))
            ._senderAgent_(URI("urn:ids:agent:ConsumerA"))
            ._recipientAgent_(arrayListOf(URI("urn:ids:agent:ProviderA")))

    /** Helper function for assuming failures in camel routes */
    private fun assumeFailure(function: () -> Unit) {
        val numExchangesStart = mockEndpoint.exchanges.size
        function.invoke()
        Assertions.assertEquals(numExchangesStart, mockEndpoint.exchanges.size)
    }

    /** Insert contract offers into the PAP */
    @Test
    @Order(1)
    fun testPapContractInsert() {
        val old =
            Calendar.getInstance()
                .apply {
                    time = Date()
                    add(Calendar.MINUTE, -1)
                }
                .time
                .run { DateUtil.asXMLGregorianCalendar(this) }
        val new =
            Calendar.getInstance()
                .apply {
                    time = Date()
                    add(Calendar.MINUTE, 1)
                }
                .time
                .run { DateUtil.asXMLGregorianCalendar(this) }
        val contract =
            ContractAgreementBuilder(URI("urn:contracts:usageAgreementNUsageTemplate"))
                ._provider_(URI("urn:ids:agent:ProviderA"))
                ._consumer_(URI("urn:ids:agent:ConsumerA"))
                ._contractStart_(DateUtil.now())
                ._permission_(
                    arrayListOf(
                        PermissionBuilder()
                            ._action_(arrayListOf(Action.READ, Action.WRITE))
                            ._target_(URI("urn:ids:regex:.*artifact-1"))
                            ._constraint_(
                                arrayListOf<AbstractConstraint>(
                                    ConstraintBuilder()
                                        ._leftOperand_(LeftOperand.COUNT)
                                        ._operator_(BinaryOperator.LT)
                                        ._rightOperand_(RdfResource("2"))
                                        .build()))
                            .build()))
                .build()
        println(contract.toJsonLD())
        policyAdministrationPoint.insertContract(contract)
        policyAdministrationPoint.insertContract(
            ContractAgreementBuilder(URI("urn:contracts:oldpolicy"))
                ._provider_(URI("urn:ids:agent:ProviderA"))
                ._consumer_(URI("urn:ids:agent:ConsumerA"))
                ._contractStart_(DateUtil.now())
                ._permission_(
                    arrayListOf(
                        PermissionBuilder()
                            ._action_(arrayListOf(Action.READ, Action.WRITE))
                            ._target_(URI("urn:ids:tno:artifact-2"))
                            ._constraint_(
                                arrayListOf<AbstractConstraint>(
                                    ConstraintBuilder()
                                        ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                                        ._operator_(BinaryOperator.BEFORE)
                                        ._rightOperand_(RdfResource(old.toXMLFormat()))
                                        .build()))
                            .build()))
                .build())
        policyAdministrationPoint.insertContract(
            ContractAgreementBuilder(URI("urn:contracts:newpolicy"))
                ._provider_(URI("urn:ids:agent:ProviderA"))
                ._consumer_(URI("urn:ids:agent:ConsumerA"))
                ._contractStart_(DateUtil.now())
                ._permission_(
                    arrayListOf(
                        PermissionBuilder()
                            ._action_(arrayListOf(Action.READ, Action.WRITE))
                            ._target_(URI("urn:ids:tno:artifact-3"))
                            ._constraint_(
                                arrayListOf<AbstractConstraint>(
                                    ConstraintBuilder()
                                        ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                                        ._operator_(BinaryOperator.BEFORE)
                                        ._rightOperand_(RdfResource(new.toXMLFormat()))
                                        .build()))
                            .build()))
                .build())
    }

    /** Test rules that restrict the number of usages */
    @Test
    @Order(2)
    fun testNUsagesContract() {
        val header = createArtifactMessage {
            _transferContract_(URI("urn:contracts:usageAgreementNUsageTemplate"))
            _requestedArtifact_(URI("urn:ids:tno:artifact-1"))
        }

        mockEndpoint.expectedCount = 2
        callWithRetry {
            template.sendBodyAndHeaders(
                "direct:pef-start",
                "",
                mapOf(
                    Constants.Headers.idsHeader to header.toJsonLD(),
                    "PEF_STAGE" to "POST",
                    "PEF_MODE" to "EGRESS"))
        }
        callWithRetry {
            template.sendBodyAndHeaders(
                "direct:pef-start",
                "",
                mapOf(
                    Constants.Headers.idsHeader to header.toJsonLD(),
                    "PEF_STAGE" to "POST",
                    "PEF_MODE" to "EGRESS"
                )
            )
        }
        assumeFailure {
            callWithRetry {
                template.sendBodyAndHeaders(
                    "direct:pef-start",
                    "",
                    mapOf(
                        Constants.Headers.idsHeader to header.toJsonLD(),
                        "PEF_STAGE" to "POST",
                        "PEF_MODE" to "EGRESS"
                    )
                )
            }
        }
        mockEndpoint.assertIsSatisfied()
    }

    /** Test expired contract handling */
    @Test
    @Order(3)
    fun testExpiredContract() {
        val header = createArtifactMessage {
            _transferContract_(URI("urn:contracts:oldpolicy"))
            _requestedArtifact_(URI("urn:ids:tno:artifact-2"))
        }

        mockEndpoint.expectedCount = 0
        assumeFailure {
            callWithRetry {
                template.sendBodyAndHeaders(
                    "direct:pef-start",
                    "",
                    mapOf(
                        Constants.Headers.idsHeader to header.toJsonLD(),
                        "PEF_STAGE" to "POST",
                        "PEF_MODE" to "EGRESS"
                    )
                )
            }
        }
    }

    /** Test valid contract handling */
    @Test
    @Order(3)
    fun testValidContract() {
        val header = createArtifactMessage {
            _transferContract_(URI("urn:contracts:newpolicy"))
            _requestedArtifact_(URI("urn:ids:tno:artifact-3"))
        }

        mockEndpoint.expectedCount = 1
        callWithRetry {
            template.sendBodyAndHeaders(
                "direct:pef-start",
                "",
                mapOf(
                    Constants.Headers.idsHeader to header.toJsonLD(),
                    "PEF_STAGE" to "POST",
                    "PEF_MODE" to "EGRESS"
                )
            )
        }
    }

    /** Test incorrect requested artifact */
    @Test
    @Order(4)
    fun testIncorrectArtifact() {
        val header = createArtifactMessage {
            _transferContract_(URI("urn:contracts:newpolicy"))
            _requestedArtifact_(URI("urn:ids:tno:artifact-4"))
        }

        mockEndpoint.expectedCount = 0
        assumeFailure {
            callWithRetry {
                template.sendBodyAndHeaders(
                    "direct:pef-start",
                    "",
                    mapOf(
                        Constants.Headers.idsHeader to header.toJsonLD(),
                        "PEF_STAGE" to "POST",
                        "PEF_MODE" to "EGRESS"
                    )
                )
            }
        }
    }

    /** Test incorrect sender agent according to contract */
    @Test
    @Order(5)
    fun testIncorrectConsumer() {
        val header = createArtifactMessage {
            _senderAgent_(URI("urn:ids:agent:ConsumerA2"))
            _transferContract_(URI("urn:contracts:newpolicy"))
            _requestedArtifact_(URI("urn:ids:tno:artifact-3"))
        }

        mockEndpoint.expectedCount = 0
        assumeFailure {
            callWithRetry {
                template.sendBodyAndHeaders(
                    "direct:pef-start",
                    "",
                    mapOf(
                        Constants.Headers.idsHeader to header.toJsonLD(),
                        "PEF_STAGE" to "POST",
                        "PEF_MODE" to "EGRESS"
                    )
                )
            }
        }
    }

    /** Test incorrect recipient agent according to contract */
    @Test
    @Order(6)
    fun testIncorrectProvider() {
        val header = createArtifactMessage {
            _recipientAgent_(URI("urn:ids:agent:ProviderA2"))
            _transferContract_(URI("urn:contracts:newpolicy"))
            _requestedArtifact_(URI("urn:ids:tno:artifact-3"))
        }

        mockEndpoint.expectedCount = 0
        assumeFailure {
            callWithRetry {
                template.sendBodyAndHeaders(
                    "direct:pef-start",
                    "",
                    mapOf(
                        Constants.Headers.idsHeader to header.toJsonLD(),
                        "PEF_STAGE" to "POST",
                        "PEF_MODE" to "EGRESS"
                    )
                )
            }
        }
    }

    /** Test missing contract reference */
    @Test
    @Order(7)
    fun testNoContract() {
        val header = createArtifactMessage { _requestedArtifact_(URI("urn:ids:tno:artifact-3")) }

        mockEndpoint.expectedCount = 0
        assumeFailure {
            callWithRetry {
                template.sendBodyAndHeaders(
                    "direct:pef-start",
                    "",
                    mapOf(
                        Constants.Headers.idsHeader to header.toJsonLD(),
                        "PEF_STAGE" to "POST",
                        "PEF_MODE" to "EGRESS"
                    )
                )
            }
        }
    }
}
