package nl.tno.ids.pef

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.configuration.serialization.Serialization
import org.junit.jupiter.api.MethodOrderer
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestMethodOrder
import java.net.URI

/**
 * Test correct serialization and deserialization of default policy classes
 *
 * See https://internationaldataspaces.org/wp-content/uploads/dlm_uploads/IDSA-Position-Paper-Usage-Control-in-the-IDS-V3..pdf#page=76
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
class PolicyClassesTest {
    @Test
    @Order(1)
    fun allowOrInhibitUsage() {
        val permission = PermissionBuilder()
            ._action_(Action.READ)
            ._action_(Action.DISTRIBUTE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            .build()

        val prohibition = ProhibitionBuilder()
            ._action_(Action.WRITE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            .build()

        assertCorrectInfomodel(permission)
        assertCorrectInfomodel(prohibition)
    }

    @Test
    @Order(2)
    fun restrictUsageSpecificConnector() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(ConstraintBuilder()
                ._leftOperand_(LeftOperand.SYSTEM)
                ._operator_(BinaryOperator.SAME_AS)
                ._rightOperandReference_(URI.create("urn:ids:ConnectorIdentifier"))
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(3)
    fun restrictUsageSystemsOrApplications() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(LogicalConstraintBuilder()
                ._and_(ConstraintBuilder()
                    ._leftOperand_(LeftOperand.SYSTEM)
                    ._operator_(BinaryOperator.SAME_AS)
                    ._rightOperandReference_(URI.create("urn:ids:SystemOrApplicationIdentifier1"))
                    .build())
                ._and_(ConstraintBuilder()
                    ._leftOperand_(LeftOperand.SYSTEM)
                    ._operator_(BinaryOperator.SAME_AS)
                    ._rightOperandReference_(URI.create("urn:ids:SystemOrApplicationIdentifier2"))
                    .build())
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(4)
    fun restrictUsageUserGroups() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(LogicalConstraintBuilder()
                ._and_(ConstraintBuilder()
                    ._leftOperand_(LeftOperand.USER)
                    ._operator_(BinaryOperator.MEMBER_OF)
                    ._rightOperandReference_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                    ._pipEndpoint_(URI.create("https://pipendpoint/member_of"))
                    .build())
                ._and_(ConstraintBuilder()
                    ._leftOperand_(LeftOperand.USER)
                    ._operator_(BinaryOperator.HAS_MEMBERSHIP)
                    ._rightOperandReference_(URI.create("urn:ids:RoleIdentifier"))
                    ._pipEndpoint_(URI.create("https://pipendpoint/has_membership"))
                    .build())
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(5)
    fun restrictUsageLocations() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(ConstraintBuilder()
                ._leftOperand_(LeftOperand.ABSOLUTE_SPATIAL_POSITION)
                ._operator_(BinaryOperator.SPATIAL_EQUALS)
                ._rightOperandReference_(URI.create("http://dbpedia.org/resource/Europe"))
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(6)
    fun restrictUsagePurposes() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(ConstraintBuilder()
                ._leftOperand_(LeftOperand.PURPOSE)
                ._operator_(BinaryOperator.SAME_AS)
                ._rightOperandReference_(URI.create("urn:ids:PurposeIdentifier"))
                ._pipEndpoint_(URI("/purpose"))
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(7)
    fun restrictUsageOnEvent() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(ConstraintBuilder()
                ._leftOperand_(LeftOperand.EVENT)
                ._operator_(BinaryOperator.SAME_AS)
                ._rightOperandReference_(URI.create("urn:ids:EventIdentifier"))
                ._pipEndpoint_(URI("https://pipendpoint/event"))
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(8)
    fun restrictUsageOnSecurityLevel() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(LogicalConstraintBuilder()
                ._and_(ConstraintBuilder()
                    ._leftOperand_(LeftOperand.SECURITY_LEVEL)
                    ._operator_(BinaryOperator.SAME_AS)
                    ._rightOperandReference_(URI.create("idsc:TRUST_SECURITY_PROFILE"))
                    .build())
                ._and_(ConstraintBuilder()
                    ._leftOperand_(LeftOperand.SECURITY_LEVEL)
                    ._operator_(BinaryOperator.SAME_AS)
                    ._rightOperandReference_(URI.create("idsc:TRUST_PLUS_SECURITY_PROFILE"))
                    .build())
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(9)
    fun restrictUsageOnTimeInterval() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(LogicalConstraintBuilder()
                ._and_(ConstraintBuilder()
                    ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                    ._operator_(BinaryOperator.AFTER)
                    ._rightOperand_(TypedLiteral("2021-01-01T00:00:00Z", "http://www.w3.org/2001/XMLSchema#dateTimeStamp"))
                    .build())
                ._and_(ConstraintBuilder()
                    ._leftOperand_(LeftOperand.POLICY_EVALUATION_TIME)
                    ._operator_(BinaryOperator.BEFORE)
                    ._rightOperand_(TypedLiteral("2022-01-01T00:00:00Z", "http://www.w3.org/2001/XMLSchema#dateTimeStamp"))
                    .build())
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(10)
    fun restrictUsageOnTimeDuration() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(ConstraintBuilder()
                ._leftOperand_(LeftOperand.ELAPSED_TIME)
                ._operator_(BinaryOperator.SHORTER_EQ)
                ._rightOperand_(TypedLiteral("PT3M", "http://www.w3.org/2001/XMLSchema#duration"))
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(11)
    fun restrictUsageNTimes() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(ConstraintBuilder()
                ._leftOperand_(LeftOperand.COUNT)
                ._operator_(BinaryOperator.LTEQ)
                ._rightOperand_(TypedLiteral("5", "http://www.w3.org/2001/XMLSchema#nonNegativeInteger"))
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(12)
    fun restrictUsageAndDelete() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._postDuty_(DutyBuilder()
                ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                ._action_(Action.DELETE)
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(13)
    fun restrictUsageModifyInTransit() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._postDuty_(DutyBuilder()
                ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                ._action_(Action.ANONYMIZE)
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(14)
    fun restrictUsageModifyInRest() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._postDuty_(DutyBuilder()
                ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                ._action_(Action.ANONYMIZE)
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(15)
    fun restrictUsageLogUsage() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._postDuty_(DutyBuilder()
                ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                ._action_(Action.LOG)
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(16)
    fun restrictUsageLogNotification() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._postDuty_(DutyBuilder()
                ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                ._action_(Action.NOTIFY)
                ._constraint_(ConstraintBuilder()
                    ._leftOperand_(LeftOperand.ENDPOINT)
                    ._operator_(BinaryOperator.DEFINES_AS)
                    ._rightOperandReference_(URI("https://notificationendpoint"))
                    .build())
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(17)
    fun restrictDistributeAttachPolicy() {
        val permission = PermissionBuilder()
            ._action_(Action.DISTRIBUTE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._preDuty_(DutyBuilder()
                ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                ._action_(Action.NEXT_POLICY)
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(18)
    fun restrictDistributeEncrypted() {
        val permission = PermissionBuilder()
            ._action_(Action.DISTRIBUTE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._preDuty_(DutyBuilder()
                ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                ._action_(Action.ENCRYPT)
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }

    @Test
    @Order(19)
    fun saleAgreement() {
        val agreement = SalesAgreementBuilder()
            ._provider_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._consumer_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._contractStart_(DateUtil.now())
            ._permission_(PermissionBuilder()
                ._action_(Action.USE)
                ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                ._target_(URI.create("urn:ids:ResourceIdentifier"))
                ._preDuty_(DutyBuilder()
                    ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                    ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                    ._action_(Action.COMPENSATE)
                    ._constraint_(ConstraintBuilder()
                        ._leftOperand_(LeftOperand.PAY_AMOUNT)
                        ._operator_(BinaryOperator.EQ)
                        ._rightOperand_(TypedLiteral("99.99", "http://www.w3.org/2001/XMLSchema#double"))
                        ._unit_(URI.create("http://dbpedia.org/resource/Euro"))
                        .build())
                    .build())
                .build())
            .build()

        assertCorrectInfomodel(agreement)
    }

    @Test
    @Order(20)
    fun rentalRestrictions() {
        val agreement = RentalAgreementBuilder()
            ._provider_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._consumer_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._contractStart_(DateUtil.now())
            ._permission_(PermissionBuilder()
                ._action_(Action.USE)
                ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                ._target_(URI.create("urn:ids:ResourceIdentifier"))
                ._preDuty_(DutyBuilder()
                    ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
                    ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
                    ._action_(Action.COMPENSATE)
                    ._constraint_(ConstraintBuilder()
                        ._leftOperand_(LeftOperand.PAY_AMOUNT)
                        ._operator_(BinaryOperator.EQ)
                        ._rightOperand_(TypedLiteral("9.99", "http://www.w3.org/2001/XMLSchema#double"))
                        ._unit_(URI.create("http://dbpedia.org/resource/Euro"))
                        .build())
                    .build())
                    ._constraint_(ConstraintBuilder()
                        ._leftOperand_(LeftOperand.ELAPSED_TIME)
                        ._operator_(BinaryOperator.SHORTER_EQ)
                        ._rightOperand_(TypedLiteral("PT3M", "http://www.w3.org/2001/XMLSchema#duration"))
                        .build())
                .build())
            .build()

        assertCorrectInfomodel(agreement)
    }

    @Test
    @Order(21)
    fun usageRestrictState() {
        val permission = PermissionBuilder()
            ._action_(Action.USE)
            ._assigner_(URI.create("urn:ids:ProviderParticipantIdentifier"))
            ._assignee_(URI.create("urn:ids:ConsumerParticipantIdentifier"))
            ._target_(URI.create("urn:ids:ResourceIdentifier"))
            ._constraint_(ConstraintBuilder()
                ._leftOperand_(LeftOperand.STATE)
                ._operator_(BinaryOperator.NOT)
                ._rightOperandReference_(URI.create("urn:ids:terminatedState"))
                ._pipEndpoint_(URI("https://pipendpoint/state"))
                .build())
            .build()

        assertCorrectInfomodel(permission)
    }


    private fun assertCorrectInfomodel(modelClass: ModelClass) {
        val serialized = modelClass.toJsonLD()
        println(serialized)
        Serialization.fromJsonLD(serialized, modelClass.javaClass)
    }

}