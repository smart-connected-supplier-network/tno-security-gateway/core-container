# IDS Configuration

Helper module that contains generic configuration of the core container and common Spring Beans used throughout the repository.

The module contains the following packages:
- `events`: Spring `ApplicationEvent` support, to indicate modifications that impact the Self Description of the Connector.
- `http`: HttpClient provider, contains the Truststore configuration so that servers that use their IDS certificate for TLS encryption are trusted.
- `infomodel`: Helper functions and message converters for IDS Information Model related classes.
- `keystore`: Parser for PEM-based certificates and private keys
- `model`: Main Configuration model used for Spring Configuration Properties that are loaded via `application.yaml`, `application.properties`, or environment variables 
- `multipart`: MultiPartMessage (de)serialization support.
- `reloading`: Hot-reloading of Spring properties and classes support. (_Note: does not fully work_)
- `serialization`: Provides Serialization objects
- `truststore`: X509 Trust Manager helpers for merged truststores (e.g. IDS truststore combined with the systems truststore)
- `validation`: Adds additional validation constraints that can be used by model classes
- `websecurity`: Spring firewall configuration