plugins {
    kotlin("plugin.spring")
    kotlin("plugin.serialization")
}

dependencies {
    // Spring Boot Test
    implementation("org.springframework.boot:spring-boot-starter-webflux")

    // Spring configuration validation support
    implementation("org.hibernate:hibernate-validator:${LibraryVersions.hibernate}")

    api("org.apache.httpcomponents:httpclient:${LibraryVersions.httpclient}")

    implementation("org.apache.camel:camel-core-model:${LibraryVersions.Camel.camel}")

    // Spring Hot Reload Configuration
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-security")
    implementation("org.springframework.boot:spring-boot-configuration-processor")

    implementation("org.springframework.cloud:spring-cloud-starter")

    implementation("commons-configuration:commons-configuration:${LibraryVersions.commonsConfiguration}")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${LibraryVersions.Kotlin.kotlinxSerialization}")
    // JSON support
    api("org.json:json:${LibraryVersions.json}")

    // IDS Information Model
    api("de.fraunhofer.iais.eis.ids.infomodel:java:${LibraryVersions.IDS.infomodel}")
    api("de.fraunhofer.iais.eis.ids:infomodel-serializer:${LibraryVersions.IDS.infomodelSerializer}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-opt-in=kotlinx.serialization.ExperimentalSerializationApi"
    }
}