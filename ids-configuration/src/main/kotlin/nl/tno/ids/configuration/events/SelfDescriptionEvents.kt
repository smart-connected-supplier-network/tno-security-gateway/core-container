/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.events

import org.springframework.context.ApplicationEvent

/**
 * Self Description Reload Event, triggered when a component requests the SelfDescriptionGenerator
 * to rebuild the self description
 */
class SelfDescriptionReloadEvent(source: Any) : ApplicationEvent(source)

/**
 * Self Description Change Event, triggered when the SelfDescriptionGenerator has updated the self
 * description
 */
class SelfDescriptionChangeEvent(source: Any) : ApplicationEvent(source)
