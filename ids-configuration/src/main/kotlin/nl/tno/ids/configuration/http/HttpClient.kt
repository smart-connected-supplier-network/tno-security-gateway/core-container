/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.http

import nl.tno.ids.configuration.keystore.KeystoreGenerator
import nl.tno.ids.configuration.model.TruststoreConfig
import nl.tno.ids.configuration.model.TruststoreType
import org.apache.http.conn.ssl.NoopHostnameVerifier
import org.apache.http.conn.ssl.SSLConnectionSocketFactory
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.impl.client.HttpClients
import org.apache.http.ssl.SSLContextBuilder
import org.apache.http.ssl.SSLContexts
import org.apache.http.ssl.TrustStrategy
import org.springframework.stereotype.Component
import java.security.KeyStore
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager


/**
 * Apache HTTP client generator with support for TLS trust configurations to allow connecting to IDS
 * services without system accepted TLS certificates.
 */
@Component
class HttpClientGenerator(truststore: TruststoreConfig, private val keystoreGenerator: KeystoreGenerator) {
    /** Closable HTTP Client */
    private val httpClient: CloseableHttpClient

    /** SSL Connection Socket Factory with trust material loaded */
    private val sslConnectionSocketFactory: SSLConnectionSocketFactory

    /** Initialize Http client with SSL context */
    init {
        val sslContextBuilder: SSLContextBuilder = SSLContexts.custom()
        loadTrustMaterial(truststore, sslContextBuilder)
        sslConnectionSocketFactory =
            SSLConnectionSocketFactory(sslContextBuilder.build(), NoopHostnameVerifier.INSTANCE)
        httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build()
    }

    /** Load trust material from different TruststoreConfig types */
    private fun loadTrustMaterial(
        truststore: TruststoreConfig,
        sslContextBuilder: SSLContextBuilder
    ) {
        when (truststore.type) {
            TruststoreType.PEM -> {
                truststore.pem?.let { it ->
                    val trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
                    trustManagerFactory.init(null as KeyStore?)
                    val certificates = trustManagerFactory.trustManagers
                        .filterIsInstance<X509TrustManager>()
                        .flatMap { x509TrustManager -> x509TrustManager.acceptedIssuers.toList() }
                    val combinedCaCertificates = it.chain + certificates
                    val keystore = keystoreGenerator.createTruststore(combinedCaCertificates)
                    sslContextBuilder.loadTrustMaterial(keystore, null)
                }
            }
            TruststoreType.ACCEPT_ALL -> {
                sslContextBuilder.loadTrustMaterial(null) { _, _ -> true }
            }
            TruststoreType.SYSTEM -> {
                sslContextBuilder.loadTrustMaterial(null as TrustStrategy?)
            }
        }
    }

    /**
     * Get current HTTP client Note: Might be changed on configuration change, so don't cache the
     * client
     */
    fun get(): CloseableHttpClient {
        return httpClient
    }

    /**
     * Get an HTTP client based on a client builder to set additional properties other than the
     * defaults.
     */
    fun get(httpClientBuilder: HttpClientBuilder): CloseableHttpClient {
        return httpClientBuilder.setSSLSocketFactory(sslConnectionSocketFactory).build()
    }
}
