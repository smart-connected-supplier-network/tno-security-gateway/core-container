/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.validation

import java.net.URI
import javax.validation.Constraint
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext
import kotlin.reflect.KClass

/**
 * IDSIdentifier validation annotation.
 *
 * Used to annotate fields that should be validated against the @see IDSIdentifierValidator
 * @property message Validation error message
 * @property groups Required for validation compatibility
 * @property payload Required for validation compatibility
 */
@MustBeDocumented
@Constraint(validatedBy = [IDSIdentifierValidator::class])
@Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class IDSIdentifier(
    val message: String =
        "Invalid IDS identifier, must start with either 'http://', 'https://', or 'urn:'",
    val groups: Array<KClass<*>> = [],
    val payload: Array<KClass<out Any>> = []
)

/** Validator for IDS identifiers from URIs */
class IDSIdentifierValidator : ConstraintValidator<IDSIdentifier, URI> {
    private val regex = """^(https?:|urn:).*""".toRegex()
    override fun initialize(idsIdentifier: IDSIdentifier) {}

    /**
     * Check if the URI is an valid identifier, i.e. starting with either http:, https:, urn: and
     * being a valid URI
     */
    override fun isValid(idsIdentifier: URI?, cxt: ConstraintValidatorContext): Boolean {
        return regex.matches(idsIdentifier.toString())
    }
}
