/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.infomodel

import de.fraunhofer.iais.eis.*
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.configuration.serialization.Serialization
import org.apache.camel.model.ProcessorDefinition
import java.net.URI

/**
 * Kotlin extension function to add capability of directly converting an IDS Infomodel class to
 * JSON-LD
 */
fun ModelClass.toJsonLD(): String {
    return Serialization.toJsonLD(this)
}

fun Collection<ModelClass>.toJsonLD(): String {
    return Serialization.toJsonLD(this)
}


/** Kotlin extension function to convert an IDS Infomodel Message to a MultiPartMessage object */
fun <T : Message> T.toMultiPartMessage(
    payload: String? = null,
    contentType: String? = null,
    httpHeaders: Map<String, String>? = null
): MultiPartMessage {
    return MultiPartMessage(
        this, payload, httpHeaders?.toMutableMap() ?: mutableMapOf(), contentType)
}

/**
 * Kotlin extension function to set default IDS Message fields that are almost always the same value
 */
fun <T : Message> T.defaults(dat: String? = null): T {
    securityToken =
        DynamicAttributeTokenBuilder()
            ._tokenFormat_(TokenFormat.JWT)
            ._tokenValue_(dat ?: "DUMMY")
            .build()
    modelVersion = "4.2.7"
    issued = DateUtil.now()
    return this
}

/** Kotlin extension function to set default values for an IDS response to an earlier request */
fun <T : ResponseMessage> T.isResponseTo(
    message: Message?,
    connectorInfo: ConnectorInfo? = null,
    dat: String? = null
): T {
    defaults(dat)
    correlationMessage = message?.id ?: URI("urn:ids:unknown")
    issuerConnector =
        connectorInfo?.idsid ?: message?.recipientConnector?.firstOrNull() ?: URI("urn:ids:unknown")
    senderAgent =
        connectorInfo?.curator ?: message?.recipientAgent?.firstOrNull() ?: URI("urn:ids:unknown")
    recipientConnector = arrayListOf(message?.issuerConnector ?: URI("urn:ids:unknown"))
    recipientAgent = arrayListOf(message?.senderAgent ?: URI("urn:ids:unknown"))
    return this
}

/** Kotlin extension function to set default values for an IDS response to an earlier request */
fun <T : NotificationMessage> T.isResponseTo(
    message: Message?,
    connectorInfo: ConnectorInfo? = null,
    dat: String? = null
): T {
    defaults(dat)
    correlationMessage = message?.id ?: URI("urn:ids:unknown")
    issuerConnector =
        connectorInfo?.idsid ?: message?.recipientConnector?.firstOrNull() ?: URI("urn:ids:unknown")
    senderAgent =
        connectorInfo?.curator ?: message?.recipientAgent?.firstOrNull() ?: URI("urn:ids:unknown")
    recipientConnector = arrayListOf(message?.issuerConnector ?: URI("urn:ids:unknown"))
    recipientAgent = arrayListOf(message?.senderAgent ?: URI("urn:ids:unknown"))
    return this
}

/** Kotlin extension function to to build an Infomodel Builder with defaults */
fun <T : Message> Builder<T>.buildWithDefaults(dat: String? = null): T {
    return this.build().defaults(dat)
}

/**
 * Kotlin extension function to to build an Infomodel Builder as an response to an earlier request
 */
fun <T : ResponseMessage> Builder<T>.buildAsResponseTo(
    message: Message?,
    connectorInfo: ConnectorInfo? = null,
    dat: String? = null
): T {
    return this.build().isResponseTo(message, connectorInfo, dat)
}

/**
 * Kotlin extension function to to build an Infomodel concept as an response to an earlier request
 */
fun <T : NotificationMessage> Builder<T>.buildAsResponseTo(
    message: Message?,
    connectorInfo: ConnectorInfo? = null,
    dat: String? = null
): T {
    return this.build().isResponseTo(message, connectorInfo, dat)
}

/** Helper function for adding an optional processor to Camel routes */
fun ProcessorDefinition<*>.process(enabled: Boolean, ref: String): ProcessorDefinition<*> {
    return if (enabled) process(ref) else this
}

/** Helper function for adding optional processors to Camel routes */
fun ProcessorDefinition<*>.process(enabled: Boolean, block: ProcessorDefinition<*>.() -> ProcessorDefinition<*>): ProcessorDefinition<*> {
    return if (enabled) block() else this
}
