/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.model

import javax.validation.constraints.NotNull
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated

/**
 * Remote Attestation Trusted Third Party Configuration for verification of TPM Platform
 * Configuration Register values.
 */
@Component
@ConfigurationProperties(prefix = "remote-attestation.ttp")
@ConditionalOnProperty(prefix = "remote-attestation", name = ["ttp"])
@Validated
class TrustedThirdPartyConfig {
    /** Hostname of the Trusted Third Party */
    @field:NotNull lateinit var hostname: String

    /** Port of the Trusted Third Party */
    var port: Int? = 443
}

/** Trusted Platform Module (TPM) Configuration */
@Component
@ConfigurationProperties(prefix = "remote-attestation.tpm")
@ConditionalOnProperty(prefix = "remote-attestation", name = ["tpm"])
class TPMConfig {
    /** Deploy a simulator instead of using a physical TPM */
    var simulator: Boolean = false

    /** TPM Server host */
    var host: String? = null

    /** TPM Server port */
    var port: Int? = null
}
