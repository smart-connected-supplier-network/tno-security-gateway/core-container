/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.infomodel

import de.fraunhofer.iais.eis.ModelClass
import nl.tno.ids.configuration.http.HttpClientGenerator
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.configuration.serialization.Serialization
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpInputMessage
import org.springframework.http.HttpOutputMessage
import org.springframework.http.MediaType
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.http.converter.AbstractGenericHttpMessageConverter
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.KotlinSerializationJsonHttpMessageConverter
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.http.converter.xml.Jaxb2RootElementHttpMessageConverter
import org.springframework.web.client.RestTemplate
import java.io.InputStream
import java.lang.reflect.GenericArrayType
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.lang.reflect.WildcardType
import kotlin.reflect.KClass

/** IDS Message Converters Configuration providing Message Converters to be used by Spring */
@Configuration
class IDSMessageConverters(private val httpClientGenerator: HttpClientGenerator) {
    /** Spring RestTemplate with configured message converters for IDS related messaging */
    @Bean
    fun createRestTemplate(): RestTemplate {
        return RestTemplate().apply {
            requestFactory = HttpComponentsClientHttpRequestFactory(
                httpClientGenerator.get()
            )
            messageConverters.removeIf { it is Jaxb2RootElementHttpMessageConverter || it is MappingJackson2HttpMessageConverter }
            messageConverters.add(0, KotlinSerializationJsonHttpMessageConverter())
            messageConverters.add(0, createIDSCollectionMessageConverter())
            messageConverters.add(0, createIDSMessageConverter())
            messageConverters.add(0, createMultiPartMessageConverter())
            messageConverters.add(0, createInputStreamMessageConverter())
        }
    }

    /** IDS Infomodel Message Converter, for (de)serializing Infomodel concepts */
    @Bean
    fun createIDSMessageConverter(): HttpMessageConverter<ModelClass> {
        return object : AbstractGenericHttpMessageConverter<ModelClass>() {
            private val LOG = LoggerFactory.getLogger(this::class.java)

            override fun canRead(type: Type, contextClass: Class<*>?, mediaType: MediaType?) =
                (type is Class<*> || type is KClass<*>) && ModelClass::class.java.isAssignableFrom(type.kclass().java)

            override fun canRead(clazz: Class<*>, mediaType: MediaType?) =
                ModelClass::class.java.isAssignableFrom(clazz)

            override fun canWrite(clazz: Class<*>, mediaType: MediaType?) =
                ModelClass::class.java.isAssignableFrom(clazz)

            override fun getSupportedMediaTypes(): MutableList<MediaType> =
                mutableListOf(
                    MediaType.APPLICATION_JSON, MediaType.parseMediaType("application/ld+json"))

            @Suppress("UNCHECKED_CAST")
            override fun read(type: Type, contextClass: Class<*>?, inputMessage: HttpInputMessage) =
                readInternal(type as Class<out ModelClass>, inputMessage)

            override fun readInternal(clazz: Class<out ModelClass>, inputMessage: HttpInputMessage): ModelClass {
                val content =
                    String(inputMessage.body.readAllBytes()).let {
                        if (!it.contains("@context")) {
                            JSONObject(it)
                                .put(
                                    "@context",
                                    JSONObject()
                                        .put("ids", "https://w3id.org/idsa/core/")
                                        .put("idsc", "https://w3id.org/idsa/code/"))
                                .toString()
                        } else {
                            it
                        }
                    }
                try {
                    return Serialization.fromJsonLD(content, clazz)
                } catch (e: Exception) {
                    LOG.warn("Could not parse JSON-LD", e)
                    throw e
                }
            }

            override fun writeInternal(t: ModelClass, type: Type?, outputMessage: HttpOutputMessage) {
                outputMessage.body.write(t.toJsonLD().toByteArray())
            }
        }
    }

    /** IDS Infomodel Collection Message Converter, for (de)serializing Infomodel concepts */
    @Bean
    fun createIDSCollectionMessageConverter(): HttpMessageConverter<Collection<ModelClass>> {
        return object : AbstractGenericHttpMessageConverter<Collection<ModelClass>>() {
            private val LOG = LoggerFactory.getLogger(this::class.java)
            override fun canRead(type: Type, contextClass: Class<*>?, mediaType: MediaType?) =
                type is ParameterizedType && ModelClass::class.java.isAssignableFrom(type.kclass().java)

            override fun canRead(clazz: Class<*>, mediaType: MediaType?) = false

            override fun canWrite(clazz: Class<*>, mediaType: MediaType?) =
                Collection::class.java.isAssignableFrom(clazz) &&
                    mediaType?.equalsTypeAndSubtype(
                        MediaType.parseMediaType("application/ld+json")) == true

            override fun getSupportedMediaTypes() =
                mutableListOf(
                    MediaType.APPLICATION_JSON, MediaType.parseMediaType("application/ld+json"))

            override fun read(
                type: Type,
                contextClass: Class<*>?,
                inputMessage: HttpInputMessage
            ): Collection<ModelClass> {
                @Suppress("UNCHECKED_CAST")
                val clazz = type.kclass().java as Class<ModelClass>
                val content = String(inputMessage.body.readAllBytes())
                try {
                    return Serialization.fromJsonLDCollection(content, clazz)
                } catch (e: Exception) {
                    LOG.warn("Could not parse JSON-LD", e)
                    throw e
                }
            }

            override fun readInternal(
                clazz: Class<out Collection<ModelClass>>,
                inputMessage: HttpInputMessage
            ): Collection<ModelClass> {
                throw UnsupportedOperationException("Not enough type information")
            }

            override fun writeInternal(t: Collection<ModelClass>, type: Type?, outputMessage: HttpOutputMessage) {
                outputMessage.body.write(Serialization.toJsonLD(t).toByteArray())
            }
        }
    }

    /**
     * HTTP Mime Multipart Message Converter, specifically for IDS-based Multipart messages (i.e.
     * containing a "header" (IDS Infomodel Message) part and optionally "payload" part)
     */
    @Bean
    fun createMultiPartMessageConverter(): HttpMessageConverter<MultiPartMessage> {
        return object : HttpMessageConverter<MultiPartMessage> {
            private val LOG = LoggerFactory.getLogger(this::class.java)
            override fun canRead(clazz: Class<*>, mediaType: MediaType?) =
                clazz == MultiPartMessage::class.java

            override fun canWrite(clazz: Class<*>, mediaType: MediaType?) =
                clazz == MultiPartMessage::class.java

            override fun getSupportedMediaTypes() =
                mutableListOf(
                    MediaType.MULTIPART_FORM_DATA, MediaType.MULTIPART_MIXED, MediaType.ALL)

            override fun read(
                clazz: Class<out MultiPartMessage>,
                inputMessage: HttpInputMessage
            ): MultiPartMessage {
                return MultiPartMessage.parse(
                    inputMessage.body, inputMessage.headers.contentType?.toString())
            }

            override fun write(
                t: MultiPartMessage,
                contentType: MediaType?,
                outputMessage: HttpOutputMessage
            ) {
                t.writeToOutputStream(outputMessage.body)
                try {
                    t.httpHeaders.forEach { (key, value) -> outputMessage.headers.set(key, value) }
                } catch (e: UnsupportedOperationException) {
                    LOG.trace("Can't write HTTP Header to outputMessage")
                }
            }
        }
    }

    /** InputStream Message Converter */
    @Bean
    fun createInputStreamMessageConverter(): HttpMessageConverter<InputStream> {
        return object : HttpMessageConverter<InputStream> {
            override fun canRead(clazz: Class<*>, mediaType: MediaType?) =
                InputStream::class.java.isAssignableFrom(clazz)

            override fun canWrite(clazz: Class<*>, mediaType: MediaType?) =
                InputStream::class.java.isAssignableFrom(clazz)

            override fun getSupportedMediaTypes(): MutableList<MediaType> =
                mutableListOf(MediaType.ALL)

            override fun read(
                clazz: Class<out InputStream>,
                inputMessage: HttpInputMessage
            ): InputStream {
                return inputMessage.body
            }

            override fun write(
                t: InputStream,
                contentType: MediaType?,
                outputMessage: HttpOutputMessage
            ) {
                t.transferTo(outputMessage.body)
            }
        }
    }
}

/**
 * Helper function for retrieving the Kotlin class of a Type
 */
private fun Type.kclass(): KClass<*> = when (val it = this) {
    is KClass<*> -> it
    is Class<*> -> it.kotlin
    is ParameterizedType -> it.actualTypeArguments.first().kclass()
    is WildcardType -> it.upperBounds.first().kclass()
    is GenericArrayType -> it.genericComponentType.kclass()
    else -> throw IllegalArgumentException("typeToken should be an instance of Class<?>, GenericArray, ParametrizedType or WildcardType, but actual type is $it ${it::class}")
}