package nl.tno.ids.configuration

/** String constants used across the project */
object Constants {
    object Headers {
        const val idsId = "IDS-ID"
        const val idsHeader = "IDS-Header"
        const val forwardTo = "Forward-To"
        const val header = "header"
        const val payload = "payload"
        const val authorization = "Authorization"
    }
}