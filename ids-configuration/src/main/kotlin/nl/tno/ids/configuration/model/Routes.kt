/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.model

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.Serializer
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import nl.tno.ids.configuration.Constants
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.NestedConfigurationProperty
import org.springframework.lang.NonNull
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import java.net.URL

/** Serializer for converting Strings to URLs by KotlinX Serialization */
@Serializer(forClass = URL::class)
object URLSerializer : KSerializer<URL> {
    override fun deserialize(decoder: Decoder): URL {
        return URL(decoder.decodeString())
    }

    override fun serialize(encoder: Encoder, value: URL) {
        encoder.encodeString(value.toString())
    }
}

/** Camel Route configuration */
@Component
@ConfigurationProperties(prefix = "routes")
class RouteConfig {
    /** Ingress route configuration */
    @NestedConfigurationProperty var ingress: IngressRouteConfig = IngressRouteConfig()

    /** Egress route configuration */
    @NestedConfigurationProperty var egress: EgressRouteConfig = EgressRouteConfig()

    /** Enable TLS for Camel routes, based on IDS certificate */
    var https: Boolean = false

    @Value("\${camel.port:8080}") protected lateinit var camelPort: String

    /** Concatenation of both ingress & egress routes */
    val allRoutes: List<Route>
        get() = ingress.allRoutes + egress.allRoutes

    /** Default HTTP egress route */
    val defaultEgress: HTTPSEgressRoute?
        get() = egress.http.firstOrNull()

    /** Default HTTP egress endpoint */
    val defaultEgressUrl: String
        get() = egress.http.firstOrNull()?.let {
            "http${if (https) "s" else ""}://localhost:${it.listenPort?: camelPort}/${it.endpoint}"
        } ?: "http${if (https) "s" else ""}://localhost:8080/https_out"
}

/**
 * Ingress Route configuration
 * @property http Http Ingress route configurations
 * @property idscp IDSCP Ingress route configurations
 */
class IngressRouteConfig(
    @NestedConfigurationProperty var http: List<HTTPSIngressRoute> = emptyList(),
    @NestedConfigurationProperty var idscp: List<IDSCPIngressRoute> = emptyList()
) {
    /** Concatenation of both http & idscp routes */
    val allRoutes: List<Route>
        get() = http + idscp
}

/**
 * Egress Route configuration
 * @property http Http Ingress route configurations
 * @property idscp IDSCP Ingress route configurations
 */
class EgressRouteConfig(
    @NestedConfigurationProperty var http: List<HTTPSEgressRoute> = emptyList(),
    @NestedConfigurationProperty var idscp: List<IDSCPEgressRoute> = emptyList()
) {
    /** Concatenation of both http & idscp routes */
    val allRoutes: List<Route>
        get() = http + idscp
}

/** Generic Route configuration */
@Validated
@Serializable
sealed class Route {
    /** Identifier of the route (locally unique) */
    var id: String? = null

    /** Pre processing steps to be executed */
    var preProcessing: List<String> = emptyList()

    /** Post processing steps to be executed */
    var postProcessing: List<String> = emptyList()
}

/** HTTPS Ingress Route Configuration */
@Validated
@Serializable
@SerialName("httpIngress")
class HTTPSIngressRoute : Route() {
    /** Exposed port for incoming traffic */
    var port: Int? = null

    /** Endpoint prefix for incoming traffic */
    var endpoint: String = ""

    /**
     * Additional Camel HTTP parameters (@see
     * https://camel.apache.org/components/3.7.x/http-component.html)
     */
    var parameters: String = ""

    /** Data App Endpoint */
    @field:NonNull lateinit var dataApp: String

    /** Clearing House flag for automatic clearing of message */
    var clearing: Boolean = false

    /** DAPS Verification flag for automatic verification of incoming requests */
    var dapsVerify: Boolean = true

    /** Policy Enforcement flag */
    var policyEnforcement: Boolean = false

    /** Delegate policy negotiation to the data app */
    var delegatedPolicyNegotiation: Boolean = false
}

/** IDSCP Ingress Route Configuration */
@Validated
@Serializable
@SerialName("idscpIngress")
class IDSCPIngressRoute : Route() {
    /** Exposed port for incoming traffic */
    var port: Int? = null

    /** Data App Endpoint */
    @field:NonNull lateinit var dataApp: String

    /** Clearing House flag for automatic clearing of message */
    var clearing: Boolean = false

    /** Flag for enabling hostname verification on TLS level */
    var tlsClientHostnameVerification: Boolean = true
}

/** HTTPS Egress Route Configuration */
@Validated
@Serializable
@SerialName("httpEgress")
open class HTTPSEgressRoute : Route() {
    /** Exposed port for internal traffic */
    var listenPort: Int? = null

    /** Endpoint prefix for internal traffic */
    var endpoint: String = "https_out"

    /** Clearing House flag for automatic clearing of message */
    var clearing: Boolean = false

    /**
     * DAPS Injection flag for automatic injection of Dynamic Attribute Tokens to outgoing requests
     */
    var dapsInject: Boolean = true

    /** Header used for indicating the intended recipient of the request */
    var forwardHeader: String = Constants.Headers.forwardTo
}

/** IDSCP Egress Route Configuration */
@Validated
@Serializable
@SerialName("idscpEgress")
class IDSCPEgressRoute : Route() {
    /** Exposed port for internal traffic */
    var listenPort: Int? = null

    /** Endpoint prefix for internal traffic */
    var endpoint: String = "idscp_out"

    /** Clearing House flag for automatic clearing of message */
    var clearing: Boolean = false

    /**
     * DAPS Injection flag for automatic injection of Dynamic Attribute Tokens to outgoing requests
     */
    var dapsInject: Boolean = true

    /** Header used for indicating the intended recipient of the request */
    var forwardHeader: String = Constants.Headers.forwardTo

    /** Flag that indicates that REST over IDSCP is used */
    var restOverIDSCP: Boolean = false
}

/** Custom XML Route Configuration */
class CustomRoute : Route() {
    /** XML Route definition */
    @field:NonNull lateinit var xml: String
}
