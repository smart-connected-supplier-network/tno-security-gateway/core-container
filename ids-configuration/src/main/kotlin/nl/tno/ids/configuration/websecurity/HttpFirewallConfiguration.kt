package nl.tno.ids.configuration.websecurity

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.web.firewall.HttpFirewall
import org.springframework.security.web.firewall.StrictHttpFirewall

/**
 * Spring HttpFirewall configuration
 */
@Configuration
class HttpFirewallConfiguration {

    /** HttpFirewal bean with allowed encoded slashes */
    @Bean
    fun httpFirewall(): HttpFirewall {
        return StrictHttpFirewall().apply {
            setAllowUrlEncodedSlash(true)
            setAllowUrlEncodedDoubleSlash(true)
            setAllowUrlEncodedPercent(true)
        }
    }
}