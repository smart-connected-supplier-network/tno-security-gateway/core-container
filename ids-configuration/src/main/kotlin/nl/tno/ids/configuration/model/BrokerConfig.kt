/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.model

import nl.tno.ids.configuration.validation.IDSIdentifier
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import java.net.URI
import java.net.URL
import javax.validation.constraints.NotNull

/** IDS Broker Configuration */
@Component
@Validated
@ConfigurationProperties(prefix = "broker")
class BrokerConfig {
    /** Broker IDS Identifier */
    @field:NotNull @field:IDSIdentifier lateinit var id: URI

    /** Broker Access URL */
    @field:NotNull lateinit var address: URL

    /** Automatic registration at the Broker */
    var autoRegister = true

    /** Self-Description Profile to share with the Broker */
    var profile = Profile.MINIMAL

    /** Interval (in hours) for automatic re-registration */
    @Deprecated("Automatic re-registration will be fased out in favor of health messages")
    var reRegisterInterval = 1f

    /** Initial delay (in milliseconds) for initial Broker registration */
    var brokerInitialDelay = 10000

    /** Maximum of retries for initial Broker registration */
    var registrationMaxRetries = 10

    /** Backoff period (in milliseconds) for initial Broker registration */
    var registrationBackoffPeriod = 10000

    /** Broker health check interval (in milliseconds) */
    var brokerHealthCheckInterval = 3600000

    /** Broker registration profiles */
    enum class Profile {
        MINIMAL,
        FULL
    }
}
