/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.model

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotNull

interface DapsUrls {
    /** URL of the DAPS */
    var url: String
    /** Issuer URL of the DAPS */
    var issuerUrl: String?
    /** JWKS URL of the DAPS */
    var jwksUrl: String?
}

/** Dynamic Attribute Provisioning Service Configuration */
@Component
@ConfigurationProperties(prefix = "daps")
@Validated
class DapsConfig: DapsUrls {
    /** URL of the DAPS */
    @field:NotNull
    override lateinit var url: String

    /** Issuer URL of the DAPS */
    override var issuerUrl: String? = null
    /** Token URL of the DAPS */
    var tokenUrl: String? = null
    /** JWKS URL of the DAPS */
    override var jwksUrl: String? = null

    /** Additional Daps (for extra JWKS that you want to allow) */
    var additionalDaps: List<AdditionalDapsConfig> = emptyList()

    /** Minimal required security profile */
    var requiredSecurityProfile: Profile = Profile.BASE

    /** Dynamic Claims request */
    var dynamicClaims: DynamicClaims? = null

    /** DAPS Security Profiles, based on IDS Certification scheme */
    enum class Profile {
        /** Base Security Profile */
        BASE,

        /** Trust Security Profile */
        TRUST,

        /** Trust+ Security Profile */
        TRUST_PLUS
    }
}

/** Dynamic Claims configuration */
class DynamicClaims {
    /** Transport certificate binding */
    var transportCertsSha256: TransportCertsSha256? = null
}

class TransportCertsSha256 {
    /** Transport certificate location */
    @field:NotNull
    lateinit var certLocation: String
}

@Validated
class AdditionalDapsConfig: DapsUrls {
    /** URL of the DAPS */
    @field:NotNull
    override lateinit var url: String

    /** Issuer URL of the DAPS */
    override var issuerUrl: String? = null
    /** JWKS URL of the DAPS */
    override var jwksUrl: String? = null
}
