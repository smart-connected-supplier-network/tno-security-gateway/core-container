/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.infomodel

import de.fraunhofer.iais.eis.ModelClass
import nl.tno.ids.configuration.serialization.Serialization
import org.json.JSONObject
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Configuration
import org.springframework.core.convert.converter.Converter
import org.springframework.core.convert.converter.ConverterFactory
import org.springframework.format.FormatterRegistry
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.util.UrlPathHelper
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


/** Spring Web MCV configurer for allowing direct IDS concept parsing */
@Configuration
class WebConfig : WebMvcConfigurer {
    companion object {
        private val LOG = LoggerFactory.getLogger(WebConfig::class.java)
    }

    /**
     * Disable URL decoding, necessary for allowing URL encoded identifiers (URIs) to be set as path
     * parameters
     */
    override fun configurePathMatch(configurer: PathMatchConfigurer) {
        configurer.setUrlPathHelper(UrlPathHelper().apply { isUrlDecode = false })
        super.configurePathMatch(configurer)
    }

    /**
     * ConverterFactory for IDS Infomodel Concepts
     *
     * Adds @context field to the json if it does not exists, for when a subresource is copied (e.g.
     * a ContractOffer from a SelfDescription)
     */
    override fun addFormatters(registry: FormatterRegistry) {
        registry.addConverterFactory(
            object : ConverterFactory<String, ModelClass> {
                override fun <T : ModelClass> getConverter(
                    targetType: Class<T>
                ): Converter<String, T> {
                    return Converter {
                        val jsonld =
                            if (!it.contains("@context")) {
                                JSONObject(it)
                                    .put(
                                        "@context",
                                        JSONObject()
                                            .put("ids", "https://w3id.org/idsa/core/")
                                            .put("idsc", "https://w3id.org/idsa/code/"))
                                    .toString()
                            } else {
                                it
                            }
                        try {
                            Serialization.fromJsonLD(jsonld, targetType)
                        } catch (e: Exception) {
                            LOG.warn("Could not parse JSON-LD", e)
                            throw e
                        }
                    }
                }
            })
    }
}

@Component("corsFilter2")
class CorsFilter : OncePerRequestFilter() {
    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        response.addHeader("Access-Control-Allow-Origin", "*")
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, PATCH, HEAD")
        response.addHeader(
            "Access-Control-Allow-Headers",
            "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
        )
        response.addHeader(
            "Access-Control-Expose-Headers",
            "Access-Control-Allow-Origin, Access-Control-Allow-Credentials"
        )
        response.addHeader("Access-Control-Allow-Credentials", "true")
        response.addIntHeader("Access-Control-Max-Age", 10)
        filterChain.doFilter(request, response)
    }
}