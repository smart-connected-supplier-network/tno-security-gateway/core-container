package nl.tno.ids.configuration.serialization

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.node.ObjectNode
import de.fraunhofer.iais.eis.ids.jsonld.Serializer
import java.io.IOException
import java.text.SimpleDateFormat

/**
 * IDS Serialization object
 */
object Serialization {
    /** IDS Infomodel Serializer */
    private val serializer: Serializer = Serializer()

    /** Deserialize JSON-LD into a InfoModel class */
    @Synchronized
    fun <I> fromJsonLD(json: String?, clazz: Class<I>): I {
        try {
            return serializer.deserialize(json, clazz)
        } catch (e: IOException) {
            throw e
        }
    }

    /** Deserialize JSON-LD into a Infomodel class, with reified type parameter */
    @Suppress("UNCHECKED_CAST")
    inline fun <reified I> fromJsonLD(json: String?): I {
        return when (I::class) {
            is Collection<*> -> {
                val typeReference = object: TypeReference<I>() {}
                val type = ObjectMapper().typeFactory.constructType(typeReference)
                val contentType = type.contentType
                val clazz = contentType.rawClass
                fromJsonLDCollection(json, clazz) as I
            }
            else -> {
                this.fromJsonLD(json, I::class.java)
            }
        }
    }

    /** Deserialize JSON array into list of Infomodel classes */
    @Synchronized
    fun <I> fromJsonLDCollection(json: String?, clazz: Class<I>): List<I> {
        val om = ObjectMapper()
        om.dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        val jsonNode = om.readTree(json)
        val result: MutableList<I> = ArrayList()
        if (jsonNode.isArray) {
            for (item in jsonNode.iterator()) {
                if (!item.has("@context")) {

                    (item as ObjectNode).set<ObjectNode>("@context",
                        om.createObjectNode()
                            .put("ids", "https://w3id.org/idsa/core/")
                            .put("idsc", "https://w3id.org/idsa/code/")
                        )
                }
                result.add(this.fromJsonLD(om.writeValueAsString(item), clazz))
            }
        }
        return result
    }

    /** Serialize object into JSON-LD */
    @Synchronized
    fun toJsonLD(obj: Any?): String {
        return serializer.serialize(obj)
    }
}
