package nl.tno.ids.configuration.model

import org.slf4j.LoggerFactory
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding
import org.springframework.core.convert.converter.Converter
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.File
import java.nio.charset.StandardCharsets
import java.security.KeyFactory
import java.security.KeyStoreException
import java.security.PrivateKey
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.security.spec.InvalidKeySpecException
import java.security.spec.PKCS8EncodedKeySpec
import java.util.*
import java.util.regex.Pattern


/**
 * Certificate Converter for converting Strings into X509Certificates
 */
@Component
@ConfigurationPropertiesBinding
class CertificateConverter: Converter<String, X509Certificate> {
    companion object {
        private val LOG = LoggerFactory.getLogger(CertificateConverter::class.java)
        /** PEM Certificate regular expression */
        private val CERT_PATTERN =
            Pattern.compile(
                "-+BEGIN\\s+.*CERTIFICATE[^-]*-+(?:\\s|\\r|\\n)+" + // Header
                        "([a-z\\d+/=\\r\\n]+)" + // Base64 text
                        "-+END\\s+.*CERTIFICATE[^-]*-+", // Footer
                Pattern.CASE_INSENSITIVE)
    }

    /**
     * Convert configuration property to X509 certificate
     *
     * Supports converting files (both plain as Base64), embedded Strings, and embedded Base64 encoded strings
     */
    override fun convert(source: String): X509Certificate {
        val cert = if (source.startsWith("file:")) {
            File(source.drop(5)).readText()
        } else {
            source
        }
        return try {
            readCertificate(cert)
        } catch (e1: KeyStoreException) {
            try {
                readCertificate(base64Decode(cert).decodeToString())
            } catch (e2: KeyStoreException) {
                LOG.debug("Exception reading plain certificate", e1)
                LOG.debug("Exception reading base64 encoded certificate", e2)
                throw IllegalArgumentException("Provided certificate is not in PEM or Base64 encoded PEM format")
            }
        }
    }

    /** Convert String to X509Certificate */
    private fun readCertificate(certificate: String): X509Certificate {
        val matcher = CERT_PATTERN.matcher(certificate)
        val certificateFactory = CertificateFactory.getInstance("X.509")
        if (matcher.find()) {
            val buffer = base64Decode(matcher.group(1))
            return certificateFactory.generateCertificate(ByteArrayInputStream(buffer)) as X509Certificate
        }
        throw KeyStoreException("Did not find certificate")
    }

    /** Base64 encoding helper function */
    private fun base64Decode(base64: String): ByteArray {
        return Base64.getMimeDecoder().decode(base64.toByteArray(StandardCharsets.US_ASCII))
    }
}

/**
 * Private Key Converter for converting Strings into X509Certificates
 */
@Component
@ConfigurationPropertiesBinding
class PrivateKeyConverter: Converter<String, PrivateKey> {
    companion object {
        private val LOG = LoggerFactory.getLogger(PrivateKeyConverter::class.java)
        /** PKCS#8 Key regular expression */
        private val KEY_PATTERN =
            Pattern.compile(
                ("-+BEGIN\\s+.*PRIVATE\\s+KEY[^-]*-+(?:\\s|\\r|\\n)+" + // Header
                        "([a-z0-9+/=\\r\\n]+)" + // Base64 text
                        "-+END\\s+.*PRIVATE\\s+KEY[^-]*-+"), // Footer
                Pattern.CASE_INSENSITIVE)
    }

    /**
     * Convert configuration property to PrivateKey
     *
     * Supports converting files (both plain as Base64), embedded Strings, and embedded Base64 encoded strings
     */
    override fun convert(source: String): PrivateKey {
        val key = if (source.startsWith("file:")) {
            File(source.drop(5)).readText()
        } else {
            source
        }
        return try {
            readPrivateKey(key)
        } catch (e1: KeyStoreException) {
            try {
                readPrivateKey(base64Decode(key).decodeToString())
            } catch (e2: KeyStoreException) {
                LOG.debug("Exception reading plain private key", e1)
                LOG.debug("Exception reading base64 encoded private key", e2)
                throw IllegalArgumentException("Provided private key is not in PEM or Base64 encoded PEM format")
            } catch (e2: IllegalArgumentException) {
                throw IllegalArgumentException("Provided private key is not in PEM or Base64 encoded PEM format")
            }
        }
    }

    /** Read String based private key to PrivateKey object */
    private fun readPrivateKey(privateKey: String): PrivateKey {
        val encodedKeySpec = readPrivateKeySpec(privateKey)
        return try {
            val keyFactory = KeyFactory.getInstance("RSA")
            keyFactory.generatePrivate(encodedKeySpec)
        } catch (ignore: InvalidKeySpecException) {
            val keyFactory = KeyFactory.getInstance("DSA")
            keyFactory.generatePrivate(encodedKeySpec)
        }
    }

    /** Read PKCS#8 key to PKCS8EncodedKeySpec */
    private fun readPrivateKeySpec(key: String): PKCS8EncodedKeySpec {
        val matcher = KEY_PATTERN.matcher(key)
        if (!matcher.find()) {
            throw KeyStoreException("found no private key: ")
        }
        val encodedKey = base64Decode(matcher.group(1))
        return PKCS8EncodedKeySpec(encodedKey)
    }

    /** Base64 encoding helper function */
    private fun base64Decode(base64: String): ByteArray {
        return Base64.getMimeDecoder().decode(base64.toByteArray(StandardCharsets.US_ASCII))
    }
}

/**
 * Certificate Chain Converter for converting Strings into X509Certificates lists
 */
@Component
@ConfigurationPropertiesBinding
class CertificateChainConverter: Converter<String, List<X509Certificate>> {
    companion object {
        private val LOG = LoggerFactory.getLogger(CertificateConverter::class.java)
        /** PEM Certificate regular expression */
        private val CERT_PATTERN =
            Pattern.compile(
                "-+BEGIN\\s+.*CERTIFICATE[^-]*-+(?:\\s|\\r|\\n)+" + // Header
                        "([a-z\\d+/=\\r\\n]+)" + // Base64 text
                        "-+END\\s+.*CERTIFICATE[^-]*-+", // Footer
                Pattern.CASE_INSENSITIVE)
    }

    /**
     * Convert configuration property to X509 certificate
     *
     * Supports converting files (both plain as Base64), embedded Strings, and embedded Base64 encoded strings
     */
    override fun convert(source: String): List<X509Certificate> {
        val certs = if (source.startsWith("file:")) {
            File(source.drop(5)).readText()
        } else {
            source
        }
        return try {
            readCertificate(certs)
        } catch (e1: KeyStoreException) {
            try {
                readCertificate(base64Decode(certs).decodeToString())
            } catch (e2: KeyStoreException) {
                LOG.debug("Exception reading plain certificate", e1)
                LOG.debug("Exception reading base64 encoded certificate", e2)
                throw IllegalArgumentException("Provided certificate is not in PEM or Base64 encoded PEM format")
            }
        }
    }

    /** Convert String to X509Certificate */
    private fun readCertificate(certificate: String): List<X509Certificate> {
        val matcher = CERT_PATTERN.matcher(certificate)
        val certificateFactory = CertificateFactory.getInstance("X.509")
        val certificates: MutableList<X509Certificate> = ArrayList()
        var start = 0
        while (matcher.find(start)) {
            val buffer = base64Decode(matcher.group(1))
            certificates.add(
                certificateFactory.generateCertificate(ByteArrayInputStream(buffer)) as
                        X509Certificate)
            start = matcher.end()
        }
        if (certificates.isNotEmpty()) {
            return certificates
        }
        throw KeyStoreException("Did not find certificate")
    }

    /** Base64 encoding helper function */
    private fun base64Decode(base64: String): ByteArray {
        return Base64.getMimeDecoder().decode(base64.toByteArray(StandardCharsets.US_ASCII))
    }
}