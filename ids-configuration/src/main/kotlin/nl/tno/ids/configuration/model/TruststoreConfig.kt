/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.model

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.NestedConfigurationProperty
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import java.security.cert.X509Certificate
import javax.validation.constraints.NotNull

/** Primary Truststore Configuration */
@Component
@ConfigurationProperties(prefix = "truststore")
class TruststoreConfig {
    /** Type of supplied truststore */
    var type: TruststoreType = TruststoreType.PEM

    /** Optional PEM configuration */
    @NestedConfigurationProperty var pem: PEMTruststoreConfig? = null
}

enum class TruststoreType {
    /** Truststore that accepts all certificates, regardless of CA */
    ACCEPT_ALL,

    /** Default System truststore, nearly the same as Browser accepted certificates (by default) */
    SYSTEM,

    /** PEM-based Certficate Authority Chain configuration */
    PEM
}

/** PEM Truststore Configuration */
@Validated
class PEMTruststoreConfig {
    /** Concatenated PEM CA certificates to be loaded in the Truststore */
    @field:NotNull lateinit var chain: List<X509Certificate>
}
