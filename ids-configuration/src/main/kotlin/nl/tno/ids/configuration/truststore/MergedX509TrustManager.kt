/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.truststore

import nl.tno.ids.configuration.keystore.KeystoreGenerator
import org.slf4j.LoggerFactory
import java.security.cert.*
import javax.net.ssl.X509TrustManager

/** X509TrustManager that merges two separate trust managers into one */
class MergedX509TrustManager(
    private val defaultTm: X509TrustManager,
    private val additionalTm: X509TrustManager
) : X509TrustManager {
    private val ocsp = !System.getenv().containsKey("ENABLE_OCSP") || System.getenv()["ENABLE_OCSP"].toBoolean()
    private lateinit var params: PKIXParameters

    init {
        if (ocsp) {
            val trustStore =
                KeystoreGenerator().createTruststore((defaultTm.acceptedIssuers + additionalTm.acceptedIssuers).toList())
            params = PKIXParameters(trustStore)
            params.isRevocationEnabled = true
        }
    }

    /** Get merged accepted issuers (i.e. trusted CA certificates) */
    override fun getAcceptedIssuers(): Array<X509Certificate> {
        return defaultTm.acceptedIssuers + additionalTm.acceptedIssuers
    }

    /**
     * Validate whether the X509 certificate chain of a server can be trusted
     *
     * @throws CertificateException if the certificates are not trusted by either trust manager
     */
    @Throws(CertificateException::class)
    override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
        try {
            additionalTm.checkServerTrusted(chain, authType)
        } catch (e: CertificateException) {
            defaultTm.checkServerTrusted(chain, authType)
        }
        checkOCSP(chain)
    }

    /**
     * Validate whether the X509 certificate chain of a client can be trusted
     *
     * @throws CertificateException if the certificates are not trusted by either trust manager
     */
    @Throws(CertificateException::class)
    override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
        try {
            additionalTm.checkClientTrusted(chain, authType)
        } catch (e: CertificateException) {
            defaultTm.checkClientTrusted(chain, authType)
        }
    }

    fun checkOCSP(chain: Array<X509Certificate>) {
        if (ocsp) {
            try {
                LOG.info("${chain.map { it.subjectX500Principal }}")
                val path = CertificateFactory.getInstance("X.509").generateCertPath(chain.asList())
                val validator = CertPathValidator.getInstance("PKIX")
                val result = validator.validate(path, params)
                LOG.debug("OCSP result: $result")
            } catch (e: Exception) {
                LOG.warn("OCSP Error: ${e.message}")
                LOG.debug("", e)
                throw e
            }
        }
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(MergedX509TrustManager::class.java)
    }
}
