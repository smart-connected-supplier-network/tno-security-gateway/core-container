package nl.tno.ids.configuration.serialization

import java.util.*
import javax.xml.datatype.DatatypeFactory
import javax.xml.datatype.XMLGregorianCalendar

/**
 * Date utility for creating XMLGregorianCalendars
 */
object DateUtil {
    /** DatatypeFactory  */
    private val df = DatatypeFactory.newInstance()

    /** Create XMLGregorianCalendar for the current timestamp */
    fun now(): XMLGregorianCalendar {
        val gc = GregorianCalendar()
        gc.timeInMillis = Date().time
        return df.newXMLGregorianCalendar(gc)
    }

    /** Create XMLGregorianCalendar for a given java.util.Date class */
    fun asXMLGregorianCalendar(date: Date): XMLGregorianCalendar {
        val gc = GregorianCalendar()
        gc.timeInMillis = date.time
        return df.newXMLGregorianCalendar(gc)
    }

    /** Create XMLGregorianCalendar for a given date */
    fun asXMLGregorianCalendar(year: Int, month: Int, day: Int): XMLGregorianCalendar {
        return asXMLGregorianCalendar(GregorianCalendar(year, month, day).time)
    }

    /** Convert XMLGregorianCalendar to a java.util.Date */
    fun asDate(xmlGC: XMLGregorianCalendar): Date {
        return xmlGC.toGregorianCalendar().time
    }
}
