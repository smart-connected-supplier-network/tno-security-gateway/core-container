/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.keystore

import java.security.KeyStore
import java.security.PrivateKey
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

/** Helper object to convert PEM encoded certificates and keys (PKCS#8) to java.security objects */
@Component
class KeystoreGenerator {
    companion object {
        private val LOG = LoggerFactory.getLogger(KeystoreGenerator::class.java)
    }

    /** Create Truststore from certificate list */
    fun createTruststore(certificateChain: List<X509Certificate>): KeyStore {
        val keyStore = KeyStore.getInstance("JKS")
        keyStore.load(null, null)
        certificateChain.forEach { certificate ->
            val principal = certificate.subjectX500Principal
            LOG.debug("Adding certificate to CA chain: {}", principal.getName("RFC2253"))
            keyStore.setCertificateEntry(principal.getName("RFC2253"), certificate)
        }
        return keyStore
    }

    /** Create Keystore from PEM certificate(s) and Private Key */
    fun createKeystore(
        certificateChain: List<X509Certificate>,
        privateKey: PrivateKey,
        keystoreType: String = "JKS",
        alias: String = "key",
        password: String = ""
    ): KeyStore {
        val orderedCertificateList = certificateChain.let { list ->
            if (list.isEmpty()) {
                throw CertificateException(
                    "Certificate file does not contain any certificates: $certificateChain")
            }
            if (list.size > 1) {
                val mutableList = mutableListOf(list.first())
                do {
                    val current = mutableList.last()
                    if (current.issuerX500Principal == current.subjectX500Principal) {
                        //                        mutableList.add(current)
                        break
                    }
                    val next = list.find { it.subjectX500Principal == current.issuerX500Principal }
                    if (next != null) {
                        mutableList.add(next)
                    }
                } while (next != null)
                mutableList
            } else {
                list
            }
        }
        val keyStore = KeyStore.getInstance(keystoreType)
        keyStore.load(null, null)

        keyStore.setKeyEntry(alias, privateKey, password.toCharArray(), orderedCertificateList.toTypedArray())
        return keyStore
    }
}
