/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.model

import de.fraunhofer.iais.eis.SecurityProfile
import nl.tno.ids.configuration.validation.IDSIdentifier
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import java.net.URI
import java.net.URL
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

/** Main administrative connector information */
@Component
@Validated
@ConfigurationProperties(prefix = "info")
class ConnectorInfo {
    /** Connector IDS identifier */
    @field:NotNull @field:IDSIdentifier lateinit var idsid: URI

    /** Connector titles */
    @field:NotNull @field:Size(min = 1) lateinit var titles: List<String>

    /** Connector descriptions */
    @field:NotNull @field:Size(min = 1) lateinit var descriptions: List<String>

    /** Connector Access URL for external connectors */
    @field:NotNull @field:Size(min = 1) lateinit var accessUrl: List<URL>

    /** Curator of the contents of this connector, an IDS participant identifier */
    @field:NotNull @field:IDSIdentifier lateinit var curator: URI

    /** Technical administrator of this connector, an IDS participant identifier */
    @field:NotNull @field:IDSIdentifier lateinit var maintainer: URI

    /** Security Profile as set as self-declaration in the metadata of this connector */
    var securityProfile: SecurityProfile = SecurityProfile.TRUST_SECURITY_PROFILE
}
