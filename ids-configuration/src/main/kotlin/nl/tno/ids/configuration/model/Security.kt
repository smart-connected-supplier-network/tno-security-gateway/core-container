package nl.tno.ids.configuration.model

/**
 * Security Roles used throughout the Core Container to limit access to API endpoints to certain User/API key roles
 */
object SecurityRoles {
    /** Administrator role for the primary administrator that has full access and inherits all of the other roles */
    const val ADMIN = "ROLE_ADMIN"
    /** Data App role that can be given to Data Apps for managing the resources that they offer */
    const val DATA_APP = "ROLE_DATA_APP"
    /** Reader role that is allowed to read all resources in the core container but not modify them */
    const val READER = "ROLE_READER"
    /** Internal role for calling especially HTTPS Out routes */
    const val INTERNAL = "ROLE_INTERNAL"

    /** Artifact Provider Manager role that is allowed to administrate provided artifact */
    const val ARTIFACT_PROVIDER_MANAGER = "ROLE_ARTIFACT_PROVIDER_MANAGER"
    /** Artifact Provider Reader role that is allowed to list provided artifacts */
    const val ARTIFACT_PROVIDER_READER = "ROLE_ARTIFACT_PROVIDER_READER"
    /** Artifact Consumer role that is allowed to request artifacts from other connectors in the network */
    const val ARTIFACT_CONSUMER = "ROLE_ARTIFACT_CONSUMER"
    /** Orchestration Manager role that is allowed to orchestrate containers */
    const val ORCHESTRATION_MANAGER = "ROLE_ORCHESTRATION_MANAGER"
    /** Orchestration Reader role that is allowed to list orchestrated containers */
    const val ORCHESTRATION_READER = "ROLE_ORCHESTRATION_READER"
    /** Policy Enforcement Manager role that is allowed to initiate new contracts and contract negotiations */
    const val PEF_MANAGER = "ROLE_PEF_MANAGER"
    /** Policy Enforcement Reader role that is allowed to list agreed upon contracts and contract offers */
    const val PEF_READER = "ROLE_PEF_READER"
    /** Resource Manager role that is allowed to modify the resources provided by the core container and data apps */
    const val RESOURCE_MANAGER = "ROLE_RESOURCE_MANAGER"
    /** Resource Reader role that is allowed to list resources provided by the core container and data apps */
    const val RESOURCE_READER = "ROLE_RESOURCE_READER"
    /** Route Manager role that is allowed to modify Camel routes offered by the Core Container */
    const val ROUTE_MANAGER = "ROLE_ROUTE_MANAGER"
    /** Route Reader role that is allowed to list Camel routes offered by the Core Container */
    const val ROUTE_READER = "ROLE_ROUTE_READER"
    /** Description Reader role that is allowed to send Description Request messages to other connectors in the network */
    const val DESCRIPTION_READER = "ROLE_DESCRIPTION_READER"
    /** Description Manager role that is allowed to modify the registration at Metadata Broker(s) in the network */
    const val DESCRIPTION_MANAGER = "ROLE_DESCRIPTION_MANAGER"
    /** Workflow Manager role that is allowed to initiate new workflows */
    const val WORKFLOW_MANAGER = "ROLE_WORKFLOW_MANAGER"
    /** Workflow Reader role that is allowed to list workflows and show the results and status of the workflow */
    const val WORKFLOW_READER = "ROLE_WORKFLOW_READER"

    /**
     * Get inherited roles from a specific role
     */
    fun getRelatedRoles(role: String): Set<String> {
        return when(role) {
            ADMIN -> setOf(ADMIN, ARTIFACT_PROVIDER_MANAGER, ARTIFACT_PROVIDER_READER, ARTIFACT_CONSUMER, ORCHESTRATION_MANAGER, ORCHESTRATION_READER, PEF_MANAGER, PEF_READER,
            RESOURCE_MANAGER, RESOURCE_READER, ROUTE_MANAGER, ROUTE_READER, DESCRIPTION_READER, DESCRIPTION_MANAGER, WORKFLOW_MANAGER, WORKFLOW_READER)
            DATA_APP -> setOf(DATA_APP, RESOURCE_MANAGER, RESOURCE_READER, DESCRIPTION_MANAGER, DESCRIPTION_READER)
            READER -> setOf(READER, ARTIFACT_PROVIDER_READER, ARTIFACT_CONSUMER, ORCHESTRATION_READER, PEF_READER, RESOURCE_READER, ROUTE_READER, DESCRIPTION_READER, WORKFLOW_READER)
            ARTIFACT_PROVIDER_MANAGER -> setOf(ARTIFACT_PROVIDER_MANAGER, ARTIFACT_PROVIDER_READER, ARTIFACT_CONSUMER)
            ORCHESTRATION_MANAGER -> setOf(ORCHESTRATION_MANAGER, ORCHESTRATION_READER)
            PEF_MANAGER -> setOf(PEF_MANAGER, PEF_READER)
            RESOURCE_MANAGER -> setOf(RESOURCE_MANAGER, RESOURCE_READER)
            ROUTE_MANAGER -> setOf(ROUTE_MANAGER, ROUTE_READER)
            WORKFLOW_MANAGER -> setOf(WORKFLOW_MANAGER, WORKFLOW_READER)
            DESCRIPTION_MANAGER -> setOf(DESCRIPTION_MANAGER, DESCRIPTION_READER)
            else -> setOf(role)
        }
    }
}