/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.model

import javax.validation.constraints.NotNull
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.cloud.context.config.annotation.RefreshScope
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated

/** MongoDB Connection Configuration */
@Validated
sealed class MongoDbConfig {
    /** MongoDB hostname */
    @field:NotNull lateinit var hostname: String

    /** MongoDB port */
    var port: Int = 27017

    /** MongoDB username */
    var username: String? = null

    /** MongoDB password */
    var password: String? = null

    /** Authentication Database that contains the user to use */
    var authenticationDatabase: String? = null

    /** MongoDB database containing the relevant collection */
    @field:NotNull lateinit var database: String

    /** MongoDB collection */
    var collection: String? = null

    /** Connect to MongoDB via SSL */
    var isSslEnabled: Boolean = false

    /** Configuration on whether the MongoDB is watchable, i.e. is a replica set */
    var isWatchable: Boolean = false
}

/** MongoDB Configuration for Resource Database */
@Component
@ConditionalOnProperty(name = ["resource-database.hostname"])
@ConfigurationProperties(prefix = "resource-database")
class ResourceDbConfig : MongoDbConfig()
