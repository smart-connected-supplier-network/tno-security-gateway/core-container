package nl.tno.ids.configuration.model

import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.RequestMessage
import de.fraunhofer.iais.eis.RequestMessageBuilder
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.multipart.MultiPartMessage
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.ComponentScan
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.http.RequestEntity
import org.springframework.test.context.TestPropertySource
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate
import java.io.InputStream
import java.net.URI

/**
 * Helper RestController that echos incoming requests
 */
@RestController
@RequestMapping("api/test")
class MessageConverterController {
    /**
     * Post mapping that accepts IDS Message classes
     */
    @PostMapping("ids",
        consumes = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"],
        produces = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"])
    fun ids(@RequestBody body: Message): Message {
        return body
    }

    /**
     * Post mapping that accepts IDS Message collections
     */
    @PostMapping("idsCollection",
        consumes = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"],
        produces = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"])
    fun idsCollection(@RequestBody body: Collection<Message>): Collection<Message> {
        return body
    }

    /**
     * Post mapping that accepts multipart messages
     */
    @PostMapping("multipart",
        consumes = [MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.MULTIPART_MIXED_VALUE],
        produces = [MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.MULTIPART_MIXED_VALUE])
    fun multipart(@RequestBody(required = false) body: MultiPartMessage?, @RequestPart(required = false) header: Message?, @RequestPart(required = false) payload: InputStream?): MultiPartMessage {
        return body ?: MultiPartMessage(header!!, payload?.readAllBytes()?.decodeToString())
    }

    /**
     * Post mapping that accepts inputstreams
     */
    @PostMapping("inputstream",
        consumes = [MediaType.APPLICATION_OCTET_STREAM_VALUE],
        produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE])
    fun inputstream(@RequestBody body: InputStream): InputStream {
        val bodyString = body.readAllBytes().decodeToString()
        return bodyString.byteInputStream()
    }
}

/**
 * Class to test automatic message conversions by Spring
 */
@SpringBootTest(classes = [MessageConvertersTest.Application::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
    properties = ["spring.config.location = classpath:application.yaml"])
class MessageConvertersTest {
    /** Application context */
    @SpringBootApplication(exclude = [SecurityAutoConfiguration::class])
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Port Spring listens on */
    @LocalServerPort
    private var port: Int = 0

    /** RestTemplate for creating HTTP requests */
    @Autowired private lateinit var restTemplate: RestTemplate

    /** Test automatic IDS Infomodel class conversion */
    @Test
    fun testIDSConverter() {
        val message = RequestMessageBuilder()
            ._issuerConnector_(URI("urn:ids:test"))
            ._recipientConnector_(URI("urn:ids:test"))
            ._senderAgent_(URI("urn:ids:test"))
            ._recipientAgent_(URI("urn:ids:test"))
            .buildWithDefaults()
        val response = restTemplate.postForObject("http://localhost:$port/api/test/ids", message, RequestMessage::class.java)

        Assertions.assertEquals(message, response)
    }

    /** Test automatic IDS Infomodel collection conversion */
    @Test
    @Disabled("TODO: Dates are converted to 2022-05-25T15:03:45.660UTC syntax somewhere, which can't be parsed correctly")
    fun testIDSCollectionConverter() {
        val message = RequestMessageBuilder()
            ._issuerConnector_(URI("urn:ids:test"))
            ._recipientConnector_(URI("urn:ids:test"))
            ._senderAgent_(URI("urn:ids:test"))
            ._recipientAgent_(URI("urn:ids:test"))
            .buildWithDefaults()
        val response = restTemplate.exchange(
            RequestEntity.post("http://localhost:$port/api/test/idsCollection")
                .contentType(MediaType.parseMediaType("application/ld+json"))
                .body(listOf(message, message)),
            object : ParameterizedTypeReference<List<Message>>() {})
        Assertions.assertEquals(message, response.body)
        Assertions.assertEquals(2, response.body?.size)
    }

    /** Test automatic multipart conversion */
    @Test
    fun testMultipartConverter() {
        val message = RequestMessageBuilder()
            ._issuerConnector_(URI("urn:ids:test"))
            ._recipientConnector_(URI("urn:ids:test"))
            ._senderAgent_(URI("urn:ids:test"))
            ._recipientAgent_(URI("urn:ids:test"))
            .buildWithDefaults()
            .toMultiPartMessage()
        val response = restTemplate.exchange(
            RequestEntity.post("http://localhost:$port/api/test/multipart")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(message),
            MultiPartMessage::class.java)
        Assertions.assertEquals(message.header, response.body?.header)
    }

    /** Test automatic inputstream conversion */
    @Test
    fun testInputStreamConverter() {
        val message = "Test Bytes".byteInputStream()
        val response = restTemplate.exchange(
            RequestEntity.post("http://localhost:$port/api/test/inputstream")
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(message),
            String::class.java)
        Assertions.assertEquals("Test Bytes", response.body)
    }
}