/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.configuration.model

import de.fraunhofer.iais.eis.Action
import de.fraunhofer.iais.eis.ContractOffer
import de.fraunhofer.iais.eis.ContractOfferBuilder
import de.fraunhofer.iais.eis.PermissionBuilder
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.serialization.Serialization
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.TestPropertySource
import java.net.URI

/**
 * Test to test the inclusion of addtional properties in IDS classes
 */
@SpringBootTest(classes = [AdditionalPropertyTest.Application::class])
@TestPropertySource(
    properties = ["spring.config.location = classpath:application.yaml"])
class AdditionalPropertyTest {
    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Test serialization and deserialization of additional property with custom namespace */
    @Test
    fun testAdditionalProperty() {
        val test = ContractOfferBuilder()
            ._permission_(
                PermissionBuilder()
                    ._action_(Action.READ)
                    ._target_(URI("urn:ids:TEST"))
                    .build()
            )
            .build()
            .apply { setProperty("nlaic:Test", "Test Value") }
            .toJsonLD()

        println(test)

        val test2 = Serialization.fromJsonLD<ContractOffer>(test)

        Assertions.assertEquals(
            "Test Value", (test2.properties["nlaic:Test"] as TypedLiteral).value)
    }
}
