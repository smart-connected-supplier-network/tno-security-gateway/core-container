# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.9] - 2024-01-09
## Changed
- Updated dependencies
- Upgrade Java to 21

## [1.1.8] - 2023-12-07
### Fixed
- Fixed OCSP client verification in X509 Trust Manager
- Added missing autowire qualifier for SSL context

### Added
- Added TLS client authentication handling

## [1.1.7] - 2023-11-23
### Fixed
- Password rotation only update values on new save.


## [1.1.6] - 2023-11-15
### Fixed
- Password rotation when using initial users in values.yaml


## [1.1.5] - 2023-11-14
### Added
- Password rotation

### Changed
- Cannot reuse the same password when changing the password.

## [1.1.4] - 2023-11-10
### Added
- Logout functionality. Puts JWT in blacklist, so it cannot be used anymore after login.
- Check if there is only 1 admin user, if this is the case the user cannot be deleted.

## [1.1.3] - 2023-11-03
### Added
- JaCoCo plugin
- `affectedConnector` parameter for Broker deregistration, for support of the Testbed Broker
- `validate` parameter for Description Requests

### Fixed
- CORS handling in case client provides CORS headers

### Security
- Changed Camel error page to reduce information

### Updated
- Improved account locking capabilities

## [1.1.2] - 2023-10-09
### Added
- Dynamic DAT Claims support for including transport certificates inside DAPS DATs
- TLS encryption on Camel routes (in a global setting)
- Basic security audit in the healthcheck, displays information around the security of the configuration.
- Added option to deploy secure only, which means all endpoints should be communicating via HTTPS only.
- Added encryption at rest

### Fixed
- Fixed default egress URL parsing in ArtifactConsumerController
- Set the correct IANA mediatype identifier for uploaded artifacts
- Only allow TLS1.3 for egress and ingress (if configured)
- Improved configuration audit logging with request body and response code
- Make OCSP configurable

## [1.1.1] - 2023-08-02
### Security
- Updated dependency versions.

### Fixed
- Renewed DAPS test certificate

## [1.1.0] - 2022-12-13
### Added
- Enabled additionalDaps option to specify multiple DAPS.
- External authentication provider

### Changed
- Extracted common DAPS test configuration to a test extension
- Changed content type to application/json in federated brokers endpoint.
- Use Camel Port to get default egress url, to prevent port from being null.
- Set default scope and language for `QueryMessage`s

### Fixed
- Combining PEM & System truststores for HttpClient

## [1.0.0] - 2022-09-01
### Added
- Allow description requests for artifacts route, for more convenience for remote connectors.
- Internal message clearing with API endpoint for management purposes
- Micrometer & Prometheus metric support
- Broker Query API endpoint to be used from the UI
- Public key included in self-description
- Broker controller for managing Broker registration(s)
- Added audit logging
- Configurable Camel port used in the Jetty-based Camel providers
- Federated Broker API endpoint

### Changed
- Upgraded IDS Information Model to 4.2.7
- Camel route generation uses Java DSL instead of XML DSL
- Upgrade Gradle to 7.4.2 with JDK version 17

### Fixed
- Set correct declared versions in self-description
- Set correct Connector class in self-description
- Added clearing on return flows

### Security
- Fixed mask in message clearing to prevent sensitive data to leak
- Added NTP difference check with maximum skew of 60 seconds
- Camel route response do not copy internal Camel headers any more preventing leaking of potentially sensitive data

### Deprecated
- Broker information injection in the MultipartOutputProcessor is deprecated, the use of the API to query the broker is now recommended

### Removed
- Configuration hot reloading functionality, since it was not used and added unnecessary complexity

## [0.9.0] - 2022-06-22
### Changed
- Upgraded IDS Information Model to 4.1.1
