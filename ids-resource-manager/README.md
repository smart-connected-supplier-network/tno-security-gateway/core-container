# IDS Resource Manager

This IDS Resource Manager module provides the capability to manage the IDS metadata of resources the connector offers in the network.

The `ResourceManagerService` is used by the [`ids-selfdescription` module](../ids-selfdescription) as source of the `resourceCatalog` property in the Connectors self-description that will be published at the broker.

Data Apps are able to use the endpoints provided by the `ResourceController` to manage the resources that the Data App offers.

## Persistent storage

By default, the resource manager uses in-memory storage for the exposed resources. When using data apps this is often sufficient, but when using the [artifact-manager](../ids-artifact-manager) it is recommended to configure a MongoDB for storage of the resources.
