plugins {
    kotlin("plugin.spring")
}

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation(project(":ids-security"))

    implementation(project(":ids-configuration"))

    implementation("org.mongodb:mongodb-driver-sync:${LibraryVersions.mongodb}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("de.bwaldvogel:mongo-java-server:${LibraryVersions.Test.mongoJava}")
    testImplementation(project(":ids-test-extensions"))
}