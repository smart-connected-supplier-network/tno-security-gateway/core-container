/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.resourcemanager

import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters
import com.mongodb.client.model.ReplaceOptions
import de.fraunhofer.iais.eis.Resource
import de.fraunhofer.iais.eis.ResourceCatalog
import de.fraunhofer.iais.eis.ResourceCatalogBuilder
import java.net.URI
import nl.tno.ids.configuration.events.SelfDescriptionReloadEvent
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.ResourceDbConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.context.ApplicationEventPublisher
import org.springframework.lang.Nullable
import org.springframework.stereotype.Service

/** MongoDB-based Resource Manager Service, stores the metadata in the configured MongoDB */
@Service
@ConditionalOnBean(ResourceDbConfig::class)
class MongoResourceManagerService(
    /** Spring Event publisher, for publishing events that trigger reloading of self-descriptions */
    private val applicationEventPublisher: ApplicationEventPublisher,
    /** Connector Info for prepending resource endpoints with the access url */
    @Nullable private val connectorInfo: ConnectorInfo?
) : ResourceManagerService() {
    /** Mongo collection to be used */
    @Autowired private lateinit var resourceCatalogCollection: MongoCollection<ResourceCatalog>

    /** Add new Resources to a catalog (will be created if it does not exist) */
    override fun addResource(catalogId: String, resources: ArrayList<Resource>) {
        resources.forEach { r ->
            r.resourceEndpoint.forEach { re ->
                re.accessURL = connectorInfo?.accessUrl?.firstOrNull()?.toURI() ?: re.accessURL
            }
        }

        val resourceCatalog =
            resourceCatalogCollection.find(Filters.eq("@id", catalogId)).firstOrNull()
                ?: ResourceCatalogBuilder(URI(catalogId)).build()
        resourceCatalog.offeredResource.addAll(resources)
        resourceCatalogCollection.replaceOne(
            Filters.eq("@id", catalogId), resourceCatalog, ReplaceOptions().upsert(true))
        applicationEventPublisher.publishEvent(SelfDescriptionReloadEvent(this))
    }

    /**
     * Replace Resources from a catalog
     *
     * When upsert is true, the resource will be added if it does not exist, otherwise the call will
     * fail
     */
    override fun replaceResource(
        catalogId: String,
        resources: ArrayList<Resource>,
        upsert: Boolean
    ) {
        resources.forEach { r ->
            r.resourceEndpoint.forEach { re ->
                re.accessURL = connectorInfo?.accessUrl?.firstOrNull()?.toURI() ?: re.accessURL
            }
        }
        val resourceCatalog =
            resourceCatalogCollection.find(Filters.eq("@id", catalogId)).firstOrNull()
                ?: throw ResourceNotFoundException("Resource catalog $catalogId can't be found")
        resources.forEach { resource ->
            if (!resourceCatalog.offeredResource.removeIf { it.id == resource.id } && !upsert) {
                throw ResourceNotFoundException(
                    "Resource ${resource.id} can't be found in catalog $catalogId")
            }
        }
        resourceCatalog.offeredResource.addAll(resources)
        resourceCatalogCollection.replaceOne(Filters.eq("@id", catalogId), resourceCatalog)
        applicationEventPublisher.publishEvent(SelfDescriptionReloadEvent(this))
    }

    /** Delete Resources from a catalog */
    override fun deleteResource(catalogId: String, resourceId: ArrayList<String>): Boolean {
        val resourceCatalog =
            resourceCatalogCollection.find(Filters.eq("@id", catalogId)).firstOrNull()
                ?: throw ResourceNotFoundException("Resource catalog $catalogId can't be found")
        val result =
            resourceCatalog.offeredResource.removeIf { offeredResource ->
                resourceId.any { offeredResource.id.toString() == it }
            }
        resourceCatalogCollection.replaceOne(Filters.eq("@id", catalogId), resourceCatalog)
        if (result) {
            applicationEventPublisher.publishEvent(SelfDescriptionReloadEvent(this))
        }
        return result
    }

    /** Delete a ResourceCatalog */
    override fun deleteCatalog(catalogId: String): Boolean {
        return if (resourceCatalogCollection.deleteOne(Filters.eq("@id", catalogId)).deletedCount ==
            1L) {
            applicationEventPublisher.publishEvent(SelfDescriptionReloadEvent(this))
            true
        } else {
            false
        }
    }

    /** Retrieve Resource Catalog */
    override fun getResourceCatalog(catalogId: String): ResourceCatalog {
        return resourceCatalogCollection.find(Filters.eq("@id", catalogId)).firstOrNull()
            ?: throw ResourceNotFoundException("Resource catalog $catalogId can't be found")
    }

    /** Retrieve all stored Resources */
    override fun getResourceCatalogs(): List<ResourceCatalog> {
        return resourceCatalogCollection.find().toList()
    }
}
