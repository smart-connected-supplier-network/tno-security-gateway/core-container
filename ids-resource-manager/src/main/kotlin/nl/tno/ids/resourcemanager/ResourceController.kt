/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.resourcemanager

import de.fraunhofer.iais.eis.Resource
import de.fraunhofer.iais.eis.ResourceCatalog
import nl.tno.ids.configuration.model.SecurityRoles
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.util.UriUtils
import java.nio.charset.Charset

/** REST Resource Controller for managing resources */
@RestController
@RequestMapping("api/resources")
@Secured(SecurityRoles.RESOURCE_MANAGER)
class ResourceController(
    /** Resource Manager Service for storage of resource metadata */
    private val resourceManagerService: ResourceManagerService
) {

    /** Retrieve all Resource Catalogs */
    @GetMapping(produces = ["application/ld+json"])
    @Secured(SecurityRoles.RESOURCE_READER)
    fun getResources(): List<ResourceCatalog> {
        return try {
            resourceManagerService.getResourceCatalogs()
        } catch (e: ResourceNotFoundException) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }

    /** Retrieve specific Resource Catalog */
    @GetMapping(path = ["{catalogId}"], produces = ["application/ld+json"])
    @Secured(SecurityRoles.RESOURCE_READER)
    fun getResourceCatalog(@PathVariable catalogId: String): ResourceCatalog {
        return try {
            resourceManagerService.getResourceCatalog(
                UriUtils.decode(catalogId, Charset.defaultCharset()))
        } catch (e: ResourceNotFoundException) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }

    /** List identifiers of offered resources in a resource catalog */
    @GetMapping(path = ["{catalogId}/ids"], produces = ["application/ld+json"])
    @Secured(SecurityRoles.RESOURCE_READER)
    fun getResourceCatalogResourceIds(@PathVariable catalogId: String): List<String> {
        return try {
            resourceManagerService.getResourceCatalog(
                UriUtils.decode(catalogId, Charset.defaultCharset())).offeredResource.map {
                it.id.toString()
            }
        } catch (e: ResourceNotFoundException) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }

    /** Add a new Resource to a catalog */
    @PostMapping(
        path = ["{catalogId}"],
        consumes = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"])
    fun addResource(@RequestBody resource: Resource, @PathVariable catalogId: String) {
        resourceManagerService.addResource(
            UriUtils.decode(catalogId, Charset.defaultCharset()), resource)
    }

    /** Add a new Resource to a catalog */
    @PostMapping(
        path = ["{catalogId}/batch"],
        consumes = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"])
    fun addResources(@RequestBody resources: ResourceCatalog, @PathVariable catalogId: String, @RequestParam(required = false, defaultValue = "true") replaceAll: Boolean) {
        if (replaceAll) {
            resourceManagerService.deleteCatalog(UriUtils.decode(catalogId, Charset.defaultCharset()))
        }
        resourceManagerService.addResource(
            UriUtils.decode(catalogId, Charset.defaultCharset()),
            ArrayList(resources.offeredResource))
    }

    /** Replace a Resource in a catalog */
    @PutMapping(
        path = ["{catalogId}"],
        consumes = [MediaType.APPLICATION_JSON_VALUE, "application/ld+json"])
    fun replaceResource(@RequestBody resource: Resource, @PathVariable catalogId: String) {
        try {
            resourceManagerService.replaceResource(
                UriUtils.decode(catalogId, Charset.defaultCharset()), resource, false)
        } catch (e: ResourceNotFoundException) {
            throw ResponseStatusException(HttpStatus.NOT_FOUND)
        }
    }

    /** Delete a ResourceCatalog from a catalog */
    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @DeleteMapping(path = ["{catalogId}"])
    fun deleteResourceCatalog(@PathVariable catalogId: String) {
        resourceManagerService.deleteCatalog(UriUtils.decode(catalogId, Charset.defaultCharset()))
    }

    /** Delete a Resource from a catalog */
    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @DeleteMapping(path = ["{catalogId}/{resourceId}"])
    fun deleteResource(@PathVariable catalogId: String, @PathVariable resourceId: String) {
        resourceManagerService.deleteResource(
            UriUtils.decode(catalogId, Charset.defaultCharset()),
            UriUtils.decode(resourceId, Charset.defaultCharset()))
    }
}
