/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.resourcemanager

import com.mongodb.MongoClientSettings
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.connection.ClusterSettings
import com.mongodb.connection.SslSettings
import de.fraunhofer.iais.eis.ModelClass
import de.fraunhofer.iais.eis.ResourceCatalog
import nl.tno.ids.configuration.model.ResourceDbConfig
import nl.tno.ids.configuration.serialization.Serialization
import org.bson.BsonReader
import org.bson.BsonWriter
import org.bson.codecs.Codec
import org.bson.codecs.DecoderContext
import org.bson.codecs.EncoderContext
import org.bson.codecs.configuration.CodecProvider
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.configuration.CodecRegistry
import org.bson.json.JsonReader
import org.bson.json.JsonWriter
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.io.StringWriter

/** Spring Configuration for providing MongoDB client and collection beans */
@Configuration
@ConditionalOnBean(ResourceDbConfig::class)
class ResourceMongoConfiguration(
    private val mongoConfig: ResourceDbConfig
) {
    /** Mongo Client bean, uses mongoConfig to connect to an MongoDB instance */
    @get:Bean
    val mongoClient: MongoClient =
        MongoClients.create(
            MongoClientSettings.builder()
                .applyToClusterSettings {
                    it.applySettings(
                        ClusterSettings.builder()
                            .hosts(listOf(ServerAddress(mongoConfig.hostname, mongoConfig.port)))
                            .build())
                }
                .apply {
                    if (mongoConfig.username != null && mongoConfig.password != null) {
                        credential(
                            MongoCredential.createCredential(
                                mongoConfig.username!!,
                                mongoConfig.authenticationDatabase
                                    ?: throw Exception(
                                        "MongoDB Authentication Database must be set"),
                                mongoConfig.password!!.toCharArray()))
                    }
                }
                .applyToSslSettings {
                    it.applySettings(
                        SslSettings.builder().enabled(mongoConfig.isSslEnabled).build())
                }
                .codecRegistry(
                    CodecRegistries.fromRegistries(
                        MongoClientSettings.getDefaultCodecRegistry(),
                        CodecRegistries.fromProviders(idsCodecProvider()),
                    ))
                .build())

    /** MongoDB Codec Provider for IDS Infomodel classes */
    private final fun idsCodecProvider() = object : CodecProvider {
        override fun <T : Any?> get(
            clazz: Class<T>,
            registry: CodecRegistry?
        ): Codec<T>? {
            if (ModelClass::class.java.isAssignableFrom(clazz)) {
                return object : Codec<T> {
                    override fun encode(
                        writer: BsonWriter,
                        value: T,
                        encoderContext: EncoderContext
                    ) {
                        writer.pipe(
                            JsonReader(Serialization.toJsonLD(value))
                        )
                    }

                    override fun getEncoderClass(): Class<T> {
                        return clazz
                    }

                    override fun decode(
                        reader: BsonReader,
                        decoderContext: DecoderContext?
                    ): T {
                        val result =
                            StringWriter()
                                .apply { JsonWriter(this).pipe(reader) }
                                .toString()
                        return Serialization.fromJsonLD(result, clazz)
                    }
                }
            }
            return null
        }
    }

    /** Mongo Collection bean for storage of ResourceCatalogs metadata */
    @Bean("resourceCatalogCollection")
    fun getResourceCatalogCollection(mongoClient: MongoClient): MongoCollection<ResourceCatalog> {
        return mongoClient
            .getDatabase(mongoConfig.database)
            .getCollection(mongoConfig.collection ?: "resources", ResourceCatalog::class.java)
    }
}
