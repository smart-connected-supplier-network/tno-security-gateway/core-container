/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.resourcemanager

import de.fraunhofer.iais.eis.Resource
import de.fraunhofer.iais.eis.ResourceCatalog

/** Resource Exception encapsulating exceptions occurred during the management of resources */
class ResourceNotFoundException(s: String) : Exception(s)

/**
 * Resource Manager Service for managing dynamic resource metadata for the connector.
 *
 * Is used both by core-container internally as exposed to deployed data apps
 */
abstract class ResourceManagerService {
    /** Add a new Resource to a ResourceCatalog (will be created if it does not exist) */
    fun addResource(catalogId: String, resource: Resource) {
        addResource(catalogId, arrayListOf(resource))
    }

    /** Add new Resources to a catalog (will be created if it does not exist) */
    abstract fun addResource(catalogId: String, resources: ArrayList<Resource>)

    /**
     * Replace an Resource from a catalog
     *
     * When upsert is true, the resource will be added if it does not exist, otherwise the call will
     * fail
     */
    fun replaceResource(catalogId: String, resource: Resource, upsert: Boolean = true) {
        replaceResource(catalogId, arrayListOf(resource), upsert)
    }

    /**
     * Replace Resources from a catalog
     *
     * When upsert is true, the resource will be added if it does not exist, otherwise the call will
     * fail
     */
    abstract fun replaceResource(
        catalogId: String,
        resources: ArrayList<Resource>,
        upsert: Boolean = true
    )

    /** Delete a Resource from a catalog */
    fun deleteResource(catalogId: String, resourceId: String): Boolean {
        return deleteResource(catalogId, arrayListOf(resourceId))
    }

    /** Delete Resources from a catalog */
    abstract fun deleteResource(catalogId: String, resourceId: ArrayList<String>): Boolean

    /** Delete a ResourceCatalog */
    abstract fun deleteCatalog(catalogId: String): Boolean

    /** Retrieve Resource Catalog */
    abstract fun getResourceCatalog(catalogId: String): ResourceCatalog

    /** Retrieve all stored Resources */
    abstract fun getResourceCatalogs(): List<ResourceCatalog>
}
