/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.resourcemanager

import de.fraunhofer.iais.eis.Resource
import de.fraunhofer.iais.eis.ResourceCatalog
import de.fraunhofer.iais.eis.ResourceCatalogBuilder
import nl.tno.ids.configuration.events.SelfDescriptionReloadEvent
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.ResourceDbConfig
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean
import org.springframework.context.ApplicationEventPublisher
import org.springframework.lang.Nullable
import org.springframework.stereotype.Service
import java.net.URI
import java.util.concurrent.ConcurrentHashMap

/** In-Memory-based Resource Manager Service, stores the metadata in a Map in memory */
@Service
@ConditionalOnMissingBean(ResourceDbConfig::class)
class InMemoryResourceManagerService(
    /** Spring Event publisher, for publishing events that trigger reloading of self-descriptions */
    private val applicationEventPublisher: ApplicationEventPublisher,
    /** Connector Info for prepending resource endpoints with the access url */
    @Nullable private val connectorInfo: ConnectorInfo?
) : ResourceManagerService() {
    /** In-memory concurrent map of resource catalogs */
    private val resourceCatalogs: MutableMap<String, ResourceCatalog> = ConcurrentHashMap()

    /** Add new Resources to a catalog (will be created if it does not exist) */
    override fun addResource(catalogId: String, resources: ArrayList<Resource>) {
        resources.forEach { r ->
            r.resourceEndpoint.forEach { re ->
                re.accessURL = connectorInfo?.accessUrl?.firstOrNull()?.toURI() ?: re.accessURL
            }
        }
        if (resourceCatalogs.containsKey(catalogId)) {
            resourceCatalogs[catalogId]!!.offeredResource.addAll(resources)
        } else {
            resourceCatalogs[catalogId] =
                ResourceCatalogBuilder(URI(catalogId))._offeredResource_(resources).build()
        }
        applicationEventPublisher.publishEvent(SelfDescriptionReloadEvent(this))
    }

    /**
     * Replace Resources from a catalog
     *
     * When upsert is true, the resource will be added if it does not exist, otherwise the call will
     * fail
     */
    override fun replaceResource(
        catalogId: String,
        resources: ArrayList<Resource>,
        upsert: Boolean
    ) {
        resources.forEach { r ->
            r.resourceEndpoint.forEach { re ->
                re.accessURL = connectorInfo?.accessUrl?.firstOrNull()?.toURI() ?: re.accessURL
            }
        }
        val resourceCatalog =
            resourceCatalogs[catalogId]
                ?: run {
                    if (upsert) {
                        ResourceCatalogBuilder(URI(catalogId)).build().also {
                            resourceCatalogs[catalogId] = it
                        }
                    } else {
                        throw ResourceNotFoundException("Resource catalog $catalogId can't be found")
                    }
                }
        val missingResources =
            resources.filter { resource ->
                resourceCatalog.offeredResource.none { resource.id == it.id }
            }
        if (missingResources.isNotEmpty() && !upsert) {
            throw ResourceNotFoundException(
                "Resource(s) ${missingResources.joinToString { it.id.toString() }} can't be found in catalog $catalogId")
        }
        resourceCatalog.offeredResource.removeIf { offeredResource ->
            resources.any { offeredResource.id == it.id }
        }
        resourceCatalog.offeredResource.addAll(resources)
        applicationEventPublisher.publishEvent(SelfDescriptionReloadEvent(this))
    }

    /** Delete Resources from a catalog */
    override fun deleteResource(catalogId: String, resourceId: ArrayList<String>): Boolean {
        return resourceCatalogs[catalogId]?.offeredResource
            ?.removeIf { offeredResource -> resourceId.any { offeredResource.id.toString() == it } }
            ?.apply { applicationEventPublisher.publishEvent(SelfDescriptionReloadEvent(this)) }
            ?: throw ResourceNotFoundException("Resource catalog $catalogId can't be found")
    }

    /** Delete a ResourceCatalog */
    override fun deleteCatalog(catalogId: String): Boolean {
        return resourceCatalogs.remove(catalogId)?.apply {
            applicationEventPublisher.publishEvent(SelfDescriptionReloadEvent(this))
        } != null
    }

    /** Retrieve Resource Catalog */
    override fun getResourceCatalog(catalogId: String): ResourceCatalog {
        return resourceCatalogs[catalogId]
            ?: throw ResourceNotFoundException("Resource catalog $catalogId can't be found")
    }

    /** Retrieve all stored Resources */
    override fun getResourceCatalogs(): List<ResourceCatalog> {
        return resourceCatalogs.values.toList()
    }
}
