/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.resourcemanager

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.testextensions.DapsTestExtension
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.TestPropertySource
import java.math.BigInteger
import java.net.URI

/**
 * Resource Manager Controller test using the MongoResourceManagerService
 */
@SpringBootTest(classes = [ResourceManagerServiceTest.Application::class])
@TestPropertySource(
    properties =
        [
            "spring.config.location = classpath:properties.yaml",
            "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration"])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class ResourceManagerServiceTest {
    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Resource Manager Service */
    @Autowired private lateinit var resourceManagerService: ResourceManagerService

    /** Dummy resource 1 */
    private val resource1: DataResource =
        DataResourceBuilder(URI("urn:ids:resources:resource1"))
            ._sovereign_(URI("urn:ids:test"))
            ._representation_(
                RepresentationBuilder()
                    ._mediaType_(IANAMediaTypeBuilder()._filenameExtension_("csv").build())
                    ._instance_(
                        ArtifactBuilder()
                            ._byteSize_(BigInteger("7000"))
                            ._fileName_("lungPhotos-2020.csv")
                            ._creationDate_(DateUtil.now())
                            .build())
                    .build())
            ._title_(TypedLiteral("mnist", "en"))
            ._description_(TypedLiteral("(28,28,15000)", "SHAPE"))
            .build()
    /** Dummy resource 2 */
    private val resource2: DataResource =
        DataResourceBuilder(URI("urn:ids:resources:resource2"))
            ._sovereign_(URI("urn:ids:test"))
            ._representation_(
                RepresentationBuilder()
                    ._mediaType_(IANAMediaTypeBuilder()._filenameExtension_("csv").build())
                    ._instance_(
                        ArtifactBuilder()
                            ._byteSize_(BigInteger("7001"))
                            ._fileName_("lungPhotos-2021.csv")
                            ._creationDate_(DateUtil.now())
                            .build())
                    .build())
            ._title_(TypedLiteral("mnist", "en"))
            ._description_(TypedLiteral("(28,28,15000)", "SHAPE"))
            .build()
    /** Dummy resource 2b */
    private val resource2b: DataResource =
        DataResourceBuilder(URI("urn:ids:resources:resource2"))
            ._sovereign_(URI("urn:ids:test"))
            ._representation_(
                RepresentationBuilder()
                    ._mediaType_(IANAMediaTypeBuilder()._filenameExtension_("csv").build())
                    ._instance_(
                        ArtifactBuilder()
                            ._byteSize_(BigInteger("7001"))
                            ._fileName_("lungPhotos-2021.csv")
                            ._creationDate_(DateUtil.now())
                            .build())
                    .build())
            ._title_(TypedLiteral("mnist2", "en"))
            ._description_(TypedLiteral("(28,28,15000)", "SHAPE"))
            .build()

    /** Add resource */
    @Test
    @Order(0)
    fun testAddResource() {
        resourceManagerService.addResource("urn:ids:connector:catalog-1", resource1)
        Assertions.assertEquals(1, resourceManagerService.getResourceCatalogs().count())
        Assertions.assertEquals(
            1, resourceManagerService.getResourceCatalogs().first().offeredResource.count())
    }

    /** Replace missing resource without upsert */
    @Test
    @Order(1)
    fun testReplaceMissingResource() {
        Assertions.assertThrows(ResourceNotFoundException::class.java) {
            resourceManagerService.replaceResource("urn:ids:connector:catalog-1", resource2, false)
        }
    }

    /** Replace missing resource with upsert */
    @Test
    @Order(2)
    fun testReplaceMissingResourceUpsert() {
        resourceManagerService.replaceResource("urn:ids:connector:catalog-1", resource2)
        Assertions.assertEquals(1, resourceManagerService.getResourceCatalogs().count())
        Assertions.assertEquals(
            2, resourceManagerService.getResourceCatalogs().first().offeredResource.count())
    }

    /** Replace existing resource */
    @Test
    @Order(3)
    fun testReplaceResource() {
        resourceManagerService.replaceResource("urn:ids:connector:catalog-1", resource2b)

        Assertions.assertEquals(1, resourceManagerService.getResourceCatalogs().count())
        Assertions.assertEquals(
            2, resourceManagerService.getResourceCatalogs().first().offeredResource.count())
        Assertions.assertEquals(
            "mnist2",
            resourceManagerService
                .getResourceCatalogs()
                .first()
                .offeredResource
                .find { it.id == resource2.id }
                ?.title
                ?.first()
                ?.value)
    }

    /** Delete existing resource */
    @Test
    @Order(4)
    fun testDeleteResource() {
        val result =
            resourceManagerService.deleteResource(
                "urn:ids:connector:catalog-1", resource2b.id.toString())
        Assertions.assertTrue(result)
        Assertions.assertEquals(1, resourceManagerService.getResourceCatalogs().count())
        Assertions.assertEquals(
            1, resourceManagerService.getResourceCatalogs().first().offeredResource.count())
    }

    /** Delete missing resource */
    @Test
    @Order(5)
    fun testDeleteMissingResource() {
        val result =
            resourceManagerService.deleteResource(
                "urn:ids:connector:catalog-1", resource2b.id.toString())
        Assertions.assertFalse(result)
    }

    /** Delete Resource Catalog */
    @Test
    @Order(6)
    fun testDeleteCatalog() {
        resourceManagerService.deleteCatalog("urn:ids:connector:catalog-1")

        Assertions.assertEquals(0, resourceManagerService.getResourceCatalogs().count())
    }
}
