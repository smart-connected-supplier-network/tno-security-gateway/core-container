/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.resourcemanager

import de.fraunhofer.iais.eis.*
import de.fraunhofer.iais.eis.util.TypedLiteral
import nl.tno.ids.configuration.serialization.DateUtil
import nl.tno.ids.testextensions.DapsTestExtension
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.ComponentScan
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.test.context.TestPropertySource
import org.springframework.web.client.RestTemplate
import java.math.BigInteger
import java.net.URI

/**
 * ResourceManagerController test using the InMemoryResourceManagerService
 */
@SpringBootTest(
    classes = [ResourceManagerControllerTest.Application::class],
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@TestPropertySource(
    properties =
        [
            "spring.config.location = classpath:properties.yaml",
            "spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration"])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class ResourceManagerControllerTest {
    /** Port Spring listens on */
    @LocalServerPort
    private var port: Int = 0

    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Resource Manager Service */
    @Autowired private lateinit var resourceManagerService: ResourceManagerService

    /** Rest Template for sending HTTP requests */
    @Autowired private lateinit var restTemplate: RestTemplate

    /** Dummy resource 1 */
    private val resource1: DataResource =
        DataResourceBuilder(URI("urn:ids:resources:resource1"))
            ._sovereign_(URI("urn:ids:test"))
            ._representation_(
                RepresentationBuilder()
                    ._mediaType_(IANAMediaTypeBuilder()._filenameExtension_("csv").build())
                    ._instance_(
                        ArtifactBuilder()
                            ._byteSize_(BigInteger("7000"))
                            ._fileName_("lungPhotos-2020.csv")
                            ._creationDate_(DateUtil.now())
                            .build())
                    .build())
            ._title_(TypedLiteral("mnist", "en"))
            ._description_(TypedLiteral("(28,28,15000)", "SHAPE"))
            .build()
    /** Dummy resource 2 */
    private val resource2: DataResource =
        DataResourceBuilder(URI("urn:ids:resources:resource2"))
            ._sovereign_(URI("urn:ids:test"))
            ._representation_(
                RepresentationBuilder()
                    ._mediaType_(IANAMediaTypeBuilder()._filenameExtension_("csv").build())
                    ._instance_(
                        ArtifactBuilder()
                            ._byteSize_(BigInteger("7001"))
                            ._fileName_("lungPhotos-2021.csv")
                            ._creationDate_(DateUtil.now())
                            .build())
                    .build())
            ._title_(TypedLiteral("mnist", "en"))
            ._description_(TypedLiteral("(28,28,15000)", "SHAPE"))
            .build()

    /** Adding the dummy resources & validating the API to reflect the resources */
    @Test
    @Order(0)
    fun testAddResource() {
        resourceManagerService.addResource("urn:ids:connector:catalog-1", resource1)
        resourceManagerService.addResource("urn:ids:connector:catalog-1", resource2)

        val response = restTemplate.exchange(
            "http://localhost:$port/api/resources",
            HttpMethod.GET,
            HttpEntity("", HttpHeaders().apply {
                accept = listOf(MediaType.parseMediaType("application/ld+json"))
            }),
            object : ParameterizedTypeReference<List<ResourceCatalog>>() {}
        )
        Assertions.assertEquals(1, response.body?.size)
    }

    /** Test deletion of the resource catalog */
    @Test
    @Order(6)
    fun testDeleteCatalog() {
        resourceManagerService.deleteCatalog("urn:ids:connector:catalog-1")

        Assertions.assertEquals(0, resourceManagerService.getResourceCatalogs().count())
    }
}
