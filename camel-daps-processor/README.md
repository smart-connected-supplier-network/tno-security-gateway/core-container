# Camel DAPS Processor

This module contains Apache Camel processors to intercept ingress and egress routes.

The Camel Processors are used in all the Camel routes that are generated, primarily in the [`ids-resource-manager` module](../ids-resource-manager).

## DAPS Token Injector

The `DapsTokenInjector` class is a Camel Processor and Spring Component that can be automatically detected and used in a Spring Context.

The DAPS Token Injector will ensure that processed egress messages always contain an IDS Dynamic Attribute Token (DAT) received from the Dynamic Attribute Provisioning Service (DAPS). Acquiring tokens is handled by [`ids-token-manager` module](../ids-token-manager)

## DAPS Token Verifier

The `DapsTokenVerifier` class is a Camel Processor and Spring Component that can be automatically detected and used in a Spring Context.

The DAPS Token Verifier will ensure that processed ingress messages always contain a valid IDS Dynamic Attribute Token (DAT) from a trusted DAPS. Verification of claims made in tokens can be configured by means of the `ids.daps.verification` property:
- **IDSA** (_default_): Verification conforming to the DAPS v2 specification (see [IDS-G - Components - Identity Provider - DAPS](https://github.com/International-Data-Spaces-Association/IDS-G/tree/main/Components/IdentityProvider/DAPS)). Will verify the `ids.daps.requiredSecurityProfile` property to validate the required security profile is met.
- **ISHARE**: iSHARE enabled DAPS validation, includes `party_info` property retrieved from the iSHARE Scheme Owner. Follows the **TNO_LEGACY** validation scheme.
- **NONE**: Disable verification of claims in the token, only ensures the token is signed by a trusted DAPS

