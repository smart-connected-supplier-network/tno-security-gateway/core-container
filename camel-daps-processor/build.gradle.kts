plugins {
    kotlin("plugin.spring")
}

dependencies {
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")

    implementation(project(":ids-configuration"))

    implementation(project(":ids-token-manager"))

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.apache.camel:camel-test-spring-junit5")
    testImplementation("com.github.tomakehurst:wiremock-jre8:${LibraryVersions.Test.wiremock}")
    testImplementation("org.bitbucket.b_c:jose4j:${LibraryVersions.Jwt.jose4j}")
    testImplementation("com.google.guava:guava:${LibraryVersions.Test.guava}")
    testImplementation(project(":ids-test-extensions"))
}