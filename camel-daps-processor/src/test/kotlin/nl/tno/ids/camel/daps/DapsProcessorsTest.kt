package nl.tno.ids.camel.daps

import de.fraunhofer.iais.eis.RequestMessageBuilder
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.testextensions.DapsTestExtension
import nl.tno.ids.tokenmanager.TokenManagerService
import org.apache.camel.CamelExecutionException
import org.apache.camel.EndpointInject
import org.apache.camel.ProducerTemplate
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.test.spring.junit5.CamelSpringBootTest
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Timeout
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.test.context.TestPropertySource
import java.net.URI
import java.util.concurrent.TimeUnit

/**
 * Test class for testing the injection and verification of tokens by the DAPS processors
 */
@CamelSpringBootTest
@SpringBootTest(classes = [DapsProcessorsTest.Application::class])
@TestPropertySource(properties = ["spring.config.location = classpath:application.yaml"])
@Timeout(value = 30, unit = TimeUnit.SECONDS)
@ExtendWith(DapsTestExtension::class)
@Disabled
class DapsProcessorsTest {
    /** Application context */
    @SpringBootApplication
    @ConfigurationPropertiesScan("nl.tno.ids")
    @ComponentScan("nl.tno.ids")
    class Application

    /** Test routes */
    @TestConfiguration
    class Configuration {
        @Bean
        fun testRoute(): RouteBuilder {
            return object : RouteBuilder() {
                override fun configure() {
                    from("direct:start-inject?block=true")
                        .process("dapsTokenInjector")
                        .to("mock:result-inject")
                    from("direct:start-verify?block=true")
                        .process("dapsTokenVerifier")
                        .to("mock:result-verify")
                }
            }
        }
    }
    /** Camel ProducerTemplate for sending messages to test routes */
    @Autowired private lateinit var template: ProducerTemplate
    /** TokenManagerService for requesting tokens (mocked by WireMock) */
    @Autowired private lateinit var tokenManagerService: TokenManagerService

    /** Injection result mock endpoint */
    @EndpointInject("mock:result-inject") private lateinit var injectEndpoint: MockEndpoint
    /** Verification result mock endpoint */
    @EndpointInject("mock:result-verify") private lateinit var verifyEndpoint: MockEndpoint

    /** Test injection by validating that the DUMMY token is replaced */
    @Test
    fun testInject() {
        val header = RequestMessageBuilder()
            ._issuerConnector_(URI("urn:ids:test"))
            ._recipientConnector_(URI("urn:ids:test"))
            ._senderAgent_(URI("urn:ids:test"))
            ._recipientAgent_(URI("urn:ids:test"))
            .buildWithDefaults("DUMMY")

        injectEndpoint.message(0).apply {
            header(Constants.Headers.idsHeader).not().contains("DUMMY")
        }

        template.sendBodyAndHeader("direct:start-inject", "", Constants.Headers.idsHeader, header.toJsonLD())

        injectEndpoint.assertIsSatisfied()
    }

    /** Test verification of correct token */
    @Test
    fun testVerify() {
        val dat = tokenManagerService.acquireToken("urn:ids:test")
        val header = RequestMessageBuilder()
            ._issuerConnector_(URI("urn:ids:test"))
            ._recipientConnector_(URI("urn:ids:test"))
            ._senderAgent_(URI("urn:ids:test"))
            ._recipientAgent_(URI("urn:ids:test"))
            .buildWithDefaults(dat)

        verifyEndpoint.expectedCount = 1

        template.sendBodyAndHeader("direct:start-verify", "", Constants.Headers.idsHeader, header.toJsonLD())

        verifyEndpoint.assertIsSatisfied()
    }

    /** Test failing verification of missing token */
    @Test
    fun testVerifyMissing() {
        val header = RequestMessageBuilder()
            ._issuerConnector_(URI("urn:ids:test"))
            ._recipientConnector_(URI("urn:ids:test"))
            ._senderAgent_(URI("urn:ids:test"))
            ._recipientAgent_(URI("urn:ids:test"))
            .buildWithDefaults("DUMMY")

        verifyEndpoint.expectedCount = 0
        assertThrows<CamelExecutionException> {
            template.sendBodyAndHeader("direct:start-verify", "", Constants.Headers.idsHeader, header.toJsonLD())
        }

        verifyEndpoint.assertIsSatisfied()
    }

}