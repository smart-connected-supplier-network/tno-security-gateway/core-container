/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.camel.daps

import de.fraunhofer.iais.eis.DynamicAttributeTokenBuilder
import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.TokenFormat
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.tokenmanager.TokenManagerService
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.security.InvalidKeyException
import java.security.SignatureException

/** Camel Processor that injects DAPS Tokens into the headers of an IDS message */
@Component("dapsTokenInjector")
class DapsTokenInjector(
    /** Token Manager Service for requesting DAPS token */
    private val tokenManagerService: TokenManagerService
) : Processor {

    /**
     * Process messages that are being sent on the egress side of the connector.
     *
     * Requests a token from the TokenManagerService and injects it in the header.
     */
    override fun process(exchange: Exchange) {
        try {
            val idsHeader = exchange.getIn().getHeader(Constants.Headers.idsHeader, String::class.java)
            if (idsHeader == null) {
                LOG.info("[${exchange.fromRouteId}] No IDS-Header found!")
                return
            }
            val body = exchange.getIn().getBody(String::class.java)
            val header = Serialization.fromJsonLD<Message>(idsHeader)

            val tokenAudience = header.recipientConnector.firstOrNull()?.toString() ?: ""
            val token = tokenManagerService.acquireToken(tokenAudience)

            header.securityToken =
                DynamicAttributeTokenBuilder()
                    ._tokenFormat_(TokenFormat.JWT)
                    ._tokenValue_(token)
                    .build()

            exchange
                .getIn()
                .setHeader(Constants.Headers.idsHeader, header.toJsonLD())
            exchange.getIn().body = body
        } catch (e: SignatureException) {
            LOG.error(
                "[${exchange.fromRouteId}] Signature Exception, cannot sign messages: ${e.message}")
        } catch (e: InvalidKeyException) {
            LOG.error(
                "[${exchange.fromRouteId}] Signature Exception, cannot sign messages: ${e.message}")
        }
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(DapsTokenInjector::class.java)
    }
}
