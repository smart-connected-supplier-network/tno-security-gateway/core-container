/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.camel.daps

import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.RejectionMessageBuilder
import de.fraunhofer.iais.eis.RejectionReason
import de.fraunhofer.iais.eis.TokenFormat
import io.jsonwebtoken.Claims
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildAsResponseTo
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.CertificateConverter
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.model.DapsConfig
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.configuration.truststore.MergedX509TrustManager
import nl.tno.ids.tokenmanager.IDSCertificateIdentifier
import nl.tno.ids.tokenmanager.TokenManagerService
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.apache.camel.support.jsse.SSLContextParameters
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Component
import java.io.IOException
import java.net.URLDecoder
import java.nio.charset.StandardCharsets

/** Exception class for indicating exceptions in the verification process */
class TokenVerificationException(message: String?) : Exception(message)

/** Camel Processor that verifies incoming requests on valid IDS Dynamic Attribute Token presence */
@Component("dapsTokenVerifier")
class DapsTokenVerifier(
    /** DAPS Configuration */
    private val dapsConfig: DapsConfig,
    /** Token Manager Service for validating DAPS tokens */
    private val tokenManagerService: TokenManagerService,
    /** Connector Info for constructing RejectionMessages in case of errors */
    private val connectorInfo: ConnectorInfo,
    /** Certificate Identifier utility */
    private val idsCertificateIdentifier: IDSCertificateIdentifier
) : Processor {
    @Qualifier("idsSslContextParameters") @Autowired(required = false, ) private var idsSslContextParameters: SSLContextParameters? = null

    /**
     * Parse incoming messages and check whether required parts are present and verify the token
     * claims
     */
    override fun process(exchange: Exchange) {
        val idsHeader =
            exchange.message.getHeader(Constants.Headers.idsHeader, String::class.java)
                ?: return stopRoute(
                    exchange, null, RejectionReason.MALFORMED_MESSAGE, 400, "No ${Constants.Headers.idsHeader} found!")
        val header: Message =
            try {
                Serialization.fromJsonLD(idsHeader)
            } catch (e: IOException) {
                LOG.warn("[${exchange.fromRouteId}] Cannot parse header: {}", idsHeader, e)
                return stopRoute(
                    exchange,
                    null,
                    RejectionReason.MALFORMED_MESSAGE,
                    400,
                    "Error in parsing IDS header")
            }
        if (header.securityToken?.tokenFormat == TokenFormat.JWT) {
            val claims = verifyTokenClaims(header)
            val clientCertificatePem = exchange.message.getHeader("X-Ssl-Cert", String::class.java)?.let {
                URLDecoder.decode(it, StandardCharsets.UTF_8)
            }
            if (clientCertificatePem !== null) {
                val clientCertificate = CertificateConverter().convert(clientCertificatePem)
                val clientId = idsCertificateIdentifier.retrieve(clientCertificate)
                if (claims.subject != clientId) {
                    LOG.info("Client certificate and DAT subject mismatch: $clientId vs ${claims.subject}")
                    return stopRoute(
                        exchange,
                        header,
                        RejectionReason.NOT_AUTHENTICATED,
                        401,
                        "Client certificate does not match DAT subject")
                }
                LOG.info("Client certificate and DAT subject match")
                if (!System.getenv().containsKey("ENABLE_OCSP") || System.getenv()["ENABLE_OCSP"].toBoolean()) {
                    idsSslContextParameters?.let {
                        LOG.info("Checking OCSP status of client certificate")
                        when (val tm = it.trustManagers.trustManager) {
                            is MergedX509TrustManager -> {
                                try {
                                    tm.checkOCSP(arrayOf(clientCertificate))
                                } catch (e: Exception) {
                                    return stopRoute(
                                        exchange,
                                        header,
                                        RejectionReason.NOT_AUTHENTICATED,
                                        401,
                                        "Client certificate OCSP error")
                                }
                            }
                        }
                    }
                }
                exchange.message.removeHeader("X-Ssl-Cert")
            }
        } else {
            return stopRoute(
                exchange,
                header,
                RejectionReason.NOT_AUTHENTICATED,
                401,
                "No JWT token found in IDS header")
        }
        exchange.message.setHeader(Constants.Headers.idsId, header.issuerConnector.toString())
    }

    /**
     * Stop the current Camel exchange, send the exchange message directly back to the Camel
     * Consumer without further processing
     */
    private fun stopRoute(
        exchange: Exchange,
        message: Message?,
        rejectionReason: RejectionReason,
        statusCode: Int,
        payload: String? = null
    ) {
        val multiPartMessage =
            RejectionMessageBuilder()
                ._rejectionReason_(rejectionReason)
                .buildAsResponseTo(message, connectorInfo, tokenManagerService.acquireToken(""))
                .toMultiPartMessage(payload = payload)
        exchange.message.removeHeaders("*")
        exchange.message.body = multiPartMessage.toString()
        exchange.message.setHeader("Content-Type", multiPartMessage.httpHeaders["Content-Type"])
        exchange.message.setHeader(Exchange.HTTP_RESPONSE_CODE, statusCode)
        exchange.isRouteStop = true
    }

    /**
     * Parse the Claims of the token and depending on the type of verification process the claims
     * accordingly
     */
    private fun verifyTokenClaims(header: Message): Claims {
        val claims = tokenManagerService.verifyToken(header.securityToken.tokenValue)
        idsaClaimVerification(claims)
        return claims
    }

    /** IDSA standard based verification of the claims */
    private fun idsaClaimVerification(claims: Claims) {
        claims.asSequence().firstOrNull { it.key.contains("securityProfile") }?.let {
            when (it.value.toString().replace("https://w3id.org/idsa/code/", "idsc:")) {
                baseConnector, base -> {
                    if (dapsConfig.requiredSecurityProfile in
                        listOf(DapsConfig.Profile.TRUST, DapsConfig.Profile.TRUST_PLUS)) {
                        throw TokenVerificationException(
                            "Found BASE security profile, required: ${dapsConfig.requiredSecurityProfile}")
                    }
                }
                trustConnector, trust -> {
                    if (dapsConfig.requiredSecurityProfile in
                        listOf(DapsConfig.Profile.TRUST_PLUS)) {
                        throw TokenVerificationException(
                            "Found TRUST security profile, required: ${dapsConfig.requiredSecurityProfile}")
                    }
                }
                trustPlusConnector, trustPlus -> {}
                else ->
                    throw TokenVerificationException("Unrecognized securityProfile: ${it.value}")
            }
        }
            ?: throw TokenVerificationException("No securityProfile claim present in DAT claims")
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(DapsTokenVerifier::class.java)
        const val baseConnector = "idsc:BASE_CONNECTOR_SECURITY_PROFILE"
        const val base = "idsc:BASE_SECURITY_PROFILE"
        const val trustConnector = "idsc:TRUSTED_CONNECTOR_SECURITY_PROFILE"
        const val trust = "idsc:TRUST_SECURITY_PROFILE"
        const val trustPlusConnector = "idsc:TRUSTED_CONNECTOR_PLUS_SECURITY_PROFILE"
        const val trustPlus = "idsc:TRUST_PLUS_SECURITY_PROFILE"
    }
}
