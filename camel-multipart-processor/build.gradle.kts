dependencies {
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")

    implementation("org.apache.camel:camel-attachments")

    implementation(project(":ids-configuration"))
    implementation(project(":ids-token-manager"))

    // HTTP Mime
    implementation("org.apache.httpcomponents:httpmime:${LibraryVersions.httpclient}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.apache.camel:camel-test-spring-junit5")
    testImplementation("org.apache.camel.springboot:camel-jetty-starter")
}