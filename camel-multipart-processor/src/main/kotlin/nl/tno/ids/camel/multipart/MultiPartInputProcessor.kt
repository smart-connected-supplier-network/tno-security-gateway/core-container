/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.camel.multipart

import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.RejectionMessageBuilder
import de.fraunhofer.iais.eis.RejectionReason
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildAsResponseTo
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.ConnectorInfo
import nl.tno.ids.configuration.multipart.MultiPartMessage
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.tokenmanager.TokenManagerService
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.apache.camel.attachment.AttachmentMessage
import org.slf4j.LoggerFactory
import org.springframework.lang.Nullable
import org.springframework.stereotype.Component
import java.io.InputStream

/**
 * HTTP Mime Multipart input processor.
 *
 * Converts multipart into Apache Camel construct with headers.
 */
@Component("multiPartInputProcessor")
class MultiPartInputProcessor(
    /** TokenManagerService for requesting DAPS tokens for RejectionMessages */
    @Nullable private val tokenManagerService: TokenManagerService?,
    /** Connector Info for constructing RejectionMessages */
    @Nullable private val connectorInfo: ConnectorInfo?
) : Processor {

    /**
     * Process incoming message, only if the message is not already processed (i.e. containing
     * IDS-Header header)
     */
    override fun process(exchange: Exchange) {
        if (!exchange.message.headers.containsKey(Constants.Headers.idsHeader)) {
            val message = exchange.getMessage(AttachmentMessage::class.java)
            if (exchange.message.headers.containsKey("header") || message.attachmentNames?.contains("header") == true) {
                parseMultipartFormData(exchange)
            } else {
                parseMultipartMixed(exchange)
            }
        }
    }

    /**
     * Parse multipart/form-data message Camel automatically parses the different parts of the
     * message to Camel headers, so the IDS header can be parsed separately and the payload part is
     * put to the Camel body.
     *
     * Uses the Camel Attachments module for extracting the parts.
     */
    private fun parseMultipartFormData(exchange: Exchange) {
        val message = exchange.getMessage(AttachmentMessage::class.java)
        val header = if (message.attachmentNames?.contains(Constants.Headers.header) == true) {
            message.getAttachment(Constants.Headers.header).dataSource.inputStream.readAllBytes().decodeToString()
        } else {
            message.getHeader(Constants.Headers.header, String::class.java)
        }

        try {
            Serialization.fromJsonLD<Message>(header)
        } catch (e: Exception) {
            LOG.info("[${exchange.fromRouteId}] Could not serialize header: ${e.message}\n$header")
            return stopRoute(
                exchange,
                "Could not serialize incoming IDS header. ${e.message ?: ""}"
            )
        }
        message.removeHeaders("Content-Type")
        message.setHeader(Constants.Headers.idsHeader, header)
        if (message.attachmentNames?.contains(Constants.Headers.payload) == true) {
            LOG.debug("Read payload from attachment")
            val payload = message.getAttachment(Constants.Headers.payload)
            message.setHeader("Content-Type", payload.contentType)
            message.body = payload.inputStream
        } else if (message.headers.containsKey(Constants.Headers.payload)) {
            LOG.debug("Read payload from header (${message.getHeader(Constants.Headers.payload)::class.java.name})")
            message.body = message.getHeader(Constants.Headers.payload, InputStream::class.java)
        }
        message.removeHeaders(Constants.Headers.header)
        message.removeHeaders(Constants.Headers.payload)
    }

    /**
     * Parse multipart/mixed message The whole multipart message is contained in the Camel body,
     * without prior parsing, so this function performs the parsing manually and splits the IDS
     * header from the payload
     */
    private fun parseMultipartMixed(exchange: Exchange) {
        val contentType = exchange.message.getHeader("Content-Type")?.toString()
        LOG.debug("Read payload manually")
        val multiPartMessage =
            try {
                val body = exchange.message.getBody(InputStream::class.java)
                MultiPartMessage.parse(body, contentType)
            } catch (e: Exception) {
                LOG.info(
                    "[${exchange.fromRouteId}] Could not serialize multipart message:\n${e.message}")
                return stopRoute(
                    exchange,
                    "Could not serialize incoming multipart message. ${e.message ?: ""}"
                )
            }

        LOG.debug("Successfully read payload manually")
        // Parse Multipart message
        exchange.message.setHeader(Constants.Headers.idsHeader, multiPartMessage.header.toJsonLD())
        exchange.message.removeHeader("Content-Type")
        multiPartMessage.payload?.getContentType()?.let { payloadContentType ->
            exchange.message.setHeader("Content-Type", payloadContentType)
        }

        // Populate body with extracted payload
        exchange.message.body = multiPartMessage.payload?.inputStream()
    }

    /**
     * Stop the current Camel exchange, send the exchange message directly back to the Camel
     * Consumer without further processing
     */
    private fun stopRoute(
        exchange: Exchange,
        payload: String? = null
    ) {
        val multiPartMessage =
            RejectionMessageBuilder()
                ._rejectionReason_(RejectionReason.MALFORMED_MESSAGE)
                .buildAsResponseTo(null, connectorInfo, tokenManagerService?.acquireToken(""))
                .toMultiPartMessage(payload = payload)
        exchange.message.removeHeaders("*")
        exchange.message.body = multiPartMessage.toString()
        exchange.message.setHeader("Content-Type", multiPartMessage.httpHeaders["Content-Type"])
        exchange.message.setHeader(Exchange.HTTP_RESPONSE_CODE, 400)
        exchange.isRouteStop = true
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(MultiPartInputProcessor::class.java)
    }
}
