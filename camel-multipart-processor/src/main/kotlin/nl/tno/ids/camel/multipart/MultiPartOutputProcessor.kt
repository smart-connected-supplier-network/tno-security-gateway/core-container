/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.camel.multipart

import de.fraunhofer.iais.eis.Message
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.model.BrokerConfig
import nl.tno.ids.configuration.model.HttpMode
import nl.tno.ids.configuration.multipart.MultiPartParseException
import nl.tno.ids.configuration.serialization.Serialization
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.apache.http.entity.ContentType
import org.apache.http.entity.mime.HttpMultipartMode
import org.apache.http.entity.mime.MultipartEntityBuilder
import org.apache.http.entity.mime.content.InputStreamBody
import org.apache.http.entity.mime.content.StringBody
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.nio.file.Files
import java.util.*

/**
 * HTTP Mime Multipart output processor
 *
 * Converts Camel based headers to a multipart format
 *
 * @param httpMode mode of multipart used (mixed or form-data)
 * @property brokerConfig IDS broker configuration used to send messages to the broker
 */
@Component("multiPartOutputProcessor")
class MultiPartOutputProcessor(
    /** Http Multipart mode used */
    httpMode: HttpMode,
    /** Broker Configuration for dynamically adding the Broker details to messages */
    private val brokerConfig: BrokerConfig
) : Processor {
    /** HTTP Content-Type header prefix */
    private var contentType: String? =
        when (httpMode.httpMode) {
            HttpMode.Mode.MULTIPART_MIXED -> "multipart/mixed; boundary="
            HttpMode.Mode.MULTIPART_FORM_DATA -> "multipart/form-data; boundary="
            else -> "multipart/form-data; boundary="
        }

    /** Convert Camel header structure to HTTP MIME multipart */
    override fun process(exchange: Exchange) {
        val boundary: String = UUID.randomUUID().toString()
        if (exchange.message.headers.containsKey(Constants.Headers.idsHeader)) {
            val multipartEntityBuilder: MultipartEntityBuilder = MultipartEntityBuilder.create()
            multipartEntityBuilder.setMode(HttpMultipartMode.STRICT)
            multipartEntityBuilder.setBoundary(boundary)
            var idsHeader = exchange.message.getHeader(Constants.Headers.idsHeader, String::class.java)
            val forwardTo = exchange.message.getHeader(Constants.Headers.forwardTo, String::class.java)
            if (forwardTo == null || forwardTo.isEmpty()) {
                idsHeader = brokerAccessUrlProcessing(exchange, idsHeader)
            }

            idsHeader?.let {
                multipartEntityBuilder.addPart(
                    MultiPartConstants.MULTIPART_HEADER,
                    StringBody(idsHeader, ContentType.create("application/ld+json")))
            }
                ?: throw MultiPartParseException("Outgoing IDS header is null")

            // Get the Exchange body and turn it into the second part named "payload"
            var payload = exchange.message.getBody(InputStream::class.java)
            if (payload != null) {
                val contentType: ContentType =
                    if (exchange.message.getHeader("Content-Type") != null) {
                        ContentType.create(
                            exchange
                                .message
                                .getHeader("Content-Type")
                                .toString()
                                .split(";".toRegex())
                                .toTypedArray()[0])
                    } else {
                        ContentType.create("text/plain")
                    }
                multipartEntityBuilder.addPart(
                    MultiPartConstants.MULTIPART_PAYLOAD,
                    InputStreamBody(payload, contentType, null))
            } else {
                payload = ByteArrayInputStream(byteArrayOf())
            }

            processMultipartOutput(exchange, idsHeader, payload, boundary, multipartEntityBuilder)
        }
    }

    /** Process Camel exchange for compliance to HTTP MIME multipart */
    private fun processMultipartOutput(
        exchange: Exchange,
        idsHeader: String,
        payload: InputStream,
        boundary: String,
        multipartEntityBuilder: MultipartEntityBuilder
    ) {
        LOG.info(
            "[${exchange.fromRouteId}] Creating MIME Multipart for header of {} bytes and payload of {} bytes.",
            idsHeader.length,
            payload.available())
        LOG.debug("[${exchange.fromRouteId}] Header: {}", idsHeader)
        exchange.message.removeHeader(Constants.Headers.idsHeader)
        exchange.message.removeHeader(Constants.Headers.idsId)
        exchange.message.removeHeader(Constants.Headers.authorization)
        exchange.message.removeHeaders("Camel*")
        exchange.message.removeHeader("Content-Type")
        exchange.message.setHeader("Content-Type", contentType + boundary)
        LOG.debug("[${exchange.fromRouteId}] Exchange headers: {}", exchange.message.headers)

        if (payload.available() < 10 * 1024 * 1024) {
            val baos = ByteArrayOutputStream()
            multipartEntityBuilder.build().writeTo(baos)
            exchange.message.body = baos
        } else {
            val file = Files.createTempFile("multipart", ".output").toFile()
            multipartEntityBuilder.build().writeTo(file.outputStream())
            exchange.message.body = file.inputStream()
        }
    }

    @Deprecated("Use API (api/description/query) instead for sending messages to a broker from a data app")
    /** Process IDS header for injection of Broker Access URL */
    private fun brokerAccessUrlProcessing(exchange: Exchange, idsHeader: String?): String {
        LOG.debug("[${exchange.fromRouteId}] Adding broker address as forward")
        exchange.message.setHeader(Constants.Headers.forwardTo, brokerConfig.address.toString())
        val header: Message =
            try {
                Serialization.fromJsonLD(idsHeader)
            } catch (e: Exception) {
                LOG.error("[${exchange.fromRouteId}] Can't parse IDS header: {}", e.message)
                throw MultiPartParseException("Could not serialize outgoing IDS header")
            }
        header.recipientConnector = arrayListOf(brokerConfig.id)
        return header.toJsonLD()
    }

    companion object {
        private val LOG = LoggerFactory.getLogger(MultiPartOutputProcessor::class.java)
    }

    internal object MultiPartConstants {
        const val MULTIPART_HEADER = "header"
        const val MULTIPART_PAYLOAD = "payload"
    }
}
