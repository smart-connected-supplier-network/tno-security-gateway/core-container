/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.camel.multipart

import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.RequestMessageBuilder
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toMultiPartMessage
import nl.tno.ids.configuration.model.BrokerConfig
import nl.tno.ids.configuration.model.HttpMode
import org.apache.camel.EndpointInject
import org.apache.camel.ProducerTemplate
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.test.spring.junit5.CamelSpringBootTest
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import java.io.InputStream
import java.net.URI
import java.net.URL

/**
 * Test class for validating parsing incoming multipart messages
 */
@CamelSpringBootTest
@SpringBootApplication
open class MultiPartInputTest {
    @TestConfiguration
    open class Configuration {
        /** Set HTTP mode */
        @get:Bean open val httpMode = HttpMode().apply { httpMode = HttpMode.Mode.MULTIPART_MIXED }

        /** Set dummy Broker configuration */
        @get:Bean open val brokerConfig =
            BrokerConfig().apply {
                id = URI("urn:ids:broker")
                address = URL("https://broker.ids")
                autoRegister = false
            }

        /** Test routes with the input processor */
        @Bean
        open fun testRoute(): RouteBuilder {
            return object : RouteBuilder() {
                override fun configure() {
                    from("direct:start?block=true")
                        .streamCaching()
                        .process(
                            MultiPartInputProcessor( null, null))
                        .to("mock:result")

                    from("jetty:http://0.0.0.0:47654/multipart?enableMultipartFilter=false")
                        .process(
                            MultiPartInputProcessor(null, null))
                        .to("mock:resultJetty")

                    from("jetty:http://0.0.0.0:47654/multipart-camel")
                        .streamCaching()
                        .process(
                            MultiPartInputProcessor(null, null))
                        .to("mock:resultJetty")
                }
            }
        }
    }

    /** Camel ProducerTemplate for sending messages to test routes */
    @Autowired private lateinit var template: ProducerTemplate

    /** Test result mock endpoint */
    @EndpointInject("mock:result") private lateinit var mockEndpoint: MockEndpoint
    /** Jetty-based results mock endpoint */
    @EndpointInject("mock:resultJetty") private lateinit var jettyMockEndpoint: MockEndpoint

    /** Test direct route without processing of Jetty */
    @Test
    fun testInputProcessor() {
        mockEndpoint.reset()
        val header: Message =
            RequestMessageBuilder()
                ._issuerConnector_(URI("https://ids.tno.nl"))
                ._recipientConnector_(arrayListOf(URI("https://ids.tno.nl")))
                ._senderAgent_(URI("https://ids.tno.nl"))
                ._recipientAgent_(arrayListOf(URI("https://ids.tno.nl")))
                .buildWithDefaults()
        val payload = "TEST-MESSAGE"
        val multiPartMessage = header.toMultiPartMessage(payload)
        mockEndpoint.message(0).apply { header(Constants.Headers.idsHeader).contains(header.id.toString()) }

        val inputStream = multiPartMessage.toString().byteInputStream()
        template.sendBody("direct:start", inputStream)
        mockEndpoint.assertIsSatisfied()

        // Can't be done via MockEndpoint clause due to missing InputStream conversion
        Assertions.assertTrue(
            mockEndpoint.exchanges.first().message.getBody(String::class.java).contains(payload))
    }

    /** Test messages via HTTP with both manual processing as processing by Jetty */
    @Test
    fun testJettyInputProcessor() {
        jettyMockEndpoint.reset()
        val header: Message =
            RequestMessageBuilder()
                ._issuerConnector_(URI("https://ids.tno.nl"))
                ._recipientConnector_(arrayListOf(URI("https://ids.tno.nl")))
                ._senderAgent_(URI("https://ids.tno.nl"))
                ._recipientAgent_(arrayListOf(URI("https://ids.tno.nl")))
                .buildWithDefaults()

        val payload = "TEST-MESSAGE"
        val multiPartMessage = header.toMultiPartMessage(payload, "text/plain")

        jettyMockEndpoint.allMessages().apply {
            header(Constants.Headers.idsHeader).contains(header.id.toString())
            header("Content-Type").isEqualTo("text/plain")
        }
        jettyMockEndpoint.expectedCount = 2

        listOf("http://localhost:47654/multipart", "http://localhost:47654/multipart-camel")
            .forEach { url ->
                val status =
                    HttpClients.createDefault()
                        .execute(
                            HttpPost(url).apply {
                                entity = StringEntity(multiPartMessage.toString())
                                addHeader(
                                    "Content-Type", multiPartMessage.httpHeaders["Content-Type"])
                            })
                        .use { it.statusLine.statusCode }
                Assertions.assertTrue(status in 200..299, "200 <= $status <= 299")
            }

        jettyMockEndpoint.assertIsSatisfied()
        Assertions.assertTrue(
            jettyMockEndpoint.exchanges.all {
                it.message
                    .getBody(InputStream::class.java)
                    .apply { reset() }
                    .readAllBytes()
                    .decodeToString()
                    .contains(payload)
            })
    }
}
