/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.camel.multipart

import de.fraunhofer.iais.eis.Message
import de.fraunhofer.iais.eis.RequestMessageBuilder
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.configuration.model.BrokerConfig
import nl.tno.ids.configuration.model.HttpMode
import org.apache.camel.EndpointInject
import org.apache.camel.ProducerTemplate
import org.apache.camel.builder.RouteBuilder
import org.apache.camel.component.mock.MockEndpoint
import org.apache.camel.test.spring.junit5.CamelSpringBootTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import java.net.URI
import java.net.URL

/**
 * Test class for validating processing outgoing multipart messages
 */
@CamelSpringBootTest
@SpringBootApplication
open class MultiPartOutputTest {
    @TestConfiguration
    open class Configuration {

        /** Set HTTP mode */
        @get:Bean open val httpMode: HttpMode = HttpMode().apply { httpMode = HttpMode.Mode.MULTIPART_MIXED }

        /** Set dummy Broker configuration */
        @get:Bean open val brokerConfig: BrokerConfig =
            BrokerConfig().apply {
                id = URI("urn:ids:broker")
                address = URL("https://broker.ids")
                autoRegister = false
            }

        /** Test routes with the output processor */
        @Bean
        open fun testRoute(): RouteBuilder {
            return object : RouteBuilder() {
                override fun configure() {
                    from("direct:start?block=true")
                        .streamCaching()
                        .process(
                            MultiPartOutputProcessor(
                                HttpMode().apply { httpMode = HttpMode.Mode.MULTIPART_MIXED },
                                brokerConfig))
                        .to("mock:mixed")
                    from("direct:start2?block=true")
                        .streamCaching()
                        .process(
                            MultiPartOutputProcessor(
                                HttpMode().apply { httpMode = HttpMode.Mode.MULTIPART_FORM_DATA },
                                brokerConfig))
                        .to("mock:formdata")
                }
            }
        }
    }

    /** Camel ProducerTemplate for sending messages to test routes */
    @Autowired private lateinit var template: ProducerTemplate
    /** Multipart mixed result mock endpoint */
    @EndpointInject("mock:mixed") private lateinit var mixedMockEndpoint: MockEndpoint
    /** Multipart form-data result mock endpoint */
    @EndpointInject("mock:formdata") private lateinit var formDataMockEndpoint: MockEndpoint

    /** IDS header for test messages */
    private val header: Message =
        RequestMessageBuilder()
            ._issuerConnector_(URI("https://ids.tno.nl"))
            ._recipientConnector_(arrayListOf(URI("https://ids.tno.nl")))
            ._senderAgent_(URI("https://ids.tno.nl"))
            ._recipientAgent_(arrayListOf(URI("https://ids.tno.nl")))
            .buildWithDefaults()
    /** Payload for test messages */
    private val payload = "TEST-MESSAGE"

    /** Test and validate multipart mixed processing */
    @Test
    fun testMultipartMixed() {
        mixedMockEndpoint.message(0).apply {
            header("Content-Type").contains("multipart/mixed")
            header(Constants.Headers.forwardTo).contains("https://broker.ids")
            body().convertToString().apply {
                contains(header.id.toString())
                contains("urn:ids:broker")
                contains(payload)
            }
        }
        template.sendBodyAndHeader(
            "direct:start",
            payload,
            Constants.Headers.idsHeader,
            header.toJsonLD())
        mixedMockEndpoint.assertIsSatisfied()
    }

    /** Test and validate multipart form data processing */
    @Test
    fun testMultipartFormData() {
        formDataMockEndpoint.message(0).apply {
            header("Content-Type").contains("multipart/form-data")
            header(Constants.Headers.forwardTo).contains("https://broker.ids")
            body().convertToString().apply {
                contains(header.id.toString())
                contains("urn:ids:broker")
                contains(payload)
            }
        }
        template.sendBodyAndHeader(
            "direct:start2",
            payload,
            Constants.Headers.idsHeader,
            header.toJsonLD())
        formDataMockEndpoint.assertIsSatisfied()
    }
}
