# Camel Multipart Processor

This module contains Apache Camel processors to process ingress and egress HTTP Mime Multipart messages.

The Camel Processors are used in all the Camel routes that are generated, primarily in the [`ids-resource-manager` module](../ids-resource-manager).

## Multipart Input Processor

The `MultiPartInputProcessor` class is a Camel Processor and Spring Component that can be automatically detected and used in a Spring Context. 

The Multipart Input Processor will transform an incoming HTTP Mime Multipart request towards a header based approach used in Apache Camel. The multipart formatted request will contain the following parts: 
- `header`: Required `de.fraunhofer.iais.eis.Message` (in JSON-LD representation) metadata of the request following the IDS Information Model.
- `payload` (_optional_): Arbitrary payload of the request.

The `header` part will be stored in the `IDS-Header` header in the Camel Exchange, the `payload` part will be stored in the body of the Camel Exchange.

## Multipart Output Processor

The `MultiPartOutputProcessor` class is a Camel Processor and Spring Component that can be automatically detected and used in a Spring Context.

The Multipart Output Processor will inverse the Multipart Input Processor and will construct a HTTP Mime Multipart message with `header` and `payload` parts. For this it will use the `IDS-Header` header and the exchange payload. There are no verifications enabled for the output processor, so the header and payload will be transparently passed through and only the formatting will be changed.
