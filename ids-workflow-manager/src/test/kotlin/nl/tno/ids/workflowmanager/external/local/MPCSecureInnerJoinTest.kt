/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.external.local

import nl.tno.ids.routemanager.CamelRouteManager
import nl.tno.ids.testextensions.DapsTestExtension
import nl.tno.ids.workflowmanager.NetworkProcessor.Companion.FROM_KEY
import nl.tno.ids.workflowmanager.TestApplication
import nl.tno.ids.workflowmanager.TestIO
import nl.tno.ids.workflowmanager.WorkflowManager
import org.apache.camel.builder.RouteBuilder
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.http.MediaType
import org.springframework.http.RequestEntity
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.test.context.TestPropertySource
import kotlin.math.roundToLong
import kotlin.random.Random

@SpringBootTest(
    classes = [TestApplication::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
    properties =
        [
            "spring.config.location = classpath:application.yaml",
            "workflow.incrementalIds = true",
            "workflow.type = IDS",
            "workflow.internalHostname = host.docker.internal",
            "workflow.useOrchestration = false"])
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class MPCSecureInnerJoinTest {
    @LocalServerPort private var port: Int = 0

    @Autowired private lateinit var camelRouteManager: CamelRouteManager

    @Autowired private lateinit var workflowManager: WorkflowManager

    @Autowired private lateinit var restTemplate: TestRestTemplate

    private val executor =
        ThreadPoolTaskExecutor().apply {
            corePoolSize = 50
            maxPoolSize = 100
            initialize()
        }

    private fun executeAsync(url: String, delay: Long? = null) {
        executor.execute {
            delay?.let { Thread.sleep(delay) }
            HttpClients.createDefault().execute(
                    HttpPost(url).apply {
                        setHeader("Content-Type", "application/json")
                        entity = StringEntity("{}")
                    }) {}
        }
    }

    @Test
    @Order(0)
    fun testCamelInitialisation() {
        listOf("alice.yaml", "bob.yaml", "dave.yaml").forEach { file ->
            val response =
                restTemplate.exchange(
                        RequestEntity.post("http://localhost:$port/api/workflow")
                            .contentType(MediaType.parseMediaType("text/yaml"))
                            .body(
                                MPCSecureInnerJoinTest::class.java.getResource(
                                        "/workflows-mpc/$file")!!
                                    .readBytes()),
                        String::class.java)
                    .body
            println(response)
        }

        val workflows = workflowManager.workflows.values

        camelRouteManager.addRoute(
            object : RouteBuilder() {
                override fun configure() {
                    // Data App Alice
                    from("jetty:http://0.0.0.0:40000/alice/initialize/start")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .process {
                            executeAsync(
                                "http://localhost:8080/router/workflow/0/initialize/output/0")
                            executeAsync(
                                "http://localhost:8080/router/workflow/0/initialize/output/0")
                            executeAsync(
                                "http://localhost:8080/router/workflow/0/initialize/output/0")
                            executeAsync(
                                "http://localhost:8080/router/workflow/0/learning/output/0", 5000)
                        }
                        .setBody(constant("OK"))
                    from("jetty:http://0.0.0.0:40000/alice/initialize/mpc-constants")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .setBody(constant("OK"))
                    from("jetty:http://0.0.0.0:40000/alice/learning/input")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .process {
                            executeAsync(
                                "http://localhost:8080/router/workflow/0/distributeShare/output/0")
                        }
                        .setBody(constant("OK"))
                    from("jetty:http://0.0.0.0:40000/alice/distributeShare/input")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .setBody(constant("OK"))

                    // Data App Bob
                    from("jetty:http://0.0.0.0:40000/bob/initialize/start")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .process {
                            executeAsync(
                                "http://localhost:8080/router/workflow/1/initialize/output/0")
                            executeAsync(
                                "http://localhost:8080/router/workflow/1/initialize/output/0")
                            executeAsync(
                                "http://localhost:8080/router/workflow/1/initialize/output/0")
                            executeAsync(
                                "http://localhost:8080/router/workflow/1/learning/output/0", 5000)
                        }
                        .setBody(constant("OK"))
                    from("jetty:http://0.0.0.0:40000/bob/initialize/mpc-constants")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .setBody(constant("OK"))
                    from("jetty:http://0.0.0.0:40000/bob/learning/input")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .process {
                            executeAsync(
                                "http://localhost:8080/router/workflow/1/distributeShare/output/0")
                        }
                        .setBody(constant("OK"))
                    from("jetty:http://0.0.0.0:40000/bob/distributeShare/input")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .setBody(constant("OK"))

                    // Data App Dave
                    from("jetty:http://0.0.0.0:40000/dave/intersect/input")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .process { exchange ->
                            when (exchange.message.getHeader(FROM_KEY)) {
                                "urn:ids:alice" ->
                                    executeAsync(
                                        "http://localhost:8080/router/workflow/2/intersect/output/1")
                                "urn:ids:bob" ->
                                    executeAsync(
                                        "http://localhost:8080/router/workflow/2/intersect/output/0")
                            }
                        }
                        .setBody(constant("OK"))
                    // Data App Dave
                    from("jetty:http://0.0.0.0:40000/dave/distributeShares/input")
                        .delay((Random.nextDouble() * 100).roundToLong())
                        .process { exchange ->
                            when (exchange.message.getHeader(FROM_KEY)) {
                                "urn:ids:alice" ->
                                    executeAsync(
                                        "http://localhost:8080/router/workflow/2/distributeShares/output/1")
                                "urn:ids:bob" ->
                                    executeAsync(
                                        "http://localhost:8080/router/workflow/2/distributeShares/output/0")
                            }
                        }
                        .setBody(constant("OK"))
                }
            })

        camelRouteManager
            .getRoutes()
            .mapNotNull { camelRouteManager.getRoute(it.id)?.content }
            .forEach { println(it) }

        executeTestCalls(
            listOf(
                TestIO(
                    "http://localhost:8080/router/workflow/0/initialize/input/0",
                    200,
                    "input",
                    "self"),
                TestIO(
                    "http://localhost:8080/router/workflow/1/initialize/input/0",
                    200,
                    "input",
                    "self")))

        for (i in 1..1000) {
            Thread.sleep(100)
            if (workflows.all { it.finished() }) {
                break
            }
        }
        Assertions.assertTrue(workflows.all { it.finished() })
    }

    @Test
    @Order(1)
    fun cleanUp() {
        camelRouteManager.getRoutes().forEach { camelRouteManager.removeRoute(it.id) }
    }

    private fun executeTestCalls(calls: List<TestIO>) {
        val executor = ThreadPoolTaskExecutor()
        executor.corePoolSize = 5
        executor.maxPoolSize = 10
        executor.initialize()
        calls.forEach { call ->
            executor.execute {
                HttpClients.createDefault()
                    .execute(
                        HttpPost(call.url).apply {
                            addHeader(FROM_KEY, call.from)
                            addHeader("Content-Type", "application/json")
                        })
                    .use {
                        Assertions.assertEquals(
                            call.expected,
                            it.statusLine.statusCode,
                            "Response should match specified expectation")
                    }
            }
            Thread.sleep(10)
        }
    }
}
