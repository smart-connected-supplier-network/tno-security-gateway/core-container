/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager

import de.fraunhofer.iais.eis.Message
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.serialization.Serialization
import org.apache.camel.Processor
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ConfigurationPropertiesScan("nl.tno.ids")
@ComponentScan("nl.tno.ids")
class TestApplication {
    @get:Bean("errorHandlerProcessor")
    val errorHandlerProcessor = Processor { println("Empty processor") }

    @get:Bean("dapsTokenInjector") val dapsTokenInjector = Processor {}

    @get:Bean("dapsTokenVerifier")
    val dapsTokenVerifier = Processor {
        it.message.setHeader(
            Constants.Headers.idsId,
            Serialization.fromJsonLD<Message>(it.message.getHeader(Constants.Headers.idsHeader, String::class.java)).issuerConnector.toString()
        )
    }

    @get:Bean("clearing") val clearing = Processor { println("Empty clearing processor") }
}

data class TestIO(
    val url: String,
    val expected: Int,
    val direction: String,
    val from: String? = null
)
