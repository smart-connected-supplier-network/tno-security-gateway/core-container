/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.internal.local

import com.charleskorn.kaml.PolymorphismStyle
import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.YamlConfiguration
import com.fkorotkov.kubernetes.metadata
import com.fkorotkov.kubernetes.newService
import com.fkorotkov.kubernetes.newServicePort
import com.fkorotkov.kubernetes.spec
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.client.KubernetesClient
import kotlinx.serialization.decodeFromString
import nl.tno.ids.artifacts.ArtifactManagerService
import nl.tno.ids.routemanager.CamelRouteManager
import nl.tno.ids.testextensions.DapsTestExtension
import nl.tno.ids.workflowmanager.NetworkProcessor
import nl.tno.ids.workflowmanager.TestApplication
import nl.tno.ids.workflowmanager.TestIO
import nl.tno.ids.workflowmanager.WorkflowManager
import nl.tno.ids.workflowmanager.models.NetworkModel
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.junit.jupiter.api.*
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor
import org.springframework.test.context.TestPropertySource

@SpringBootTest(
    classes = [TestApplication::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(
    properties =
        [
            "spring.config.location = classpath:application.yaml",
            "workflow.incrementalIds = true",
            "workflow.type = IDS",
            "workflow.internalHostname = host.docker.internal",
            "orchestrationManagerConfig.pullSecretName = ids-pull-secret",
            "orchestrationManagerConfig.enableKubernetes = true"
        ]
)
@TestMethodOrder(MethodOrderer.OrderAnnotation::class)
@ExtendWith(DapsTestExtension::class)
class NetworkModelTest {
    @LocalServerPort private var port: Int = 0

    @Autowired private lateinit var camelRouteManager: CamelRouteManager

    @Autowired private lateinit var workflowManager: WorkflowManager

    @Autowired private lateinit var kubernetesClient: KubernetesClient

    @Autowired private lateinit var artifactManagerService: ArtifactManagerService

    @AfterEach
    fun cleanUp() {
        kubernetesClient
            .services()
            .list()
            .items
            .filter { it.metadata.labels.containsKey("unit-test") }
            .forEach { kubernetesClient.services().withName(it.metadata.name).delete() }
    }

    @Test
    fun testCamelInitialisation() {
        val workflow = Yaml(
            configuration =
            YamlConfiguration(
                polymorphismStyle = PolymorphismStyle.Property,
                encodeDefaults = false)
        ).decodeFromString<NetworkModel>(
                    NetworkModelTest::class.java.getResource(
                            "/workflows-artifact.yaml")!!
                        .readText())

        val name = "${workflow.container?.name}-0"
        val containerPort = workflow.container?.ports?.firstOrNull() ?: "8080"
        kubernetesClient
            .services()
            .resource(
                newService {
                    metadata {
                        this.name = "$name-nodeport"
                        labels = mapOf("unit-test" to "true")
                    }
                    spec {
                        type = "NodePort"
                        selector = mapOf("app.kubernetes.io/name" to name)
                        ports =
                            listOf(
                                newServicePort {
                                    port = containerPort.toInt()
                                    targetPort = IntOrString(containerPort.toInt())
                                    nodePort = 32500
                                })
                    }
                }).create()
        val workflow2 = workflow.copy(
            accessUrl = "http://localhost:32500"
        )

        workflowManager.startNetworking(workflow2)

        Thread.sleep(1000)

        camelRouteManager
            .getRoutes()
            .mapNotNull { camelRouteManager.getRoute(it.id)?.content }
            .forEach { println(it) }

        Thread.sleep(1000)
        executeTestCalls(
            listOf(
                TestIO(
                    "http://localhost:8080/router/workflow/0/preprocessing/input/0",
                    200,
                    "input")
            )
        )
        Thread.sleep(10000)
        for (i in 1..100) {
            Thread.sleep(100)
            if (artifactManagerService.getArtifactMetadata("urn:ids:tno-fraunhofer:result") !== null) {
                break
            }
        }
        Assertions.assertNotNull(artifactManagerService.getArtifactMetadata("urn:ids:tno-fraunhofer:result"))
    }

    private fun executeTestCalls(calls: List<TestIO>) {
        val executor = ThreadPoolTaskExecutor()
        executor.corePoolSize = 5
        executor.maxPoolSize = 10
        executor.initialize()
        calls.forEach { call ->
            executor.execute {
                HttpClients.createDefault()
                    .execute(
                        HttpPost(call.url).apply {
                            addHeader(NetworkProcessor.FROM_KEY, call.from)
                            entity = StringEntity("""
                                {
                                    "test": "test object",
                                    "test2": 5,
                                    "test3": [1,2,3,4,5,6,7,8,9,0]
                                }
                            """.trimIndent())
                        })
                    .use {
                        Assertions.assertEquals(
                            call.expected,
                            it.statusLine.statusCode,
                            "Response should match specified expectation")
                    }
            }
            Thread.sleep(10)
        }
    }
}
