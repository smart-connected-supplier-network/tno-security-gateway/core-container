/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.internal

import com.charleskorn.kaml.PolymorphismStyle
import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.YamlConfiguration
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import nl.tno.ids.workflowmanager.bim.WorkflowConverter
import nl.tno.ids.workflowmanager.models.WorkflowObjectModel
import org.junit.jupiter.api.Test

class WorkflowModelTest {
    private val yaml =
        Yaml(
            configuration =
                YamlConfiguration(
                    polymorphismStyle = PolymorphismStyle.Property, encodeDefaults = false))
    @Test
    fun testDeserialization() {
        WorkflowModelTest::class
            .java
            .getResource("/workflows-bim/bim-workflow-camel.yaml")
            ?.readText()
            ?.let {
                val parsed = yaml.decodeFromString<WorkflowObjectModel>(it)

                println(parsed)
            }
            ?: throw Exception("Cannot read YAML file")
    }

    @Test
    fun testDeserialization2() {
        WorkflowModelTest::class
            .java
            .getResource("/workflows-bim/bim-combiner.yaml")
            ?.readText()
            ?.let {
                val parsed = yaml.decodeFromString<WorkflowObjectModel>(it)

                println(parsed)
            }
            ?: throw Exception("Cannot read YAML file")
    }

    @Test
    fun testConversion() {
        WorkflowModelTest::class
            .java
            .getResource("/workflows-bim/bim-workflow-camel.yaml")
            ?.readText()
            ?.let {
                val parsed = yaml.decodeFromString<WorkflowObjectModel>(it)

                println(yaml.encodeToString(WorkflowConverter().convert(parsed)))
            }
            ?: throw Exception("Cannot read YAML file")
    }

    @Test
    fun testConversion2() {
        WorkflowModelTest::class
            .java
            .getResource("/workflows-bim/bim-combiner.yaml")
            ?.readText()
            ?.let {
                val parsed = yaml.decodeFromString<WorkflowObjectModel>(it)

                println(yaml.encodeToString(WorkflowConverter().convert(parsed)))
            }
            ?: throw Exception("Cannot read YAML file")
    }
}
