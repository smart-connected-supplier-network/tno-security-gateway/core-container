/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager

import java.io.File
import nl.tno.ids.workflowmanager.models.HttpInput
import nl.tno.ids.workflowmanager.models.IOSpec
import nl.tno.ids.workflowmanager.models.NetworkStep
import org.apache.camel.Exchange
import org.apache.camel.Processor
import org.apache.http.HttpMessage
import org.slf4j.LoggerFactory
import java.util.*

/**
 * Data class for monitoring the number of invocations of a certain Input/Output of a Network Step
 */
data class IOCounts(var count: Int, var loop: Int)

/** Data class for monitoring the full state of a Network Step */
data class IOState(
    val spec: IOSpec,
    val counts: IOCounts? = null,
    val mappedCounts: Map<String, IOCounts>? = null
)

/** Generic Network Exception class */
open class NetworkException(message: String) : Exception(message)

/** Network Configuration Exception class */
class NetworkConfigurationException(message: String) : NetworkException(message)

/** Network Step State Exception class */
class NetworkStateException(message: String) : NetworkException(message)

/** Network Message Exception */
class NetworkIncorrectMessageException(message: String) : NetworkException(message)

/**
 * Apache Camel Network Processor that is added to all Network Step Camel routes that monitors the
 * Network Step
 */
class NetworkProcessor(
    private val networkState: NetworkState,
    private val step: NetworkStep,
    private val storeResults: Boolean
) : Processor {
    companion object {
        private val LOGGER = LoggerFactory.getLogger(NetworkProcessor::class.java)

        /** Input/Output identifier header key */
        const val IO_KEY = "io"

        /** Input header value */
        const val IO_INPUT = "input"

        /** Output header value */
        const val IO_OUTPUT = "output"

        /** Input/Output Index identifier header key */
        const val IO_INDEX_KEY = "io-index"

        /** Header key identifying the external party the message was received from */
        const val FROM_KEY = "ids-id"
    }

    /** Input state map for all inputs of the Network Step */
    private val inputState: Map<Int, IOState>

    /** Output state map for all outputs of the Network Step */
    private val outputState: Map<Int, IOState>

    /** Initialize the inputState and outputState from the Network Step specification */
    init {
        inputState =
            step.input
                .mapIndexed { index, inputSpec ->
                    if (inputSpec.from.size > 1) {
                        index to
                            IOState(
                                inputSpec,
                                mappedCounts = inputSpec.from.associateWith { IOCounts(0, 0) })
                    } else {
                        index to IOState(inputSpec, counts = IOCounts(0, 0))
                    }
                }
                .toMap()
                .toMutableMap()
        outputState =
            step.output
                .mapIndexed { index, outputSpec ->
                    index to IOState(outputSpec, counts = IOCounts(0, 0))
                }
                .toMap()
                .toMutableMap()
    }

    /**
     * Processor function that will be executed on all messages of the Network Step
     *
     * The process follows the following steps: check whether the current state is acceptable, check
     * whether the sender is allowed to send messages to this Network Step, update the counts of the
     * Network Step, check whether the Network Step is completed.
     */
    override fun process(exchange: Exchange) {
        checkCurrentState()
        val io = exchange.properties[IO_KEY] as String
        val ioIndex = exchange.properties[IO_INDEX_KEY] as Int

        val state =
            when (io) {
                IO_INPUT -> inputState[ioIndex]
                IO_OUTPUT -> outputState[ioIndex]
                else -> null
            }
                ?: throw NetworkConfigurationException("IO State not found for $io[$ioIndex]")

        val from: String? = parseFrom(io, state, exchange)

        updateCounts(state, from)

        LOGGER.info(
            "Count: ${state.counts?.count}, loop: ${state.counts?.loop}, mappedCounts: ${state.mappedCounts}")
        if (inputState.values.all { isIOFinished(it) } &&
            outputState.values.all { isIOFinished(it) }) {
            networkState.stepCompleted(step.name)
            LOGGER.info("Network step ${step.name} completed")
        }

        if (storeResults) {
            val body = exchange.message.getBody(ByteArray::class.java)
            val folder =
                File("/workflowResults/${networkState.networkId}/${step.name}/${io}/${ioIndex}")
            folder.mkdirs()
            val file = File(folder, "${System.currentTimeMillis()}.log")
            file.writeBytes(body ?: ByteArray(0))
            exchange.message.body = file.inputStream()
        }
    }

    /**
     * Check whether the current Network Step is allowed to receive messages (i.e. being in the
     * RUNNING state)
     *
     * A timeout period is used when the current step is not yet started but might start within the
     * timeout
     */
    private fun checkCurrentState() {
        if (networkState.currentState[step.name] === State.NOT_STARTED) {
            for (i in 1..networkState.networkModel.flowTimeout) {
                LOGGER.info(
                    "Step \"${step.name}\" not started yet, waiting ${networkState.networkModel.flowTimeout - i} seconds")
                Thread.sleep(1000)
                if (networkState.currentState[step.name] !== State.NOT_STARTED) {
                    break
                }
            }
            if (networkState.currentState[step.name] === State.NOT_STARTED) {
                throw NetworkStateException("Step \"${step.name}\" not started yet")
            }
        }
        if (networkState.currentState[step.name] === State.COMPLETED) {
            throw NetworkStateException("Step \"${step.name}\" already completed")
        }
    }

    /**
     * Parse the From header key and check whether the sender is allowed to send messages at this
     * stage
     */
    private fun parseFrom(io: String, state: IOState, exchange: Exchange): String? {
        return if (io == IO_INPUT && state.spec is HttpInput) {
            if (state.spec.from.size == 1 && state.spec.from.find { it == "self" } != null) {
                "self"
            } else {
                if (exchange.message is HttpMessage) {
                    throw NetworkConfigurationException(
                        "Expected HttpMessage instead of ${exchange.message::class.java}")
                }
                (exchange.message.headers.entries
                        .find { it.key.lowercase(Locale.getDefault()) == FROM_KEY }
                        ?.value as
                        String?)
                    ?.also {
                        if (it !in state.spec.from) {
                            throw NetworkIncorrectMessageException(
                                "Non-expected $FROM_KEY \"${it}\" expected one of: ${state.spec.from}")
                        }
                    }
                    ?: throw NetworkIncorrectMessageException(
                        "Input routes require $FROM_KEY property/header")
            }
        } else {
            null
        }
    }

    /** Update the counts of the Network Step to reflect the actual current state */
    private fun updateCounts(state: IOState, from: String?) {
        state.counts?.let {
            it.count++
            if (it.count >= state.spec.count) {
                it.loop++
                it.count = 0
            }
        }
        state.mappedCounts?.let { mappedCounts ->
            mappedCounts[from]?.let {
                it.count++
                if (it.count >= state.spec.count) {
                    it.loop++
                    it.count = 0
                }
            }
        }
    }

    /** Check whether all the Inputs/Outputs are satisfied */
    private fun isIOFinished(ioState: IOState): Boolean {
        if (ioState.counts != null) {
            return ioState.counts.loop > ioState.spec.loop
        } else if (ioState.mappedCounts != null) {
            return ioState.mappedCounts.values.all { it.loop > ioState.spec.loop }
        }
        return false
    }
}
