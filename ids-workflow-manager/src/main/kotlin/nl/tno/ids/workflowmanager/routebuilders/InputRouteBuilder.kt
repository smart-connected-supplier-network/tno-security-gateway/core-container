/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.routebuilders

import nl.tno.ids.workflowmanager.NetworkProcessor
import nl.tno.ids.workflowmanager.models.InputSpec
import nl.tno.ids.workflowmanager.models.NetworkModel
import nl.tno.ids.workflowmanager.models.NetworkStep
import org.apache.camel.builder.RouteBuilder

/** Abstract class for Camel Route Builders for Input routes of networks */
abstract class InputRouteBuilder {

    abstract fun generate(
        routeBuilder: RouteBuilder,
        networkStep: NetworkStep,
        networkProcessor: NetworkProcessor,
        index: Int,
        spec: InputSpec,
    )

    /** Helper function to generate the correct URL of the Data App for correct Camel routing */
    protected fun generateDataAppAccessUrl(
        networkModel: NetworkModel,
        networkId: String,
        networkStep: NetworkStep,
        spec: InputSpec
    ): String {
        val prefix =
            when {
                networkStep.accessUrl != null -> networkStep.accessUrl
                networkModel.accessUrl != null -> networkModel.accessUrl
                networkStep.container != null ->
                    "http://${networkStep.container.name}-${networkStep.name}-$networkId:${networkStep.container.ports.firstOrNull() ?: "8080"}"
                networkModel.container != null ->
                    "http://${networkModel.container.name}-$networkId:${networkModel.container.ports.firstOrNull() ?: "8080"}"
                else -> networkModel.httpPrefix
            }
        return "$prefix${spec.endpoint}?bridgeEndpoint=true"
    }
}
