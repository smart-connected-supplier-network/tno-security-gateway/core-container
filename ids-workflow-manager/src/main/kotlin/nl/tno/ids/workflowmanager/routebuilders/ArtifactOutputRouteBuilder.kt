/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.routebuilders

import de.fraunhofer.iais.eis.ContractOffer
import nl.tno.ids.artifacts.ArtifactManagerService
import nl.tno.ids.configuration.serialization.Serialization
import nl.tno.ids.workflowmanager.NetworkProcessor
import nl.tno.ids.workflowmanager.models.ArtifactOutput
import nl.tno.ids.workflowmanager.models.NetworkStep
import nl.tno.ids.workflowmanager.models.OutputSpec
import org.apache.camel.builder.RouteBuilder
import java.io.InputStream

/** Camel Route Builder for IDS Artifact Output routes */
class ArtifactOutputRouteBuilder(
    private val networkId: String,
    private val artifactManagerService: ArtifactManagerService?,
    private val camelPort: String
) : OutputRouteBuilder() {

    override fun generate(
        routeBuilder: RouteBuilder,
        networkStep: NetworkStep,
        networkProcessor: NetworkProcessor,
        index: Int,
        spec: OutputSpec,
    ) {
        if (artifactManagerService == null) {
            throw Exception("Artifact Management must be enabled when using Artifact Output steps in workflows")
        }
        val contractOffer = (spec as ArtifactOutput).contractOffer?.let { Serialization.fromJsonLD<ContractOffer>(it) }
        routeBuilder.apply {
            from(
                    "jetty:http://0.0.0.0:$camelPort/router/workflow/$networkId/${networkStep.name}/output/$index?enableMultipartFilter=false")
                .routeId("$networkId-${networkStep.name}-output-${index}")
                .log("Route \${routeId}: \${headers.CamelServletContextPath}, \${headers[ids-id]}")
                .setProperty(NetworkProcessor.IO_KEY, constant("output"))
                .setProperty(NetworkProcessor.IO_INDEX_KEY, constant(index))
                .process(networkProcessor)
                .process {
                    artifactManagerService.addArtifact(
                        artifactId = spec.artifactId,
                        filename = spec.filename,
                        data = it.message.getBody(InputStream::class.java),
                        title = spec.title,
                        description = spec.description,
                        contractOffer = contractOffer
                    )
                }
                .setBody(constant("OK"))
        }
    }
}
