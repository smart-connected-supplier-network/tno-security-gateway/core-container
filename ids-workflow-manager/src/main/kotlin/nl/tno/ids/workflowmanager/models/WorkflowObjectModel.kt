/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.models

import kotlinx.serialization.Serializable
import nl.tno.ids.orchestrationmanager.models.ContainerModel

/**
 * Workflow Object Model describing a set of workflows
 *
 * Derived from the Building, Infrastructure, and Maritime bots (BIM-bots)
 */
@Serializable
data class WorkflowObjectModel(val capabilities: List<Capability>, val workflows: List<Workflow>)

/**
 * Capability description that are used in Invocations
 *
 * A capability is build up around an image that can be instantiated to a container
 */
@Serializable
data class Capability(
    val id: Long,
    val container: ContainerModel? = null,
    val name: String,
    val description: String,
    val version: String = "1.0.0",
    val persistent: Boolean = false,
    val nodes: List<Node> = emptyList(),
    val accessUrl: String? = null
)

/** Input/Output specification for a capability */
@Serializable
data class Node(
    val name: String,
    val mandatory: Boolean,
    val schema: String,
    val dataType: String,
    val stepType: StepType = StepType.INPUT,
    val transferType: TransferType = TransferType.DIRECT
)

/** Enum for describing the type of a Node */
enum class StepType {
    INPUT,
    OUTPUT
}

/** Enum for indicating the type of data transfer for the Node */
enum class TransferType {
    DIRECT,
    HTTP_REFERENCE,
    FILE_POINTER
}

/**
 * Workflow description containing a list of invocation flow maps that can be instantiated to a
 * Network
 */
@Serializable
data class Workflow(
    val id: Long,
    val name: String,
    val invocationFlowMaps: List<InvocationFlowMap>
)

/** Invocation Flow Map description */
@Serializable data class InvocationFlowMap(val invocation: List<Invocation>)

/** Invocation description that can be used as a Network Step in the workflow manager */
@Serializable
data class Invocation(
    val name: String,
    val capabilityId: Long,
    val environment: Map<String, String> = emptyMap(),
    val nodes: List<InvocationNode>
)

/** Reference to a Node of a capability, matched on the name of the Node */
@Serializable
data class InvocationNode(val name: String, val requiredNode: InvocationNodeRef? = null)

/** Reference to a Node of a previous invocation to automatically pipe these Nodes */
@Serializable data class InvocationNodeRef(val invocation: String, val node: String)
