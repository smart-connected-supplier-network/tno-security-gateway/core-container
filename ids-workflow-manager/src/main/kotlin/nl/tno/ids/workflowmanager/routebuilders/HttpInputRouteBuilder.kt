/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.routebuilders

import nl.tno.ids.workflowmanager.NetworkProcessor
import nl.tno.ids.workflowmanager.models.InputSpec
import nl.tno.ids.workflowmanager.models.NetworkModel
import nl.tno.ids.workflowmanager.models.NetworkStep
import org.apache.camel.ExchangePattern
import org.apache.camel.Processor
import org.apache.camel.builder.RouteBuilder

/** Camel Route Builder for HTTP Input routes */
class HttpInputRouteBuilder(
    private val tokenVerifier: Processor,
    private val networkId: String,
    private val networkModel: NetworkModel,
    private val camelPort: String
) : InputRouteBuilder() {

    override fun generate(
        routeBuilder: RouteBuilder,
        networkStep: NetworkStep,
        networkProcessor: NetworkProcessor,
        index: Int,
        spec: InputSpec
    ) {
        routeBuilder.apply {
            from(
                    "jetty:http://0.0.0.0:$camelPort/router/workflow/$networkId/${networkStep.name}/input/$index?enableMultipartFilter=false")
                .routeId("$networkId-${networkStep.name}-input-${index}")
                .log("Route \${routeId}: \${headers.CamelServletContextPath}, \${headers[ids-id]}")
                .setProperty(NetworkProcessor.IO_KEY, constant("input"))
                .setProperty(NetworkProcessor.IO_INDEX_KEY, constant(index))
                .run {
                    if (spec.from.isNotEmpty() && spec.from.none { it == "self" }) {
                        process("multiPartInputProcessor").process(tokenVerifier)
                    } else {
                        this
                    }
                }
                .process(networkProcessor)
                .to(
                    ExchangePattern.InOnly,
                    generateDataAppAccessUrl(networkModel, networkId, networkStep, spec))
                .setBody(constant("OK"))
        }
    }
}
