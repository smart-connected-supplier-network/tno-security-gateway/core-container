/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.api

import com.charleskorn.kaml.PolymorphismStyle
import com.charleskorn.kaml.Yaml
import com.charleskorn.kaml.YamlConfiguration
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import nl.tno.ids.configuration.model.SecurityRoles
import nl.tno.ids.workflowmanager.State
import nl.tno.ids.workflowmanager.WorkflowManager
import nl.tno.ids.workflowmanager.models.NetworkModel
import nl.tno.ids.workflowmanager.models.WorkflowObjectModel
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.HttpClients
import org.json.JSONArray
import org.json.JSONObject
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.io.File
import java.util.concurrent.CompletableFuture

/** Data class for sharing the Network identifier and its current state */
@Serializable data class NetworkInfo(val id: String, val state: Map<String, State>)

/**
 * Spring Rest Controller for exposing information and to allow interaction with the Workflow
 * Manager
 */
@RestController
@RequestMapping("/api/workflow")
@Secured(SecurityRoles.WORKFLOW_MANAGER)
@kotlinx.serialization.ExperimentalSerializationApi
class WorkflowManagerController(private val workflowManager: WorkflowManager) {
    /** Kotlinx Serialization support for YAML */
    private val yaml =
        Yaml(
            configuration =
                YamlConfiguration(
                    polymorphismStyle = PolymorphismStyle.Property, encodeDefaults = false))

    /** Get all active networks */
    @GetMapping(produces = ["application/json"])
    @Secured(SecurityRoles.WORKFLOW_READER)
    fun listWorkflows(): String {
        return Json.encodeToString(
            workflowManager.workflows.map { NetworkInfo(it.key, it.value.currentState) })
    }

    /** Get a specific network */
    @GetMapping(value = ["{networkId}"], produces = ["application/json"])
    @Secured(SecurityRoles.WORKFLOW_READER)
    fun getWorkflow(@PathVariable networkId: String): String {
        return workflowManager.workflows[networkId]?.let {
            Json.encodeToString(NetworkInfo(it.networkId, it.currentState))
        }
            ?: throw ResponseStatusException(HttpStatus.NOT_FOUND, "The workflow can't be found")
    }

    /** Start a new network based on a YAML or JSON representation of the Network specification */
    @PostMapping(consumes = ["text/yaml", "application/json"], produces = ["application/json"])
    fun startWorkflow(
        @RequestBody workflowBody: String,
        @RequestHeader("Content-Type") contentType: String
    ): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            val workflow: NetworkModel =
                when (contentType) {
                    "application/json" -> Json.decodeFromString(workflowBody)
                    else -> yaml.decodeFromString(workflowBody)
                }
            val network = workflowManager.startNetworking(workflow)
            Json.encodeToString(NetworkInfo(id = network.networkId, network.currentState))
        }
    }

    /** Start a group of networks based on the WorkflowObjectModel */
    @PostMapping(
        value = ["group"],
        consumes = ["text/yaml", "application/json"],
        produces = ["application/json"])
    fun startWorkflows(
        @RequestBody workflowBody: String,
        @RequestHeader("Content-Type") contentType: String
    ): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            val workflows: WorkflowObjectModel =
                when (contentType) {
                    "application/json" -> Json.decodeFromString(workflowBody)
                    else -> yaml.decodeFromString(workflowBody)
                }
            val networks = workflowManager.startWorkflows(workflows)
            Json.encodeToString(
                networks.mapValues {
                    it.value.map { networkState ->
                        NetworkInfo(id = networkState.networkId, networkState.currentState)
                    }
                })
        }
    }

    /** Invoke workflow step to start a workflow manually */
    @PostMapping("invoke/{workflowId}/{stepName}/{inputIndex}")
    fun invokeWorkflow(
        @RequestBody(required = false) body: String?,
        @PathVariable workflowId: String,
        @PathVariable stepName: String,
        @PathVariable inputIndex: String,
        @RequestHeader("Content-Type", required = false) contentType: String?
    ): CompletableFuture<ResponseEntity<String>> {
        return CompletableFuture.supplyAsync {
            HttpClients.createDefault()
                .execute(
                    HttpPost(
                            "http://localhost:8080/router/workflow/$workflowId/$stepName/input/$inputIndex")
                        .apply {
                            entity = StringEntity(body ?: "")
                            addHeader("ids-id", "self")
                            contentType?.let { addHeader("Content-Type", it) }
                        })
                .use {
                    ResponseEntity.status(it.statusLine.statusCode)
                        .body(String(it.entity.content.readAllBytes()))
                }
        }
    }

    /** Stop a running network and return its current state */
    @CrossOrigin(origins = ["*"], allowedHeaders = ["*"])
    @DeleteMapping("{networkId}")
    fun stopWorkflow(@PathVariable networkId: String): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            workflowManager.stopNetwork(networkId)?.let {
                Json.encodeToString(NetworkInfo(it.networkId, it.currentState))
            }
                ?: throw ResponseStatusException(
                    HttpStatus.NOT_FOUND, "The workflow can't be found")
        }
    }

    /** Get debug results of a workflow */
    @GetMapping("{networkId}/results", produces = ["application/json"])
    fun getResults(@PathVariable networkId: String): CompletableFuture<String> {
        return CompletableFuture.supplyAsync {
            val workflowFolder = File("/workflowResults/$networkId")
            if (workflowFolder.exists()) {
                val results =
                    JSONArray(
                        workflowFolder.listFiles()?.map { stepFolder ->
                            val stepName = stepFolder.name
                            JSONObject()
                                .put("step", stepName)
                                .put(
                                    "results",
                                    JSONArray(
                                        stepFolder.listFiles()?.map { ioFolder ->
                                            val io = ioFolder.name
                                            ioFolder.listFiles()?.map { ioIndexFolder ->
                                                val ioIndex = ioIndexFolder.name
                                                JSONObject()
                                                    .put("io", io)
                                                    .put("index", ioIndex)
                                                    .put(
                                                        "results",
                                                        JSONArray(
                                                            ioIndexFolder.listFiles()?.map { parseLogfile(it) }
                                                        )
                                                    )
                                            }
                                        }
                                    )
                                )
                        }
                    )
                results.toString(2)
            } else {
                """
                    {
                        "error": "No workflow results available"
                    }
                """.trimIndent()
            }
        }
    }

    /** Parse log file into JSONObject */
    fun parseLogfile(logFile: File): JSONObject {
        val data = logFile.readText()
        val output =
            if (data.length < 10240) {
                try {
                    if (data.trimStart()
                            .first() == '{') {
                        JSONObject(data)
                    } else if (data.trimStart()
                            .first() == '[') {
                        JSONArray(data)
                    } else {
                        data
                    }
                } catch (e: Exception) {
                    data
                }
            } else {
                data.substring(0, 10240)
            }
        return JSONObject()
            .put(
                "timestamp",
                logFile.name.dropLast(4))
            .put("data", output)
    }
}
