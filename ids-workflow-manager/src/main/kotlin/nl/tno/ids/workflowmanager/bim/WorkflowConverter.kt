/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.bim

import nl.tno.ids.workflowmanager.models.*
import org.springframework.stereotype.Component

/** Converter helper to convert from the WorkflowObjectModel to a set of NetworkModel */
@Component
class WorkflowConverter {

    fun convert(workflowObjectModel: WorkflowObjectModel): Map<String, List<List<NetworkStep>>> {
        return workflowObjectModel.workflows.associate { workflow ->
            workflow.name to
                workflow.invocationFlowMaps.map { invocationFlowMap ->
                    invocationFlowMap.invocation.map { invocation ->
                        val capability =
                            workflowObjectModel.capabilities.firstOrNull {
                                it.id == invocation.capabilityId
                            }
                                ?: throw Exception(
                                    "Cannot find capability with id ${invocation.capabilityId}")
                        NetworkStep(
                            name = invocation.name,
                            depends_on =
                                invocation.nodes.mapNotNull { it.requiredNode?.invocation },
                            container =
                                capability.container?.let {
                                    it.copy(environment = it.environment + invocation.environment)
                                },
                            accessUrl = capability.accessUrl,
                            input =
                            List(invocation
                                .nodes
                                .map { node ->
                                    capability.nodes.firstOrNull { it.name == node.name }
                                        ?: throw Exception(
                                            "Cannot find node ${node.name} in ${capability.name} available nodes: ${capability.nodes.map { it.name }}")
                                }
                                .filter { it.stepType == StepType.INPUT }.size) { idx ->
                                HttpInput(
                                    endpoint = "/bot/${invocation.name}/input/$idx",
                                    from = listOf("self"))
                            },
                            output =
                                invocation
                                    .nodes
                                    .map { node ->
                                        capability.nodes.firstOrNull { it.name == node.name }
                                            ?: throw Exception(
                                                "Cannot find node ${node.name} in ${capability.name} available nodes: ${capability.nodes.map { it.name }}")
                                    }
                                    .filter { it.stepType == StepType.OUTPUT }
                                    .map { outputNode ->
                                        HttpOutput(
                                            to =
                                                invocationFlowMap
                                                    .invocation
                                                    .flatMap { tmpInvocations ->
                                                        tmpInvocations.nodes.mapIndexed {
                                                            index,
                                                            inputNode ->
                                                            Triple(
                                                                tmpInvocations.name,
                                                                index,
                                                                inputNode.requiredNode?.let {
                                                                    it.invocation ==
                                                                        invocation.name &&
                                                                        it.node == outputNode.name
                                                                }
                                                                    ?: false)
                                                        }
                                                    }
                                                    .filter { it.third }
                                                    .map {
                                                        ToSpec(
                                                            internal = true,
                                                            endpoint =
                                                                "/${it.first}/input/${it.second}")
                                                    })
                                    })
                    }
                }
        }
    }
}
