/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager

import nl.tno.ids.artifacts.ArtifactManagerService
import nl.tno.ids.routemanager.CamelRouteManager
import nl.tno.ids.workflowmanager.bim.WorkflowConverter
import nl.tno.ids.workflowmanager.containers.WorkflowContainerHelper
import nl.tno.ids.workflowmanager.models.*
import nl.tno.ids.workflowmanager.routebuilders.ArtifactOutputRouteBuilder
import nl.tno.ids.workflowmanager.routebuilders.HttpInputRouteBuilder
import nl.tno.ids.workflowmanager.routebuilders.HttpOutputRouteBuilder
import org.apache.camel.Exchange
import org.apache.camel.LoggingLevel
import org.apache.camel.Processor
import org.apache.camel.builder.RouteBuilder
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.lang.Nullable
import org.springframework.stereotype.Component
import java.util.*

/** Workflow Manager Spring Configuration for configuring the working of the Workflow Manager */
@Component
@ConfigurationProperties(prefix = "workflow")
data class WorkflowManagerConfiguration(
    /** Use incremental identifiers rather than random UUIDs */
    var incrementalIds: Boolean = false,

    /**
     * Boolean that indicates whether or not Data App Orchestration should be used by the workflow
     * manager
     */
    var useOrchestration: Boolean = true,

    /** The internal hostname of this core container that can be reached by Data Apps */
    var internalHostname: String = System.getenv("HOSTNAME") ?: "localhost",

    /** Debug flag for following intermediate results */
    var saveIntermediateResults: Boolean = false
)

/** Workflow Manager that is able to start and stop workflows */
@Component
class WorkflowManager(
    /** Camel Route Manager used for managing camel routes for workflows */
    private val camelRouteManager: CamelRouteManager,
    /** Workflow Manager Spring Configuration */
    private val workflowManagerConfiguration: WorkflowManagerConfiguration,
    /** Workflow Object Model to Network Model converter */
    private val workflowConverter: WorkflowConverter,
    /** Helper component to instruct the Orchestration Manager which containers to start */
    private val workflowContainerHelper: WorkflowContainerHelper,
    /** Artifact Manager Service for storage of Artifact Outputs */
    @Nullable private val artifactManagerService: ArtifactManagerService?,
    /**
     * DAPS Token Injector Processor used in routes created that send messages to other connectors
     */
    @Qualifier("dapsTokenInjector") private val dapsTokenInjector: Processor,

    /**
     * DAPS Token Verifier Processor ussed in routes created that receive messages from other
     * connectors
     */
    @Qualifier("dapsTokenVerifier") private var dapsTokenVerifier: Processor,
    /** Camel port modifier */
    @Value("\${camel.port:8080}") private val camelPort: String
) {
    companion object {
        private val LOG = LoggerFactory.getLogger(WorkflowManager::class.java)
    }

    /** In-memory storage of deployed workflows */
    val workflows: MutableMap<String, NetworkState> = mutableMapOf()

    /** Workflow index used when incrementalIds are turned on */
    var workflowIndex = 0

    /**
     * Start a group of workflows given the WorkflowObjectModel, uses the Workflow Converter to
     * translate the WorkflowObjectModel to a set of NetworkModels and start them sequentially.
     */
    fun startWorkflows(workflowObjectModel: WorkflowObjectModel): Map<String, List<NetworkState>> {
        return workflowConverter.convert(workflowObjectModel).mapValues {
            it.value.map { networkSteps ->
                startNetworking(
                    NetworkModel(parties = emptyList(), idsid = "", steps = networkSteps))
            }
        }
    }

    /**
     * Start a NetworkModel by instructing the workflowContainerHelper to start containers if
     * required and build up the Camel routes for this NetworkModel
     */
    fun startNetworking(networkModel: NetworkModel): NetworkState {
        val networkId =
            if (workflowManagerConfiguration.incrementalIds) "${workflowIndex++}"
            else UUID.randomUUID().toString()
        val networkState =
            NetworkState(networkModel, networkId) {
                LOG.info(
                    "All steps completed of workflow $networkId, cleaning up camel routes in 5 seconds")
                Thread {
                        Thread.sleep(5000)
                        stopNetwork(networkId)
                    }
                    .start()
            }
        workflows[networkId] = networkState

        workflowContainerHelper.startContainers(networkModel, networkId, networkState)

        val httpInputRouteBuilder =
            HttpInputRouteBuilder(dapsTokenVerifier, networkId, networkModel, camelPort)
        val httpOutputRouteBuilder =
            HttpOutputRouteBuilder(dapsTokenInjector, networkId, networkModel, camelPort)
        val artifactOutputBuilder = ArtifactOutputRouteBuilder(networkId, artifactManagerService, camelPort)


        camelRouteManager.addRoute(
            object : RouteBuilder() {
                override fun configure() {
                    onException(NetworkException::class.java)
                        .useOriginalMessage()
                        .handled(true)
                        .log(
                            LoggingLevel.ERROR,
                            "Error in network ${networkId}: \${exception.message}")
                        .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(405))

                    from("direct:bin-$networkId").setBody(constant("OK"))

                    from("seda:ids-$networkId").toD("\${header.url}")

                    networkModel.steps.forEach { networkStep ->
                        val networkProcessor =
                            NetworkProcessor(
                                networkState,
                                networkStep,
                                workflowManagerConfiguration.saveIntermediateResults)
                        networkStep.input.forEachIndexed { index, spec ->
                            when (spec) {
                                is HttpInput ->
                                    httpInputRouteBuilder.generate(
                                        this, networkStep, networkProcessor, index, spec)
                            }
                        }
                        networkStep.output.forEachIndexed { index, spec ->
                            when (spec) {
                                is HttpOutput ->
                                    httpOutputRouteBuilder.generate(
                                        this, networkStep, networkProcessor, index, spec)
                                is ArtifactOutput ->
                                    artifactOutputBuilder.generate(
                                        this, networkStep, networkProcessor, index, spec)
                            }
                        }
                    }
                }
            })
        return networkState
    }

    /** Stop the workflow and remove all resources on Camel and Kubernetes */
    fun stopNetwork(id: String): NetworkState? {
        return workflows[id]?.let { networkState ->
            LOG.info("Stopping network routes ($id)")
            camelRouteManager.getRoutes().filter { it.id.startsWith(id) }.forEach {
                camelRouteManager.removeRoute(it.id)
            }
            workflowContainerHelper.stopContainers(networkState)
            networkState.stopNework()
            networkState
        }
    }
}
