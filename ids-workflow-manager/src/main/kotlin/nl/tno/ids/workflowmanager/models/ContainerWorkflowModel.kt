/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.models

import kotlinx.serialization.Serializable

/** Description of a workflow to be used inside containers for a workflow */
@Serializable
data class ContainerWorkflowDescription(
    val workflowOutputPrefix: String,
    val steps: List<StepDescription>
)

/** Description of a step in a workflow containing the Inputs/Outputs */
@Serializable
data class StepDescription(
    val name: String,
    val inputs: List<InputDescription>,
    val outputs: List<OutputDescription>
)

/** Input Description relevant for deployed containers */
@Serializable
data class InputDescription(
    val endpoint: String,
    val count: Int,
    val loop: Int,
    val from: List<String>
)

/** Output Description relevant for deployed containers */
@Serializable
data class OutputDescription(
    val endpoint: String,
    val count: Int,
    val loop: Int,
    val to: List<String>
)
