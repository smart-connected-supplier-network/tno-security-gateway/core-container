/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager

import kotlinx.serialization.Serializable
import nl.tno.ids.workflowmanager.models.NetworkModel
import org.slf4j.LoggerFactory

/** Enum indicating the state a network step is in */
@Serializable
enum class State {
    NOT_STARTED,
    RUNNING,
    COMPLETED,
    STOPPED
}

/**
 * Class containing the state a workflow is in, which is composed of the state of all the network
 * steps of the workflow
 */
class NetworkState(
    /** Specification of the workflow */
    val networkModel: NetworkModel,
    /** Workflow identifier */
    val networkId: String,
    /** Completion handler called when all steps are completed */
    private val completionHandler: () -> Unit
) {
    companion object {
        private val LOGGER = LoggerFactory.getLogger(NetworkState::class.java)
    }

    /** List of orchestrated container IDs for this workflow */
    val containers: MutableList<String> = mutableListOf()

    /** Provide the current state of the steps in this workflow */
    val currentState =
        networkModel
            .steps
            .associate {
                it.name to if (it.depends_on.isEmpty()) State.RUNNING else State.NOT_STARTED
            }
            .toMutableMap()

    /** Indicate whether all steps in this workflow are completed */
    fun finished(): Boolean = currentState.values.all { it == State.COMPLETED }

    /**
     * Handler for receiving the completion of a step from its NetworkProcessor Sets, if necessary,
     * next steps that depend on this step to running if all dependencies are met
     */
    fun stepCompleted(name: String) {
        currentState[name] = State.COMPLETED
        networkModel.steps.filter { currentState[it.name] == State.NOT_STARTED }.forEach {
            networkStep ->
            if (networkStep.depends_on.all { currentState[it] == State.COMPLETED }) {
                currentState[networkStep.name] = State.RUNNING
            }
        }
        if (currentState.values.all { it == State.COMPLETED }) {
            completionHandler()
        }
        LOGGER.info("Step: $name completed, networkState: $currentState")
    }

    /** Handler for receiving the request to stop the Network */
    fun stopNework() {
        currentState.filterValues { it == State.RUNNING }.forEach { (key, _) ->
            currentState[key] = State.STOPPED
        }
    }
}
