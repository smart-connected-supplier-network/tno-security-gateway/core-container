/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.containers

import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import nl.tno.ids.orchestrationmanager.OrchestrationManager
import nl.tno.ids.orchestrationmanager.models.ConfigurationEntry
import nl.tno.ids.orchestrationmanager.models.ContainerModel
import nl.tno.ids.workflowmanager.NetworkState
import nl.tno.ids.workflowmanager.WorkflowManagerConfiguration
import nl.tno.ids.workflowmanager.models.*
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*

/**
 * Workflow Container Helper to abstract the interaction with the Orchestration Manager for a
 * workflow
 */
@Component
class WorkflowContainerHelper(
    /** Workflow Manager Configuration, for indicating whether or not to use orchestration */
    private val workflowManagerConfiguration: WorkflowManagerConfiguration
) {
    /** Optionally autowired IdsOrchestrationManager for interacting with the orchestrator */
    @Autowired(required = false) private var orchestrationManager: OrchestrationManager? = null

    companion object {
        private val LOG = LoggerFactory.getLogger(WorkflowContainerHelper::class.java)
    }

    /** Stop the containers for a given Network */
    fun stopContainers(networkState: NetworkState) {
        if (workflowManagerConfiguration.useOrchestration) {
            LOG.info("Stopping network containers (${networkState.networkId})")
            networkState.containers.forEach { orchestrationManager?.stopContainer(it) }
        }
    }

    /**
     * Start all the containers necessary for a Network, both on a Network level as well as on a
     * Network Step level
     */
    fun startContainers(networkModel: NetworkModel, networkId: String, networkState: NetworkState) {
        if (workflowManagerConfiguration.useOrchestration) {
            val containerFutures =
                networkModel.steps.filter { it.container !== null }.map { networkStep ->
                    orchestrationManager?.launchContainer(
                        enrichContainerConfiguration(
                            networkStep.container!!,
                            networkId,
                            "${networkStep.name}-$networkId",
                            networkStep = networkStep))
                } +
                    networkModel.container?.let { containerModel ->
                        orchestrationManager?.launchContainer(
                            enrichContainerConfiguration(
                                containerModel, networkId, networkId, networkModel = networkModel))
                    }

            containerFutures.filterNotNull().forEach {
                networkState.containers.add(
                    it.join()?.containerModel?.name ?: throw Exception("Couldn't retrieve container name"))
            }
        }
    }

    /**
     * Enrich the Container configuration with information on the context the container will be
     * deployed for
     */
    private fun enrichContainerConfiguration(
        containerModel: ContainerModel,
        networkId: String,
        postfix: String,
        networkModel: NetworkModel? = null,
        networkStep: NetworkStep? = null
    ): ContainerModel {
        val networkSteps =
            networkModel?.steps
                ?: networkStep?.let { listOf(it) }
                    ?: throw Exception(
                    "Either networkConfiguration or networkstep should be provided")
        return containerModel.copy(
            name = "${containerModel.name}-$postfix".lowercase(Locale.getDefault()),
            configuration =
                containerModel.configuration +
                    ConfigurationEntry(
                        filename = "workflow.json",
                        content =
                            Json.encodeToString(
                                ContainerWorkflowDescription(
                                    workflowOutputPrefix =
                                        "http://${workflowManagerConfiguration.internalHostname}:8080/router/workflow/$networkId",
                                    steps =
                                        networkSteps.map { step ->
                                            StepDescription(
                                                name = step.name,
                                                inputs =
                                                    step.input.map { inputSpec ->
                                                        InputDescription(
                                                            endpoint = inputSpec.endpoint,
                                                            count = inputSpec.count,
                                                            loop = inputSpec.loop,
                                                            from = inputSpec.from)
                                                    },
                                                outputs =
                                                    step.output.mapIndexed { index, outputSpec ->
                                                        OutputDescription(
                                                            endpoint =
                                                                "/${step.name}/output/$index",
                                                            count = outputSpec.count,
                                                            loop = outputSpec.loop,
                                                            to =
                                                                outputSpec.to.map {
                                                                    it.id ?: "self"
                                                                })
                                                    })
                                        }))))
    }
}
