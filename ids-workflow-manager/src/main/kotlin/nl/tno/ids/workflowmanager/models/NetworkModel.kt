/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.tno.ids.orchestrationmanager.models.ContainerModel

/** Network specification describing the Network */
@Serializable
data class NetworkModel(
    val parties: List<Party> = emptyList(),
    val idsid: String,
    val container: ContainerModel? = null,
    val accessUrl: String? = null,
    val httpPrefix: String? = null,
    val steps: List<NetworkStep>,
    val flowTimeout: Int = 120
)

/** Party description */
@Serializable
data class Party(
    val id: String,
    val name: String,
    val type: PartyType = PartyType.IDS,
    val accessUrl: String
)

/**
 * Party Type
 *
 * Currently only IDS is supported
 */
enum class PartyType {
    IDS
}

/** Network Step description */
@Serializable
data class NetworkStep(
    val name: String,
    val depends_on: List<String> = emptyList(),
    val input: List<InputSpec> = emptyList(),
    val output: List<OutputSpec> = emptyList(),
    val container: ContainerModel? = null,
    val accessUrl: String? = null
)

/** Abstract Input/Output specification */
@Serializable
sealed class IOSpec {
    abstract val loop: Int
    abstract val count: Int
}

/** Abstract Input specification */
@Serializable
sealed class InputSpec : IOSpec() {
    abstract val from: List<String>
    abstract val endpoint: String
    abstract val autoInitiate: Boolean
}

/** Abstract Output specification */
@Serializable
sealed class OutputSpec : IOSpec() {
    abstract val to: List<ToSpec>
}

/** Description of the intended receiver for an Output */
@Serializable
data class ToSpec(val id: String? = null, val internal: Boolean = false, val endpoint: String)

/** Http Input specification */
@SerialName("http")
@Serializable
data class HttpInput(
    override val endpoint: String,
    override val loop: Int = 0,
    override val count: Int = 1,
    override val from: List<String> = emptyList(),
    override val autoInitiate: Boolean = false
) : InputSpec()

/** HTTP Output specification */
@SerialName("http")
@Serializable
data class HttpOutput(
    override val loop: Int = 0,
    override val count: Int = 1,
    override val to: List<ToSpec> = emptyList(),
) : OutputSpec()

/** HTTP Output specification */
@SerialName("artifact")
@Serializable
data class ArtifactOutput(
    val artifactId: String? = null,
    val filename: String,
    val title: String,
    val description: String,
    val contractOffer: String? = null,
    override val loop: Int = 0,
    override val count: Int = 1,
    override val to: List<ToSpec> = emptyList()
) : OutputSpec()

