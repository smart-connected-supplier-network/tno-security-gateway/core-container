/*
 * Copyright (C) 2021 TNO
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package nl.tno.ids.workflowmanager.routebuilders

import de.fraunhofer.iais.eis.InvokeOperationMessageBuilder
import nl.tno.ids.configuration.Constants
import nl.tno.ids.configuration.infomodel.buildWithDefaults
import nl.tno.ids.configuration.infomodel.toJsonLD
import nl.tno.ids.workflowmanager.NetworkProcessor
import nl.tno.ids.workflowmanager.models.NetworkModel
import nl.tno.ids.workflowmanager.models.NetworkStep
import nl.tno.ids.workflowmanager.models.OutputSpec
import org.apache.camel.ExchangePattern
import org.apache.camel.Processor
import org.apache.camel.builder.RouteBuilder
import java.net.URI

/** Camel Route Builder for HTTP Output routes */
class HttpOutputRouteBuilder(
    private val tokenInjector: Processor,
    private val networkId: String,
    private val networkModel: NetworkModel,
    private val camelPort: String
) : OutputRouteBuilder() {
    private val internalRouter: String = "localhost:8080"

    override fun generate(
        routeBuilder: RouteBuilder,
        networkStep: NetworkStep,
        networkProcessor: NetworkProcessor,
        index: Int,
        spec: OutputSpec,
    ) {
        routeBuilder.apply {
            from(
                "jetty:http://0.0.0.0:$camelPort/router/workflow/$networkId/${networkStep.name}/output/$index?enableMultipartFilter=false"
            )
                .routeId("$networkId-${networkStep.name}-output-${index}")
                .log("Route \${routeId}: \${headers.CamelServletContextPath}, \${headers[ids-id]}")
                .setProperty(NetworkProcessor.IO_KEY, constant("output"))
                .setProperty(NetworkProcessor.IO_INDEX_KEY, constant(index))
                .process(networkProcessor)
                .run {
                    if (spec.to.isNotEmpty()) {
                        setHeader("Content-Type", constant("application/json"))
                        val preprocessing =
                            if (spec.to.none { it.internal }) {
                                process {
                                    it.message.setHeader(
                                        Constants.Headers.idsHeader,
                                        InvokeOperationMessageBuilder()
                                            ._issuerConnector_(URI(networkModel.idsid))
                                            ._senderAgent_(URI(networkModel.idsid))
                                            ._operationReference_(
                                                URI(
                                                    "urn:ids:workflow:$networkId:${networkStep.name}"
                                                )
                                            )
                                            ._recipientConnector_(
                                                arrayListOf(
                                                    *spec.to
                                                        .map { to ->
                                                            URI(
                                                                to.id
                                                                    ?: networkModel
                                                                        .idsid
                                                            )
                                                        }
                                                        .toTypedArray()))
                                            .buildWithDefaults()
                                            .toJsonLD()
                                    )
                                }
                                    .process(tokenInjector)
                                    .process("multiPartOutputProcessor")
                            } else {
                                this
                            }

                        preprocessing
                            .multicast()
                            .parallelProcessing()
                            .to(ExchangePattern.InOnly,
                                *spec.to
                                    .map { party ->
                                        if (party.internal) {
                                            "http://$internalRouter/router/workflow/$networkId${party.endpoint}?bridgeEndpoint=true"
                                        } else {
                                            "${networkModel.parties.first { it.id == party.id }.accessUrl}${party.endpoint}?bridgeEndpoint=true"
                                        }
                                    }
                                    .toTypedArray())
                            .end()
                    } else {
                        this
                    }
                }
                .setBody(constant("OK"))
        }
    }
}
