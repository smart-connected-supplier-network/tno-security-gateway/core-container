plugins {
    kotlin("plugin.spring")
    kotlin("plugin.serialization")
}

dependencies {
    // Camel Spring Boot integration
    implementation("org.apache.camel.springboot:camel-spring-boot-starter")
    implementation(project(":ids-security"))

    implementation(project(":ids-configuration"))
    implementation(project(":ids-artifact-manager"))
    implementation(project(":ids-route-manager"))
    implementation(project(":ids-orchestration-manager"))
    implementation(project(":camel-multipart-processor"))

    implementation("org.apache.camel.springboot:camel-http-starter")

    // YAML Serialization support
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${LibraryVersions.Kotlin.kotlinxSerialization}")
    implementation("com.charleskorn.kaml:kaml:${LibraryVersions.Kotlin.kaml}")

    // Spring Boot Test
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.apache.camel.springboot:camel-jetty-starter")

    testImplementation("io.fabric8:kubernetes-client:${LibraryVersions.K8s.fabric8}")
    testImplementation("com.github.fkorotkov:k8s-kotlin-dsl:${LibraryVersions.K8s.k8sKotlin}") {
        exclude("io.fabric8")
    }
    testImplementation(project(":ids-test-extensions"))
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = freeCompilerArgs + "-opt-in=kotlinx.serialization.ExperimentalSerializationApi"
    }
}