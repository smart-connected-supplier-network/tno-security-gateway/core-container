repositories {
    mavenCentral()
}

plugins {
    java

    // Spring Boot
    id("org.springframework.boot") version LibraryVersions.Spring.boot apply false

    // Kotlin specific
    kotlin("jvm") version LibraryVersions.Kotlin.kotlin apply false
    kotlin("plugin.spring") version LibraryVersions.Kotlin.kotlin apply false
    kotlin("plugin.serialization") version LibraryVersions.Kotlin.kotlin apply false

    // Spotless
    id("com.diffplug.spotless") version LibraryVersions.spotless apply false
}

// Set default group and version for the submodules
allprojects {
    group = "nl.tno.ids"
    version = "1.1.9"
}

subprojects {
    // Configure used maven repositories
    repositories {
        // Third party libraries (e.g. Spring, Camel, ...)
        mavenCentral()

        // References IAIS repository that contains the infomodel artifacts
        maven {
            url = uri("https://maven.iais.fraunhofer.de/artifactory/eis-ids-public/")
        }

        // TNO Nexus Repository containing common TNO artifacts
        maven {
            url = uri("https://nexus.dataspac.es/repository/tsg-maven")
        }

        // Required for K8s kotlin dsl
        maven {
            url = uri("https://jitpack.io")
        }
    }

    // Apply default plugins for each submodule
    apply(plugin = "java")
    apply(plugin = "java-library")
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "io.spring.dependency-management")
    apply(plugin = "com.diffplug.spotless")


    java {
        sourceCompatibility = JavaVersion.VERSION_21
        targetCompatibility = JavaVersion.VERSION_21
    }

    // Configure Kotlin SDK for each submodule
    dependencies {
        implementation(kotlin("stdlib-jdk8"))

        // Custom resolutionStrategy for kotlin libs (see: https://github.com/industrial-data-space/trusted-connector)
        configurations.all {
            resolutionStrategy.eachDependency {
                if (requested.group == "org.jetbrains.kotlin") {
                    useVersion(LibraryVersions.Kotlin.kotlin)
                }
                if (requested.group == "com.squareup.okhttp3") {
                    useVersion(LibraryVersions.okhttp)
                }
                if (requested.group == "io.fabric8" && requested.name.startsWith("kubernetes")) {
                    useVersion(LibraryVersions.K8s.fabric8)
                }
                if (requested.group == "org.yaml") {
                    useVersion(LibraryVersions.snakeYaml)
                }
                if (requested.group == "org.apache.commons" && requested.name == "commons-compress") {
                    useVersion(LibraryVersions.commonsCompress)
                }
                if (requested.group == "org.apache.jena") {
                    useVersion("4.9.0")
                }
                if (requested.group == "ch.qos.logback") {
                    useVersion("1.2.13")
                }
                if (requested.group == "org.bouncycastle" && requested.name == "bcprov-jdk15on") {
                    useVersion(LibraryVersions.Jwt.bouncyCastle)
                }
            }
            resolutionStrategy.dependencySubstitution {
                substitute(module("org.bouncycastle:bcprov-jdk15on"))
                    .using(module("org.bouncycastle:bcprov-jdk18on:${LibraryVersions.Jwt.bouncyCastle}"))
                substitute(module("org.bouncycastle:bcprov-ext-jdk15on"))
                    .using(module("org.bouncycastle:bcprov-ext-jdk18on:${LibraryVersions.Jwt.bouncyCastle}"))
                substitute(module("org.bouncycastle:bcpkix-jdk15on"))
                    .using(module("org.bouncycastle:bcpkix-jdk18on:${LibraryVersions.Jwt.bouncyCastle}"))
            }
        }
    }

    configure<com.diffplug.gradle.spotless.SpotlessExtension> {
        kotlin {
            ktfmt().dropboxStyle()
            licenseHeader("""
                /*
                 * Copyright (C) 2021 TNO
                 *
                 * Licensed to the Apache Software Foundation (ASF) under one
                 * or more contributor license agreements.  See the NOTICE file
                 * distributed with this work for additional information
                 * regarding copyright ownership.  The ASF licenses this file
                 * to you under the Apache License, Version 2.0 (the
                 * "License"); you may not use this file except in compliance
                 * with the License.  You may obtain a copy of the License at
                 *
                 *   http://www.apache.org/licenses/LICENSE-2.0
                 *
                 * Unless required by applicable law or agreed to in writing,
                 * software distributed under the License is distributed on an
                 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
                 * KIND, either express or implied.  See the License for the
                 * specific language governing permissions and limitations
                 * under the License.
                 */
            """.trimIndent())
        }
    }

    // Configure dependency management for used larger libraries
    configure<io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension> {
        // Camel Spring Boot dependencies
        imports {
            // need to stick to 3.0 because of org.apache.camel.support.dump.RouteStatDump and ModelHelper
            mavenBom("org.apache.camel.springboot:camel-spring-boot-dependencies:${LibraryVersions.Camel.spring}")
        }

        // Spring Boot dependencies
        imports {
            mavenBom("org.springframework.boot:spring-boot-dependencies:${LibraryVersions.Spring.boot}")
        }

        // Spring Cloud dependencies
        imports {
            mavenBom("org.springframework.cloud:spring-cloud-dependencies:${LibraryVersions.Spring.cloud}")
        }
    }

    // Enable jUnit
    tasks.withType<Test> {
        useJUnitPlatform()
        if (!System.getenv().containsKey("CI")) {
            testLogging {
                events(
                    org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED,
                    org.gradle.api.tasks.testing.logging.TestLogEvent.STANDARD_ERROR,
                    org.gradle.api.tasks.testing.logging.TestLogEvent.STANDARD_OUT
                )
                exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
                showExceptions = true
                showCauses = true
                showStackTraces = true
            }
        }
    }

    tasks.register<Test>("ciTest") {
        group = "verification"
        filter {
            excludeTestsMatching("*.integration.*")
            excludeTestsMatching("*.local.*")
        }

        shouldRunAfter(tasks.testClasses)

        maxHeapSize = "512m"
        systemProperty("junit.jupiter.execution.timeout.default", "60 s")
    }

    // Specify generic Kotlin JVM target
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "21"
            languageVersion = LibraryVersions.Kotlin.api
            apiVersion = LibraryVersions.Kotlin.api
        }
    }

    tasks.withType<org.springframework.boot.gradle.tasks.bundling.BootJar> {
        enabled = false
    }
}
